﻿using Hino.API.Domain.Base.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Hino.API.Infra.Data.Repositories.Base.Configurations
{
    public abstract class BaseEntityConfigurarion<T> : IEntityTypeConfiguration<T> where T : BaseEntity
    {
        protected string Table { get; }

        public BaseEntityConfigurarion(string Table)
        {
            this.Table = Table;
        }

        public virtual void Configure(EntityTypeBuilder<T> config)
        {
            config.ToTable(Table);

            config.HasKey(b => b.Id);

            config.Property(b => b.EstablishmentKey)
                .HasMaxLength(36)
                .IsRequired();

            config.Property(b => b.UniqueKey)
                .HasMaxLength(36)
                .IsRequired();

            config.Property(b => b.Created)
                .HasDefaultValueSql("SYSDATE")
                .IsRequired();
            config.Property(b => b.Modified)
                .HasDefaultValueSql("SYSDATE")
                .IsRequired();

            config.HasIndex("Id");
            config.HasIndex("EstablishmentKey");
            config.HasIndex("UniqueKey");
            config.HasIndex("IsActive");
            config.HasIndex("Id")
              .IsUnique(true);
        }
    }
}
