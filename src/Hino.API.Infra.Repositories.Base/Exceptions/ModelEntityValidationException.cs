﻿using Hino.API.Domain.Base.Interfaces.Exceptions;
using Hino.API.Infra.Cross.Utils.Exceptions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.API.Infra.Data.Repositories.Base.Exceptions
{
    public class ModelEntityValidationException : Exception, IModelEntityValidationException
    {
        public ModelEntityValidationException(DbUpdateException innerException) :
           base(null, innerException)
        {
        }

        public override string Message
        {
            get
            {
                if (InnerException is DbUpdateException innerException)
                {
                    StringBuilder sb = new();
                    sb.AppendLine(innerException.Message);

                    /*sb.AppendLine();
                    sb.AppendLine();
                    foreach (var eve in innerException.EntityValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().FullName, eve.Entry.State));
                        foreach (var ve in eve.ValidationErrors)
                        {
                            sb.AppendLine(string.Format("-- Property: \"{0}\", Value: \"{1}\", Error: \"{2}\"",
                                ve.PropertyName,
                                eve.Entry.CurrentValues.GetValue<object>(ve.PropertyName),
                                ve.ErrorMessage));
                        }
                    }

                    sb.AppendLine();*/

                    return sb.ToString();
                }

                return base.Message;
            }
        }

        public List<ModelException> MException
        {
            get
            {
                var Exceptions = new List<ModelException>();
                if (InnerException is DbUpdateException innerException)
                {
                    Exceptions.Add(ModelException.CreateSqlError(innerException, Cross.Utils.Enums.EExceptionErrorCodes.SaveSQLError));

                    /*
                    var eve = innerException.EntityValidationErrors.First();

                    foreach (var ve in eve.ValidationErrors)
                    {
                        Exceptions.Add(new ModelException
                        {
                            ErrorCode = (int)EExceptionErrorCodes.SaveSQLError,
                            Field = ve.PropertyName,
                            Value = eve.Entry.CurrentValues.GetValue<object>(ve.PropertyName)?.ToString(),
                            Messages = new[]
                            {
                                ve.ErrorMessage
                            }
                        });
                    }
                    */
                }

                return Exceptions;
            }
        }
    }
}
