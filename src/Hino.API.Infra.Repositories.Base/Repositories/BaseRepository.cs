﻿using Hino.API.Domain.Base.Interfaces.Repositories;
using Hino.API.Domain.Base.Models;
using Hino.API.Infra.Cross.Utils;
using Hino.API.Infra.Cross.Utils.Enums;
using Hino.API.Infra.Cross.Utils.Exceptions;
using Hino.API.Infra.Cross.Utils.Paging;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hino.API.Infra.Data.Repositories.Base.Repositories
{
    public class BaseRepository<T> : IDisposable, IBaseRepository<T> where T : BaseEntity
    {
        protected DbContext DbConn;
        protected DbSet<T> DbEntity;

        public BaseRepository(DbContext appDbContext)
        {
            DbConn = appDbContext;
            DbEntity = DbConn.Set<T>();
        }

        protected IQueryable<T> AddQueryProperties(IQueryable<T> query, params Expression<Func<T, object>>[] includeProperties)
        {
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query.AsNoTracking();
        }

        protected async Task<PagedResult<T>> PaginateQueryAsync(IQueryable<T> query, int page, int pageSize, int count = 0)
        {
            var result = new PagedResult<T>
            {
                CurrentPage = page,
                PageSize = pageSize,
                RowCount = 1,
                PageCount = 1
            };

            if (page > -1 && pageSize > 0)
            {
                result.RowCount = count;
                result.PageCount = (int)Math.Ceiling((double)result.RowCount / pageSize);

                var skip = (page - 1) * pageSize;
                result.Results = await query.OrderByDescending(s => s.Id).Skip(skip).Take(pageSize).ToListAsync();
            }
            else
            {
                result.Results = await query.ToListAsync();
                result.RowCount = result.Results.Count;
                result.CurrentPage = 1;
                result.PageSize = result.RowCount;
            }

            return result;
        }

        public virtual async Task<PagedResult<T>> GetAllPagedAsync(int page, int pageSize,
                                       params Expression<Func<T, object>>[] includeProperties) =>
            await PaginateQueryAsync(
                    AddQueryProperties(DbEntity, includeProperties)
                        .AsNoTracking(), page, pageSize);

        public virtual async Task<PagedResult<T>> QueryPagedAsync(int page, int pageSize,
            Expression<Func<T, bool>> predicate,
            params Expression<Func<T, object>>[] includeProperties)
        {
            var count = DbEntity.Count(predicate);
            return await PaginateQueryAsync(AddQueryProperties(DbEntity, includeProperties)
                            .AsNoTracking()
                            .Where(predicate), page, pageSize, count);
        }

        public virtual async Task<T> FirstOrDefaultAsync(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties)
        {
            var Results = await QueryAsync(predicate, includeProperties);
            return Results.FirstOrDefault();
        }

        public virtual async Task<IEnumerable<T>> QueryAsync(Expression<Func<T, bool>> predicate,
            params Expression<Func<T, object>>[] includeProperties) =>
            await AddQueryProperties(DbEntity, includeProperties)
                            .AsNoTracking()
                            .Where(predicate)
                            .ToListAsync();

        public virtual async Task<IEnumerable<T>> GetAllAsync(params Expression<Func<T, object>>[] includeProperties) =>
            await AddQueryProperties(DbEntity, includeProperties).AsNoTracking().ToListAsync();

        public virtual async Task<T> GetByIdAsync(long id, string pEstablishmentKey, string pUniqueKey, params Expression<Func<T, object>>[] includeProperties) =>
            await AddQueryProperties(DbEntity, includeProperties)
            .AsNoTracking()
            .Where(r =>
                r.Id == id &&
                r.EstablishmentKey == pEstablishmentKey &&
                r.UniqueKey == pUniqueKey)
            .FirstOrDefaultAsync();

        public virtual T GetById(long id, string pEstablishmentKey, string pUniqueKey, params Expression<Func<T, object>>[] includeProperties) =>
            AddQueryProperties(DbEntity, includeProperties)
            .AsNoTracking()
            .Where(r =>
                   r.Id == id &&
                   r.EstablishmentKey == pEstablishmentKey &&
                   r.UniqueKey == pUniqueKey).FirstOrDefault();

        public virtual T GetByIdToUpdate(long id, string pEstablishmentKey, string pUniqueKey) =>
            DbEntity.Where(r =>
                   r.Id == id &&
                   r.EstablishmentKey == pEstablishmentKey)
            .FirstOrDefault();

        public virtual bool Add(T model)
        {
            model.Created = DateTime.Now;
            model.Modified = DateTime.Now;
            model.IsActive = true;
            model.UniqueKey = Guid.NewGuid().ToString();
            if (model.Id == 0)
                model.Id = NextSequence();

            DbEntity.Add(model);

            return true;
        }

        public virtual bool Update(T model)
        {
            model.Modified = DateTime.Now;
            var entry = DbConn.Entry(model);
            if (entry.State == EntityState.Detached || entry.State == EntityState.Modified)
            {
                entry.State = EntityState.Modified;

                DbConn.Set<T>().Attach(model);
            }
            return true;
        }

        public virtual bool Remove(T model)
        {
            DbEntity.Attach(model);
            DbConn.Entry(model).State = EntityState.Deleted;
            DbEntity.Remove(model);
            return true;
        }

        public virtual async Task<T> RemoveById(long id, string pEstablishmentKey, string pUniqueKey)
        {
            var model = await Task.Run(() => GetByIdToUpdate(id, pEstablishmentKey, pUniqueKey));
            Remove(model);
            return model;
        }

        public virtual void SetModelState(T model, EModelDataState pState) =>
            DbConn.Entry(model).State = pState.ToEntityState();

        async Task<long> GetNextSequency()
        {
            var sql = $"SELECT SEQ_{typeof(T).Name}.NEXTVAL FROM DUAL";
            var result = await DbConn.RawSqlQueryAsync(sql);
            return Convert.ToInt64(result);
        }

        public virtual long NextSequence() =>
           Task.Run(async () => await GetNextSequency()).Result;

        public virtual async Task<long> NextSequenceAsync() =>
            await GetNextSequency();

        public virtual async Task<int> SaveChanges()
        {
            int ret;
            try
            {
                ret = await DbConn.SaveChangesAsync();
            }
            catch (DbUpdateException e)
            {
                Logging.Exception(e);
                /*
                if (!AppSettings.IsDebug)
                {
                    if (e.InnerException.InnerException is OracleException)
                    {
                        var sqlex = e.InnerException.InnerException as OracleException;
                        throw new DBOracleCodeException(sqlex);
                    }
                }
                */
                throw;
            }
            catch (Exception e)
            {
                Logging.Exception(e);
                throw;
            }

            return ret;
        }

        public virtual void RollBackChanges()
        {
            var changedEntries = DbConn.ChangeTracker.Entries()
                .Where(x => x.State != EntityState.Unchanged).ToList();

            foreach (var entry in changedEntries)
            {
                switch (entry.State)
                {
                    case EntityState.Modified:
                        entry.CurrentValues.SetValues(entry.OriginalValues);
                        entry.State = EntityState.Unchanged;
                        break;
                    case EntityState.Added:
                        entry.State = EntityState.Detached;
                        break;
                    case EntityState.Deleted:
                        entry.State = EntityState.Unchanged;
                        break;
                }
            }
        }

        public void Dispose()
        {
            DbConn.Dispose();
            GC.SuppressFinalize(this);
        }

    }
}