﻿using Hino.API.Domain.Base.Interfaces.UoW;
using Hino.API.Infra.Cross.Utils.Exceptions;
using Hino.API.Infra.Data.Repositories.Base.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Infra.Data.Repositories.Base.UoW
{
    public class BaseUnitOfWork : IBaseUnitOfWork
    {
        public virtual long RowsAffected { get; protected set; }
        public List<ModelException> Errors { get; protected set; }

        private readonly BaseDbContext _BaseDbContext;

        public BaseUnitOfWork(BaseDbContext appDbContext)
        {
            _BaseDbContext = appDbContext;
            Errors = new List<ModelException>();
        }

        public virtual void Commit()
        {
            RowsAffected = _BaseDbContext.SaveChanges();
        }

        public virtual void Rollback()
        {
            _BaseDbContext
                   .ChangeTracker
                   .Entries()
                   .ToList()
                   .ForEach(x =>
                   {
                       try
                       {
                           x.Reload();
                       }
                       catch (Exception e)
                       {
                           Logging.Exception(e);
                       }
                   });
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _BaseDbContext.Dispose();
                }
            }
            disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
