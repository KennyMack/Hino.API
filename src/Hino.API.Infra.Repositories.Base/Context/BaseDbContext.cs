﻿using Hino.API.Infra.Cross.Utils.Settings;
using Hino.API.Infra.Data.Repositories.Base.Exceptions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Hino.API.Infra.Data.Repositories.Base.Context
{
    public class BaseDbContext : DbContext
    {
        protected IConfiguration Configuration { get; }
        protected string ConnectionString { get; }
        protected string DefaultSchema { get; }

        public BaseDbContext()
        {
            if (!AppSettings.HasBeenLoaded)
            {
                ConnectionString = Environment.GetEnvironmentVariable("CONNBASE");
                DefaultSchema = Environment.GetEnvironmentVariable("DEFAULTSCHEMA");
            }
            else
            {
                ConnectionString = AppSettings.GetConnectionString("BaseConnection");
                Configuration = AppSettings.Configuration;
                DefaultSchema = Configuration["Connection:DefaultSchema"];
            }
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseOracle(ConnectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema(this.DefaultSchema);
             
            var cascadeFKs = modelBuilder.Model.GetEntityTypes()
                .SelectMany(t => t.GetForeignKeys())
                .Where(fk => !fk.IsOwnership && fk.DeleteBehavior == DeleteBehavior.Cascade);

            foreach (var fk in cascadeFKs)
                fk.DeleteBehavior = DeleteBehavior.Restrict;

            var tables = modelBuilder.Model.GetEntityTypes();

            foreach (var table in tables)
            {
                table.SetTableName(table.GetTableName().ToUpper());
                foreach (var col in table.GetProperties())
                {
                    if (col.PropertyInfo != null)
                        col.SetColumnName(col.PropertyInfo.Name.ToUpper());
                }
            }

            base.OnModelCreating(modelBuilder);
        }

        void GetNextSequence(EntityEntry pEntity)
        {
            if (pEntity.Property("Id").CurrentValue.ToString() == "0")
            {
                var sql = $"SELECT SEQ_{pEntity.Entity.GetType().Name}.NEXTVAL FROM DUAL";
                var SeqItem = Task.Run(async () => await this.RawSqlQueryAsync(sql)).Result;

                pEntity.Property("Id").CurrentValue = SeqItem;
            }
        }

        void DefaultData()
        {
            var props = ChangeTracker.Entries()
                .Where(p =>
                    p.Entity.GetType().GetProperty("Created") != null
                );

            foreach (var item in props)
            {
                switch (item.State)
                {
                    case EntityState.Detached:
                        break;
                    case EntityState.Unchanged:
                        if (item.Property("Id").CurrentValue.ToString() == "0")
                        {
                            item.State = EntityState.Added;
                            item.Property("Created").CurrentValue = DateTime.Now;
                            item.Property("Modified").CurrentValue = DateTime.Now;
                            item.Property("IsActive").CurrentValue = true;
                            item.Property("UniqueKey").CurrentValue = Guid.NewGuid().ToString();
                            GetNextSequence(item);
                        }
                        else
                        {
                            item.Property("Created").IsModified = false;
                            item.Property("Modified").CurrentValue = DateTime.Now;
                            item.State = EntityState.Modified;
                        }
                        break;
                    case EntityState.Added:
                        item.Property("Created").CurrentValue = DateTime.Now;
                        item.Property("Modified").CurrentValue = DateTime.Now;
                        item.Property("IsActive").CurrentValue = true;
                        item.Property("UniqueKey").CurrentValue = Guid.NewGuid().ToString();
                        GetNextSequence(item);
                        break;
                    case EntityState.Deleted:
                        // item.Property("IsActive").CurrentValue = false;
                        // item.State = EntityState.Modified;
                        break;
                    case EntityState.Modified:
                        item.Property("Created").IsModified = false;
                        item.Property("Modified").CurrentValue = DateTime.Now;
                        break;
                    default:
                        break;
                }
            }
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            DefaultData();
            int ret;
            try
            {
                ret = await base.SaveChangesAsync(cancellationToken);
            }
            catch (DbUpdateException e)
            {
                Cross.Utils.Exceptions.Logging.Exception(e);
                throw new ModelEntityValidationException(e);
            }
            return ret;
        }

        public override int SaveChanges()
        {
            DefaultData();
            try
            {
                return base.SaveChanges();
            }
            catch (DbUpdateException e)
            {
                Cross.Utils.Exceptions.Logging.Exception(e);
                throw new ModelEntityValidationException(e);
            }
        }
    }
}
