using Hino.API.Domain.Establishments.Interfaces.Repositories;
using Hino.API.Domain.Establishments.Models;
using Hino.API.Domain.Establishments.Services;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.API.Tests.Establishments
{
    [TestFixture]
    public class TestEstablishments
    {
        private Mock<IGEEstablishmentsRepository> GEEstablishmentsRepositoryMock;
        private Mock<IGEEstabMenuRepository> GEEstabMenuRepository;
        private Mock<IGEEstabDevicesRepository> GEEstabDevicesRepository;

        public static IEnumerable<TestCaseData> EstablishmentsTestCases
        {
            get
            {
                yield return new TestCaseData("08d08c79-f4a0-4a9e-884a-78bb48bd51fd", 1, new List<GEEstablishments> { new GEEstablishments() { EstablishmentKey = "08d08c79-f4a0-4a9e-884a-78bb48bd51fd", RazaoSocial = "DISEMP - DISTR. EMPRESARIAL DE PNEUS E ACESSORIOS LTDA" } });
                yield return new TestCaseData("f48c8bd0-a048-4ae4-a988-75b791fdd80b", 2, new List<GEEstablishments> { new GEEstablishments() { EstablishmentKey = "f48c8bd0-a048-4ae4-a988-75b791fdd80b", RazaoSocial = "HOLEC INDUSTRIAS ELETRICAS LTDA" } });
                yield return new TestCaseData("78bb48bd51fd-f4a0-4a9e-884a-08d08c79", 10, new List<GEEstablishments> { new GEEstablishments() { EstablishmentKey = "78bb48bd51fd-f4a0-4a9e-884a-08d08c79" } });
            }
        }

        public static IEnumerable<TestCaseData> CreateOrUpdateEstablishmentsTestCases
        {
            get
            {
                yield return new TestCaseData(
                    new GEEstablishments()
                    {
                        RazaoSocial = "Basil Hayes",
                        NomeFantasia = "Skyler Sharp",
                        Email = "s_basilhayes1357@outlook.org",
                        Phone = "(23) 7631-9521",
                        CNPJCPF = "13.313.775/0001-69",
                        Devices = 445,
                        PIS = 9,
                        COFINS = 7
                    },
                    new GEEstablishments()
                    {
                        RazaoSocial = "Basil Hayes",
                        NomeFantasia = "Skyler Sharp",
                        Email = "s_basilhayes1357@outlook.org",
                        Phone = "(23) 7631-9521",
                        CNPJCPF = "13.313.775/0001-69",
                        Devices = 445,
                        PIS = 9,
                        COFINS = 7
                    },
                    1,
                    false,
                    "Sem erro");
                yield return new TestCaseData(
                    new GEEstablishments()
                    {
                        RazaoSocial = "Hanae Guthrie",
                        NomeFantasia = "Aurora Stevens",
                        Email = "aurorastevens-hanaeguthrie2354@yahoo.com",
                        Phone = "(81) 4998-3626",
                        CNPJCPF = "13.313.775/0001-69",
                        Devices = 112,
                        PIS = 5,
                        COFINS = 9
                    },
                    new GEEstablishments()
                    {
                        RazaoSocial = "Hanae Guthrie",
                        NomeFantasia = "Aurora Stevens",
                        Email = "aurorastevens-hanaeguthrie2354@yahoo.com",
                        Phone = "(81) 4998-3626",
                        CNPJCPF = "13.313.775/0001-69",
                        Devices = 112,
                        PIS = 5,
                        COFINS = 9
                    },
                    1,
                    false,
                    "Sem erro");
                yield return new TestCaseData(
                    new GEEstablishments() 
                    { 
                        RazaoSocial = "Marny Hale",
                        Email = "aurorastevens-hanaeguthrie2354@yahoo.com",
                        Phone = "(81) 4998-3626",
                        CNPJCPF = "90.766.632/7508-88",
                        Devices = 112,
                        PIS = 5,
                        COFINS = 9
                    },
                    new GEEstablishments() 
                    { 
                        RazaoSocial = "Marny Hale",
                        Email = "aurorastevens-hanaeguthrie2354@yahoo.com",
                        Phone = "(81) 4998-3626",
                        CNPJCPF = "90.766.632/7508-88",
                        Devices = 112,
                        PIS = 5,
                        COFINS = 9
                    },
                    0,
                    true,
                    "Sem Nome fantasia");
                yield return new TestCaseData(
                    new GEEstablishments()
                    {
                        NomeFantasia = "Marny Hale",
                        Email = "aurorastevens-hanaeguthrie2354@yahoo.com",
                        Phone = "(81) 4998-3626",
                        CNPJCPF = "90.766.632/7508-88",
                        Devices = 112,
                        PIS = 5,
                        COFINS = 9
                    },
                    new GEEstablishments()
                    {
                        NomeFantasia = "Marny Hale",
                        Email = "aurorastevens-hanaeguthrie2354@yahoo.com",
                        Phone = "(81) 4998-3626",
                        CNPJCPF = "90.766.632/7508-88",
                        Devices = 112,
                        PIS = 5,
                        COFINS = 9
                    },
                    0,
                    true,
                    "Sem Raz�o social");
                yield return new TestCaseData(
                    new GEEstablishments()
                    {
                        NomeFantasia = "Marny Hale",
                        RazaoSocial = "Marny Hale",
                        Phone = "(81) 4998-3626",
                        CNPJCPF = "90.766.632/7508-88",
                        Devices = 112,
                        PIS = 5,
                        COFINS = 9
                    },
                    new GEEstablishments()
                    {
                        NomeFantasia = "Marny Hale",
                        RazaoSocial = "Marny Hale",
                        Phone = "(81) 4998-3626",
                        CNPJCPF = "90.766.632/7508-88",
                        Devices = 112,
                        PIS = 5,
                        COFINS = 9
                    },
                    0,
                    true,
                    "Sem E-mail");
                yield return new TestCaseData(
                    new GEEstablishments()
                    {
                        NomeFantasia = "Marny Hale",
                        RazaoSocial = "Marny Hale",
                        Email = "aurorastevens-hanaeguthrie2354@yahoo.com",
                        CNPJCPF = "90.766.632/7508-88",
                        Devices = 112,
                        PIS = 5,
                        COFINS = 9
                    },
                    new GEEstablishments()
                    {
                        NomeFantasia = "Marny Hale",
                        RazaoSocial = "Marny Hale",
                        Email = "aurorastevens-hanaeguthrie2354@yahoo.com",
                        CNPJCPF = "90.766.632/7508-88",
                        Devices = 112,
                        PIS = 5,
                        COFINS = 9
                    },
                    0,
                    true,
                    "Sem telefone");
                yield return new TestCaseData(
                    new GEEstablishments()
                    {
                        RazaoSocial = "Hanae Guthrie",
                        NomeFantasia = "Aurora Stevens",
                        Email = "aurorastevens-hanaeguthrie2354@yahoo.com",
                        Phone = "(81) 4998-3626",
                        Devices = 112,
                        PIS = 5,
                        COFINS = 9
                    },
                    new GEEstablishments()
                    {
                        RazaoSocial = "Hanae Guthrie",
                        NomeFantasia = "Aurora Stevens",
                        Email = "aurorastevens-hanaeguthrie2354@yahoo.com",
                        Phone = "(81) 4998-3626",
                        Devices = 112,
                        PIS = 5,
                        COFINS = 9
                    },
                    0,
                    true,
                    "Sem CNPJ");
                yield return new TestCaseData(
                    new GEEstablishments()
                    {
                        RazaoSocial = "Basil Hayes",
                        NomeFantasia = "Skyler Sharp",
                        Email = "s_basilhayes1357@outlook.org",
                        Phone = "(23) 7631-9521",
                        CNPJCPF = "50.420.749/3691-97",
                        Devices = 445,
                        PIS = 9,
                        COFINS = 7
                    },
                    new GEEstablishments()
                    {
                        RazaoSocial = "Basil Hayes",
                        NomeFantasia = "Skyler Sharp",
                        Email = "s_basilhayes1357@outlook.org",
                        Phone = "(23) 7631-9521",
                        CNPJCPF = "50.420.749/3691-97",
                        Devices = 445,
                        PIS = 9,
                        COFINS = 7
                    },
                    0,
                    true,
                    "CNPJ Inv�lido");
            }
        }

        [SetUp]
        public void Setup()
        {
            GEEstablishmentsRepositoryMock = new Mock<IGEEstablishmentsRepository>(MockBehavior.Strict);
            GEEstabMenuRepository = new Mock<IGEEstabMenuRepository>(MockBehavior.Strict);
            GEEstabDevicesRepository = new Mock<IGEEstabDevicesRepository>(MockBehavior.Strict);

        }

        [TestCase("08d08c79-f4a0-4a9e-884a-78bb48bd51fd", true)]
        [TestCase("f48c8bd0-a048-4ae4-a988-75b791fdd80b", true)]
        [TestCase("78bb48bd51fd-f4a0-4a9e-884a-08d08c79", false)]
        public async Task TestExistsEstablishmentAsync(string EstablishmentKey, bool expectedResult)
        {
            GEEstablishmentsRepositoryMock.Setup(p => p.ExistsEstablishmentAsync(EstablishmentKey)).ReturnsAsync(expectedResult);

            var Service = new GEEstablishmentsService(
                GEEstablishmentsRepositoryMock.Object,
                GEEstabMenuRepository.Object,
                GEEstabDevicesRepository.Object);

            var result = await Service.ExistsEstablishmentAsync(EstablishmentKey);

            Assert.That(result, Is.EqualTo(expectedResult));

            GEEstablishmentsRepositoryMock.VerifyAll();
        }

        [TestCaseSource("EstablishmentsTestCases")]
        public async Task TestGetByIdAndEstablishmentKeyAsync(string EstablishmentKey, long pId, IEnumerable<GEEstablishments> Establishments)
        {
            GEEstablishmentsRepositoryMock.Setup(p => p.GetByIdAndEstablishmentKeyAsync(pId, EstablishmentKey)).ReturnsAsync(Establishments);

            var Service = new GEEstablishmentsService(
                GEEstablishmentsRepositoryMock.Object,
                GEEstabMenuRepository.Object,
                GEEstabDevicesRepository.Object);

            var result = await Service.GetByIdAndEstablishmentKeyAsync(pId, EstablishmentKey);

            Assert.That(result, Is.EqualTo(Establishments));

            GEEstablishmentsRepositoryMock.VerifyAll();
        }

        [TestCaseSource("EstablishmentsTestCases")]
        public async Task TestGetByEstablishmentKeyAsync(string EstablishmentKey, long pId, IEnumerable<GEEstablishments> Establishments)
        {
            GEEstablishmentsRepositoryMock.Setup(p => p.GetByEstablishmentKeyAsync(EstablishmentKey)).ReturnsAsync(Establishments.First());

            var Service = new GEEstablishmentsService(
                GEEstablishmentsRepositoryMock.Object,
                GEEstabMenuRepository.Object,
                GEEstabDevicesRepository.Object);

            var result = await Service.GetByEstablishmentKeyAsync(EstablishmentKey);

            Assert.That(result, Is.EqualTo(Establishments.First()));

            GEEstablishmentsRepositoryMock.VerifyAll();
        }
        
        [TestCaseSource("CreateOrUpdateEstablishmentsTestCases")]
        public async Task TestCreateEstablishmentAsync(GEEstablishments Establishment, GEEstablishments EstablishmentResult, int rows, bool HasError, string DescValidacao)
        {
            GEEstablishmentsRepositoryMock.Setup(p => p.Add(It.IsAny<GEEstablishments>())).Returns(true);
            GEEstablishmentsRepositoryMock.Setup(p => p.SaveChanges()).ReturnsAsync(rows);

            var Service = new GEEstablishmentsService(
                GEEstablishmentsRepositoryMock.Object,
                GEEstabMenuRepository.Object,
                GEEstabDevicesRepository.Object);

            var result = await Service.CreateEstablishmentAsync(Establishment);

            Assert.That(HasError, Is.EqualTo(Service.Errors.Any()), DescValidacao);

            if (HasError)
                Assert.IsNull(result);
            else
            {
                Assert.That(result.RazaoSocial, Is.EqualTo(EstablishmentResult.RazaoSocial));
                Assert.That(result.NomeFantasia, Is.EqualTo(EstablishmentResult.NomeFantasia));
                Assert.That(result.Email, Is.EqualTo(EstablishmentResult.Email));
                Assert.That(result.Phone, Is.EqualTo(EstablishmentResult.Phone));
                Assert.That(result.CNPJCPF, Is.EqualTo(EstablishmentResult.CNPJCPF));
                Assert.That(result.Devices, Is.EqualTo(EstablishmentResult.Devices));
                Assert.That(result.PIS, Is.EqualTo(EstablishmentResult.PIS));
                Assert.That(result.COFINS, Is.EqualTo(EstablishmentResult.COFINS));
            }

        }
    }
}