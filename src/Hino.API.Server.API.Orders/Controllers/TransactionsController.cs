﻿using Hino.API.Application.Interfaces.Services.Orders;
using Hino.API.Application.MapperModels;
using Hino.API.Application.ViewModel.Orders;
using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Orders.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using Hino.API.Infra.Cross.Utils.Paging;
using Hino.API.Server.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.API.Server.API.Orders.Controllers
{
    [Route("api/Orders/Transactions/{pEstablishmentKey}")]
    [ApiController]
    [Authorize]
    public class TransactionsController : BaseApiController
    {
        private readonly IVETransactionsAS _IVETransactionsAS;

        public TransactionsController(IVETransactionsAS pIVETransactionsAS)
        {
            _IVETransactionsAS = pIVETransactionsAS;

            Services = new IErrorBaseService[]
            {
                _IVETransactionsAS
            };
        }

        [Route("all")]
        [HttpGet]
        public async Task<IActionResult> GetAll(string pEstablishmentKey)
        {
            try
            {
                var Results =
                    Mapper.Map<PagedResult<VETransactions>, PagedResult<VETransactionsVM>>
                    (
                        await _IVETransactionsAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                            r => r.EstablishmentKey == pEstablishmentKey
                        )
                    );

                if (Results == null)
                    throw new HinoException(_IVETransactionsAS.Errors.FirstOrDefault());

                return RequestOK(Results);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IActionResult> GetById(string pEstablishmentKey, long pId)
        {
            try
            {
                var Result =
                    Mapper.Map<VETransactions, VETransactionsVM>
                    (
                        await _IVETransactionsAS.FirstOrDefaultAsync(
                            r => r.Id == pId &&
                                 r.EstablishmentKey == pEstablishmentKey)
                    );

                if (Result == null)
                    return NotFoundRequest();

                return RequestOK(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [Route("auth-code/{pAuthCode}")]
        [HttpGet]
        public async Task<IActionResult> GetByAuthCode(string pEstablishmentKey, string pAuthCode)
        {
            try
            {
                var Result = await _IVETransactionsAS.GetByAuthCode(pEstablishmentKey, pAuthCode);

                if (Result == null)
                    return NotFoundRequest();

                return RequestOK(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [Route("terminal/{pTerminalId}/nsuHost/{pNsuHost}")]
        [HttpGet]
        public async Task<IActionResult> GetByNsuHost(string pEstablishmentKey, string pTerminalId, string pNsuHost)
        {
            try
            {
                var Result = await _IVETransactionsAS.GetByNsuHost(pEstablishmentKey, pTerminalId, pNsuHost);

                return RequestOK(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [Route("Order/{pOrderID}/all")]
        [HttpGet]
        public async Task<IActionResult> GetOrderTransactions(string pEstablishmentKey, long pOrderID)
        {
            try
            {
                var Results = Mapper.Map<PagedResult<VETransactions>, PagedResult<VETransactionsVM>>
                (
                    await _IVETransactionsAS.QueryPagedAsync(GetPageNumber(),
                                                             GetLimitNumber(),
                                                             r => r.EstablishmentKey == pEstablishmentKey &&
                                                                  r.OrderID == pOrderID)
                );

                if (Results == null)
                    return NotFoundRequest();

                return RequestOK(Results);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        public async Task<IActionResult> Put(string pUniqueKey, long id, [FromBody] VETransactionsVM pVETransactionsVM)
        {
            _IVETransactionsAS.Errors.Clear();
            pVETransactionsVM.Id = id;
            pVETransactionsVM.UniqueKey = pUniqueKey;

            ValidateModelState(pVETransactionsVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pVETransactionsVM, ModelState);

            try
            {
                var Result = _IVETransactionsAS.UpdateTransaction(pVETransactionsVM);

                if (Result != null && !_IVETransactionsAS.Errors.Any())
                    await _IVETransactionsAS.SaveChanges();

                return RequestResult(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpPost]
        [Route("save")]
        public async Task<IActionResult> Post(string pEstablishmentKey, [FromBody] VETransactionsVM pVETransactionsVM)
        {
            _IVETransactionsAS.Errors.Clear();
            pVETransactionsVM.EstablishmentKey = pEstablishmentKey;
            ValidateModelState(pVETransactionsVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pVETransactionsVM, ModelState);

            try
            {
                var Result = _IVETransactionsAS.CreateTransaction(pVETransactionsVM);

                if (Result != null && !_IVETransactionsAS.Errors.Any())
                    await _IVETransactionsAS.SaveChanges();

                return RequestResult(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        public async Task<IActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IVETransactionsAS.Errors.Clear();

            try
            {
                var Result = await _IVETransactionsAS.RemoveById(pId, pEstablishmentKey, pUniqueKey);

                if (Result != null && !_IVETransactionsAS.Errors.Any())
                    await _IVETransactionsAS.SaveChanges();

                return RequestResult(Mapper.Map<VETransactions, VETransactionsVM>(Result));
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IVETransactionsAS.Dispose();
            }
        }
    }
}
