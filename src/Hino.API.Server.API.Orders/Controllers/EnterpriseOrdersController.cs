﻿using Hino.API.Application.Interfaces.Services.Orders;
using Hino.API.Application.MapperModels;
using Hino.API.Application.ViewModel.Orders;
using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Orders.Models;
using Hino.API.Infra.Cross.Utils.Paging;
using Hino.API.Server.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Hino.API.Server.API.Orders.Controllers
{
    [Route("api/Enterprise/Orders/{pEstablishmentKey}")]
    [ApiController]
    [Authorize]
    public class EnterpriseOrdersController : BaseApiController
    {
        private readonly IVEOrdersAS _IVEOrdersAS;

        public EnterpriseOrdersController(IVEOrdersAS pIVEOrdersAS)
        {
            _IVEOrdersAS = pIVEOrdersAS;

            Services = new IErrorBaseService[]
            {
                _IVEOrdersAS
            };
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IActionResult> GetById(string pEstablishmentKey, long pId)
        {
            try
            {
                var Result =
                    Mapper.Map<PagedResult<VEOrders>, PagedResult<VEOrdersVM>>
                    (
                        await _IVEOrdersAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                            r => r.EnterpriseID == pId &&
                                 r.EstablishmentKey == pEstablishmentKey)
                    );

                if (Result == null)
                    return NotFoundRequest();

                return RequestOK(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IVEOrdersAS.Dispose();
            }
        }
    }
}
