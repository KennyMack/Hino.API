﻿using Hino.API.Application.Interfaces.Services;
using Hino.API.Application.Interfaces.Services.Orders;
using Hino.API.Application.MapperModels;
using Hino.API.Application.ViewModel.Orders;
using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Orders.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using Hino.API.Infra.Cross.Utils.Paging;
using Hino.API.Server.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.API.Server.API.Orders.Controllers
{
    [Route("api/Proposals/{pEstablishmentKey}")]
    [ApiController]
    [Authorize]
    public class ProposalsController : BaseApiController
    {
        private readonly IVEProposalsAS _IVEProposalsAS;

        public ProposalsController(IVEProposalsAS pIVEProposalsAS)
        {
            _IVEProposalsAS = pIVEProposalsAS;

            Services = new IErrorBaseService[]
            {
                _IVEProposalsAS
            };
        }

        [Route("all")]
        [HttpGet]
        public async Task<IActionResult> GetAll(string pEstablishmentKey)
        {
            try
            {
                var Results =
                    Mapper.Map<PagedResult<VEOrders>, PagedResult<VEOrdersVM>>
                    (
                        await _IVEProposalsAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                            r => r.EstablishmentKey == pEstablishmentKey &&
                            r.IsProposal
                        )
                    );

                if (Results == null)
                    throw new HinoException(_IVEProposalsAS.Errors.FirstOrDefault());

                return RequestOK(Results);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IActionResult> GetById(string pEstablishmentKey, long pId)
        {
            try
            {
                var Result =
                    Mapper.Map<VEOrders, VEOrdersVM>
                    (
                        await _IVEProposalsAS.FirstOrDefaultAsync(
                            r => r.Id == pId &&
                                 r.IsProposal &&
                                 r.EstablishmentKey == pEstablishmentKey)
                    );

                if (Result == null)
                    return NotFoundRequest();

                return RequestOK(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [Route("user/{pUserId}/all")]
        [HttpGet]
        public async Task<IActionResult> GetProposalsByUserId(string pEstablishmentKey, long pUserId)
        {
            try
            {
                var Results =
                    Mapper.Map<PagedResult<VEOrders>, PagedResult<VEOrdersVM>>
                    (
                        await _IVEProposalsAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                            r => r.EstablishmentKey == pEstablishmentKey &&
                                 r.IsProposal &&
                                 r.UserID == pUserId
                        )
                    );

                if (Results == null)
                    throw new HinoException(_IVEProposalsAS.Errors.FirstOrDefault());

                return RequestOK(Results);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [Route("id/{pId}/convertible")]
        [HttpGet]
        public async Task<IActionResult> GetCanConvert(string pEstablishmentKey, long pId)
        {
            try
            {
                var Result = await _IVEProposalsAS.CanConvertToOrderAsync(pEstablishmentKey, pId);

                if (Result == null)
                    return NotFoundRequest();

                return RequestOK(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        public async Task<IActionResult> Put(string pUniqueKey, long id, [FromBody] VEOrdersVM pVEOrdersVM)
        {
            _IVEProposalsAS.Errors.Clear();
            pVEOrdersVM.Id = id;
            pVEOrdersVM.UniqueKey = pUniqueKey;

            ValidateModelState(pVEOrdersVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pVEOrdersVM, ModelState);

            try
            {
                var Result = await _IVEProposalsAS.UpdateOrderAsync(pVEOrdersVM);

                return RequestResult(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpPost]
        [Route("save")]
        public async Task<IActionResult> Post(string pEstablishmentKey, [FromBody] VEOrdersVM pVEOrdersVM)
        {
            _IVEProposalsAS.Errors.Clear();
            pVEOrdersVM.EstablishmentKey = pEstablishmentKey;
            ValidateModelState(pVEOrdersVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pVEOrdersVM, ModelState);

            try
            {
                var Result = await _IVEProposalsAS.CreateOrderAsync(pVEOrdersVM);

                return RequestResult(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        public async Task<IActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IVEProposalsAS.Errors.Clear();

            try
            {
                var Result = await _IVEProposalsAS.RemoveByIdAsync(pId, pEstablishmentKey, pUniqueKey);

                return RequestResult(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IVEProposalsAS.Dispose();
            }
        }
    }
}

