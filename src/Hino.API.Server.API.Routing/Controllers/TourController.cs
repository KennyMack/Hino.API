﻿using Hino.API.Application.Integrations.Locations;
using Hino.API.Application.Interfaces.Services.Routing;
using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Infra.Cross.External.Locations.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using Hino.API.Server.Base;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.API.Server.API.Routing.Controllers
{
    [Route("api/Tour")]
    [ApiController]
    public class TourController : BaseApiController
    {
        private readonly ILOWaypointsAS _ILOWaypointsAS;
        private readonly ILORoutesAS _ILORoutesAS;
        private readonly IMapAS _IMapAS;
        public TourController(ILOWaypointsAS pILOWaypointsAS,
            ILORoutesAS pILORoutesAS,
            IMapAS pIMapAS)
        {
            _ILOWaypointsAS = pILOWaypointsAS;
            _ILORoutesAS = pILORoutesAS;
            _IMapAS = pIMapAS;

            Services = new IErrorBaseService[]
            {
                _ILOWaypointsAS,
                _ILORoutesAS
            };
        }

        [HttpPost]
        [Route("Find/Sequence")]
        public async Task<IActionResult> FindSequence([FromBody] OptimizedVM pLocations)
        {
            try
            {
                _IMapAS.Errors.Clear();
                var result = await _IMapAS.FindSequenceAsync(pLocations);

                if (_IMapAS.Errors.Any())
                    return InvalidRequest(result, _IMapAS.Errors);

                await _ILORoutesAS.SaveTripAsync(pLocations, result);

                return RequestOK(result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpGet]
        [Route("{pEstablishmentKey}/Waypoints/{pId}")]
        public async Task<IActionResult> FindWaypoints(string pEstablishmentKey, long pId)
        {
            try
            {
                var Results =
                    await _ILOWaypointsAS.QueryAsync(r => r.EstablishmentKey == pEstablishmentKey &&
                    r.RouteID == pId);

                if (Results == null)
                    throw new HinoException(_ILOWaypointsAS.Errors.FirstOrDefault());

                if (!Results.Any())
                    return NotFoundRequest();

                return RequestOK(Results);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                _ILOWaypointsAS.Dispose();
                _ILORoutesAS.Dispose();
            }
        }
    }
}
