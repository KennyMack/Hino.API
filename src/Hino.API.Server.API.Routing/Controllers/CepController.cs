﻿using Hino.API.Application.Integrations.Locations;
using Hino.API.Infra.Cross.External.Locations.Models;
using Hino.API.Server.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.API.Server.API.Routing.Controllers
{
    [Route("api/Tour")]
    [ApiController]
    public class CepController : BaseApiController
    {
        private readonly ICepAS _ICepAS;
        public CepController(ICepAS pICepAS)
        {
            _ICepAS = pICepAS;
        }

        [Route("{pCep}")]
        [HttpGet]
        public async Task<IActionResult> Get(string pCep)
        {
            try
            {
                var number = "0";

                try
                {
                    number = QueryString["number"].ToString() ?? "0";
                }
                catch (Exception)
                {
                    number = "0";
                }

                var allInfo = "none";
                try
                {
                    allInfo = QueryString["allinfo"].ToString() ?? "none";
                }
                catch (Exception)
                {
                    allInfo = "none";
                }

                var country = "BR";
                try
                {
                    country = (QueryString["country"].ToString() ?? "BR").ToUpper();
                }
                catch (Exception)
                {
                    country = "BR";
                }

                var cepvm = await _ICepAS.GetCEPAddressAsync(pCep, number, country, allInfo != "none");

                if (_ICepAS.Errors.Any())
                    return InvalidRequest(null, _ICepAS.Errors);

                return RequestOK(cepvm);
            }
            catch (Exception)
            {
                return InvalidRequest(new CepVM(), "CEP Inválido.");
            }
        }
    }
}
