﻿using Hino.API.Application.MapperModels.Profiles;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Hino.API.Server.API.Routing.Configurations
{
    public static class AutoMapperConfig
    {
        public static void AddAutoMapperConfiguration(this IServiceCollection services)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            services.AddAutoMapper(typeof(DomainToViewRoutingProfile), typeof(ViewToDomainRoutingProfile));
        }
    }
}
