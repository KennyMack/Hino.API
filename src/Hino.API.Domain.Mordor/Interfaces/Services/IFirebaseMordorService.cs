﻿using Hino.API.Domain.Mordor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Domain.Mordor.Interfaces.Services
{
    public interface IFirebaseMordorService
    {
        Task<SignInResult> SignInWithEmailAsync(GEUsers pUser);
        Task<SignUpResult> SignUpAsync(GEUsers pUser);
        Task<UserAuth> GetUserByUid(string pUid);
        Task<TokenResult> VerifyTokenAsync(string pToken);
        
    }
}
