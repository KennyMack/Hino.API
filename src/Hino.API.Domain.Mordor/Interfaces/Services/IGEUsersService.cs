﻿using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Mordor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Domain.Mordor.Interfaces.Services
{
    public interface IGEUsersService : IBaseService<GEUsers>
    {
        GEUsers Create(GEUsers pUser);
        GEUsers GetByEmail(string pEstablishmentKey, string pEmail);
        bool ExistsEmailOnEstablishment(string pEstablishmentKey, string pEmail);
        bool ExistsUserNameOnEstablishment(string pEstablishmentKey, string pUserName);
        bool ExistsUserKey(string pUserKey);
        bool ExistsUid(string pUid);
    }
}
