﻿using Hino.API.Domain.Base.Services;
using Hino.API.Domain.Mordor.Interfaces.Repositories;
using Hino.API.Domain.Mordor.Interfaces.Services;
using Hino.API.Domain.Mordor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Domain.Mordor.Services
{
    public class GEUsersService : BaseService<GEUsers>, IGEUsersService
    {
        private readonly IGEUsersRepository _IGEUsersRepository;

        public GEUsersService(IGEUsersRepository pIGEUsersRepository) :
             base(pIGEUsersRepository)
        {
            _IGEUsersRepository = pIGEUsersRepository;
        }

        public GEUsers Create(GEUsers pUser)
        {
            var Error = pUser.IsValid(this);

            if (Error != null)
                Errors.Add(Error);

            pUser.Id = 0;

            return _IGEUsersRepository.Create(pUser);
        }

        public bool ExistsEmailOnEstablishment(string pEstablishmentKey, string pEmail) =>
            _IGEUsersRepository.ExistsEmailOnEstablishment(pEstablishmentKey, pEmail);

        public bool ExistsUid(string pUid) =>
            _IGEUsersRepository.ExistsUid(pUid);

        public bool ExistsUserKey(string pUserKey) =>
            _IGEUsersRepository.ExistsUserKey(pUserKey);

        public bool ExistsUserNameOnEstablishment(string pEstablishmentKey, string pUserName) =>
            _IGEUsersRepository.ExistsUserNameOnEstablishment(pEstablishmentKey, pUserName);

        public GEUsers GetByEmail(string pEstablishmentKey, string pEmail) =>
            _IGEUsersRepository.GetByEmail(pEstablishmentKey, pEmail);
    }
}
