﻿using Hino.API.Domain.Mordor.Interfaces.Repositories;
using Hino.API.Domain.Mordor.Interfaces.Services;
using Hino.API.Domain.Mordor.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using Firebase.Auth;
using FirebaseAdmin.Auth;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FirebaseAuth = FirebaseAdmin.Auth.FirebaseAuth;
using GEUsers = Hino.API.Domain.Mordor.Models.GEUsers;
using Hino.API.Infra.Cross.Utils.Enums;
using Hino.API.Infra.Cross.Utils.Settings;

namespace Hino.API.Domain.Mordor.Services
{
    public class FirebaseMordorService : IFirebaseMordorService
    {
        private readonly IGEUsersRepository _IGEUsersRepository;
        private readonly FirebaseAuth _FirebaseAuth;
        private string ApiKey { get; }
        private readonly IFirebaseAuthProvider _IFirebaseAuthProvider;

        public FirebaseMordorService(
            IGEUsersRepository pIUserRepository)
        {
            _IGEUsersRepository = pIUserRepository;
            ApiKey = AppSettings.Firebase.ApiKey;
            _FirebaseAuth = FirebaseAuth.DefaultInstance;
            _IFirebaseAuthProvider = new FirebaseAuthProvider(
              new FirebaseConfig(ApiKey));
        }

        public async Task<SignInResult> SignInWithEmailAsync(GEUsers pUser)
        {
            var fbAuthLink = await _IFirebaseAuthProvider
                            .SignInWithEmailAndPasswordAsync(pUser.Email, pUser.Password);

            if (fbAuthLink.FirebaseToken == null)
                throw new HinoException("Login inválido", new ModelException()
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "Email",
                    Messages =  new[] { "Login inválido" },
                    Value = pUser.Email
                });

            var user = await FirebaseAuth.DefaultInstance.GetUserAsync(fbAuthLink.User.LocalId);

            return new SignInResult(pUser, user, fbAuthLink);
        }

        public async Task<SignUpResult> SignUpAsync(GEUsers pUser)
        {
            var claims = new Dictionary<string, object>
            {
                { "role", pUser.UserType.ToString() },
                { "UserKey", pUser.UserKey },
                { "EstablishmentKey", pUser.EstablishmentKey }
            };

            UserRecord createdUser = await _FirebaseAuth.CreateUserAsync(new UserRecordArgs
            {
                Email = pUser.Email,
                EmailVerified = true,
                Password = pUser.Password,
                DisplayName = pUser.UserName,
                Disabled = false
            });
            await FirebaseAuth.DefaultInstance.SetCustomUserClaimsAsync(createdUser.Uid, claims);
            
            var user = await GetUserByUid(createdUser.Uid);

            return new SignUpResult(user);
        }

        public async Task<TokenResult> VerifyTokenAsync(string pToken)
        {
            var decoded = await FirebaseAuth.DefaultInstance.VerifyIdTokenAsync(pToken);
            var user = await GetUserByUid(decoded.Uid);
            return new TokenResult(decoded, user);
        }

        public async Task<UserAuth> GetUserByUid(string pUid)
        {
            var user = await FirebaseAuth.DefaultInstance.GetUserAsync(pUid);

            return new UserAuth(
                user.Uid,
                user.DisplayName,
                user.Email,
                user.PhoneNumber,
                user.PhotoUrl,
                user.ProviderId,
                user.EmailVerified,
                user.Disabled,
                user.TokensValidAfterTimestamp,
                user.CustomClaims,
                user.TenantId);
        }
    }
}
