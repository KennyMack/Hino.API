﻿using System.ComponentModel.DataAnnotations;

namespace Hino.API.Domain.Mordor.Enums
{
    public enum EUserType
    {
        [Display(Description = "Ajudante")]
        Assistant = 0,

        [Display(Description = "Vendedor")]
        Sallesman = 1,

        [Display(Description = "Representante")]
        Representant = 2,

        [Display(Description = "Credenciado")]
        Accredited = 3,

        [Display(Description = "Diretor")]
        Director = 4,

        [Display(Description = "Administrador")]
        Master = 5,

        [Display(Description = "Hino")]
        Hino = 6
    }
}
