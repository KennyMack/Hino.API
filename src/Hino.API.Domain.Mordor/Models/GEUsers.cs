﻿using Hino.API.Domain.Base.Models;
using Hino.API.Domain.Mordor.Enums;
using Hino.API.Domain.Mordor.Interfaces.Repositories;
using Hino.API.Infra.Cross.Utils.Exceptions;
using System;
using ValidationMessages = Hino.API.Infra.Cross.Resources.ValidationMessagesResource;
using DefaultMessages = Hino.API.Infra.Cross.Resources.MessagesResource;
using Hino.API.Domain.Mordor.Interfaces.Services;
using Hino.API.Infra.Cross.Utils;

namespace Hino.API.Domain.Mordor.Models
{
    public class GEUsers : BaseEntity
    {
        public string Uid { get; private set; }
        public string UserName { get; private set; }
        public string Name { get; private set; }
        public string Email { get; private set; }
        public string Password { get; private set; }
        public DateTime LastLogin { get; private set; }
        public EUserType UserType { get; private set; }
        public decimal PercDiscount { get; private set; }
        public decimal PercCommission { get; private set; }
        public decimal PercDiscountPrice { get; private set; }
        public decimal PercIncreasePrice { get; private set; }
        public string StoreId { get; private set; }
        public string TerminalId { get; private set; }
        public string UserKey { get; private set; }
        public bool IsBlockedByPay { get; private set; }

        public string Role { get; private set; }

        public GEUsers()
        {

        }

        public GEUsers(string EstablishmentKey, string Email, string Password)
        {
            this.Email = Email;
            this.Password = Password;
            this.EstablishmentKey = EstablishmentKey;
        }

        public GEUsers(
            long Id,
            string EstablishmentKey,
            string UniqueKey,
            string Uid,
            string UserName,
            string Email,
            string Password,
            EUserType UserType,
            string UserKey,
            string Role)
        {
            this.Id = Id;
            this.Uid = Uid;
            this.EstablishmentKey = EstablishmentKey;
            this.UniqueKey = UniqueKey;
            this.UserName = UserName;
            this.UserType = UserType;
            this.UserKey = UserKey;

            this.Email = Email;
            this.Password = Password;
            this.Role = Role;
        }

        public GEUsers(
            long Id,
            string EstablishmentKey,
            string UniqueKey,
            string Uid,
            string UserName,
            string Email,
            string Password,
            EUserType UserType,
            decimal PercDiscount,
            decimal PercCommission,
            decimal PercDiscountPrice,
            decimal PercIncreasePrice,
            string StoreId,
            string TerminalId,
            string UserKey,
            bool IsBlockedByPay,
            string Role)
        {
            this.Id = Id;
            this.Uid = Uid;
            this.EstablishmentKey = EstablishmentKey;
            this.UniqueKey = UniqueKey;
            this.UserName = UserName;
            this.UserType = UserType;
            this.PercDiscount = PercDiscount;
            this.PercCommission = PercCommission;
            this.PercDiscountPrice = PercDiscountPrice;
            this.PercIncreasePrice = PercIncreasePrice;
            this.StoreId = StoreId;
            this.TerminalId = TerminalId;
            this.UserKey = UserKey;
            this.IsBlockedByPay = IsBlockedByPay;

            this.Email = Email;
            this.Password = Password;
            this.Role = Role;
        }

        public ModelException IsValidToAuth()
        {
            if (Uid.IsEmpty())
            {
                return ModelException.CreateValidationError(
                    ValidationMessages.FirebaseKeyIsEmpty,
                    "Uid",
                    Uid
                );
            }

            if (!IsActive)
            {
                return ModelException.CreateValidationError(
                    ValidationMessages.UserIsInactive,
                    "IsActive",
                    IsActive.ToString()
                );
            }

            if (!IsActive)
            {
                return ModelException.CreateValidationError(
                    ValidationMessages.UserIsInactive,
                    "IsActive",
                    IsActive.ToString()
                );
            }

            return null;
        }

        public ModelException IsValid(IGEUsersService pIGEUsersService)
        {
            var ExistEmail = pIGEUsersService.ExistsEmailOnEstablishment(EstablishmentKey, Email);
            if (ExistEmail)
            {
                return ModelException.CreateValidationError(
                    ValidationMessages.EmailWasUsed,
                    "Email",
                    Email
                );
            }
            var ExistUserName = pIGEUsersService.ExistsEmailOnEstablishment(EstablishmentKey, Email);
            if (ExistUserName)
            {
                return ModelException.CreateValidationError(
                    ValidationMessages.UserNameWasUsed,
                    "UserName",
                    UserName
                );
            }
            var ExistUserKey = pIGEUsersService.ExistsUserKey(UserKey);
            if (ExistUserKey)
            {
                return ModelException.CreateValidationError(
                    ValidationMessages.UserNameWasUsed,
                    "UserKey",
                    UserKey
                );
            }
            var ExistUid = pIGEUsersService.ExistsUid(Uid);
            if (ExistUid)
            {
                return ModelException.CreateValidationError(
                    ValidationMessages.UserNameWasUsed,
                    "Uid",
                    Uid
                );
            }

            return null;
        }
    }
}
