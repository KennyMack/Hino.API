﻿using Hino.API.Application.Interfaces.Services;
using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Infra.Cross.Utils.Enums;
using Hino.API.Infra.Cross.Utils.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.Json;
using System.Text.RegularExpressions;

namespace Hino.API.Server.Base
{
    [ApiController]
    public class BaseApiController: ControllerBase
    {
        private readonly List<ModelException> _errors = new List<ModelException>();
        protected IErrorBaseService[] Services { get; set; }
        protected IQueryCollection QueryString { get => HttpContext.Request.Query; }

        protected ModelException[] GetErrors()
        {
            if (!IsOperationValid())
                return _errors.ToArray();

            if (Services != null)
            {
                return Services
                    .SelectMany(r => r.Errors)
                    .Select(r => r)
                    .ToArray();
            }

            return null;
        }

        protected bool HasAnyError()
        {
            if (!IsOperationValid())
                return true;

            if (Services != null)
                return Services.Where(r => r.Errors.Any()).Any();

            return false;
        }

        protected bool IsOperationValid()
        {
            return !_errors.Any();
        }

        protected void AddError(ModelException erro)
        {
            _errors.Add(erro);
        }

        protected void AddError(ModelException[] erro)
        {
            _errors.AddRange(erro);
        }

        protected void ClearErrors()
        {
            _errors.Clear();
        }

        private bool IsSuccessReponse(HttpStatusCode code)
        {
            if (IsInformational(code))
                return true;
            else if (IsSuccess(code))
                return true;
            else if (IsRedirect(code))
                return true;
            else if (IsClientError(code))
                return false;
            else if (IsServerError(code))
                return false;

            return false;
        }

        private bool IsInformational(HttpStatusCode code)
        {
            return (int)code >= 100 && (int)code <= 199;
        }

        private bool IsRedirect(HttpStatusCode code)
        {
            return (int)code >= 300 && (int)code <= 399;
        }

        private bool IsSuccess(HttpStatusCode code)
        {
            return (int)code >= 200 && (int)code <= 299;
        }

        private bool IsClientError(HttpStatusCode code)
        {
            return (int)code >= 400 && (int)code <= 499;
        }

        private bool IsServerError(HttpStatusCode code)
        {
            return (int)code >= 500 && (int)code <= 599;
        }

        protected int GetPageNumber()
        {
            int page;
            try
            {
                var value = QueryString["page"];
                if (!int.TryParse(value, out page))
                    page = 1;
            }
            catch (Exception)
            {
                page = 1;
            }

            return page;
        }

        protected int GetLimitNumber()
        {
            int limit;
            try
            {
                var value = QueryString["limit"];
                if (!int.TryParse(value, out limit))
                    limit = 10;
            }
            catch (Exception)
            {
                limit = 10;
            }

            return limit;
        }

        protected string GetQueryFilter()
        {
            string filter;

            try
            {
                filter = QueryString["filter"].ToString().ToUpper();
            }
            catch (Exception)
            {
                filter = "";
            }
            return filter;
        }

        protected string GetQueryColumn()
        {
            string column;

            try
            {
                column = QueryString["column"];
            }
            catch (Exception)
            {
                column = "";
            }
            return column;
        }

        protected DateTime? ConvertDateUrl(string pDateStr)
        {
            try
            {
                Regex _RegDate = new Regex(@"^(\d{2})(\d{2})(\d{4})(\d{2})(\d{2})(\d{2})$", 
                    RegexOptions.Compiled);
                var Matches = _RegDate.Match(pDateStr);
                var Date = new DateTime(
                    Convert.ToInt32(Matches.Groups[3].Value),
                    Convert.ToInt32(Matches.Groups[2].Value),
                    Convert.ToInt32(Matches.Groups[1].Value),
                    Convert.ToInt32(Matches.Groups[4].Value),
                    Convert.ToInt32(Matches.Groups[5].Value),
                    Convert.ToInt32(Matches.Groups[6].Value));

                return Date;
            }
            catch (Exception)
            {

            }
            return null;
        }

        private object GetContent(object obj, object error, HttpStatusCode responseCode)
        {
            return new
            {
                status = responseCode,
                success = IsSuccessReponse(responseCode),
                data = obj ?? new { },
                error = error ?? new List<ModelException>()
            };
        }

        protected ActionResult RequestOK(object result, HttpStatusCode httpStatusCode = HttpStatusCode.OK) =>
            Ok(GetContent(result, null, httpStatusCode));

        protected ActionResult NotFoundRequest() =>
            NotFound(GetContent(null, "Recurso não encontrado", HttpStatusCode.NotFound));

        protected ActionResult UnauthorizedRequest() =>
            Unauthorized(GetContent(null, "Acesso negado", HttpStatusCode.NotFound));

        protected ActionResult InvalidRequest(object obj, object error)
        {
            if (error.GetType().Name == "ModelStateDictionary")
            {
                var errors = new List<ModelException>();
                ((ModelStateDictionary)error).ToList()
                    .ForEach(r =>
                    {
                        if (r.Value.Errors.FirstOrDefault() != null)
                        {
                            var MErr = new ModelException
                            {
                                ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                                Field = r.Key,//.Contains(".") ? r.Key.Split('.')[1] : r.Key,
                                Messages = new[] { r.Value.Errors.FirstOrDefault()?.ErrorMessage },
                                Value = ""
                            };

                            errors.Add(MErr);
                        }
                    });

                return BadRequest(GetContent(obj, errors, HttpStatusCode.BadRequest));
            }

            return BadRequest(GetContent(obj, error, HttpStatusCode.BadRequest));

        }

        protected ActionResult RequestResult(object obj = null)
        {
            if (HasAnyError())
                return InvalidRequest(obj ?? null, GetErrors());

            return RequestOK(obj ?? true);
        }

        protected bool ValidateModelState<TEntity>(TEntity pModel) where TEntity : class
        {
            ModelState.Clear();
            return TryValidateModel(pModel);
        }

    }
}
