﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Server.Base
{
    public class RequestResultData
    {
        public HttpStatusCode status { get; set; }
        public bool success { get; set; }
        public object data { get; set; }
        public object error { get; set; }
    }
}
