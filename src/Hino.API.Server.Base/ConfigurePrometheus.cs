﻿using Microsoft.AspNetCore.Builder;
using Prometheus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Server.Base
{
    public static class ConfigurePrometheus
    {
        public static void AddPrometheusEndPointCounter(this IApplicationBuilder app, string pName, string pDescription)
        {
            var counter = Metrics.CreateCounter(pName, pDescription,
                new CounterConfiguration
                {
                    LabelNames = new[] { "method", "endpoint" }
                });

            app.Use((context, next) =>
            {
                counter.WithLabels(context.Request.Method, context.Request.Path).Inc();

                return next();
            });

            app.UseMetricServer();
            app.UseHttpMetrics();
        }
    }
}
