﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Domain.Base.Interfaces.Models
{
    public interface IBaseEntity
    {
        long Id { get; set; }
        string EstablishmentKey { get; set; }
        string UniqueKey { get; set; }
        DateTime Created { get; set; }
        DateTime Modified { get; set; }
        bool IsActive { get; set; }
    }
}
