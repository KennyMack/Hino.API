﻿using Hino.API.Infra.Cross.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Domain.Base.Interfaces.Services
{
    public interface IErrorBaseService
    {
        List<ModelException> Errors { get; set; }
    }
}
