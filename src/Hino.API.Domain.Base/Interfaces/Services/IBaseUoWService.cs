﻿using Hino.API.Domain.Base.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Domain.Base.Interfaces.Services
{
    public interface IBaseUoWService<T> where T : BaseEntity
    {
        IList<T> AddedOrUpdatedItems { get; }
    }
}
