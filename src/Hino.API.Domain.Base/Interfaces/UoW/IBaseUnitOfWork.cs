﻿using Hino.API.Infra.Cross.Utils.Exceptions;
using System;
using System.Collections.Generic;

namespace Hino.API.Domain.Base.Interfaces.UoW
{
    public interface IBaseUnitOfWork: IDisposable
    {
        List<ModelException> Errors { get; }
        long RowsAffected { get; }
        void Commit();
        void Rollback();
    }
}
