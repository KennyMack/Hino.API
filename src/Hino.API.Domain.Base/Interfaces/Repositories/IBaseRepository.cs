﻿using Hino.API.Infra.Cross.Utils.Enums;
using Hino.API.Infra.Cross.Utils.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Domain.Base.Interfaces.Repositories
{
    public interface IBaseRepository<T> where T : class
    {
        Task<PagedResult<T>> GetAllPagedAsync(int page, int pageSize, params Expression<Func<T, object>>[] includeProperties);
        Task<T> GetByIdAsync(long id, string pEstablishmentKey, string pUniqueKey, params Expression<Func<T, object>>[] includeProperties);
        T GetById(long id, string pEstablishmentKey, string pUniqueKey, params Expression<Func<T, object>>[] includeProperties);
        T GetByIdToUpdate(long id, string pEstablishmentKey, string pUniqueKey);
        Task<IEnumerable<T>> GetAllAsync(params Expression<Func<T, object>>[] includeProperties);
        Task<T> FirstOrDefaultAsync(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties);
        Task<IEnumerable<T>> QueryAsync(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties);
        Task<PagedResult<T>> QueryPagedAsync(int page, int pageSize, Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties);
        bool Add(T model);
        bool Update(T model);
        bool Remove(T model);
        Task<T> RemoveById(long id, string pEstablishmentKey, string pUniqueKey);
        long NextSequence();
        Task<long> NextSequenceAsync();
        void SetModelState(T model, EModelDataState pState);
        Task<int> SaveChanges();
        void RollBackChanges();
        void Dispose();
    }
}
