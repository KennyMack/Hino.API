﻿using Hino.API.Domain.Base.Interfaces.Models;
using Hino.API.Domain.Base.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Domain.Base.Interfaces.Messaging
{
    public interface IMessageService
    {
        void PublishMessage(string pRoutingKey, string pQueue, IQueueItem Item);
        void PublishMessage(IMessageConfiguration Configuration, IQueueItem Item);
    }
}
