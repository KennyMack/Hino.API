﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Domain.Base.Interfaces.Messaging
{
    public interface IQueueConfiguration
    {
        string Name { get; set; }
        bool Exclusive { get; set; }
        bool Durable { get; set; }
        bool AutoDelete { get; set; }
        bool Persistent { get; set; }
    }
}
