﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Domain.Base.Interfaces.Messaging
{
    public interface IMessageConfiguration
    {
        IQueueConfiguration QueueConfig { get; set; }
        IExchangeConfiguration ExchangeConfig { get; set; }
    }
}
