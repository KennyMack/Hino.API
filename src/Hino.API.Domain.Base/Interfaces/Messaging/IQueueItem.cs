﻿using Hino.API.Domain.Base.Interfaces.Models;
using Hino.API.Domain.Base.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Domain.Base.Interfaces.Messaging
{
    public interface IQueueItem : IBaseEntity
    {
        public string EntryName { get; set; }
        public DateTime DateTimePublished { get; set; }
        public BaseEntity Tag { get; set; }
    }
}
