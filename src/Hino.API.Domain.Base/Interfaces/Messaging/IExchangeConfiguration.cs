﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Domain.Base.Interfaces.Messaging
{
    public interface IExchangeConfiguration
    {
        string Name { get; set; }
        string RoutingKey { get; set; }
        string Type { get; set; }
        bool Durable { get; set; }
        bool AutoDelete { get; set; }
    }
}
