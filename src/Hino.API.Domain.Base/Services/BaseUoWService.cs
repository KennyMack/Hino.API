﻿using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Base.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Domain.Base.Services
{
    public class BaseUoWService<T> : IErrorBaseService, IBaseUoWService<T> where T : BaseEntity
    {
        public List<ModelException> Errors { get; set; }
        public IList<T> AddedOrUpdatedItems { get; }

        public BaseUoWService()
        {
            Errors = new List<ModelException>();
            AddedOrUpdatedItems = new List<T>();
        }
    }
}
