﻿using Hino.API.Domain.Base.Interfaces.Exceptions;
using Hino.API.Domain.Base.Interfaces.Repositories;
using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Base.Models;
using Hino.API.Infra.Cross.Utils.Enums;
using Hino.API.Infra.Cross.Utils.Exceptions;
using Hino.API.Infra.Cross.Utils.Paging;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hino.API.Domain.Base.Services
{
    public class BaseService<T> : IDisposable, IBaseService<T> where T : BaseEntity
    {
        public bool DontSendToQueue { get; set; }
        public List<ModelException> Errors { get; set; }
        public IBaseRepository<T> DataRepository { get; }
        public IList<T> AddedOrUpdatedItems { get; }
        public IList<T> RemovedItems { get; }

        public BaseService(IBaseRepository<T> repo)
        {
            DataRepository = repo;
            AddedOrUpdatedItems = new List<T>();
            RemovedItems = new List<T>();
            Errors = new List<ModelException>();
        }

        public virtual T Add(T model)
        {
            try
            {
                DataRepository.Add(model);
                AddedOrUpdatedItems.Add(model);
            }
            catch (Exception e)
            {
                Logging.Exception(e);
                Errors.Add(ModelException.CreateSqlError(e, EExceptionErrorCodes.InsertSQLError));
            }
            return model;
        }

        public virtual T Update(T model)
        {
            try
            {
                DataRepository.Update(model);
                AddedOrUpdatedItems.Add(model);
            }
            catch (Exception e)
            {
                Logging.Exception(e);
                Errors.Add(ModelException.CreateSqlError(e, EExceptionErrorCodes.UpdateSQLError));
            }
            return model;
        }

        public virtual void Remove(T model)
        {
            try
            {
                DataRepository.Remove(model);
                RemovedItems.Remove(model);
            }
            catch (Exception e)
            {
                Logging.Exception(e);
                Errors.Add(ModelException.CreateSqlError(e, EExceptionErrorCodes.DeleteSQLError));
            }
        }

        public virtual async Task<T> RemoveById(long id, string pEstablishmentKey, string pUniqueKey)
        {
            try
            {
                var model = await DataRepository.RemoveById(id, pEstablishmentKey, pUniqueKey);
                RemovedItems.Add(model);
                return model;
            }
            catch (Exception e)
            {
                Logging.Exception(e);
                Errors.Add(ModelException.CreateSqlError(e, EExceptionErrorCodes.DeleteSQLError));
            }
            return default;
        }

        public virtual async Task<PagedResult<T>> GetAllPagedAsync(int page, int pageSize, params Expression<Func<T, object>>[] includeProperties) =>
            await DataRepository.GetAllPagedAsync(page, pageSize, includeProperties);

        public virtual async Task<IEnumerable<T>> GetAllAsync(params Expression<Func<T, object>>[] includeProperties) =>
            await DataRepository.GetAllAsync(includeProperties);

        public virtual async Task<T> GetByIdAsync(long id, string pEstablishmentKey, string pUniqueKey, params Expression<Func<T, object>>[] includeProperties) =>
            await DataRepository.GetByIdAsync(id, pEstablishmentKey, pUniqueKey, includeProperties);

        public virtual T GetById(long id, string pEstablishmentKey, string pUniqueKey, params Expression<Func<T, object>>[] includeProperties) =>
            DataRepository.GetById(id, pEstablishmentKey, pUniqueKey, includeProperties);

        public virtual async Task<IEnumerable<T>> QueryAsync(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties) =>
            await DataRepository.QueryAsync(predicate, includeProperties);

        public virtual async Task<PagedResult<T>> QueryPagedAsync(int page, int pageSize, Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties) =>
            await DataRepository.QueryPagedAsync(page, pageSize, predicate, includeProperties);

        public virtual async Task<T> FirstOrDefaultAsync(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties) =>
            await DataRepository.FirstOrDefaultAsync(predicate, includeProperties);

        public T GetByIdToUpdate(long id, string pEstablishmentKey, string pUniqueKey) =>
            DataRepository.GetByIdToUpdate(id, pEstablishmentKey, pUniqueKey);

        public virtual long NextSequence() => DataRepository.NextSequence();

        public virtual async Task<long> NextSequenceAsync() => await DataRepository.NextSequenceAsync();

        public async Task<int> SaveChanges()
        {
            try
            {
                var ret = await DataRepository.SaveChanges();

                if (ret > 0)
                    await GenerateEntryQueueAsync();

                return ret;
            }
            catch (Exception e)
            {
                Logging.Exception(e);
                if (e is IModelEntityValidationException exception)
                    Errors = exception.MException;
                else
                    Errors.Add(ModelException.CreateSqlError(e, EExceptionErrorCodes.SaveSQLError));
            }
            return -1;
        }

        public void RollBackChanges()
        {
            try
            {
                DataRepository.RollBackChanges();
            }
            catch (Exception e)
            {
                Logging.Exception(e);
                if (e is IModelEntityValidationException exception)
                    Errors = exception.MException;
                else
                    Errors.Add(ModelException.CreateSqlError(e, EExceptionErrorCodes.SaveSQLError));
            }
        }

        public void SetModelState(T model, EModelDataState pState)
        {
            try
            {
                DataRepository.SetModelState(model, pState);
            }
            catch (Exception e)
            {
                Logging.Exception(e);
                if (e is IModelEntityValidationException exception)
                    Errors = exception.MException;
                else
                    Errors.Add(ModelException.CreateSqlError(e, EExceptionErrorCodes.SaveSQLError));
            }
        }

        public void Dispose()
        {
            DataRepository.Dispose();
            AddedOrUpdatedItems.Clear();
            RemovedItems.Clear();
            GC.SuppressFinalize(this);
        }

        #region Generate Entry Queue
        public virtual async Task GenerateEntryQueueAsync()
        {
            await Task.Delay(1);
            if (!DontSendToQueue)
            {
                /*
                var queue = new MessageService<T>();
                foreach (var item in AddedOrUpdatedItems)
                {
                    // var Entity = (IBaseEntity)item;
                    // var dbItem = await GetByIdAsync(Entity.Id, Entity.EstablishmentKey, Entity.UniqueKey);
                    queue.SendCreatedOrUpdatedQueue(item);
                }

                foreach (var item in RemovedItems)
                    queue.SendRemovedQueue(item);
                */
            }
            AddedOrUpdatedItems.Clear();
            RemovedItems.Clear();
        }
        #endregion

    }
}
