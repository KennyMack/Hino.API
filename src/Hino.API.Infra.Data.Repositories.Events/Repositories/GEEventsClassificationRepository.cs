using Hino.API.Domain.Events.Interfaces.Repositories;
using Hino.API.Domain.Events.Models;
using Hino.API.Infra.Data.Repositories.Base.Repositories;
using Hino.API.Infra.Data.Repositories.Events.Context;

namespace Hino.API.Infra.Data.Repositories.Events.Repositories
{
    public class GEEventsClassificationRepository : BaseRepository<GEEventsClassification>, IGEEventsClassificationRepository
    {
        public GEEventsClassificationRepository(EventsDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
