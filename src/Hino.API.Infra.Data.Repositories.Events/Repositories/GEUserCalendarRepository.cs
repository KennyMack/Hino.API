using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Hino.API.Infra.Data.Repositories.Base.Repositories;
using Hino.API.Domain.Events.Models;
using Hino.API.Domain.Events.Interfaces.Repositories;
using Hino.API.Infra.Data.Repositories.Events.Context;
using Hino.API.Infra.Cross.Utils.Paging;
using Microsoft.EntityFrameworkCore;

namespace Hino.API.Infra.Data.Repositories.Events.Repositories
{
    public class GEUserCalendarRepository : BaseRepository<GEUserCalendar>, IGEUserCalendarRepository
    {
        public GEUserCalendarRepository(EventsDbContext appDbContext) : base(appDbContext)
        {
        }

        public async override Task<PagedResult<GEUserCalendar>> QueryPagedAsync(int page, int pageSize,
            Expression<Func<GEUserCalendar, bool>> predicate,
            params Expression<Func<GEUserCalendar, object>>[] includeProperties) =>
            await PaginateQueryAsync(LocalAddQueryProperties(DbEntity
                .Include(e => e.GEUsers)
                .Include(e => e.GEEvents)
                .Include(e => e.GEEvents.GEEventsClassification)
                .Include(e => e.GEEvents.GEEstabCalendar)
                .Include(e => e.GEEvents.GEEnterpriseEvent)
                .Include(e => e.GEEvents.GEUserCalendar)
                .Include("GEEvents.GEUserCalendar.GEUsers")
                .Include("GEEvents.GEEnterpriseEvent.GEEnterprises")
                .Include("GEEvents.GEEstabCalendar.GEEstablishments"), includeProperties)
                            .AsNoTracking()
                            .Where(predicate), page, pageSize);

        public override async Task<PagedResult<GEUserCalendar>> GetAllPagedAsync(int page, int pageSize,
                params Expression<Func<GEUserCalendar, object>>[] includeProperties) =>
            await PaginateQueryAsync(LocalAddQueryProperties(DbEntity
                .Include(e => e.GEUsers)
                .Include(e => e.GEEvents)
                .Include(e => e.GEEvents.GEEventsClassification)
                .Include(e => e.GEEvents.GEEstabCalendar)
                .Include(e => e.GEEvents.GEEnterpriseEvent)
                .Include(e => e.GEEvents.GEUserCalendar)
                .Include("GEEvents.GEUserCalendar.GEUsers")
                .Include("GEEvents.GEEnterpriseEvent.GEEnterprises")
                .Include("GEEvents.GEEstabCalendar.GEEstablishments"), includeProperties)
                            .AsNoTracking(), page, pageSize);

        public override async Task<IEnumerable<GEUserCalendar>> QueryAsync(
            Expression<Func<GEUserCalendar, bool>> predicate,
            params Expression<Func<GEUserCalendar, object>>[] includeProperties) =>
            await LocalAddQueryProperties(DbEntity
                .Include(e => e.GEEvents)
                .Include(e => e.GEUsers)
                .Include(e => e.GEEvents.GEEventsClassification)
                .Include(e => e.GEEvents.GEEstabCalendar)
                .Include(e => e.GEEvents.GEEnterpriseEvent)
                .Include("GEEvents.GEEnterpriseEvent.GEEnterprises")
                .Include("GEEvents.GEEstabCalendar.GEEstablishments")
                , includeProperties)
                            .Where(predicate)
                            .AsNoTracking()
                            .ToListAsync();

        public override async Task<IEnumerable<GEUserCalendar>> GetAllAsync(
            params Expression<Func<GEUserCalendar, object>>[] includeProperties) =>
            await LocalAddQueryProperties(DbEntity
                .Include(e => e.GEUsers)
                .Include(e => e.GEEvents)
                .Include(e => e.GEEvents.GEEventsClassification)
                .Include(e => e.GEEvents.GEEstabCalendar)
                .Include(e => e.GEEvents.GEEnterpriseEvent)
                .Include(e => e.GEEvents.GEUserCalendar)
                .Include("GEEvents.GEUserCalendar.GEUsers")
                .Include("GEEvents.GEEnterpriseEvent.GEEnterprises")
                .Include("GEEvents.GEEstabCalendar.GEEstablishments"), includeProperties).ToListAsync();

        public override async Task<GEUserCalendar> GetByIdAsync(long id, string pEstablishmentKey, string pUniqueKey,
             params Expression<Func<GEUserCalendar, object>>[] includeProperties) =>
             await DbEntity
                .Include(e => e.GEUsers)
                .Include(e => e.GEEvents)
                .Include(e => e.GEEvents.GEEventsClassification)
                .Include(e => e.GEEvents.GEEstabCalendar)
                .Include(e => e.GEEvents.GEEnterpriseEvent)
                .Include(e => e.GEEvents.GEUserCalendar)
                .Include("GEEvents.GEUserCalendar.GEUsers")
                .Include("GEEvents.GEEnterpriseEvent.GEEnterprises")
                .Include("GEEvents.GEEstabCalendar.GEEstablishments")
                    .AsNoTracking().Where(r =>
                       r.Id == id &&
                       r.EstablishmentKey == pEstablishmentKey &&
                       r.UniqueKey == pUniqueKey).FirstOrDefaultAsync();

        IQueryable<GEUserCalendar> LocalAddQueryProperties(IQueryable<GEUserCalendar> query,
            params Expression<Func<GEUserCalendar, object>>[] includeProperties)
        {
            foreach (var includeProperty in includeProperties)
                query = query.Include(includeProperty);

            return query;
        }

        public override GEUserCalendar GetById(long id, string pEstablishmentKey, string pUniqueKey,
            params Expression<Func<GEUserCalendar, object>>[] includeProperties) =>
            LocalAddQueryProperties(DbEntity, includeProperties)
                .Include(e => e.GEUsers)
                .Include(e => e.GEEvents)
                .Include(e => e.GEEvents.GEEventsClassification)
                .Include(e => e.GEEvents.GEEstabCalendar)
                .Include(e => e.GEEvents.GEEnterpriseEvent)
                .Include(e => e.GEEvents.GEUserCalendar)
                .Include("GEEvents.GEUserCalendar.GEUsers")
                .Include("GEEvents.GEEnterpriseEvent.GEEnterprises")
                .Include("GEEvents.GEEstabCalendar.GEEstablishments")
            .Where(r =>
                   r.Id == id &&
                   r.EstablishmentKey == pEstablishmentKey &&
                   r.UniqueKey == pUniqueKey)
                            .AsNoTracking().FirstOrDefault();

    }
}
