using Hino.API.Domain.Events.Interfaces.Repositories;
using Hino.API.Domain.Events.Models;
using Hino.API.Infra.Cross.Utils.Paging;
using Hino.API.Infra.Data.Repositories.Base.Repositories;
using Hino.API.Infra.Data.Repositories.Events.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hino.API.Infra.Data.Repositories.Events.Repositories
{
    public class GEEventsRepository : BaseRepository<GEEvents>, IGEEventsRepository
    {
        public GEEventsRepository(EventsDbContext appDbContext) : base(appDbContext)
        {
        }

        public async override Task<PagedResult<GEEvents>> QueryPagedAsync(int page, int pageSize,
            Expression<Func<GEEvents, bool>> predicate,
            params Expression<Func<GEEvents, object>>[] includeProperties) =>
            await PaginateQueryAsync(LocalAddQueryProperties(DbEntity
                .Include(e => e.GEEventsClassification)
                .Include(e => e.GEEstabCalendar)
                .Include(e => e.GEEnterpriseEvent)
                .Include(e => e.GEUserCalendar)
                .Include("GEUserCalendar.GEUsers")
                .Include("GEEnterpriseEvent.GEEnterprises")
                .Include("GEEnterpriseEvent.GEEnterprises.GEEnterpriseContacts")
                .Include("GEEstabCalendar.GEEstablishments"), includeProperties)
                            .AsNoTracking()
                            .Where(predicate), page, pageSize);

        public override async Task<PagedResult<GEEvents>> GetAllPagedAsync(int page, int pageSize,
                params Expression<Func<GEEvents, object>>[] includeProperties) =>
            await PaginateQueryAsync(LocalAddQueryProperties(DbEntity
                .Include(e => e.GEEventsClassification)
                .Include(e => e.GEEstabCalendar)
                .Include(e => e.GEEnterpriseEvent)
                .Include(e => e.GEUserCalendar)
                .Include("GEUserCalendar.GEUsers")
                .Include("GEEnterpriseEvent.GEEnterprises")
                .Include("GEEnterpriseEvent.GEEnterprises.GEEnterpriseContacts")
                .Include("GEEstabCalendar.GEEstablishments"), includeProperties)
                            .AsNoTracking(), page, pageSize);

        public override async Task<IEnumerable<GEEvents>> QueryAsync(
            Expression<Func<GEEvents, bool>> predicate,
            params Expression<Func<GEEvents, object>>[] includeProperties) =>
            await LocalAddQueryProperties(DbEntity
                .Include(e => e.GEEventsClassification)
                .Include(e => e.GEEstabCalendar)
                .Include(e => e.GEEnterpriseEvent)
                .Include(e => e.GEUserCalendar)
                .Include("GEUserCalendar.GEUsers")
                .Include("GEEnterpriseEvent.GEEnterprises")
                .Include("GEEnterpriseEvent.GEEnterprises.GEEnterpriseContacts")
                .Include("GEEstabCalendar.GEEstablishments"), includeProperties)
                            .Where(predicate)
                            .AsNoTracking()
                            .ToListAsync();

        public override async Task<IEnumerable<GEEvents>> GetAllAsync(
            params Expression<Func<GEEvents, object>>[] includeProperties) =>
            await LocalAddQueryProperties(DbEntity
                .Include(e => e.GEEventsClassification)
                .Include(e => e.GEEstabCalendar)
                .Include(e => e.GEEnterpriseEvent)
                .Include(e => e.GEUserCalendar)
                .Include("GEUserCalendar.GEUsers")
                .Include("GEEnterpriseEvent.GEEnterprises")
                .Include("GEEnterpriseEvent.GEEnterprises.GEEnterpriseContacts")
                .Include("GEEstabCalendar.GEEstablishments"), includeProperties).ToListAsync();

        public override async Task<GEEvents> GetByIdAsync(long id, string pEstablishmentKey, string pUniqueKey,
             params Expression<Func<GEEvents, object>>[] includeProperties) =>
             await DbEntity
                .Include(e => e.GEEventsClassification)
                .Include(e => e.GEEstabCalendar)
                .Include(e => e.GEEnterpriseEvent)
                .Include(e => e.GEUserCalendar)
                .Include("GEUserCalendar.GEUsers")
                .Include("GEEnterpriseEvent.GEEnterprises")
                .Include("GEEnterpriseEvent.GEEnterprises.GEEnterpriseContacts")
                .Include("GEEstabCalendar.GEEstablishments")
                    .AsNoTracking().Where(r =>
                       r.Id == id &&
                       r.EstablishmentKey == pEstablishmentKey &&
                       r.UniqueKey == pUniqueKey).FirstOrDefaultAsync();

        IQueryable<GEEvents> LocalAddQueryProperties(IQueryable<GEEvents> query,
            params Expression<Func<GEEvents, object>>[] includeProperties)
        {
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        public override GEEvents GetById(long id, string pEstablishmentKey, string pUniqueKey,
            params Expression<Func<GEEvents, object>>[] includeProperties) =>
            LocalAddQueryProperties(DbEntity, includeProperties)
                .Include(e => e.GEEventsClassification)
                .Include(e => e.GEEstabCalendar)
                .Include(e => e.GEEnterpriseEvent)
                .Include(e => e.GEUserCalendar)
                .Include("GEUserCalendar.GEUsers")
                .Include("GEEnterpriseEvent.GEEnterprises")
                .Include("GEEnterpriseEvent.GEEnterprises.GEEnterpriseContacts")
                .Include("GEEstabCalendar.GEEstablishments")
            .Where(r =>
                   r.Id == id &&
                   r.EstablishmentKey == pEstablishmentKey &&
                   r.UniqueKey == pUniqueKey)
                            .AsNoTracking().FirstOrDefault();
    }
}
