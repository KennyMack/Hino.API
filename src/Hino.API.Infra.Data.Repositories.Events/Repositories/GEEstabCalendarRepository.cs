using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Hino.API.Infra.Data.Repositories.Base.Repositories;
using Hino.API.Domain.Events.Models;
using Hino.API.Domain.Events.Interfaces.Repositories;
using Hino.API.Infra.Cross.Utils.Paging;
using Hino.API.Infra.Data.Repositories.Events.Context;
using Microsoft.EntityFrameworkCore;

namespace Hino.API.Infra.Data.Repositories.Events.Repositories
{
    public class GEEstabCalendarRepository : BaseRepository<GEEstabCalendar>, IGEEstabCalendarRepository
    {
        public GEEstabCalendarRepository(EventsDbContext appDbContext) : base(appDbContext)
        {
        }

        public async override Task<PagedResult<GEEstabCalendar>> QueryPagedAsync(int page, int pageSize,
            Expression<Func<GEEstabCalendar, bool>> predicate,
            params Expression<Func<GEEstabCalendar, object>>[] includeProperties) =>
            await PaginateQueryAsync(LocalAddQueryProperties(DbEntity
                .Include(e => e.GEEvents)
                .Include(e => e.GEEvents.GEEventsClassification)
                .Include(e => e.GEEvents.GEEstabCalendar)
                .Include(e => e.GEEvents.GEEnterpriseEvent)
                .Include(e => e.GEEvents.GEUserCalendar)
                .Include("GEEvents.GEUserCalendar.GEUsers")
                .Include("GEEvents.GEEnterpriseEvent.GEEnterprises")
                .Include("GEEvents.GEEstabCalendar.GEEstablishments"), includeProperties)
                            .AsNoTracking()
                            .Where(predicate), page, pageSize);

        public override async Task<PagedResult<GEEstabCalendar>> GetAllPagedAsync(int page, int pageSize,
                params Expression<Func<GEEstabCalendar, object>>[] includeProperties) =>
            await PaginateQueryAsync(LocalAddQueryProperties(DbEntity
                .Include(e => e.GEEvents)
                .Include(e => e.GEEvents.GEEventsClassification)
                .Include(e => e.GEEvents.GEEstabCalendar)
                .Include(e => e.GEEvents.GEEnterpriseEvent)
                .Include(e => e.GEEvents.GEUserCalendar)
                .Include("GEEvents.GEUserCalendar.GEUsers")
                .Include("GEEvents.GEEnterpriseEvent.GEEnterprises")
                .Include("GEEvents.GEEstabCalendar.GEEstablishments"), includeProperties)
                            .AsNoTracking(), page, pageSize);

        public override async Task<IEnumerable<GEEstabCalendar>> QueryAsync(
            Expression<Func<GEEstabCalendar, bool>> predicate,
            params Expression<Func<GEEstabCalendar, object>>[] includeProperties) =>
            await LocalAddQueryProperties(DbEntity
                .Include(e => e.GEEvents)
                .Include(e => e.GEEvents.GEEventsClassification)
                /*.Include(e => e.GEEvents.GEEstabCalendar)
                .Include(e => e.GEEvents.GEEnterpriseEvent)
                .Include(e => e.GEEvents.GEUserCalendar)
                .Include("GEEvents.GEUserCalendar.GEUsers")
                .Include("GEEvents.GEEnterpriseEvent.GEEnterprises")
                .Include("GEEvents.GEEstabCalendar.GEEstablishments")
                */, includeProperties)
                            .Where(predicate)
                            .AsNoTracking()
                            .ToListAsync();

        public override async Task<IEnumerable<GEEstabCalendar>> GetAllAsync(
            params Expression<Func<GEEstabCalendar, object>>[] includeProperties) =>
            await LocalAddQueryProperties(DbEntity
                .Include(e => e.GEEvents)
                .Include(e => e.GEEvents.GEEventsClassification)
                .Include(e => e.GEEvents.GEEstabCalendar)
                .Include(e => e.GEEvents.GEEnterpriseEvent)
                .Include(e => e.GEEvents.GEUserCalendar)
                .Include("GEEvents.GEUserCalendar.GEUsers")
                .Include("GEEvents.GEEnterpriseEvent.GEEnterprises")
                .Include("GEEvents.GEEstabCalendar.GEEstablishments"), includeProperties)
                .AsNoTracking()
                .ToListAsync();

        public override async Task<GEEstabCalendar> GetByIdAsync(long id, string pEstablishmentKey, string pUniqueKey,
             params Expression<Func<GEEstabCalendar, object>>[] includeProperties) =>
             await DbEntity.AsNoTracking()
                .Include(e => e.GEEvents)
                .Include(e => e.GEEvents.GEEventsClassification)
                .Include(e => e.GEEvents.GEEstabCalendar)
                .Include(e => e.GEEvents.GEEnterpriseEvent)
                .Include(e => e.GEEvents.GEUserCalendar)
                .Include("GEEvents.GEUserCalendar.GEUsers")
                .Include("GEEvents.GEEnterpriseEvent.GEEnterprises")
                .Include("GEEvents.GEEstabCalendar.GEEstablishments")
                    .AsNoTracking().Where(r =>
                       r.Id == id &&
                       r.EstablishmentKey == pEstablishmentKey &&
                       r.UniqueKey == pUniqueKey).FirstOrDefaultAsync();

        IQueryable<GEEstabCalendar> LocalAddQueryProperties(IQueryable<GEEstabCalendar> query,
            params Expression<Func<GEEstabCalendar, object>>[] includeProperties)
        {
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        public override GEEstabCalendar GetById(long id, string pEstablishmentKey, string pUniqueKey,
            params Expression<Func<GEEstabCalendar, object>>[] includeProperties) =>
            LocalAddQueryProperties(DbEntity, includeProperties)
                .Include(e => e.GEEvents)
                .Include(e => e.GEEvents.GEEventsClassification)
                .Include(e => e.GEEvents.GEEstabCalendar)
                .Include(e => e.GEEvents.GEEnterpriseEvent)
                .Include(e => e.GEEvents.GEUserCalendar)
                .Include("GEEvents.GEUserCalendar.GEUsers")
                .Include("GEEvents.GEEnterpriseEvent.GEEnterprises")
                .Include("GEEvents.GEEstabCalendar.GEEstablishments")
            .Where(r =>
                   r.Id == id &&
                   r.EstablishmentKey == pEstablishmentKey &&
                   r.UniqueKey == pUniqueKey)
                            .AsNoTracking().FirstOrDefault();
    }
}
