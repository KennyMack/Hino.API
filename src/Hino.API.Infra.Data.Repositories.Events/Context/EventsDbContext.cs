﻿using Hino.API.Domain.Events.Models;
using Hino.API.Infra.Data.Repositories.Base.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Hino.API.Infra.Data.Repositories.Events.Context
{
    public class EventsDbContext : BaseDbContext
    {
        public virtual DbSet<GEEnterpriseEvent> GEEnterpriseEvent { get; set; }
        public virtual DbSet<GEEventsClassification> GEEventsClassification { get; set; }
        public virtual DbSet<GEEstabCalendar> GEEstabCalendar { get; set; }
        public virtual DbSet<GEEvents> GEEvents { get; set; }
        public virtual DbSet<GEUserCalendar> GEUserCalendar { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            // modelBuilder.ApplyConfiguration(new GEEstabDevicesConfiguration("GEESTABDEVICES"));
            // modelBuilder.ApplyConfiguration(new GEEstabFatConfiguration("GEESTABFAT"));
            // modelBuilder.ApplyConfiguration(new GEEstablishmentsConfiguration("GEESTABLISHMENTS"));
            // modelBuilder.ApplyConfiguration(new GEEstabMenuConfiguration("GEESTABMENU"));
            // modelBuilder.ApplyConfiguration(new GEEstabPayConfiguration("GEESTABPAY"));
        }
    }
}
