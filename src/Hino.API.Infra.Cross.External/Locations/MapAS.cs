﻿using Hino.API.Application.Integrations.Locations;
using Hino.API.Application.Integrations.Locations.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Infra.Cross.External.Locations
{
    public abstract class MapAS: IMapAS
    {
        public List<ModelException> Errors { get; set; }
        public abstract Task<IMapResult> GetAddressAsync(ICepAddress pAddress);
        public abstract Task<IFindSequenceResultVM> FindSequenceAsync(IOptimizedVM pLocations);

        public MapAS()
        {
            Errors = new List<ModelException>();
        }
    }
}
