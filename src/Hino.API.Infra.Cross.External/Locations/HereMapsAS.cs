﻿using Hino.API.Application.Integrations.Locations.Models;
using Hino.API.Infra.Cross.External.Locations.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using Hino.API.Infra.Cross.Utils.Settings;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.API.Infra.Cross.External.Locations
{
    public class HereMapsAS : MapAS
    {
        private RequestHere _Request;

        public HereMapsAS()
        {
            Errors = new List<ModelException>();
            _Request = new RequestHere();
        }

        public async override Task<IMapResult> GetAddressAsync(ICepAddress pAddress)
        {
            try
            {
                var client = _Request.CreateClient();
                var request = _Request.CreateRequest($"geocode.json?", Method.Get);

                var address = pAddress.ToAddress();
                request.AddParameter("apiKey", AppSettings.HereMapsKey, ParameterType.GetOrPost);
                request.AddParameter("searchtext", address, ParameterType.GetOrPost);
                request.AddParameter("locationattributes", "all", ParameterType.GetOrPost);

                var Iresponse = await client.ExecuteAsync(request);

                var result = JsonConvert.DeserializeObject<HereMapsResult>(Iresponse.Content).GetBestResult();

                return new MapResult
                {
                    Address = new CepAddress
                    {
                       Label = result.address.label,
                        Country = result.address.countryCode,
                        CountryCode = result.address.countryCode,
                        CountryName = result.address.countryName,
                        State = result.address.stateCode,
                        StateName = result.address.state,
                        City = result.address.city,
                        ZipCode = result.address.postalCode,
                        District = result.address.district,
                        Street = result.address.street,
                        Number = result.address.houseNumber,
                        Complement = pAddress.Complement,
                    },
                    Position = new MapPosition
                    {
                        Lat = result.position.lat,
                        Lng = result.position.lng
                    },
                    MapView = new MapView
                    {
                        West = result.mapView.West,
                        South = result.mapView.South,
                        East = result.mapView.East,
                        North = result.mapView.North,
                    },
                    ReferenceId = result.id
                };
            }
            catch (Exception e)
            {
                Logging.Exception(e);
                Errors.Add(ModelException.CreateNotFoundError(
                    string.Format(Resources.MessagesResource.StatusNoOK, e.Message),
                    "HereMaps",
                    ""
                ));
                return null;
            }
        }

        public override async Task<IFindSequenceResultVM> FindSequenceAsync(IOptimizedVM pLocations)
        {
            try
            {
                var client = _Request.CreateClientFind();
                var request = _Request.CreateRequest($"findsequence.json?", Method.Get);

                var origin = pLocations.Location.First().ToStart();
                var end = pLocations.Location.First().ToEnd();

                var destinations = pLocations.GetDestinations();

                request.AddParameter("apiKey", AppSettings.HereMapsKey, ParameterType.GetOrPost);
                request.AddParameter("start", origin, ParameterType.GetOrPost);

                for (int i = 0, length = destinations.Count; i < length; i++)
                    request.AddParameter($"destination{i + 1}", destinations[i].ToDestination(), ParameterType.GetOrPost);

                request.AddParameter("end", end, ParameterType.GetOrPost);
                request.AddParameter("mode", "fastest;truck", ParameterType.GetOrPost);
                request.AddParameter("hasTrailer", true, ParameterType.GetOrPost);
                request.AddParameter("requestId", Guid.NewGuid().ToString(), ParameterType.GetOrPost);

                var Iresponse = await client.ExecuteAsync(request);

                var result = JsonConvert.DeserializeObject<HereFindSequenceResultVM>(Iresponse.Content);

                if (result.errors.Count > 0)
                    throw new RouteException(result.errors.First());

                if (result.results.Count <= 0)
                    throw new RouteException(Resources.MessagesResource.NoRoutesFinded);

                var trip = result.results.First();

                var route = new FindSequenceResultVM();
                route.Weigth = 0;
                route.Duration = trip.time;
                route.Distance = trip.distance;
                route.Note = trip.description;

                foreach (var item in trip.waypoints)
                {
                    LocationVM locationItem = null;
                    if (item.id == "Origem" ||
                        item.id == "Destino")
                        locationItem = (LocationVM)pLocations.Location.First();
                    else
                        locationItem = (LocationVM)pLocations.Location.FirstOrDefault(r => r.Name == item.id);

                    var interconection = trip.interconnections.FirstOrDefault(r => r.toWaypoint == item.id);

                    if (locationItem != null)
                    {
                        var WayPoint = new FindSequenceLocationVM
                        {
                            Distance = interconection?.distance ?? 0,
                            Time = interconection?.time ?? 0,
                            Rest = interconection?.rest ?? 0,
                            Waiting = interconection?.waiting ?? 0,
                            Name = locationItem.Name,
                            Address = locationItem.Address,
                            Sequence = item.sequence,
                            Lat = item.lat,
                            Lng = item.lng,
                            AddressFrom = interconection?.fromWaypoint,
                            AddressTo = interconection?.toWaypoint
                        };

                        route.Waypoints.Add(WayPoint);
                    }
                }

                return route;
            }
            catch (RouteException e)
            {
                Logging.Exception(e);

                Errors.Add(ModelException.CreateNotFoundError(e.Message, "Status", ""));
                return null;
            }
            catch (Exception e)
            {
                Logging.Exception(e);
                Errors.Add(ModelException.CreateNotFoundError(
                    string.Format(Resources.MessagesResource.StatusNoOK, e.Message), "Status", ""));
                return null;
            }
        }
    }
}
