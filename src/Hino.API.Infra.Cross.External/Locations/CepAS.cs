﻿using Hino.API.Application.Integrations.Enterprises.Models;
using Hino.API.Application.Integrations.Locations;
using Hino.API.Application.Integrations.Locations.Models;
using Hino.API.Application.Interfaces.Services.Demograph;
using Hino.API.Infra.Cross.External.Locations.Models;
using Hino.API.Infra.Cross.Utils;
using Hino.API.Infra.Cross.Utils.Exceptions;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Infra.Cross.External.Locations
{
    public class CepAS : ICepAS
    {
        private RequestCEP _RequestCEP;
        public List<ModelException> Errors { get; set; }
        readonly IGECountriesAS _IGECountriesAS;
        readonly IMapAS _IMapAS;

        public CepAS(
            IGECountriesAS pIGECountriesAS,
            IMapAS pIMapAS)
        {
            Errors = new List<ModelException>();
            _IGECountriesAS = pIGECountriesAS;
            _IMapAS = pIMapAS;
            _RequestCEP = new RequestCEP();
        }

        public async Task<ICepVM> GetCEPAsync(string pCEP)
        {
            if (pCEP.IsEmpty())
            {
                Errors.Add(ModelException.CreateValidationError(
                    Resources.ValidationMessagesResource.InvalidCEP,
                    "CEP",
                    ""
                ));

                return new CepVM();
            }

            try
            {
                var client = _RequestCEP.CreateClient();
                var request = _RequestCEP.CreateRequest($"{pCEP.OnlyNumbers()}/json/", Method.Get);

                var Iresponse = await client.ExecuteAsync(request);

                var resultCEP = JsonConvert.DeserializeObject<CepVM>(Iresponse.Content, new JsonSerializerSettings
                {
                    Culture = new System.Globalization.CultureInfo("pt-BR"),
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                    DateFormatString = "dd/MM/yyyy HH:mm:ss"
                });

                resultCEP.Country = _IGECountriesAS.GetByInitials("BR");
                return resultCEP;
            }
            catch (Exception e)
            {
                Logging.Exception(e);
                Errors.Add(ModelException.CreateNotFoundError(
                    string.Format(Resources.MessagesResource.StatusNoOK, e.Message),
                    "CEP",
                    ""
                ));
            }

            return new CepVM();
        }

        public async Task<ICepVM> GetCEPAddressAsync(string pCEP, string pNumber, string pCountry, bool pAllInfo)
        {
            var CepAddress = await GetCEPAsync(pCEP);

            if (Errors.Any() || !pAllInfo)
                return CepAddress;

            CepAddress = await GetMapInfoAsync(CepAddress, pCountry, pNumber);

            return CepAddress;
        }

        async Task<ICepVM> GetMapInfoAsync(ICepVM pCep, string pCountry, string pNumber)
        {
            var GeoAddress = await _IMapAS.GetAddressAsync(new CepAddress
            {
                City = pCep.localidade,
                Country = pCountry,
                District = pCep.bairro,
                Number = pNumber == "0" ? "" : pNumber,
                State = pCep.uf,
                Street = pCep.logradouro,
                ZipCode = pCep.cep,
                Complement = pCep.complemento,
                StateName = pCep.State?.Name
            });

            if (GeoAddress != null)
                pCep.MapResult = GeoAddress;

            Errors = _IMapAS.Errors;

            return pCep;
        }

        public async Task<ICepVM> GetCEPAddressEnterpriseAsync(IReceitaWSEnterpriseVM pEnterprise)
        {
            var CepAddress = await GetCEPAsync(pEnterprise.cep);

            if (Errors.Any())
                return CepAddress;

            if (CepAddress.logradouro.IsEmpty())
                CepAddress.logradouro = pEnterprise.logradouro;

            if (CepAddress.bairro.IsEmpty())
                CepAddress.bairro = pEnterprise.bairro;

            if (CepAddress.localidade.IsEmpty())
                CepAddress.localidade = pEnterprise.municipio;

            if (CepAddress.uf.IsEmpty())
                CepAddress.uf = pEnterprise.uf;

            if (CepAddress.complemento.IsEmpty())
                CepAddress.complemento = pEnterprise.complemento;

            CepAddress = await GetMapInfoAsync(CepAddress, "BR", pEnterprise.numero);

            return CepAddress;
        }
    }
}
