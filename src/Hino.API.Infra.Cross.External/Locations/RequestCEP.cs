﻿using Hino.API.Infra.Cross.Utils.RequestService;
using RestSharp;
using System;

namespace Hino.API.Infra.Cross.External.Locations
{
    public class RequestCEP : RequestAS
    {
        const string BaseRoute = "http://viacep.com.br/ws/";

        public override RestClient CreateClient() =>
            new(BaseRoute);

        public override RestClient CreateClientFind() =>
            throw new NotImplementedException();

        public override RestRequest CreateRequest(string pUri, Method pMethod) =>
            new(pUri, pMethod);
    }
}
