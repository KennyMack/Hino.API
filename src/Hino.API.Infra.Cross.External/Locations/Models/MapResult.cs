﻿using Hino.API.Application.Integrations.Locations.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Infra.Cross.External.Locations.Models
{
    public class MapResult : IMapResult
    {
        public ICepAddress Address { get; set; }
        public IMapPosition Position { get; set; }
        public IMapView MapView { get; set; }
        public string ReferenceId { get; set; }
    }

    public class MapPosition: IMapPosition
    {
        public float Lat { get; set; }
        public float Lng { get; set; }
    }

    public class MapView: IMapView
    {
        public float West { get; set; }
        public float South { get; set; }
        public float East { get; set; }
        public float North { get; set; }
    }
}
