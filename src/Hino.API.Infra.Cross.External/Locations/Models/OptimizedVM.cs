﻿using Hino.API.Application.Integrations.Locations.Models;
using Hino.API.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Infra.Cross.External.Locations.Models
{
    public class OptimizedVM : IOptimizedVM
    {
        [RequiredField]
        [DisplayField]
        public string EstablishmentKey { get; set; }
        
        [RequiredField]
        public IList<ILocationVM> Location { get; set; }

        public OptimizedVM()
        {
            Location = new List<ILocationVM>();
        }

        public override string ToString() =>
            string.Join(";", Location.Select(r => $"{r.Lng.ToString().Replace(",", ".")},{r.Lat.ToString().Replace(",", ".")}").ToArray());

        public string GetRadiuses()
        {
            return string.Join(";", Location.Select(r => $"30").ToArray());
        }

        public List<ILocationVM> GetDestinations()
        {
            var names = Location.Where(r => r.Name != Location.First().Name).Select(r => r.Name).Distinct();

            var destinations = new List<ILocationVM>();
            foreach (var name in names)
                destinations.Add(Location.First(r => r.Name == name));

            return destinations;
        }
    }

    public class LocationVM : ILocationVM
    {
        [RequiredField]
        public string Address { get; set; }
        [RequiredField]
        public string Name { get; set; }
        [RequiredField]
        public decimal Lng { get; set; }
        [RequiredField]
        public decimal Lat { get; set; }

        public string ToDestination() =>
            $"{Name};{Lat.ToString().Replace(",", ".")},{Lng.ToString().Replace(",", ".")}";

        public string ToStart() =>
            $"Origem;{Lat.ToString().Replace(",", ".")},{Lng.ToString().Replace(",", ".")}";

        public string ToEnd() =>
            $"Destino;{Lat.ToString().Replace(",", ".")},{Lng.ToString().Replace(",", ".")}";
    }
}
