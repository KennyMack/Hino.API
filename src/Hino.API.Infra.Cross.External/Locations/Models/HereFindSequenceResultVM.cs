﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Infra.Cross.External.Locations.Models
{
    public class HereFindSequenceResultVM
    {
        public HereFindSequenceResultVM()
        {
            results = new List<HereFindSequenceItemVM>();
            errors = new List<string>();
        }
        public long Id { get; set; }
        public string EstablishmentKey { get; set; }
        public string UniqueKey { get; set; }

        public List<HereFindSequenceItemVM> results { get; set; }
        public List<string> errors { get; set; }
        public string processingTimeDesc { get; set; }
        public string responseCode { get; set; }
        public string warnings { get; set; }
        public string requestId { get; set; }
    }

    public class HereFindSequenceItemVM
    {
        public HereFindSequenceItemVM()
        {
            waypoints = new List<HereWaypointVM>();
            interconnections = new List<HereInterconnectionsVM>();
        }

        public decimal distance { get; set; }
        public decimal time { get; set; }
        public string description { get; set; }

        public List<HereWaypointVM> waypoints { get; set; }
        public List<HereInterconnectionsVM> interconnections { get; set; }
        public HereTimeBreakdown timeBreakdown { get; set; }
    }

    public class HereWaypointVM
    {
        public string id { get; set; }
        public decimal lat { get; set; }
        public decimal lng { get; set; }
        public int sequence { get; set; }
        public string estimatedArrival { get; set; }
        public string estimatedDeparture { get; set; }
        public string[] fulfilledConstraints { get; set; }

    }

    public class HereInterconnectionsVM
    {
        public string fromWaypoint { get; set; }
        public string toWaypoint { get; set; }
        public decimal distance { get; set; }
        public decimal time { get; set; }
        public decimal rest { get; set; }
        public decimal waiting { get; set; }
    }

    public class HereTimeBreakdown
    {
        public decimal driving { get; set; }
        public decimal service { get; set; }
        public decimal rest { get; set; }
        public decimal waiting { get; set; }
    }
}
