﻿using Hino.API.Application.Integrations.Locations.Models;
using Hino.API.Application.ViewModel.Demograph;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Infra.Cross.External.Locations.Models
{
    public class CepVM : ICepVM
    {
        public CepVM()
        {
            State = new GEStatesVM();
            City = new GECitiesVM();
            Country = new GECountriesVM();
        }

        public string cep { get; set; }
        public string logradouro { get; set; }
        public string complemento { get; set; }
        public string bairro { get; set; }
        public string localidade { get; set; }
        public string uf { get; set; }
        public string unidade { get; set; }
        public string ibge { get; set; }
        public string gia { get; set; }
        public ICepAddress CEPAddress { get; set; }
        public GEStatesVM State { get; set; }
        public GECitiesVM City { get; set; }
        public GECountriesVM Country { get; set; }
        public IMapResult MapResult { get; set; }
    }
}
