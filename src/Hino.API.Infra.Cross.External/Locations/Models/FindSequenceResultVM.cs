﻿using Hino.API.Application.Integrations.Locations.Models;
using System.Collections.Generic;

namespace Hino.API.Infra.Cross.External.Locations.Models
{
    public class FindSequenceResultVM : IFindSequenceResultVM
    {
        public decimal Weigth { get; set; }
        public decimal Duration { get; set; }
        public decimal Distance { get; set; }
        public string Note { get; set; }
        public IList<IFindSequenceLocationVM> Waypoints { get; set; }
    }

    public class FindSequenceLocationVM : IFindSequenceLocationVM
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public decimal Lat { get; set; }
        public decimal Lng { get; set; }
        public int Sequence { get; set; }
        public string AddressFrom { get; set; }
        public string AddressTo { get; set; }
        public decimal Distance { get; set; }
        public decimal Time { get; set; }
        public decimal Rest { get; set; }
        public decimal Waiting { get; set; }
    }
}
