﻿using Hino.API.Application.Integrations.Locations.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Infra.Cross.External.Locations.Models
{
    public class CepAddress : ICepAddress
    {
        public string Label { get; set; }
        public string Country { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public string State { get; set; }
        public string StateName { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public string District { get; set; }
        public string Street { get; set; }
        public string Number { get; set; }
        public string Complement { get; set; }

        public override string ToString()
        {
            if (Country == "US")
                return $"{(Number ?? "").Replace(" ", "%20")}%20{(Street ?? "").Replace(" ", "%20")}%20{(City ?? "")}%20{(State ?? "")}%20{(ZipCode ?? "")}";
            return $"{(Street ?? "").Replace(" ", "%20")},%20{(Number ?? "").Replace(" ", "%20")}%20-%20{(District ?? "").Replace(" ", "%20")},%20{(City ?? "")}%20-%20{(State ?? "")},%20{(ZipCode ?? "")}";
        }

        public string ToAddress()
        {
            var number = Number != "0" &&
                !string.IsNullOrEmpty(Number) ? Number : "";

            // 84+rua+tulio+taques+de+lemos+parque+das+nações+Limeira
            return $"{(Street ?? "").Replace(" ", "+")},+{(number ?? "").Replace(" ", "+")}+-+{(District ?? "").Replace(" ", "+")},+{(City ?? "")}+-+{(State ?? "")},+{(ZipCode ?? "")}";
        }
    }
}
