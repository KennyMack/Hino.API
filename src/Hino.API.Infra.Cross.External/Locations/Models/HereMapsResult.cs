﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Infra.Cross.External.Locations.Models
{
    public class HereMapsResult
    {
        public List<HereMapsItem> items { get; set; }
        
        public HereMapsResult()
        {
            items = new();
        }

        public HereMapsItem GetBestResult() =>
            items
                .OrderByDescending(r => r.scoring.fieldScore)
                .First();
    }

    public class HereMapsItem
    {
        public string title { get; set; }
        public string id { get; set; }
        public string resultType { get; set; }
        public string houseNumberType { get; set; }
        public HereMapsAddress address { get; set; }
        public HereMapsPositionItem position { get; set; }
        public HereMapsPositionItem[] access { get; set; }
        public HereMapsMapView mapView { get; set; }
        public HereMapsScoring scoring { get; set; }
    }

    public class HereMapsAddress
    {
        public string label { get; set; }
        public string countryCode { get; set; }
        public string countryName { get; set; }
        public string stateCode { get; set; }
        public string state { get; set; }
        public string city { get; set; }
        public string district { get; set; }
        public string street { get; set; }
        public string postalCode { get; set; }
        public string houseNumber { get; set; }
        

    }

    public class HereMapsPositionItem
    {
        public float lat { get; set; }
        public float lng { get; set; }
    }

    public class HereMapsMapView
    {
        public float West { get; set; }
        public float South { get; set; }
        public float East { get; set; }
        public float North { get; set; }
    }

    public class HereMapsScoring
    {
        public HereMapsScoringItem fieldScore { get; set; }
        public float queryScore { get; set; }
    }

    public class HereMapsScoringItem
    {
        public float state { get; set; }
        public float city { get; set; }
        public float district { get; set; }
        public float[] streets { get; set; }
        public float houseNumber { get; set; }
        public float postalCode { get; set; }
    }

}
