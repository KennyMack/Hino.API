﻿using Hino.API.Infra.Cross.Utils.RequestService;
using RestSharp;

namespace Hino.API.Infra.Cross.External.Locations
{
    public class RequestHere : RequestAS
    {
        const string BaseRoute = "https://geocode.search.hereapi.com/v1/";
        const string BaseRouteFindSequence = "https://wse.ls.hereapi.com/2/";

        public override RestClient CreateClient() =>
            new(BaseRoute);

        public override RestClient CreateClientFind() =>
            new(BaseRouteFindSequence);

        public override RestRequest CreateRequest(string pUri, Method pMethod) =>
            new(pUri, pMethod);
    }
}
