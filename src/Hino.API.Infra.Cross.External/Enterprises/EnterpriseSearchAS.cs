﻿using Hino.API.Application.Integrations.Enterprises;
using Hino.API.Application.Integrations.Enterprises.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Infra.Cross.External.Enterprises
{
    public abstract class EnterpriseSearchAS : IEnterpriseSearchAS
    {
        public List<ModelException> Errors { get; set; }
        public abstract Task<IReceitaWSEnterpriseVM> GetEnterpriseAsync(string pCNPJ, string pTokenCNPJ);

        public EnterpriseSearchAS()
        {
            Errors = new List<ModelException>();
        }
    }
}
