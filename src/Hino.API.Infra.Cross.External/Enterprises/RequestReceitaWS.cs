﻿using Hino.API.Infra.Cross.Utils.RequestService;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Infra.Cross.External.Enterprises
{
    public class RequestReceitaWS : RequestAS
    {
        const string BaseRoute = "https://www.receitaws.com.br/v1/cnpj/";

        public override RestClient CreateClient() =>
            new(BaseRoute);

        public override RestClient CreateClientFind() =>
            throw new NotImplementedException();

        public override RestRequest CreateRequest(string pUri, Method pMethod) =>
            new(pUri, pMethod);
    }
}
