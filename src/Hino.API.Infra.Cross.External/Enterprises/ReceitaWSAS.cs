﻿using Hino.API.Application.Integrations.Enterprises.Models;
using Hino.API.Application.Integrations.Locations;
using Hino.API.Infra.Cross.External.Enterprises.Models;
using Hino.API.Infra.Cross.Utils;
using Hino.API.Infra.Cross.Utils.Enums;
using Hino.API.Infra.Cross.Utils.Exceptions;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Infra.Cross.External.Enterprises
{
    public class ReceitaWSAS : EnterpriseSearchAS
    {
        private readonly ICepAS _ICepAS;
        private readonly RequestReceitaWS _RequestReceitaWS;
        public ReceitaWSAS(ICepAS pICEPAS)
        {
            Errors = new List<ModelException>();
            _ICepAS = pICEPAS;
            _RequestReceitaWS = new();
        }

        public override async Task<IReceitaWSEnterpriseVM> GetEnterpriseAsync(string pCNPJ, string pTokenCNPJ)
        {
            if (!pCNPJ.IsCNPJ() || pCNPJ.IsEmpty())
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "CNPJ",
                    Messages = new[] { Resources.ValidationMessagesResource.InvalidCNPJ },
                    Value = ""
                });
                return null;
            }

            try
            {
                var client = _RequestReceitaWS.CreateClient();
                var request = _RequestReceitaWS.CreateRequest($"{pCNPJ.OnlyNumbers()}", Method.Get);

                request.AddHeader("Authorization", $"Bearer {pTokenCNPJ}");

                var Iresponse = await client.ExecuteAsync(request);

                var resultEnterprise = JsonConvert.DeserializeObject<ReceitaWSEnterpriseVM>(Iresponse.Content, new JsonSerializerSettings
                {
                    Culture = new System.Globalization.CultureInfo("pt-BR"),
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                    DateFormatString = "dd/MM/yyyy HH:mm:ss"
                });
                resultEnterprise.Address = await _ICepAS.GetCEPAddressEnterpriseAsync(resultEnterprise);

                return resultEnterprise;
            }
            catch (Exception e)
            {
                Logging.Exception(e);
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.RegisterNotFound,
                    Field = "CNPJ",
                    Messages = new[] { string.Format(Resources.MessagesResource.StatusNoOK, e.Message) },
                    Value = ""
                });
                return null;
            }
        }
    }
}
