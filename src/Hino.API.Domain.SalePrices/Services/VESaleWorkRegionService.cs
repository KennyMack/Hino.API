using Hino.API.Domain.Base.Services;
using Hino.API.Domain.SalePrices.Models;
using Hino.API.Domain.SalePrices.Repositories;

namespace Hino.API.Domain.SalePrices.Services
{
    public class VESaleWorkRegionService : BaseService<VESaleWorkRegion>, IVESaleWorkRegionService
    {
        private readonly IVESaleWorkRegionRepository _IVESaleWorkRegionRepository;

        public VESaleWorkRegionService(IVESaleWorkRegionRepository pIVESaleWorkRegionRepository) :
             base(pIVESaleWorkRegionRepository)
        {
            _IVESaleWorkRegionRepository = pIVESaleWorkRegionRepository;
        }
    }
}
