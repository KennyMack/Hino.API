﻿using Hino.API.Domain.Base.Services;
using Hino.API.Domain.SalePrices.Models;
using Hino.API.Infra.Cross.Utils.Paging;
using Hino.API.Domain.SalePrices.Repositories;
using System.Linq;
using System.Threading.Tasks;
using System;
using Hino.API.Infra.Cross.Utils.Exceptions;
using DefaultMessages = Hino.API.Infra.Cross.Resources.MessagesResource;
using ValidationMessages = Hino.API.Infra.Cross.Resources.ValidationMessagesResource;

namespace Hino.API.Domain.SalePrices.Services
{
    public class VESalePriceService : BaseService<VESalePrice>, IVESalePriceService
    {
        private readonly IVESalePriceRepository _veSalePriceRepository;

        public VESalePriceService(IVESalePriceRepository repo) : base(repo)
        {
            _veSalePriceRepository = repo;
        }

        public Task<PagedResult<VESalePrice>> GetAllPagedSearchProductWithStock(int page, int pageSize, string pEstablishmentKey, long pRegionId, string filter) =>
            _veSalePriceRepository.GetAllPagedSearchProductWithStock(page, pageSize, pEstablishmentKey, pRegionId, filter);

        public Task<PagedResult<VESalePrice>> GetAllPagedSearchProduct(int page, int pageSize, string pEstablishmentKey, long pRegionId, string filter) =>
            _veSalePriceRepository.GetAllPagedSearchProduct(page, pageSize, pEstablishmentKey, pRegionId, filter);

        public async Task<VESalePrice> CreateSalePrice(VESalePrice pSalePrice)
        {
            try
            {
                if (await _veSalePriceRepository.ExistsProductRegionAsync(
                pSalePrice.EstablishmentKey,
                pSalePrice.RegionId,
                pSalePrice.ProductId))
                {
                    Errors.Add(ModelException.CreateValidationError(ValidationMessages.ProductExistsRegion, "ProductId", pSalePrice.ProductId.ToString()));
                    return null;
                }

                if (pSalePrice.CodPrVenda <= 0)
                {
                    var CodPrVenda = (await _veSalePriceRepository.OthersSalePriceAsync(
                        pSalePrice.EstablishmentKey,
                        pSalePrice.RegionId)).Max(r => r.CodPrVenda);
                    if (CodPrVenda > 0)
                        pSalePrice.CodPrVenda = CodPrVenda;
                }

                Add(pSalePrice);

                return pSalePrice;
            }
            catch (Exception ex)
            {
                Errors.Add(ModelException.CreateValidationError(ex.Message, "VESalePrice", pSalePrice.ProductId.ToString()));
                return null;
            }
        }

        public async Task<VESalePrice> UpdateSalePrice(VESalePrice pSalePrice)
        {
            try
            {
                if (pSalePrice.CodPrVenda <= 0)
                {
                    var CodPrVenda = (await _veSalePriceRepository.OthersSalePriceAsync(
                        pSalePrice.EstablishmentKey,
                        pSalePrice.RegionId)).Max(r => r.CodPrVenda);
                    if (CodPrVenda > 0)
                        pSalePrice.CodPrVenda = CodPrVenda;
                }

                _veSalePriceRepository.Update(pSalePrice);

                return pSalePrice;
            }
            catch (Exception ex)
            {
                Errors.Add(ModelException.CreateValidationError(ex.Message, "VESalePrice", pSalePrice.ProductId.ToString()));
                return null;
            }
        }
    }
}
