using Hino.API.Domain.Base.Services;
using Hino.API.Domain.SalePrices.Models;
using Hino.API.Domain.SalePrices.Repositories;
using Hino.API.Infra.Cross.Utils.Enums;
using Hino.API.Infra.Cross.Utils.Exceptions;
using System;
using System.Threading.Tasks;
using DefaultMessages = Hino.API.Infra.Cross.Resources.MessagesResource;
using ValidationMessages = Hino.API.Infra.Cross.Resources.ValidationMessagesResource;

namespace Hino.API.Domain.SalePrices.Services
{
    public class VERegionSaleUFService : BaseService<VERegionSaleUF>, IVERegionSaleUFService
    {
        private readonly IVERegionSaleUFRepository _IVERegionSaleUFRepository;

        public VERegionSaleUFService(IVERegionSaleUFRepository pIVERegionSaleUFRepository) :
             base(pIVERegionSaleUFRepository)
        {
            _IVERegionSaleUFRepository = pIVERegionSaleUFRepository;
        }

        public async Task<VERegionSaleUF> GetEnterpriseSaleRegionAsync(string pEstablishmentKey, string pUF) =>
            await _IVERegionSaleUFRepository.GetEnterpriseSaleRegionAsync(pEstablishmentKey, pUF);

        public async Task<bool> ExistsUFRegionAsync(string pEstablishmentKey, string pUF) =>
            await _IVERegionSaleUFRepository.ExistsUFRegionAsync(pEstablishmentKey, pUF);


        public async Task UpdateEnterpriseRegionIdAsync(string pEstablishmentKey, string pUF, long pRegionId) =>
            await _IVERegionSaleUFRepository.UpdateEnterpriseRegionIdAsync(pEstablishmentKey, pUF, pRegionId);

        public async Task<VERegionSaleUF> CreateSalePrice(VERegionSaleUF pRegionSale)
        {
            try
            {
                if (await ExistsUFRegionAsync(
                    pRegionSale.EstablishmentKey,
                    pRegionSale.UF))
                {
                    Errors.Add(ModelException.CreateValidationError(ValidationMessages.UFRegionExists, "UF", pRegionSale.UF));
                    return null;
                }

                Add(pRegionSale);

                await UpdateEnterpriseRegionIdAsync(pRegionSale.EstablishmentKey, pRegionSale.UF, pRegionSale.RegionId);

                return pRegionSale;
            }
            catch (Exception e)
            {
                Logging.Exception(e);
                Errors.Add(ModelException.CreateSqlError(e, EExceptionErrorCodes.InsertSQLError));
                return null;
            }
        }

        public async Task<VERegionSaleUF> UpdateSalePrice(VERegionSaleUF pRegionSale)
        {
            try
            {

                Update(pRegionSale);

                await UpdateEnterpriseRegionIdAsync(pRegionSale.EstablishmentKey, pRegionSale.UF, pRegionSale.RegionId);

                return pRegionSale;
            }
            catch (Exception e)
            {
                Logging.Exception(e);
                Errors.Add(ModelException.CreateSqlError(e, EExceptionErrorCodes.InsertSQLError));
                return null;
            }
        }
    }
}
