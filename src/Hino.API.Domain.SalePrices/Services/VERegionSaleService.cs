﻿using Hino.API.Domain.Base.Services;
using Hino.API.Domain.SalePrices.Models;
using Hino.API.Domain.SalePrices.Repositories;

namespace Hino.API.Domain.SalePrices.Services
{
    public class VERegionSaleService : BaseService<VERegionSale>, IVERegionSaleService
    {
        public VERegionSaleService(IVERegionSaleRepository repo) : base(repo)
        {
        }
    }
}
