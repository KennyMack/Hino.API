using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.SalePrices.Models;

namespace Hino.API.Domain.SalePrices.Services
{
    public interface IVESaleWorkRegionService : IBaseService<VESaleWorkRegion>
    {
    }
}
