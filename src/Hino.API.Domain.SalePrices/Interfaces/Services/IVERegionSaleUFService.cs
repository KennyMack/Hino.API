using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.SalePrices.Models;
using System.Threading.Tasks;

namespace Hino.API.Domain.SalePrices.Services
{
    public interface IVERegionSaleUFService : IBaseService<VERegionSaleUF>
    {
        Task<VERegionSaleUF> GetEnterpriseSaleRegionAsync(string pEstablishmentKey, string pUF);
        Task<bool> ExistsUFRegionAsync(string pEstablishmentKey, string pUF);
        Task UpdateEnterpriseRegionIdAsync(string pEstablishmentKey, string pUF, long pRegionId);
    }
}
