﻿using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.SalePrices.Models;
using Hino.API.Infra.Cross.Utils.Paging;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.API.Domain.SalePrices.Services
{
    public interface IVESalePriceService : IBaseService<VESalePrice>
    {
        Task<VESalePrice> CreateSalePrice(VESalePrice pSalePrice);
        Task<VESalePrice> UpdateSalePrice(VESalePrice pSalePrice);
        Task<PagedResult<VESalePrice>> GetAllPagedSearchProductWithStock(int page, int pageSize, string pEstablishmentKey, long pRegionId, string filter);
        Task<PagedResult<VESalePrice>> GetAllPagedSearchProduct(int page, int pageSize, string pEstablishmentKey, long pRegionId, string filter);
    }
}
