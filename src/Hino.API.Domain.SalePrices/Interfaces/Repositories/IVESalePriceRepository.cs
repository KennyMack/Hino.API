﻿using Hino.API.Domain.Base.Interfaces.Repositories;
using Hino.API.Domain.SalePrices.Models;
using Hino.API.Infra.Cross.Utils.Paging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.API.Domain.SalePrices.Repositories
{
    public interface IVESalePriceRepository : IBaseRepository<VESalePrice>
    {
        Task<PagedResult<VESalePrice>> GetAllPagedSearchProductWithStock(int pageNumber, int pageSize, string pEstablishmentKey, long pRegionId, string filter);
        Task<PagedResult<VESalePrice>> GetAllPagedSearchProduct(int pageNumber, int pageSize, string pEstablishmentKey, long pRegionId, string filter);
        Task<bool> ExistsProductRegionAsync(string pEstablishmentKey, long pRegionId, long pProductId);
        Task<IEnumerable<VESalePrice>> OthersSalePriceAsync(string pEstablishmentKey, long pRegionId);
    }
}
