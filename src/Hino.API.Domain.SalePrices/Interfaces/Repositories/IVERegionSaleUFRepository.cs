using Hino.API.Domain.Base.Interfaces.Repositories;
using Hino.API.Domain.SalePrices.Models;
using System.Threading.Tasks;

namespace Hino.API.Domain.SalePrices.Repositories
{
    public interface IVERegionSaleUFRepository : IBaseRepository<VERegionSaleUF>
    {
        Task<VERegionSaleUF> GetEnterpriseSaleRegionAsync(string pEstablishmentKey, string pUF);
        Task<bool> ExistsUFRegionAsync(string pEstablishmentKey, string pUF);
        Task UpdateEnterpriseRegionIdAsync(string pEstablishmentKey, string pUF, long pRegionId);
    }
}
