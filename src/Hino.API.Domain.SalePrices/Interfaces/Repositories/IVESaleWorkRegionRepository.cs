using Hino.API.Domain.Base.Interfaces.Repositories;
using Hino.API.Domain.SalePrices.Models;

namespace Hino.API.Domain.SalePrices.Repositories
{
    public interface IVESaleWorkRegionRepository : IBaseRepository<VESaleWorkRegion>
    {
    }
}
