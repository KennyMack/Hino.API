﻿using Hino.API.Domain.Base.Models;
using Hino.API.Infra.Cross.Utils;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.API.Domain.SalePrices.Models
{
    public class VESaleWorkRegion : BaseEntity
    {
        public string Description { get; set; }
        public string ZIPCodeStart { get; set; }
        [NotMapped]
        public string ZIPCodeStartCleaned { get => ZIPCodeStart.OnlyNumbers(); }
        public string ZIPCodeEnd { get; set; }
        [NotMapped]
        public string ZIPCodeEndCleaned { get => ZIPCodeEnd.OnlyNumbers(); }
    }
}
