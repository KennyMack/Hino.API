﻿using Hino.API.Domain.Base.Models;
using Hino.API.Infra.Cross.Utils.Attributes;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.API.Domain.SalePrices.Models
{
    [Queue("VESalePrice")]
    public class VESalePrice : BaseEntity
    {
        public VESalePrice()
        {

        }

        public long CodPrVenda { get; set; }
        [ForeignKey("VERegionSale")]
        public long RegionId { get; set; }
        public virtual VERegionSale VERegionSale { get; set; }

        public string Description { get; set; }

        [ForeignKey("GEProducts")]
        public long ProductId { get; set; }

        public virtual VEProducts GEProducts { get; set; }

        public decimal Value { get; set; }

        [NotMapped]
        public string ProductKey { get; set; }
    }
}
