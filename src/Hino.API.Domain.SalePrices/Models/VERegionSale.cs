﻿using Hino.API.Domain.Base.Models;
using System.Collections.Generic;

namespace Hino.API.Domain.SalePrices.Models
{
    public class VERegionSale : BaseEntity
    {
        public VERegionSale()
        {
            this.VERegionSaleUF = new HashSet<VERegionSaleUF>();
        }

        public string Description { get; set; }
        public long IdERP { get; set; }

        public virtual ICollection<VERegionSaleUF> VERegionSaleUF { get; set; }

    }
}
