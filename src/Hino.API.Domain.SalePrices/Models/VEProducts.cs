﻿using Hino.API.Domain.Base.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Domain.SalePrices.Models
{
    [Table("GEPRODUCTS")]
    public class VEProducts : BaseEntity
    {
        public string ProductKey { get; set; }
        public string Name { get; set; }
        public decimal PercIPI { get; set; }
        public int Status { get; set; }
        public string NCM { get; set; }
        public decimal StockBalance { get; set; }
    }
}
