﻿using Hino.API.Domain.Base.Models;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.API.Domain.SalePrices.Models
{
    public class VERegionSaleUF : BaseEntity
    {
        public VERegionSaleUF()
        {

        }
        [ForeignKey("VERegionSale")]
        public long RegionId { get; set; }
        public virtual VERegionSale VERegionSale { get; set; }

        public string UF { get; set; }
    }
}
