﻿using Hino.API.Domain.Taxes.Interfaces.Repositories;
using Hino.API.Domain.Taxes.Models;
using Hino.API.Infra.Data.Repositories.Base.Repositories;
using Hino.API.Infra.Data.Repositories.Taxes.Context;

namespace Hino.API.Infra.Data.Repositories.Taxes.Repositories
{
    public class FSNCMRepository : BaseRepository<FSNCM>, IFSNCMRepository
    {
        public FSNCMRepository(TaxesDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
