using Hino.API.Domain.Taxes.Interfaces.Repositories;
using Hino.API.Domain.Taxes.Models;
using Hino.API.Infra.Data.Repositories.Base.Repositories;
using Hino.API.Infra.Data.Repositories.Taxes.Context;

namespace Hino.API.Infra.Data.Repositories.Taxes.Repositories
{
    public class FSFiscalOperRepository : BaseRepository<FSFiscalOper>, IFSFiscalOperRepository
    {
        public FSFiscalOperRepository(TaxesDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
