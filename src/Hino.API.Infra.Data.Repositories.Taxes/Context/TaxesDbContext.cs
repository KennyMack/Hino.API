﻿using Hino.API.Domain.Taxes.Models;
using Hino.API.Infra.Data.Repositories.Base.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Infra.Data.Repositories.Taxes.Context
{
    public class TaxesDbContext : BaseDbContext
    {
        public virtual DbSet<FSNCM> FSNCM { get; set; }
        public virtual DbSet<FSFiscalOper> FSFiscalOper { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            // modelBuilder.ApplyConfiguration(new GEUsersConfiguration("GEUSERS"));
        }
    }
}
