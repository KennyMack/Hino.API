﻿using System.ComponentModel.DataAnnotations;

namespace Hino.API.Domain.Events.Enums
{
    public enum EContactReceptivity
    {
        [Display(Description = "Não Informado")]
        NaoInformado = -1,

        [Display(Description = "Muito Baixa")]
        MuitoBaixa = 0,

        [Display(Description = "Baixa")]
        Baixa = 1,

        [Display(Description = "Média")]
        Media = 2,

        [Display(Description = "Alta")]
        Alta = 3,

        [Display(Description = "Muito Alta")]
        MuitoAlta = 4
    }
}
