﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Domain.Events.Enums
{
    public enum EStatusEnterprise
    {
        [Display(Description = "Novo")]
        New = 0,
        [Display(Description = "Ativo")]
        Active = 1,
        [Display(Description = "Inativo")]
        Inactive = 2,
        [Display(Description = "Bloqueado")]
        Blocked = 3
    }
}
