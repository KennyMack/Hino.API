﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Domain.Events.Enums
{
    public enum EEventType
    {
        [Description("Evento")]
        Event = 0,
        [Description("Chamada")]
        Call = 1,
        [Description("Tarefa")]
        Task = 2,
        [Description("Visita")]
        Visit = 3,
        [Description("E-mail")]
        Email = 4
    }
}
