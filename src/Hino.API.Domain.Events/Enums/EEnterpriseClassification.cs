﻿using System.ComponentModel.DataAnnotations;

namespace Hino.API.Domain.Events.Enums
{
    public enum EEnterpriseClassification
    {
        [Display(Description = "Cliente")]
        Client = 0,
        [Display(Description = "Prospect")]
        Prospect = 1,
        [Display(Description = "Transportadora")]
        Carrier = 2
    }
}
