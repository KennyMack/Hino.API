using Hino.API.Domain.Events.Models;
using Hino.API.Domain.Base.Interfaces.Repositories;

namespace Hino.API.Domain.Events.Interfaces.Repositories
{
    public interface IGEUserCalendarRepository : IBaseRepository<GEUserCalendar>
    {
    }
}
