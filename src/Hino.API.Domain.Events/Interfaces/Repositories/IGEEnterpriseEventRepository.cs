using Hino.API.Domain.Base.Interfaces.Repositories;
using Hino.API.Domain.Events.Models;

namespace Hino.API.Domain.Events.Interfaces.Repositories
{
    public interface IGEEnterpriseEventRepository : IBaseRepository<GEEnterpriseEvent>
    {
    }
}
