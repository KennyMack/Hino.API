using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Events.Models;

namespace Hino.API.Domain.Events.Interfaces.Services
{
    public interface IGEEnterpriseEventService : IBaseService<GEEnterpriseEvent>
    {
    }
}
