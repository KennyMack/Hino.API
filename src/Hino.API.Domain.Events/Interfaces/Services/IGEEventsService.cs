using Hino.API.Domain.Events.Models;
using Hino.API.Domain.Base.Interfaces.Services;
using System.Threading.Tasks;

namespace Hino.API.Domain.Events.Interfaces.Services
{
    public interface IGEEventsService : IBaseService<GEEvents>
    {
        Task<GEEvents> SaveReferenceAsync(string pEstablishmentKey, GEEvents Event, GECreateEvents pGEEventsVM);
        Task<GEEvents> UpdateEventAsync(string pEstablishmentKey, GECreateEvents pGEEventsVM);
        Task<GEEvents> CreateEventAsync(string pEstablishmentKey, GECreateEvents pGEEventsVM);
        Task<GEEvents> RemoveEventIdAsync(long pId, string pEstablishmentKey, string pUniqueKey);
        Task<GEEvents> ChangeEventStatusAsync(long pId, string pEstablishmentKey, string pUniqueKey, GEEventsStatus pEventStatus);
        Task<GEEvents> GenerateEventChildAsync(long pId, string pEstablishmentKey, string pUniqueKey, GEEventsChild pEventsChild);
    }
}
