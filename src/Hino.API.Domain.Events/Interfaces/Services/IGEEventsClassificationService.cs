using Hino.API.Domain.Events.Models;
using Hino.API.Domain.Base.Interfaces.Services;

namespace Hino.API.Domain.Events.Interfaces.Services
{
    public interface IGEEventsClassificationService : IBaseService<GEEventsClassification>
    {
    }
}
