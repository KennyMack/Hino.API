using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Hino.API.Domain.Base.Models;
using Hino.API.Domain.Events.Enums;
using Hino.API.Infra.Cross.Utils.Exceptions;

namespace Hino.API.Domain.Events.Models
{
    public class GEEvents : BaseEntity
    {
        public GEEvents()
        {
            this.GEUserCalendar = new HashSet<GEUserCalendar>();
            this.GEEstabCalendar = new HashSet<GEEstabCalendar>();
            this.GEEnterpriseEvent = new HashSet<GEEnterpriseEvent>();
        }

        public EEventType Type { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime DtCalendar { get; set; }
        public DateTime? Start { get; set; }
        public DateTime? End { get; set; }
        public string ZipCode { get; set; }
        public string Address { get; set; }
        public string District { get; set; }
        public string Num { get; set; }
        public string Complement { get; set; }
        public decimal DisplayLat { get; set; }
        public decimal DisplayLng { get; set; }
        public decimal NavLat { get; set; }
        public decimal NavLng { get; set; }
        public string CityName { get; set; }
        public string StateName { get; set; }
        public string UF { get; set; }
        public string IBGE { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        [ForeignKey("GEEventsClassification")]
        public long? ClassificationID { get; set; }
        public virtual GEEventsClassification GEEventsClassification { get; set; }
        public bool Priority { get; set; }
        public long? MainEventID { get; set; }
        public long? OriginEventID { get; set; }
        public bool IsComplete { get; set; }
        public bool IsSuccess { get; set; }

        public string Contact { get; set; }
        public EContactSectors? Sector { get; set; }

        public virtual ICollection<GEUserCalendar> GEUserCalendar { get; set; }
        public virtual ICollection<GEEstabCalendar> GEEstabCalendar { get; set; }
        public virtual ICollection<GEEnterpriseEvent> GEEnterpriseEvent { get; set; }

    }
}
