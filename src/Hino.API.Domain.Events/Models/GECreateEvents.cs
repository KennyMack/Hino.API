﻿using Hino.API.Infra.Cross.Utils.Enums;
using Hino.API.Infra.Cross.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Domain.Events.Models
{
    public class GECreateEvents: GEEvents
    {
        public long? EstabID { get; set; }
        public long? EnterpriseID { get; set; }
        public long? UserID { get; set; }

        public ModelException ValidateReferences()
        {
            if (UserID == null &&
                EstabID == null &&
                EnterpriseID == null)
                return ModelException.CreateValidationError(Infra.Cross.Resources.ValidationMessagesResource.EventReferenceNotInformed,
                    "Body", "");

            return null;
        }
    }
}
