﻿using Hino.API.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Domain.Events.Models
{
    public class GEEventsStatus
    {
        [RequiredField]
        [DisplayField]
        public long Id { get; set; }
        [Min36LengthField]
        [Max36LengthField]
        [RequiredField]
        [DisplayField]
        public string EstablishmentKey { get; set; }
        [RequiredField]
        [Min36LengthField]
        [Max36LengthField]
        [DisplayField]
        public string UniqueKey { get; set; }
        [DisplayField]
        [RequiredField]
        public bool IsComplete { get; set; }
        [DisplayField]
        [RequiredField]
        public bool IsSuccess { get; set; }
    }
}
