using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Hino.API.Domain.Base.Models;

namespace Hino.API.Domain.Events.Models
{
    public class GEUserCalendar : BaseEntity
    {
        [ForeignKey("GEUsers")]
        public long UserID { get; set; }
        public virtual GEUsers GEUsers { get; set; }

        [ForeignKey("GEEvents")]
        public long EventID { get; set; }
        public virtual GEEvents GEEvents { get; set; }
    }
}
