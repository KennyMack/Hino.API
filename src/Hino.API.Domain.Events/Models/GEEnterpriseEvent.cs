using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Hino.API.Domain.Base.Models;

namespace Hino.API.Domain.Events.Models
{
    public class GEEnterpriseEvent : BaseEntity
    {
        [ForeignKey("GEEvents")]
        public long EventID { get; set; }
        public virtual GEEvents GEEvents { get; set; }

        [ForeignKey("GEEnterprises")]
        public long EnterpriseID { get; set; }
        public virtual GEEnterprises GEEnterprises { get; set; }
    }
}
