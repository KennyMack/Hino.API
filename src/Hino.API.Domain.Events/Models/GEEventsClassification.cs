using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Hino.API.Domain.Base.Models;

namespace Hino.API.Domain.Events.Models
{
    [Table("GEEVENTSCLASSIF")]
    public class GEEventsClassification : BaseEntity
    {
        public GEEventsClassification()
        {
            GEEvents = new HashSet<GEEvents>();
        }

        public string Description { get; set; }

        public virtual ICollection<GEEvents> GEEvents { get; set; }
    }
}
