using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Hino.API.Domain.Base.Models;

namespace Hino.API.Domain.Events.Models
{
    public class GEEstabCalendar : BaseEntity
    {
        [ForeignKey("GEEvents")]
        public long EventID { get; set; }
        public virtual GEEvents GEEvents { get; set; }

        public long EstabID { get; set; }
    }
}
