using Hino.API.Domain.Events.Interfaces.Repositories;
using Hino.API.Domain.Events.Interfaces.Services;
using Hino.API.Domain.Events.Models;
using Hino.API.Domain.Base.Services;

namespace Hino.API.Domain.Events.Services
{
    public class GEEventsClassificationService : BaseService<GEEventsClassification>, IGEEventsClassificationService
    {
        private readonly IGEEventsClassificationRepository _IGEEventsClassificationRepository;

        public GEEventsClassificationService(IGEEventsClassificationRepository pIGEEventsClassificationRepository) : 
             base(pIGEEventsClassificationRepository)
        {
            _IGEEventsClassificationRepository = pIGEEventsClassificationRepository;
        }
    }
}
