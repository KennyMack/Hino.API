using Hino.API.Domain.Events.Interfaces.Repositories;
using Hino.API.Domain.Events.Interfaces.Services;
using Hino.API.Domain.Events.Models;
using Hino.API.Domain.Base.Services;
using DefaultMessages = Hino.API.Infra.Cross.Resources.MessagesResource;
using ValidationMessages = Hino.API.Infra.Cross.Resources.ValidationMessagesResource;
using System.Threading.Tasks;
using Hino.API.Infra.Cross.Utils.Exceptions;
using System;
using Hino.API.Infra.Cross.Utils.Enums;
using System.Linq;
using System.Collections.Generic;

namespace Hino.API.Domain.Events.Services
{
    public class GEEventsService : BaseService<GEEvents>, IGEEventsService
    {
        private readonly IGEEventsRepository _IGEEventsRepository;
        private readonly IGEUserCalendarRepository _IGEUserCalendarRepository;
        private readonly IGEEstabCalendarRepository _IGEEstabCalendarRepository;
        private readonly IGEEnterpriseEventRepository _IGEEnterpriseEventRepository;
        private readonly IGEEventsClassificationRepository _IGEEventsClassificationRepository;

        public GEEventsService(
            IGEEventsRepository pIGEEventsRepository,
            IGEUserCalendarRepository pIGEUserCalendarRepository,
            IGEEstabCalendarRepository pIGEEstabCalendarRepository,
            IGEEnterpriseEventRepository pIGEEnterpriseEventRepository,
            IGEEventsClassificationRepository pIGEEventsClassificationRepository) : 
            base(pIGEEventsRepository)
        {
            _IGEEventsClassificationRepository = pIGEEventsClassificationRepository;
            _IGEUserCalendarRepository = pIGEUserCalendarRepository;
            _IGEEstabCalendarRepository = pIGEEstabCalendarRepository;
            _IGEEnterpriseEventRepository = pIGEEnterpriseEventRepository;
            _IGEEventsRepository = pIGEEventsRepository;
        }

        public async Task<GEEvents> SaveReferenceAsync(string pEstablishmentKey, GEEvents Event, GECreateEvents pGEEventsVM)
        {
            if (pGEEventsVM.UserID != null)
            {
                try
                {
                    _IGEUserCalendarRepository.Add(new GEUserCalendar
                    {
                        UserID = (long)pGEEventsVM.UserID,
                        EventID = Event.Id,
                        EstablishmentKey = pEstablishmentKey
                    });
                    await _IGEUserCalendarRepository.SaveChanges();
                }
                catch (Exception ex)
                {
                    Errors.Add(ModelException.CreateSqlError(ex, "GEUserCalendar", EExceptionErrorCodes.InsertSQLError));
                    return null;
                }
            }

            if (pGEEventsVM.EstabID != null)
            {
                try
                {
                    _IGEEstabCalendarRepository.Add(new GEEstabCalendar
                    {
                        EstabID = (long)pGEEventsVM.EstabID,
                        EventID = Event.Id,
                        EstablishmentKey = pEstablishmentKey
                    });
                    await _IGEEstabCalendarRepository.SaveChanges();
                }
                catch (Exception ex)
                {
                    Errors.Add(ModelException.CreateSqlError(ex, "GEEstabCalendar", EExceptionErrorCodes.InsertSQLError));
                    return null;
                }
            }

            if (pGEEventsVM.EnterpriseID != null)
            {
                try
                {
                    _IGEEnterpriseEventRepository.Add(new GEEnterpriseEvent
                    {
                        EnterpriseID = (long)pGEEventsVM.EnterpriseID,
                        EventID = Event.Id,
                        EstablishmentKey = pEstablishmentKey
                    });
                    await _IGEEnterpriseEventRepository.SaveChanges();
                }
                catch (Exception ex)
                {
                    Errors.Add(ModelException.CreateSqlError(ex, "GEEnterpriseEvent", EExceptionErrorCodes.InsertSQLError));
                    return null;
                }
            }
            return pGEEventsVM;
        }

        private async Task RemoveReferencesAsync(GEEvents Event)
        {
            foreach (var User in Event.GEUserCalendar)
            {
                try
                {

                    await _IGEUserCalendarRepository.RemoveById(User.Id, User.EstablishmentKey, User.UniqueKey);
                    await _IGEUserCalendarRepository.SaveChanges();
                }
                catch (Exception ex)
                {
                    Errors.Add(ModelException.CreateSqlError(ex, "GEUserCalendar", EExceptionErrorCodes.InsertSQLError));
                }
            }

            foreach (var Estab in Event.GEEstabCalendar)
            {
                try
                {
                    await _IGEEstabCalendarRepository.RemoveById(Estab.Id, Estab.EstablishmentKey, Estab.UniqueKey);
                    await _IGEEstabCalendarRepository.SaveChanges();
                }
                catch (Exception ex)
                {
                    Errors.Add(ModelException.CreateSqlError(ex, "GEEstabCalendar", EExceptionErrorCodes.InsertSQLError));
                }
            }

            foreach (var Enterprise in Event.GEEnterpriseEvent)
            {
                try
                {
                    await _IGEEnterpriseEventRepository.RemoveById(Enterprise.Id, Enterprise.EstablishmentKey, Enterprise.UniqueKey);
                    await _IGEEnterpriseEventRepository.SaveChanges();
                }
                catch (Exception ex)
                {
                    Errors.Add(ModelException.CreateSqlError(ex, "GEEnterpriseEvent", EExceptionErrorCodes.InsertSQLError));
                }
            }
        }

        public async Task<GEEvents> UpdateEventAsync(string pEstablishmentKey, GECreateEvents pGEEventsVM)
        {
            pGEEventsVM.EstablishmentKey = pEstablishmentKey;

            if (Errors.Any())
                return null;

            var Event = pGEEventsVM;

            var EventDB = GetByIdToUpdate(Event.Id, Event.EstablishmentKey, Event.UniqueKey);
            EventDB.Id = Event.Id;
            EventDB.Type = Event.Type;
            EventDB.Title = Event.Title;
            EventDB.Description = Event.Description;
            EventDB.DtCalendar = Event.DtCalendar;
            EventDB.Start = Event.Start ?? Event.DtCalendar;
            EventDB.End = Event.End;
            EventDB.ZipCode = Event.ZipCode;
            EventDB.Address = Event.Address;
            EventDB.District = Event.District;
            EventDB.Num = Event.Num;
            EventDB.Complement = Event.Complement;
            EventDB.DisplayLat = Event.DisplayLat;
            EventDB.DisplayLng = Event.DisplayLng;
            EventDB.NavLat = Event.NavLat;
            EventDB.NavLng = Event.NavLng;
            EventDB.CityName = Event.CityName;
            EventDB.StateName = Event.StateName;
            EventDB.UF = Event.UF;
            EventDB.IBGE = Event.IBGE;
            EventDB.Email = Event.Email;
            EventDB.Phone = Event.Phone;
            EventDB.ClassificationID = Event.ClassificationID;
            EventDB.Priority = Event.Priority;
            EventDB.MainEventID = Event.MainEventID;
            EventDB.OriginEventID = Event.OriginEventID;
            EventDB.IsComplete = Event.IsComplete;
            EventDB.IsSuccess = Event.IsSuccess;
            EventDB.EstablishmentKey = Event.EstablishmentKey;
            EventDB.UniqueKey = Event.UniqueKey;
            EventDB.Created = Event.Created;
            EventDB.Modified = Event.Modified;
            EventDB.IsActive = Event.IsActive;
            EventDB.Contact = Event.Contact;
            EventDB.Sector = Event.Sector;

            EventDB.GEEnterpriseEvent = null;
            EventDB.GEEstabCalendar = null;
            EventDB.GEEventsClassification = null;
            EventDB.GEEstabCalendar = null;
            try
            {
                _IGEEventsRepository.Update(EventDB);
                await _IGEEventsRepository.SaveChanges();
            }
            catch (Exception ex)
            {
                Errors.Add(ModelException.CreateSqlError(ex, "GEEvents", EExceptionErrorCodes.InsertSQLError));
            }

            if (Errors.Any())
                return null;

            EventDB = await GetByIdAsync(Event.Id, Event.EstablishmentKey, Event.UniqueKey);

            await RemoveReferencesAsync(EventDB);

            if (Errors.Any())
                return null;

            await SaveReferenceAsync(pEstablishmentKey, Event, pGEEventsVM);

            if (Errors.Any())
                return null;

            return await GetByIdAsync(Event.Id, Event.EstablishmentKey, Event.UniqueKey, r => r.GEEventsClassification);
        }

        public async Task<GEEvents> CreateEventAsync(string pEstablishmentKey, GECreateEvents pGEEventsVM)
        {
            Errors.Clear();
            pGEEventsVM.EstablishmentKey = pEstablishmentKey;

            var Event = pGEEventsVM;

            Event.Start = Event.Start ?? Event.DtCalendar;
            Event.UniqueKey = Guid.NewGuid().ToString();
            Event.Id = await _IGEEventsRepository.NextSequenceAsync();

            if (Event.MainEventID == null)
                Event.MainEventID = Event.Id;
            try
            {
                _IGEEventsRepository.Add(Event);

                await _IGEEventsRepository.SaveChanges();
            }
            catch (Exception ex)
            {
                Errors.Add(ModelException.CreateSqlError(ex, "GEEvents", EExceptionErrorCodes.InsertSQLError));
            }

            if (Errors.Any())
                return null;

            await SaveReferenceAsync(pEstablishmentKey, Event, pGEEventsVM);

            if (Errors.Any())
                return null;

            return await GetByIdAsync(Event.Id, Event.EstablishmentKey, Event.UniqueKey, r => r.GEEventsClassification);
        }

        async Task<IEnumerable<GEEvents>> GetEventDirectLinkedAsync(string pEstablishmentKey, long id) =>
           await _IGEEventsRepository.QueryAsync(r => (r.MainEventID == id || r.OriginEventID == id) &&
               r.EstablishmentKey == pEstablishmentKey);

        public async Task<GEEvents> RemoveEventIdAsync(long pId, string pEstablishmentKey, string pUniqueKey)
        {
            Errors.Clear();

            var Event = await GetByIdAsync(pId, pEstablishmentKey, pUniqueKey);

            if (Event.IsComplete || Event.IsSuccess)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "Id",
                    Messages = new[] { ValidationMessages.CompletedEventCantBeDeleted },
                    Value = pId.ToString()
                });
                return null;
            }

            var EventLinked = await GetEventDirectLinkedAsync(pEstablishmentKey, Event.Id);

            if (EventLinked.Any(r => r.Id != Event.Id))
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "Id",
                    Messages = new[] { ValidationMessages.EventHasChild },
                    Value = pId.ToString()
                });
                return null;
            }

            await RemoveReferencesAsync(Event);

            if (Errors.Any())
                return null;

            try
            {
                await _IGEEventsRepository.RemoveById(pId, pEstablishmentKey, pUniqueKey);

                await _IGEEventsRepository.SaveChanges();
            }
            catch (Exception ex)
            {
                Errors.Add(ModelException.CreateSqlError(ex, "GEEvents", EExceptionErrorCodes.DeleteSQLError));
            }

            return Event;
        }

        public async Task<GEEvents> ChangeEventStatusAsync(long pId, string pEstablishmentKey, string pUniqueKey, GEEventsStatus pEventStatus)
        {
            Errors.Clear();

            var Event = GetByIdToUpdate(pId, pEstablishmentKey, pUniqueKey);

            Event.IsComplete = pEventStatus.IsComplete;
            Event.IsSuccess = pEventStatus.IsSuccess;
            try
            {
                _IGEEventsRepository.Update(Event);

                await _IGEEventsRepository.SaveChanges();
            }
            catch (Exception ex)
            {
                Errors.Add(ModelException.CreateSqlError(ex, "GEEvents", EExceptionErrorCodes.UpdateSQLError));
            }

            if (Errors.Any())
                return null;

            return await GetByIdAsync(Event.Id, Event.EstablishmentKey, Event.UniqueKey, r => r.GEEventsClassification);
        }

        public async Task<GEEvents> GenerateEventChildAsync(long pId, string pEstablishmentKey, string pUniqueKey, GEEventsChild pEventsChild)
        {
            Errors.Clear();

            var Event = GetById(pId, pEstablishmentKey, pUniqueKey);

            long MainID = Event.MainEventID ?? Event.Id;

            var EventsLinked = await GetEventDirectLinkedAsync(pEstablishmentKey, MainID);

            var EventDestiny = new GEEvents();

            EventDestiny.Id = 0;
            EventDestiny.Type = pEventsChild.Event.Type;
            EventDestiny.Title = pEventsChild.Event.Title;
            EventDestiny.Description = pEventsChild.Event.Description;
            EventDestiny.DtCalendar = pEventsChild.Event.DtCalendar;
            EventDestiny.Start = pEventsChild.Event.Start ?? pEventsChild.Event.DtCalendar;
            EventDestiny.End = pEventsChild.Event.End;
            EventDestiny.ZipCode = pEventsChild.Event.ZipCode;
            EventDestiny.Address = pEventsChild.Event.Address;
            EventDestiny.District = pEventsChild.Event.District;
            EventDestiny.Num = pEventsChild.Event.Num;
            EventDestiny.Complement = pEventsChild.Event.Complement;
            EventDestiny.DisplayLat = pEventsChild.Event.DisplayLat;
            EventDestiny.DisplayLng = pEventsChild.Event.DisplayLng;
            EventDestiny.NavLat = pEventsChild.Event.NavLat;
            EventDestiny.NavLng = pEventsChild.Event.NavLng;
            EventDestiny.CityName = pEventsChild.Event.CityName;
            EventDestiny.StateName = pEventsChild.Event.StateName;
            EventDestiny.UF = pEventsChild.Event.UF;
            EventDestiny.IBGE = pEventsChild.Event.IBGE;
            EventDestiny.Email = pEventsChild.Event.Email;
            EventDestiny.Phone = pEventsChild.Event.Phone;
            EventDestiny.ClassificationID = pEventsChild.Event.ClassificationID;
            EventDestiny.Priority = pEventsChild.Event.Priority;

            EventDestiny.Id = 0;
            EventDestiny.IsComplete = false;
            EventDestiny.IsSuccess = false;
            EventDestiny.Created = DateTime.Now;
            EventDestiny.Modified = DateTime.Now;
            EventDestiny.IsActive = true;
            EventDestiny.EstablishmentKey = pEstablishmentKey;
            EventDestiny.UniqueKey = Guid.NewGuid().ToString();
            EventDestiny.OriginEventID = Event.Id;
            EventDestiny.MainEventID = MainID;

            EventDestiny.GEEnterpriseEvent = null;
            EventDestiny.GEEstabCalendar = null;
            EventDestiny.GEEventsClassification = null;
            EventDestiny.GEEstabCalendar = null;

            try
            {
                _IGEEventsRepository.Add(EventDestiny);

                await _IGEEventsRepository.SaveChanges();
            }
            catch (Exception ex)
            {
                Errors.Add(ModelException.CreateSqlError(ex, "GEEvents", EExceptionErrorCodes.InsertSQLError));
            }

            try
            {
                foreach (var User in Event.GEUserCalendar)
                {
                    _IGEUserCalendarRepository.Add(new GEUserCalendar
                    {
                        UserID = User.UserID,
                        EventID = EventDestiny.Id,
                        EstablishmentKey = pEstablishmentKey
                    });

                    await _IGEUserCalendarRepository.SaveChanges();
                }

                foreach (var Estab in Event.GEEstabCalendar)
                {
                    _IGEEstabCalendarRepository.Add(new GEEstabCalendar
                    {
                        EstabID = Estab.EstabID,
                        EventID = EventDestiny.Id,
                        EstablishmentKey = pEstablishmentKey
                    });

                    await _IGEEstabCalendarRepository.SaveChanges();
                }

                foreach (var Enterprise in Event.GEEnterpriseEvent)
                {
                    _IGEEnterpriseEventRepository.Add(new GEEnterpriseEvent
                    {
                        EnterpriseID = Enterprise.EnterpriseID,
                        EventID = EventDestiny.Id,
                        EstablishmentKey = pEstablishmentKey
                    });

                    await _IGEEnterpriseEventRepository.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Errors.Add(ModelException.CreateSqlError(ex, "GEEvents", EExceptionErrorCodes.InsertSQLError));
            }

            var OriginDB = GetByIdToUpdate(pId, pEstablishmentKey, pUniqueKey);
            OriginDB.IsComplete = pEventsChild.IsComplete;
            OriginDB.IsSuccess = pEventsChild.IsSuccess;

            try
            {
                _IGEEventsRepository.Update(OriginDB);

                await _IGEEventsRepository.SaveChanges();
            }
            catch (Exception ex)
            {
                Errors.Add(ModelException.CreateSqlError(ex, "GEEvents", EExceptionErrorCodes.InsertSQLError));
            }

            return await GetByIdAsync(EventDestiny.Id, EventDestiny.EstablishmentKey, EventDestiny.UniqueKey, r => r.GEEventsClassification);
        }
    }
}
