using Hino.API.Domain.Events.Interfaces.Repositories;
using Hino.API.Domain.Events.Interfaces.Services;
using Hino.API.Domain.Events.Models;
using Hino.API.Domain.Base.Services;

namespace Hino.API.Domain.Events.Services
{
    public class GEEnterpriseEventService : BaseService<GEEnterpriseEvent>, IGEEnterpriseEventService
    {
        private readonly IGEEnterpriseEventRepository _IGEEnterpriseEventRepository;

        public GEEnterpriseEventService(IGEEnterpriseEventRepository pIGEEnterpriseEventRepository) : 
             base(pIGEEnterpriseEventRepository)
        {
            _IGEEnterpriseEventRepository = pIGEEnterpriseEventRepository;
        }
    }
}
