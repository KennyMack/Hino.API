using Hino.API.Domain.Events.Interfaces.Repositories;
using Hino.API.Domain.Events.Interfaces.Services;
using Hino.API.Domain.Events.Models;
using Hino.API.Domain.Base.Services;

namespace Hino.API.Domain.Events.Services
{
    public class GEUserCalendarService : BaseService<GEUserCalendar>, IGEUserCalendarService
    {
        private readonly IGEUserCalendarRepository _IGEUserCalendarRepository;

        public GEUserCalendarService(IGEUserCalendarRepository pIGEUserCalendarRepository) : 
             base(pIGEUserCalendarRepository)
        {
            _IGEUserCalendarRepository = pIGEUserCalendarRepository;
        }
    }
}
