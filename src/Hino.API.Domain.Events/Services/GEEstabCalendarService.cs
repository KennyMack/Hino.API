using Hino.API.Domain.Base.Services;
using Hino.API.Domain.Events.Interfaces.Repositories;
using Hino.API.Domain.Events.Interfaces.Services;
using Hino.API.Domain.Events.Models;

namespace Hino.API.Domain.Events.Services
{
    public class GEEstabCalendarService : BaseService<GEEstabCalendar>, IGEEstabCalendarService
    {
        private readonly IGEEstabCalendarRepository _IGEEstabCalendarRepository;

        public GEEstabCalendarService(IGEEstabCalendarRepository pIGEEstabCalendarRepository) : 
             base(pIGEEstabCalendarRepository)
        {
            _IGEEstabCalendarRepository = pIGEEstabCalendarRepository;
        }
    }
}
