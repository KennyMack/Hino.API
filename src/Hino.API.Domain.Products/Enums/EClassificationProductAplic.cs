﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Domain.Products.Enums
{
    /// <summary>
    /// Classificação 
    /// 0 - Comercialização 
    /// 1 - Industrialização 
    /// 2 - Serviços
    /// 3 - Uso e Consumo
    /// 4 - Outros
    /// 5 - Fretes
    /// </summary>
    public enum EClassificationProductAplic
    {
        [Description("Comercialização")]
        Commercialization = 0,
        [Description("Industrialização")]
        Industrialization = 1,
        [Description("Serviços")]
        Services = 2,
        [Description("Uso e consumo")]
        UseAndConsumption = 3,
        [Description("Outros")]
        Others = 4,
        [Description("Fretes")]
        Freight = 5
    }
}
