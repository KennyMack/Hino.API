﻿using Hino.API.Domain.Base.Interfaces.Repositories;
using Hino.API.Domain.Products.Models;

namespace Hino.API.Domain.Products.Interfaces.Repositories
{
    public interface IGEProductAplicRepository : IBaseRepository<GEProductAplic>
    {
    }
}
