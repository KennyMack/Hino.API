using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Products.Models;

namespace Hino.API.Domain.Products.Interfaces.Services
{
    public interface IGEProductsTypeService : IBaseService<GEProductsType>
    {
    }
}
