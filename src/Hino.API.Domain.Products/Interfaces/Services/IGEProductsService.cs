using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Products.Models;
using System.Threading.Tasks;

namespace Hino.API.Domain.Products.Interfaces.Services
{
    public interface IGEProductsService : IBaseService<GEProducts>
    {
        Task<bool> ExistsProductKeyAsync(string pEstablishmentKey, string pProductKey);
        Task<GEProducts> CreateAsync(GEProducts pProduct);
        Task<GEProducts> ChangeAsync(GEProducts pProduct);
    }
}
