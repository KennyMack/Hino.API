﻿using Hino.API.Domain.Base.Models;

namespace Hino.API.Domain.Products.Models
{
    public class GEProductsUnit : BaseEntity
    {
        public string Unit { get; set; }
        public string Description { get; set; }
    }
}
