﻿using Hino.API.Domain.Base.Models;

namespace Hino.API.Domain.Products.Models
{
    public class GEProductsFamily : BaseEntity
    {
        public string Family { get; set; }
        public string Description { get; set; }
        public string GroupDescription { get; set; }
        public string ClassDescription { get; set; }
        public string CatDescription { get; set; }
    }
}
