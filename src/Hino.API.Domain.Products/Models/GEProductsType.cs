﻿using Hino.API.Domain.Base.Models;

namespace Hino.API.Domain.Products.Models
{
    public class GEProductsType : BaseEntity
    {
        public string Description { get; set; }
        public string TypeProd { get; set; }
        public int? CodAnexoSimples { get; set; }
    }
}
