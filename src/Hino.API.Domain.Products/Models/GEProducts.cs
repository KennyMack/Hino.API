﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Hino.API.Domain.Base.Models;
using Hino.API.Infra.Cross.Utils.Attributes;

namespace Hino.API.Domain.Products.Models
{
    [Queue("GEProducts")]
    public class GEProducts : BaseEntity
    {
        public GEProducts()
        {
            // this.GEFilesPath = new HashSet<GEFilesPath>();
        }

        public string ProductKey { get; set; }
        public string Image { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        [ForeignKey("GEProductsType")]
        public long TypeId { get; set; }
        public virtual GEProductsType GEProductsType { get; set; }

        [ForeignKey("GEProductsFamily")]
        public long FamilyId { get; set; }
        public virtual GEProductsFamily GEProductsFamily { get; set; }

        public decimal PercIPI { get; set; }
        public decimal PercMaxDiscount { get; set; }
        public decimal Value { get; set; }
        public decimal StockBalance { get; set; }
        public int Status { get; set; }
        public string NCM { get; set; }

        [ForeignKey("FSNCM")]
        public long? NCMId { get; set; }
        public virtual GENCM FSNCM { get; set; }

        [ForeignKey("GEProductsUnit")]
        public long? UnitId { get; set; }
        public virtual GEProductsUnit GEProductsUnit { get; set; }

        public decimal Weight { get; set; }
        public decimal PackageQTD { get; set; }

        [ForeignKey("GEProductsSaleUnit")]
        public long? SaleUnitId { get; set; }
        public virtual GEProductsUnit GEProductsSaleUnit { get; set; }

        public decimal SaleFactor { get; set; }

        [ForeignKey("GEProductAplic")]
        public long? AplicationId { get; set; }
        public virtual GEProductAplic GEProductAplic { get; set; }

        public int CodOrigMerc { get; set; }

        public string AditionalInfo { get; set; }

        // public virtual ICollection<GEFilesPath> GEFilesPath { get; set; }
    }
}
