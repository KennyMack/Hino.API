using Hino.API.Domain.Base.Models;
using Hino.API.Domain.Products.Enums;
using Hino.API.Infra.Cross.Utils;
using Hino.API.Infra.Cross.Utils.Exceptions;

namespace Hino.API.Domain.Products.Models
{
    public class GEProductAplic : BaseEntity
    {
        public string Description { get; set; }
        public EClassificationProductAplic Classification { get; set; }
        public long IdERP { get; set; }

        public ModelException IsValid()
        {
            if (EstablishmentKey.IsEmpty())
            {
                return ModelException.CreateValidationError(
                    Infra.Cross.Resources.MessagesResource.InvalidEstablishmentKeyOrNull,
                    "EstablishmentKey",
                    EstablishmentKey
                );
            }

            if (Description.IsEmpty())
            {
                return ModelException.CreateValidationError(
                    string.Format(Infra.Cross.Resources.ValidationMessagesResource.RequiredDefault, "Description"),
                    "Description",
                    Description
                );
            }

            if (!((int)Classification).IsBetweenII(0, 5))
            {
                return ModelException.CreateValidationError(
                    Infra.Cross.Resources.ValidationMessagesResource.InvalidClassificationProductAplic,
                    "Classification",
                    ((int)Classification).ToString()
                );
            }
            return null;
        }
    }
}
