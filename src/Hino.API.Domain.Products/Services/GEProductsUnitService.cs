using Hino.API.Domain.Base.Services;
using Hino.API.Domain.Products.Interfaces.Repositories;
using Hino.API.Domain.Products.Interfaces.Services;
using Hino.API.Domain.Products.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using System.Threading.Tasks;
using DefaultMessages = Hino.API.Infra.Cross.Resources.MessagesResource;

namespace Hino.API.Domain.Products.Services
{
    public class GEProductsUnitService : BaseService<GEProductsUnit>, IGEProductsUnitService
    {
        private readonly IGEProductsUnitRepository _IGEProductsUnitRepository;

        public GEProductsUnitService(IGEProductsUnitRepository pIGEProductsUnitRepository) :
             base(pIGEProductsUnitRepository)
        {
            _IGEProductsUnitRepository = pIGEProductsUnitRepository;
        }

        public override void Remove(GEProductsUnit model) =>
            Errors.Add(ModelException.CreateValidationError(DefaultMessages.RemoveNotAllowed, "Id", model.Id.ToString()));

        public override Task<GEProductsUnit> RemoveById(long id, string pEstablishmentKey, string pUniqueKey)
        {
            Errors.Add(ModelException.CreateValidationError(DefaultMessages.RemoveNotAllowed, "Id", id.ToString()));

            return Task.FromResult<GEProductsUnit>(null);
        }
    }
}
