using Hino.API.Domain.Base.Services;
using Hino.API.Domain.Products.Interfaces.Repositories;
using Hino.API.Domain.Products.Interfaces.Services;
using Hino.API.Domain.Products.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using System.Threading.Tasks;
using DefaultMessages = Hino.API.Infra.Cross.Resources.MessagesResource;

namespace Hino.API.Domain.Products.Services
{
    public class GEProductsTypeService : BaseService<GEProductsType>, IGEProductsTypeService
    {
        private readonly IGEProductsTypeRepository _IGEProductsTypeRepository;

        public GEProductsTypeService(IGEProductsTypeRepository pIGEProductsTypeRepository) :
             base(pIGEProductsTypeRepository)
        {
            _IGEProductsTypeRepository = pIGEProductsTypeRepository;
        }

        public override void Remove(GEProductsType model) =>
            Errors.Add(ModelException.CreateValidationError(DefaultMessages.RemoveNotAllowed, "Id", model.Id.ToString()));

        public override Task<GEProductsType> RemoveById(long id, string pEstablishmentKey, string pUniqueKey)
        {
            Errors.Add(ModelException.CreateValidationError(DefaultMessages.RemoveNotAllowed, "Id", id.ToString()));

            return Task.FromResult<GEProductsType>(null);
        }
    }
}
