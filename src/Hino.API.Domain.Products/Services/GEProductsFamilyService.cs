using Hino.API.Domain.Base.Services;
using Hino.API.Domain.Products.Interfaces.Repositories;
using Hino.API.Domain.Products.Interfaces.Services;
using Hino.API.Domain.Products.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using System.Threading.Tasks;
using DefaultMessages = Hino.API.Infra.Cross.Resources.MessagesResource;

namespace Hino.API.Domain.Products.Services
{
    public class GEProductsFamilyService : BaseService<GEProductsFamily>, IGEProductsFamilyService
    {
        private readonly IGEProductsFamilyRepository _IGEProductsFamilyRepository;

        public GEProductsFamilyService(IGEProductsFamilyRepository pIGEProductsFamilyRepository) :
             base(pIGEProductsFamilyRepository)
        {
            _IGEProductsFamilyRepository = pIGEProductsFamilyRepository;
        }

        public override void Remove(GEProductsFamily model) =>
            Errors.Add(ModelException.CreateValidationError(DefaultMessages.RemoveNotAllowed, "Id", model.Id.ToString()));

        public override Task<GEProductsFamily> RemoveById(long id, string pEstablishmentKey, string pUniqueKey)
        {
            Errors.Add(ModelException.CreateValidationError(DefaultMessages.RemoveNotAllowed, "Id", id.ToString()));

            return Task.FromResult<GEProductsFamily>(null);
        }
    }
}
