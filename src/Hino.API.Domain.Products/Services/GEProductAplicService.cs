﻿using Hino.API.Domain.Base.Services;
using Hino.API.Domain.Products.Interfaces.Repositories;
using Hino.API.Domain.Products.Interfaces.Services;
using Hino.API.Domain.Products.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using System.Threading.Tasks;
using DefaultMessages = Hino.API.Infra.Cross.Resources.MessagesResource;

namespace Hino.API.Domain.Products.Services
{
    public class GEProductAplicService : BaseService<GEProductAplic>, IGEProductAplicService
    {
        public GEProductAplicService(IGEProductAplicRepository pIGEProductAplicRepository) :
             base(pIGEProductAplicRepository)
        {
        }

        public override GEProductAplic Add(GEProductAplic model)
        {
            var ErrorValid = model.IsValid();
            if (ErrorValid != null)
            {
                Errors.Add(ErrorValid);
                return null;
            }
            return base.Add(model);
        }

        public override GEProductAplic Update(GEProductAplic model)
        {
            var ErrorValid = model.IsValid();
            if (ErrorValid != null)
            {
                Errors.Add(ErrorValid);
                return null;
            }
            return base.Update(model);
        }

        public override void Remove(GEProductAplic model) =>
            Errors.Add(ModelException.CreateValidationError(DefaultMessages.RemoveNotAllowed, "Id", model.Id.ToString()));

        public override Task<GEProductAplic> RemoveById(long id, string pEstablishmentKey, string pUniqueKey)
        {
            Errors.Add(ModelException.CreateValidationError(DefaultMessages.RemoveNotAllowed, "Id", id.ToString()));

            return Task.FromResult<GEProductAplic>(null);
        }
    }
}
