using Hino.API.Domain.Base.Services;
using Hino.API.Domain.Products.Interfaces.Repositories;
using Hino.API.Domain.Products.Interfaces.Services;
using Hino.API.Domain.Products.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using System;
using System.Linq;
using System.Threading.Tasks;
using DefaultMessages = Hino.API.Infra.Cross.Resources.MessagesResource;
using ValidationMessages = Hino.API.Infra.Cross.Resources.ValidationMessagesResource;

namespace Hino.API.Domain.Products.Services
{
    public class GEProductsService : BaseService<GEProducts>, IGEProductsService
    {
        private readonly IGEProductsRepository _IGEProductsRepository;

        public GEProductsService(IGEProductsRepository pIGEProductsRepository) :
             base(pIGEProductsRepository)
        {
            _IGEProductsRepository = pIGEProductsRepository;
        }

        public override void Remove(GEProducts model) =>
            Errors.Add(ModelException.CreateValidationError(DefaultMessages.RemoveNotAllowed, "Id", model.Id.ToString()));

        public override Task<GEProducts> RemoveById(long id, string pEstablishmentKey, string pUniqueKey)
        {
            Errors.Add(ModelException.CreateValidationError(DefaultMessages.RemoveNotAllowed, "Id", id.ToString()));

            return Task.FromResult<GEProducts>(null);
        }

        public async Task<bool> ExistsProductKeyAsync(string pEstablishmentKey, string pProductKey)
        {
            var Result = await _IGEProductsRepository.FirstOrDefaultAsync(r => r.EstablishmentKey == pEstablishmentKey && r.ProductKey == pProductKey);
            return Result != null;
        }

        public async Task<GEProducts> CreateAsync(GEProducts pProduct)
        {
            if (await ExistsProductKeyAsync(pProduct.EstablishmentKey, pProduct.ProductKey))
            {
                Errors.Add(ModelException.CreateValidationError(ValidationMessages.ProductExists, "Id", pProduct.ProductKey));
                return null;
            }

            var Result = Add(pProduct);

            if (!Errors.Any())
                await SaveChanges();

            return Result;
        }

        public async Task<GEProducts> ChangeAsync(GEProducts pProduct)
        {
            var ProductOld = GetByIdToUpdate(pProduct.Id, pProduct.EstablishmentKey, pProduct.UniqueKey);

            if (ProductOld.ProductKey != pProduct.ProductKey && await ExistsProductKeyAsync(pProduct.EstablishmentKey, pProduct.ProductKey))
            {
                Errors.Add(ModelException.CreateValidationError(ValidationMessages.ProductExists, "Id", pProduct.ProductKey));
                return null;
            }

            ProductOld.Image = pProduct.Image;
            ProductOld.Name = pProduct.Name;
            ProductOld.Description = pProduct.Description;
            ProductOld.TypeId = pProduct.TypeId;
            ProductOld.FamilyId = pProduct.FamilyId;
            ProductOld.PercIPI = Convert.ToDecimal(pProduct.PercIPI);
            ProductOld.PercMaxDiscount = Convert.ToDecimal(pProduct.PercMaxDiscount);
            ProductOld.Value = Convert.ToDecimal(pProduct.Value);
            ProductOld.Status = pProduct.Status;
            ProductOld.NCM = pProduct.NCM;
            ProductOld.NCMId = pProduct.NCMId;
            ProductOld.UnitId = pProduct.UnitId;
            ProductOld.Weight = pProduct.Weight;
            ProductOld.PackageQTD = pProduct.PackageQTD;
            ProductOld.SaleUnitId = pProduct.SaleUnitId;
            ProductOld.SaleFactor = pProduct.SaleFactor;
            ProductOld.AplicationId = pProduct.AplicationId;
            ProductOld.CodOrigMerc = pProduct.CodOrigMerc;

            var Result = Update(ProductOld);

            if (!Errors.Any())
                await SaveChanges();

            return Result;
        }
    }
}
