﻿using Hino.API.Domain.Mordor.Models;
using Hino.API.Infra.Data.Repositories.Base.Context;
using Hino.API.Infra.Data.Repositories.Mordor.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Infra.Data.Repositories.Mordor.Context
{
    public class MordorDbContext : BaseDbContext
    {
        public virtual DbSet<GEUsers> GEUsers { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new GEUsersConfiguration("GEUSERS"));
        }
    }
}
