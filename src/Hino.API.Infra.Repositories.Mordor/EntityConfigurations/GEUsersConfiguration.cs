﻿using Hino.API.Domain.Mordor.Models;
using Hino.API.Infra.Data.Repositories.Base.Configurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Infra.Data.Repositories.Mordor.EntityConfigurations
{
    public class GEUsersConfiguration : BaseEntityConfigurarion<GEUsers>
    {
        public GEUsersConfiguration(string Table) : base(Table)
        {
        }

        public override void Configure(EntityTypeBuilder<GEUsers> config)
        {
            base.Configure(config);

            config.Property(b => b.Uid)
                .HasMaxLength(36)
                .IsRequired();
            config.Property(b => b.UserName)
                .HasMaxLength(60)
                .IsRequired();
            config.Property(b => b.Email)
                .HasMaxLength(120)
                .IsRequired();
            config.Property(b => b.Password)
                .HasMaxLength(255)
                .IsRequired();
            config.Property(b => b.Role)
                .HasMaxLength(60)
                .IsRequired();

            config.HasIndex("Id")
              .IsUnique(true);
            config.HasIndex("Email")
              .IsUnique(true);
            config.HasIndex("UserName")
              .IsUnique(true);
            config.HasIndex("Uid")
              .IsUnique(true);

            config.HasIndex("IsActive");
        }
    }
}
