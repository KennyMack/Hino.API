﻿using Hino.API.Domain.Mordor.Interfaces.Repositories;
using Hino.API.Domain.Mordor.Models;
using Hino.API.Infra.Data.Repositories.Base.Repositories;
using Hino.API.Infra.Data.Repositories.Mordor.Context;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Infra.Data.Repositories.Mordor.Repositories
{
    public class GEUsersRepository: BaseRepository<GEUsers>, IGEUsersRepository
    {
        public GEUsersRepository(MordorDbContext appDbContext) : base(appDbContext)
        {

        }

        public GEUsers Create(GEUsers pUser)
        {
            Add(pUser);
            DbConn.SaveChanges();

            return pUser;
        }

        public GEUsers GetByEmail(string pEstablishmentKey, string pEmail) =>
           Task.Run(async () => await FirstOrDefaultAsync(r =>
                r.EstablishmentKey == pEstablishmentKey &&
                r.Email == pEmail)).Result;

        public bool ExistsEmailOnEstablishment(string pEstablishmentKey, string pEmail) =>
            Task.Run(async () => await QueryAsync(r => 
                r.EstablishmentKey == pEstablishmentKey &&
                r.Email == pEmail)).Result.Any();

        public bool ExistsUid(string pUid) =>
            Task.Run(async () => await QueryAsync(r => 
                r.Uid == pUid)).Result.Any();

        public bool ExistsUserKey(string pUserKey) =>
            Task.Run(async () => await QueryAsync(r => 
                r.UserKey == pUserKey)).Result.Any();

        public bool ExistsUserNameOnEstablishment(string pEstablishmentKey, string pUserName) =>
            Task.Run(async () => await QueryAsync(r => 
                r.EstablishmentKey == pEstablishmentKey &&
                r.UserName == pUserName)).Result.Any();
    }
}
