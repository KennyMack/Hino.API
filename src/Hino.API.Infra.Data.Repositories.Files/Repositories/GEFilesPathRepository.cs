﻿using Hino.API.Domain.Files.Interfaces.Repositories;
using Hino.API.Domain.Files.Models;
using Hino.API.Infra.Data.Repositories.Base.Repositories;
using Hino.API.Infra.Data.Repositories.Files.Context;

namespace Hino.API.Infra.Data.Repositories.Files.Repositories
{
    public class GEFilesPathRepository : BaseRepository<GEFilesPath>, IGEFilesPathRepository
    {
        public GEFilesPathRepository(FilesDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
