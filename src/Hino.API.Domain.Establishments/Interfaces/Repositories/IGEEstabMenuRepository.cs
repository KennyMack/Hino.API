using Hino.API.Domain.Base.Interfaces.Repositories;
using Hino.API.Domain.Establishments.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.API.Domain.Establishments.Interfaces.Repositories
{
    public interface IGEEstabMenuRepository : IBaseRepository<GEEstabMenu>
    {
        void SaveEstablishmentMenu(GEEstablishments pEstablishment, IEnumerable<GEEstabMenu> pMenus);
    }
}
