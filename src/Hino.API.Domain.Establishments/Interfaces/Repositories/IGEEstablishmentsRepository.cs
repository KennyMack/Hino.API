using Hino.API.Domain.Base.Interfaces.Repositories;
using Hino.API.Domain.Establishments.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.API.Domain.Establishments.Interfaces.Repositories
{
    public interface IGEEstablishmentsRepository : IBaseRepository<GEEstablishments>
    {
        Task<bool> HasEstablishmentDependencyAsync(string pEstablishmentKey);
        Task<IEnumerable<GEEstablishments>> GetByIdAndEstablishmentKeyAsync(long pId, string pEstablishmentKey);
        Task<bool> ExistsEstablishmentAsync(string pEstablishmentKey);
        Task<IEnumerable<GEEstabDevices>> GetDevicesEstablishment(string pEstablishmentKey, string pUniqueKey, long pId);
        Task<GEEstablishments> GetByEstablishmentKeyAsync(string pEstablishmentKey);
    }
}
