using Hino.API.Domain.Base.Interfaces.Repositories;
using Hino.API.Domain.Establishments.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;

namespace Hino.API.Domain.Establishments.Interfaces.Repositories
{
    public interface IGEEstabDevicesRepository : IBaseRepository<GEEstabDevices>
    {
        int DevicesCount(long pIdEstablishment, string pEstablishmentKey);
        ModelException ValidateNewDeviceCount(long pGEEstabID, string pEstablishmentKey, int pDevices);
        ModelException CanChangeDeviceCount(long pGEEstabID, string pEstablishmentKey, int pDevices);
    }
}
