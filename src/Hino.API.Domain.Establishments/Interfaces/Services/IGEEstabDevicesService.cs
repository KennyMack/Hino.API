using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Establishments.Models;
using System.Threading.Tasks;

namespace Hino.API.Domain.Establishments.Interfaces.Services
{
    public interface IGEEstabDevicesService : IBaseService<GEEstabDevices>
    {
        Task<GEEstabDevices> CreateDeviceAsync(GEEstabDevices model);
        int DevicesCount(long pIdEstablishment, string pEstablishmentKey);
    }
}
