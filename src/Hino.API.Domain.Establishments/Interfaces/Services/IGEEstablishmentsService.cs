using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Establishments.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.API.Domain.Establishments.Interfaces.Services
{
    public interface IGEEstablishmentsService : IBaseService<GEEstablishments>
    {
        Task<bool> HasEstablishmentDependencyAsync(string pEstablishmentKey);
        Task<IEnumerable<GEEstablishments>> GetByIdAndEstablishmentKeyAsync(long pId, string pEstablishmentKey);
        Task<GEEstablishments> UpdateEstablishmentAsync(GEEstablishments pEstab);
        Task<GEEstablishments> CreateEstablishmentAsync(GEEstablishments pEstab);
        Task<bool> ExistsEstablishmentAsync(string pEstablishmentKey);
        Task<IEnumerable<GEEstabDevices>> GetDevicesEstablishment(string pEstablishmentKey, string pUniqueKey, long pId);
        Task<GEEstablishments> GetByEstablishmentKeyAsync(string pEstablishmentKey);
    }
}
