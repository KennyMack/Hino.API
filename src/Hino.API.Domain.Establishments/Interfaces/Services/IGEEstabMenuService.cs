using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Establishments.Models;

namespace Hino.API.Domain.Establishments.Interfaces.Services
{
    public interface IGEEstabMenuService : IBaseService<GEEstabMenu>
    {
    }
}
