﻿using Hino.API.Domain.Base.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Domain.Establishments.Models
{
    public class GEEnterpriseFiscalGroup: BaseEntity
    {
        public string Description { get; set; }
    }
}
