﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Hino.API.Domain.Base.Models;
using Hino.API.Infra.Cross.Utils;
using Hino.API.Infra.Cross.Utils.Enums;
using Hino.API.Infra.Cross.Utils.Exceptions;
using ValidationMessages = Hino.API.Infra.Cross.Resources.ValidationMessagesResource;
using FieldsMessages = Hino.API.Infra.Cross.Resources.FieldsNameResource;


namespace Hino.API.Domain.Establishments.Models
{
    [Table("GEESTABLISHMENTS")]
    public class GEEstablishments : BaseEntity
    {
        public GEEstablishments()
        {
            this.GEEstabDevices = new HashSet<GEEstabDevices>();
            this.GEEstabFat = new HashSet<GEEstabFat>();
            this.GEEstabPay = new HashSet<GEEstabPay>();
            this.GEEstabMenu = new HashSet<GEEstabMenu>();
            // this.GEFilesPath = new HashSet<GEFilesPath>();
            // this.GEEstabCalendar = new HashSet<GEEstabCalendar>();
        }

        public string RazaoSocial { get; set; }
        public string NomeFantasia { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string CNPJCPF { get; set; }
        public int Devices { get; set; }
        public decimal PIS { get; set; }
        public decimal COFINS { get; set; }
        public string TokenCNPJ { get; set; }
        /// <summary>
        /// Permite cadastrar empresas
        /// </summary>
        public bool AllowEnterprise { get; set; }
        /// <summary>
        /// Permite alterar a forma de pagamento do pedido
        /// </summary>
        public bool AllowPayment { get; set; }
        /// <summary>
        /// Permite alterar o preço dos itens do pedido
        /// </summary>
        public bool AllowChangePrice { get; set; }
        /// <summary>
        /// Permite alterar o desconto no pedido
        /// </summary>
        public bool AllowDiscount { get; set; }
        public long? EstabGroup { get; set; }
        public bool FatorR { get; set; }
        public string CapptaKey { get; set; }
        public string SitefIp { get; set; }
        [ForeignKey("FSFiscalOper")]
        public long? DefaultFiscalOperID { get; set; }
        public virtual GEFiscalOper FSFiscalOper { get; set; }
        /// <summary>
        /// Descrição do campo com informações adicionais
        /// </summary>
        public string AditionalInfo { get; set; }
        /// <summary>
        /// Observação padrão do pedido
        /// </summary>
        public string DefaultNoteOrder { get; set; }
        /// <summary>
        /// Somente pedidos de itens com estoque disponível
        /// </summary>
        public bool OnlyWithStock { get; set; }
        /// <summary>
        /// Somente permite pedidos na data
        /// </summary>
        public bool OnlyOnDate { get; set; }

        /// <summary>
        /// Grupo fiscal padrão para novos cadastros de empresa
        /// que forem do tipo Pessoa fisica
        /// </summary>
        [ForeignKey("PfFiscalGroup")]
        public long? PfFiscalGroupId { get; set; }
        public virtual GEEnterpriseFiscalGroup PfFiscalGroup { get; set; }

        /// <summary>
        /// Grupo fiscal padrão para novos cadastros de empresa
        /// que forem do tipo Pessoa jurídica
        /// </summary>
        [ForeignKey("PjFiscalGroup")]
        public long? PjFiscalGroupId { get; set; }
        public virtual GEEnterpriseFiscalGroup PjFiscalGroup { get; set; }

        /// <summary>
        /// Classificação padrão para novos cadastros de empresa
        /// que forem do tipo Pessoa fisica
        /// </summary>
        public string PfClassifClient { get; set; }

        /// <summary>
        /// Classificação padrão para novos cadastros de empresa
        /// que forem do tipo Pessoa jurídica
        /// </summary>
        public string PjClassifClient { get; set; }
       
        /// <summary>
        /// Dias para a data de pagamento a partir do pedido
        /// </summary>
        public int DaysPayment { get; set; }

        public virtual ICollection<GEEstabDevices> GEEstabDevices { get; set; }
        public virtual ICollection<GEEstabFat> GEEstabFat { get; set; }
        public virtual ICollection<GEEstabPay> GEEstabPay { get; set; }
        public virtual ICollection<GEEstabMenu> GEEstabMenu { get; set; }
        // public virtual ICollection<GEFilesPath> GEFilesPath { get; set; }
        // public virtual ICollection<GEEstabCalendar> GEEstabCalendar { get; set; }

        public ModelException IsValidCreate()
        {
            if (RazaoSocial.IsEmpty())
            {
                return new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { string.Format(ValidationMessages.RequiredDefault, FieldsMessages.RazaoSocial) }
                };
            }
            if (NomeFantasia.IsEmpty())
            {
                return new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { string.Format(ValidationMessages.RequiredDefault, FieldsMessages.NomeFantasia) }
                };
            }
            if (Email.IsEmpty())
            {
                return new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { string.Format(ValidationMessages.RequiredDefault, FieldsMessages.Email) }
                };
            }
            if (Phone.IsEmpty())
            {
                return new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { string.Format(ValidationMessages.RequiredDefault, FieldsMessages.Phone) }
                };
            }
            if (CNPJCPF.IsEmpty())
            {
                return new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { string.Format(ValidationMessages.RequiredDefault, FieldsMessages.CNPJCPF) }
                };

            }
            if (Devices <= 0)
            {
                return new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { string.Format(ValidationMessages.GreaterThan, FieldsMessages.Devices, 0) }
                };
            }
            if (!CNPJCPF.IsEmpty() && !CNPJCPF.IsCNPJ())
            {
                return new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { ValidationMessages.InvalidCNPJ }
                };
            }

            return null;
        }

        public ModelException IsStatusActive()
        {
            if (!IsActive)
            {
                return new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { Infra.Cross.Resources.MessagesResource.EstablishmentInactive }
                };
            }
            return null;
        }
    }
}
