using Hino.API.Domain.Base.Models;
using System;

namespace Hino.API.Domain.Establishments.Models
{
    public class GEEstabPay : BaseEntity
    {
        public long EstabId { get; set; }
        public DateTime Period { get; set; }
        public decimal Value { get; set; }
    }
}
