using Hino.API.Domain.Base.Models;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.API.Domain.Establishments.Models
{
    public class GEEstabFat : BaseEntity
    {
        public long EstabId { get; set; }
        public DateTime Period { get; set; }
        public decimal Value { get; set; }

        public virtual GEEstablishments GEEstablishments { get; set; }
    }
}
