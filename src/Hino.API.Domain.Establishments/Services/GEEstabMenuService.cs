using Hino.API.Domain.Base.Services;
using Hino.API.Domain.Establishments.Interfaces.Repositories;
using Hino.API.Domain.Establishments.Interfaces.Services;
using Hino.API.Domain.Establishments.Models;

namespace Hino.API.Domain.Establishments.Services
{
    public class GEEstabMenuService : BaseService<GEEstabMenu>, IGEEstabMenuService
    {
        private readonly IGEEstabMenuRepository _IGEEstabMenuRepository;

        public GEEstabMenuService(IGEEstabMenuRepository pIGEEstabMenuRepository) :
             base(pIGEEstabMenuRepository)
        {
            _IGEEstabMenuRepository = pIGEEstabMenuRepository;
        }
    }
}
