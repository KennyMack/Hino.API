using Hino.API.Domain.Establishments.Interfaces.Repositories;
using Hino.API.Domain.Establishments.Interfaces.Services;
using System;
using System.Linq;
using System.Threading.Tasks;
using Hino.API.Domain.Base.Services;
using Hino.API.Domain.Establishments.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using Hino.API.Infra.Cross.Utils.Enums;
using DefaultMessage = Hino.API.Infra.Cross.Resources.MessagesResource;
using ValidationMessage = Hino.API.Infra.Cross.Resources.ValidationMessagesResource;

namespace Hino.API.Domain.Establishments.Services
{
    public class GEEstabDevicesService : BaseService<GEEstabDevices>, IGEEstabDevicesService
    {
        private readonly IGEEstabDevicesRepository _IGEEstabDevicesRepository;
        private readonly IGEEstablishmentsRepository _IGEEstablishmentsRepository;

        public GEEstabDevicesService(IGEEstabDevicesRepository pIGEEstabDevicesRepository,
            IGEEstablishmentsRepository pIGEEstablishmentsRepository) :
             base(pIGEEstabDevicesRepository)
        {
            _IGEEstabDevicesRepository = pIGEEstabDevicesRepository;
            _IGEEstablishmentsRepository = pIGEEstablishmentsRepository;
        }

        public async Task<GEEstabDevices> CreateDeviceAsync(GEEstabDevices model)
        {
            var Establishment = (await _IGEEstablishmentsRepository.
                GetByIdAndEstablishmentKeyAsync(
                    model.GEEstabID,
                    model.EstablishmentKey))
                .FirstOrDefault();
            try
            {
                if (Establishment == null)
                {
                    throw new Exception(Infra.Cross.Resources.MessagesResource.IdAndEstablishmentKeyError)
                    {
                        HelpLink = "pEstablishmentKey",
                        Source = model.EstablishmentKey
                    };
                }
            }
            catch (Exception e)
            {
                Logging.Exception(e);

                Errors.Add(ModelException.CreateSqlError(e, EExceptionErrorCodes.RegisterNotFound));
                return model;
            }

            try
            {
                if ((DevicesCount(model.GEEstabID, model.EstablishmentKey) + 1) > Establishment.Devices)
                {
                    throw new Exception(Infra.Cross.Resources.MessagesResource.TotalDevicesReached)
                    {
                        HelpLink = "Devices",
                        Source = Establishment.Devices.ToString()
                    };
                }

                model.UserKey = Guid.NewGuid().ToString();
                _IGEEstabDevicesRepository.Add(model);
                await _IGEEstabDevicesRepository.SaveChanges();
            }
            catch (Exception e)
            {
                Logging.Exception(e);

                Errors.Add(ModelException.CreateSqlError(e, EExceptionErrorCodes.InsertSQLError));
            }

            return model;
        }

        public int DevicesCount(long pIdEstablishment, string pEstablishmentKey)
        {
            var result = _IGEEstabDevicesRepository.QueryAsync(r =>
                  r.GEEstabID == pIdEstablishment &&
                  r.EstablishmentKey == pEstablishmentKey &&
                  r.IsActive).Result;
            return result.Count();
        }

        public override async Task<GEEstabDevices> RemoveById(long id, string pEstablishmentKey, string pUniqueKey)
        {
            var Devices = await _IGEEstabDevicesRepository.GetByIdAsync(id, pEstablishmentKey, pUniqueKey);

            if (Devices == null)
            {
                Errors.Add(ModelException.CreateValidationError(ValidationMessage.NotFound, "GEEstabDevices", id.ToString()));
                return null;
            }

            _IGEEstabDevicesRepository.Remove(Devices);
            await _IGEEstabDevicesRepository.SaveChanges();
            return Devices;
        }
    }
}
