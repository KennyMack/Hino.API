using Hino.API.Domain.Base.Services;
using Hino.API.Domain.Establishments.Interfaces.Repositories;
using Hino.API.Domain.Establishments.Interfaces.Services;
using Hino.API.Domain.Establishments.Models;
using Hino.API.Infra.Cross.Utils.Enums;
using Hino.API.Infra.Cross.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.API.Domain.Establishments.Services
{
    public class GEEstablishmentsService : BaseService<GEEstablishments>, IGEEstablishmentsService
    {
        private readonly IGEEstablishmentsRepository _IGEEstablishmentsRepository;
        private readonly IGEEstabMenuRepository _IGEEstabMenuRepository;
        private readonly IGEEstabDevicesRepository _IGEEstabDevicesRepository;

        public GEEstablishmentsService(
            IGEEstablishmentsRepository pIGEEstablishmentsRepository,
            IGEEstabMenuRepository pIGEEstabMenuRepository,
            IGEEstabDevicesRepository pIGEEstabDevicesRepository) :
             base(pIGEEstablishmentsRepository)
        {
            _IGEEstabDevicesRepository = pIGEEstabDevicesRepository;
            _IGEEstabMenuRepository = pIGEEstabMenuRepository;
            _IGEEstablishmentsRepository = pIGEEstablishmentsRepository;
        }

        public async Task<bool> ExistsEstablishmentAsync(string pEstablishmentKey)
        {
            try
            {
                return await _IGEEstablishmentsRepository.ExistsEstablishmentAsync(pEstablishmentKey);
            }
            catch (Exception e)
            {
                Logging.Exception(e);
            }
            return false;
        }

        public async Task<IEnumerable<GEEstablishments>> GetByIdAndEstablishmentKeyAsync(long pId, string pEstablishmentKey) =>
            await _IGEEstablishmentsRepository.GetByIdAndEstablishmentKeyAsync(pId, pEstablishmentKey);

        public async Task<bool> HasEstablishmentDependencyAsync(string pEstablishmentKey)
        {
            try
            {
                return await _IGEEstablishmentsRepository.HasEstablishmentDependencyAsync(pEstablishmentKey);
            }
            catch (Exception e)
            {
                Logging.Exception(e);
                Errors.Add(ModelException.CreateSqlError(e, "pEstablishmentKey", pEstablishmentKey, EExceptionErrorCodes.SQLCommandError));
                return false;
            }
        }

        public override async Task<GEEstablishments> RemoveById(long id, string pEstablishmentKey, string pUniqueKey)
        {
            var result = await HasEstablishmentDependencyAsync(pEstablishmentKey);

            if (result)
            {
                Errors.Add(ModelException.CreateValidationError("Existem registros vinculados a esse estabelecimento", "GEEstablishments", id.ToString()));
                return null;
            }

            var estab = await _IGEEstablishmentsRepository.RemoveById(id, pEstablishmentKey, pUniqueKey);
            await _IGEEstablishmentsRepository.SaveChanges();
            return estab;
        }

        public async Task<GEEstablishments> CreateEstablishmentAsync(GEEstablishments pEstab)
        {
            var Estab = new GEEstablishments
            {
                RazaoSocial = pEstab.RazaoSocial,
                NomeFantasia = pEstab.NomeFantasia,
                Email = pEstab.Email,
                Phone = pEstab.Phone,
                CNPJCPF = pEstab.CNPJCPF,
                Devices = pEstab.Devices,
                EstablishmentKey = Guid.NewGuid().ToString(),
                PIS = pEstab.PIS,
                COFINS = pEstab.COFINS,
                OnlyWithStock = false,
                OnlyOnDate = true,
                AllowEnterprise = true,
                AllowPayment = true,
                AllowChangePrice = true,
                AllowDiscount = true,
                PfClassifClient = "C1",
                PjClassifClient = "C1"
            };

            var Error = Estab.IsValidCreate();
            if (Error != null)
            {
                Errors.Add(Error);
                return null;
            }

            _IGEEstablishmentsRepository.Add(Estab);
            await _IGEEstablishmentsRepository.SaveChanges();

            return Estab;
        }

        public async Task<GEEstablishments> UpdateEstablishmentAsync(GEEstablishments pEstab)
        {
            var DeviceError = _IGEEstabDevicesRepository.CanChangeDeviceCount(pEstab.Id, pEstab.EstablishmentKey, pEstab.Devices);

            if (DeviceError == null)
            {
                Errors.Add(DeviceError);
                return null;
            }

            var EstabDB = _IGEEstablishmentsRepository.GetByIdToUpdate(
                    pEstab.Id,
                    pEstab.EstablishmentKey,
                    pEstab.UniqueKey);

            if (EstabDB == null)
            {
                Errors.Add(ModelException.CreateValidationError(Infra.Cross.Resources.ValidationMessagesResource.NotFound, "Estab", pEstab.Id.ToString()));
                return null;
            }

            try
            {
                EstabDB.RazaoSocial = pEstab.RazaoSocial;
                EstabDB.NomeFantasia = pEstab.NomeFantasia;
                EstabDB.Email = pEstab.Email;
                EstabDB.Phone = pEstab.Phone;
                EstabDB.CNPJCPF = pEstab.CNPJCPF;
                EstabDB.Devices = pEstab.Devices;
                EstabDB.IsActive = pEstab.IsActive;
                EstabDB.PIS = pEstab.PIS;
                EstabDB.COFINS = pEstab.COFINS;
                EstabDB.AllowChangePrice = pEstab.AllowChangePrice;
                EstabDB.AllowDiscount = pEstab.AllowDiscount;
                EstabDB.AllowEnterprise = pEstab.AllowEnterprise;
                EstabDB.AllowPayment = pEstab.AllowPayment;
                EstabDB.AditionalInfo = pEstab.AditionalInfo;
                EstabDB.DefaultFiscalOperID = pEstab.DefaultFiscalOperID;
                EstabDB.DefaultNoteOrder = pEstab.DefaultNoteOrder;
                EstabDB.CapptaKey = pEstab.CapptaKey;
                EstabDB.SitefIp = pEstab.SitefIp;
                EstabDB.FatorR = pEstab.FatorR;
                EstabDB.TokenCNPJ = pEstab.TokenCNPJ;
                EstabDB.OnlyWithStock = pEstab.OnlyWithStock;
                EstabDB.OnlyOnDate = pEstab.OnlyOnDate;
                EstabDB.PfFiscalGroupId = pEstab.PfFiscalGroupId;
                EstabDB.PjFiscalGroupId = pEstab.PjFiscalGroupId;
                EstabDB.PfClassifClient = pEstab.PfClassifClient;
                EstabDB.PjClassifClient = pEstab.PjClassifClient;
                EstabDB.DaysPayment = pEstab.DaysPayment;


                var Error = EstabDB.IsValidCreate();
                if (Error != null)
                {
                    Errors.Add(Error);
                    return null;
                }

                _IGEEstablishmentsRepository.Update(EstabDB);

                _IGEEstabMenuRepository.SaveEstablishmentMenu(EstabDB, pEstab.GEEstabMenu);

                await _IGEEstablishmentsRepository.SaveChanges();
                await _IGEEstabMenuRepository.SaveChanges();
            }
            catch (Exception e)
            {
                Logging.Exception(e);
                Errors.Add(ModelException.CreateSqlError(e, EExceptionErrorCodes.UpdateSQLError));
            }

            return EstabDB;
        }

        public async Task<IEnumerable<GEEstabDevices>> GetDevicesEstablishment(string pEstablishmentKey, string pUniqueKey, long pId) =>
            await _IGEEstablishmentsRepository.GetDevicesEstablishment(pEstablishmentKey, pUniqueKey, pId);

        public async Task<GEEstablishments> GetByEstablishmentKeyAsync(string pEstablishmentKey) =>
            await _IGEEstablishmentsRepository.GetByEstablishmentKeyAsync(pEstablishmentKey);
    }
}
