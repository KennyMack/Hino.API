﻿using System;

namespace Hino.API.Infra.Cross.Utils.Attributes
{
    [System.AttributeUsage(System.AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    public class QueueAttribute : Attribute
    {
        public QueueAttribute(string pQueue)
        {
            this.Queue = pQueue;
        }

        public string Queue
        {
            get;
            private set;
        }
    }

    [System.AttributeUsage(System.AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    public class CollectionNameAttribute : Attribute
    {
        public CollectionNameAttribute(string pName)
        {
            this.Name = pName;
        }

        public string Name
        {
            get;
            private set;
        }
    }
}
