﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Hino.API.Infra.Cross.Utils.Attributes
{
    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class Max2LengthFieldAttribute : MaxLengthAttribute
    {
        public Max2LengthFieldAttribute() : base(2)
        {
            ErrorMessageResourceName = "MaxCharacters";
            ErrorMessageResourceType = typeof(Resources.ValidationMessagesResource);
        }
    }

    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class Max8LengthFieldAttribute : MaxLengthAttribute
    {
        public Max8LengthFieldAttribute() : base(8)
        {
            ErrorMessageResourceName = "MaxCharacters";
            ErrorMessageResourceType = typeof(Resources.ValidationMessagesResource);
        }
    }

    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class Max10LengthFieldAttribute : MaxLengthAttribute
    {
        public Max10LengthFieldAttribute() : base(10)
        {
            ErrorMessageResourceName = "MaxCharacters";
            ErrorMessageResourceType = typeof(Resources.ValidationMessagesResource);
        }
    }

    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class Max20LengthFieldAttribute : MaxLengthAttribute
    {
        public Max20LengthFieldAttribute() : base(20)
        {
            ErrorMessageResourceName = "MaxCharacters";
            ErrorMessageResourceType = typeof(Resources.ValidationMessagesResource);
        }
    }

    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class Max36LengthFieldAttribute : MaxLengthAttribute
    {
        public Max36LengthFieldAttribute() : base(36)
        {
            ErrorMessageResourceName = "MaxCharacters";
            ErrorMessageResourceType = typeof(Resources.ValidationMessagesResource);
        }
    }

    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class Max40LengthFieldAttribute : MaxLengthAttribute
    {
        public Max40LengthFieldAttribute() : base(40)
        {
            ErrorMessageResourceName = "MaxCharacters";
            ErrorMessageResourceType = typeof(Resources.ValidationMessagesResource);
        }
    }

    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class Max60LengthFieldAttribute : MaxLengthAttribute
    {
        public Max60LengthFieldAttribute() : base(60)
        {
            ErrorMessageResourceName = "MaxCharacters";
            ErrorMessageResourceType = typeof(Resources.ValidationMessagesResource);
        }
    }
    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class Max120LengthFieldAttribute : MaxLengthAttribute
    {
        public Max120LengthFieldAttribute() : base(120)
        {
            ErrorMessageResourceName = "MaxCharacters";
            ErrorMessageResourceType = typeof(Resources.ValidationMessagesResource);
        }
    }
    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class Max255LengthFieldAttribute : MaxLengthAttribute
    {
        public Max255LengthFieldAttribute() : base(255)
        {
            ErrorMessageResourceName = "MaxCharacters";
            ErrorMessageResourceType = typeof(Resources.ValidationMessagesResource);
        }
    }
    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class Max500LengthFieldAttribute : MaxLengthAttribute
    {
        public Max500LengthFieldAttribute() : base(500)
        {
            ErrorMessageResourceName = "MaxCharacters";
            ErrorMessageResourceType = typeof(Resources.ValidationMessagesResource);
        }
    }
    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class Max2000LengthFieldAttribute : MaxLengthAttribute
    {
        public Max2000LengthFieldAttribute() : base(2000)
        {
            ErrorMessageResourceName = "MaxCharacters";
            ErrorMessageResourceType = typeof(Resources.ValidationMessagesResource);
        }
    }
}
