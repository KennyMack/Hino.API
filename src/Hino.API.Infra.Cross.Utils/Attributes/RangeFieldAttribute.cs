﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Hino.API.Infra.Cross.Utils.Attributes
{
    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class LongRangeFieldAttribute : RangeAttribute
    {
        public LongRangeFieldAttribute(long minimum, long maximum) :
            base(minimum, maximum)
        {

            ErrorMessageResourceName = "RangeOnlyInteger";
            ErrorMessageResourceType = typeof(Resources.ValidationMessagesResource);
        }
    }

    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class IntegerRangeFieldAttribute : RangeAttribute
    {
        public IntegerRangeFieldAttribute(int minimum, int maximum) :
            base(minimum, maximum)
        {

            ErrorMessageResourceName = "RangeOnlyInteger";
            ErrorMessageResourceType = typeof(Resources.ValidationMessagesResource);
        }
    }

    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class DecimalRangeFieldAttribute : RangeAttribute
    {
        public DecimalRangeFieldAttribute(double minimum, double maximum) :
            base(minimum, maximum)
        {

            ErrorMessageResourceName = "RangeOnlyDecimal";
            ErrorMessageResourceType = typeof(Resources.ValidationMessagesResource);
        }
    }
}
