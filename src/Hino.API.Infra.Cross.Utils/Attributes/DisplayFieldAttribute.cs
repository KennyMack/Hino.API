﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Hino.API.Infra.Cross.Utils.Attributes
{
    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class DisplayFieldAttribute : DisplayNameAttribute
    {
        public DisplayFieldAttribute([CallerMemberName] string propertyName = "")
        {
            try
            {
                var a = new System.Resources.ResourceManager(typeof(Resources.FieldsNameResource));
                a.IgnoreCase = true;
                DisplayNameValue = a.GetString(propertyName) ?? propertyName;
            }
            catch (Exception)
            {
                DisplayNameValue = propertyName;
            }
        }
    }
}
