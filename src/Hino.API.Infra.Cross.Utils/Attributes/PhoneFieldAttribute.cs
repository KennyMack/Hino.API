﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace Hino.API.Infra.Cross.Utils.Attributes
{
    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class PhoneFieldAttribute : ValidationAttribute
    {
        private static readonly Regex PhoneRegex = new Regex(@"^(\(\d{2,3}\)\s\d{4,5}\-\d{4})$", RegexOptions.Compiled | RegexOptions.IgnoreCase);

        public PhoneFieldAttribute()
        {
            ErrorMessageResourceName = "PhoneInvalid";
            ErrorMessageResourceType = typeof(Resources.ValidationMessagesResource);
        }
        public override bool IsValid(object value)
        {
            if (string.IsNullOrEmpty((value ?? "").ToString()))
                return true;

            if (PhoneRegex.IsMatch((value ?? "").ToString()))
                return true;

            return false;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (string.IsNullOrEmpty((value ?? "").ToString()))
                return ValidationResult.Success;

            if (PhoneRegex.IsMatch((value ?? "").ToString()))
                return ValidationResult.Success;

            return new ValidationResult(ErrorMessage);
        }
    }

    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class CellPhoneFieldAttribute : ValidationAttribute
    {
        private static readonly Regex PhoneRegex = new Regex(@"^(\(\d{2,3}\)\s\d{5}\-\d{4})$", RegexOptions.Compiled | RegexOptions.IgnoreCase);

        public CellPhoneFieldAttribute()
        {
            ErrorMessageResourceName = "CellphoneInvalid";
            ErrorMessageResourceType = typeof(Resources.ValidationMessagesResource);
        }

        public override bool IsValid(object value)
        {
            if (string.IsNullOrEmpty((value ?? "").ToString()))
                return true;

            if (PhoneRegex.IsMatch((value ?? "").ToString()))
                return true;


            return false;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (string.IsNullOrEmpty((value ?? "").ToString()))
                return ValidationResult.Success;

            if (PhoneRegex.IsMatch((value ?? "").ToString()))
                return ValidationResult.Success;

            return new ValidationResult(ErrorMessage);
        }
    }
}
