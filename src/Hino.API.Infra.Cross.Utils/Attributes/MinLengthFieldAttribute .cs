﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Hino.API.Infra.Cross.Utils.Attributes
{
    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class Min8LengthFieldAttribute : MinLengthAttribute
    {
        public Min8LengthFieldAttribute() : base(8)
        {
            ErrorMessageResourceName = "MinCharacters";
            ErrorMessageResourceType = typeof(Resources.ValidationMessagesResource);
        }
    }

    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class Min10LengthFieldAttribute : MinLengthAttribute
    {
        public Min10LengthFieldAttribute() : base(10)
        {
            ErrorMessageResourceName = "MinCharacters";
            ErrorMessageResourceType = typeof(Resources.ValidationMessagesResource);
        }
    }

    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class Min20LengthFieldAttribute : MinLengthAttribute
    {
        public Min20LengthFieldAttribute() : base(20)
        {
            ErrorMessageResourceName = "MinCharacters";
            ErrorMessageResourceType = typeof(Resources.ValidationMessagesResource);
        }
    }

    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class Min36LengthFieldAttribute : MinLengthAttribute
    {
        public Min36LengthFieldAttribute() : base(36)
        {
            ErrorMessageResourceName = "MinCharacters";
            ErrorMessageResourceType = typeof(Resources.ValidationMessagesResource);
        }
    }

    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class Min40LengthFieldAttribute : MinLengthAttribute
    {
        public Min40LengthFieldAttribute() : base(40)
        {
            ErrorMessageResourceName = "MinCharacters";
            ErrorMessageResourceType = typeof(Resources.ValidationMessagesResource);
        }
    }

    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class Min60LengthFieldAttribute : MinLengthAttribute
    {
        public Min60LengthFieldAttribute() : base(60)
        {
            ErrorMessageResourceName = "MinCharacters";
            ErrorMessageResourceType = typeof(Resources.ValidationMessagesResource);
        }
    }
    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class Min120LengthFieldAttribute : MinLengthAttribute
    {
        public Min120LengthFieldAttribute() : base(120)
        {
            ErrorMessageResourceName = "MinCharacters";
            ErrorMessageResourceType = typeof(Resources.ValidationMessagesResource);
        }
    }
    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class Min255LengthFieldAttribute : MinLengthAttribute
    {
        public Min255LengthFieldAttribute() : base(255)
        {
            ErrorMessageResourceName = "MinCharacters";
            ErrorMessageResourceType = typeof(Resources.ValidationMessagesResource);
        }
    }
    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class Min2000LengthFieldAttribute : MinLengthAttribute
    {
        public Min2000LengthFieldAttribute() : base(2000)
        {
            ErrorMessageResourceName = "MinCharacters";
            ErrorMessageResourceType = typeof(Resources.ValidationMessagesResource);
        }
    }
}
