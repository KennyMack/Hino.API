﻿using Hino.API.Infra.Cross.Utils.Enums;
using Hino.API.Infra.Cross.Utils.Files;
using Microsoft.EntityFrameworkCore;
using System;

namespace Hino.API.Infra.Cross.Utils
{
    public static class ConvertEx
    {
        public static int? ToInt32(this object value)
        {
            if (value == null || value == DBNull.Value)
                return null;

            if (int.TryParse(value.ToString(), out int val))
                return val;

            return null;
        }

        public static short? ToInt16(this object value)
        {
            if (value == null || value == DBNull.Value)
                return null;

            if (short.TryParse(value.ToString(), out short val))
                return val;

            return null;
        }

        public static long? ToInt64(this object value)
        {
            if (value == null || value == DBNull.Value)
                return null;

            if (long.TryParse(value.ToString(), out long val))
                return val;

            return null;
        }

        public static bool ToBoolean(this object value)
        {
            if (value == null || value == DBNull.Value)
                return false;

            if (bool.TryParse(value.ToString(), out bool val))
                return val;

            return val;
        }

        public static decimal? ToDecimal(this object value)
        {
            if (value == null || value == DBNull.Value)
                return null;

            if (decimal.TryParse(value.ToString(), out decimal val))
                return val;

            return null;
        }

        public static sbyte? ToSByte(this object value)
        {
            if (value == null || value == DBNull.Value)
                return null;

            if (sbyte.TryParse(value.ToString(), out sbyte val))
                return val;

            return null;
        }

        public static DateTime? ToDateTime(this object value)
        {
            if (value == null || value == DBNull.Value)
                return null;

            if (DateTime.TryParse(value.ToString(), out DateTime val))
                return val;

            return null;
        }
        /*
        public static EContactSectors ToContactSectors(this int value)
        {
            switch (value)
            {
                case 0:
                    return EContactSectors.Naoinformado;
                case 1:
                    return EContactSectors.Compras;
                case 2:
                    return EContactSectors.Comercial;
                case 3:
                    return EContactSectors.Contabil;
                case 4:
                    return EContactSectors.Contas;
                case 5:
                    return EContactSectors.DepartamentoTecnico;
                case 6:
                    return EContactSectors.Engenharia;
                case 7:
                    return EContactSectors.Financeiro;
                case 8:
                    return EContactSectors.Fiscal;
                case 9:
                    return EContactSectors.Juridico;
                case 10:
                    return EContactSectors.Manutencao;
                case 11:
                    return EContactSectors.Produtivo;
                case 12:
                    return EContactSectors.Qualidade;
                case 13:
                    return EContactSectors.Sac;
                case 14:
                    return EContactSectors.Suporte; 
                case 15:
                    return EContactSectors.TI;
                case 16:
                    return EContactSectors.Vendas;
                case 17:
                    return EContactSectors.NFE;
                case 18:
                    return EContactSectors.Faturamento;
                case 19:
                    return EContactSectors.Cobranca;
                case 20:
                    return EContactSectors.Entrega;
                case 999:
                    return EContactSectors.Outros;
                default:
                    return 0;
            }
        }
        */

        public static EntityState ToEntityState(this EModelDataState value)
        {
            return value switch
            {
                EModelDataState.Detached => EntityState.Detached,
                EModelDataState.Unchanged => EntityState.Unchanged,
                EModelDataState.Added => EntityState.Added,
                EModelDataState.Deleted => EntityState.Deleted,
                EModelDataState.Modified => EntityState.Modified,
                _ => EntityState.Unchanged,
            };
        }

        public static UploadDestinationFolder ToUploadFolder(this string url)
        {
            if (url.ToLower().Contains("product"))
                return UploadDestinationFolder.Products;
            else if (url.ToLower().Contains("order/item"))
                return UploadDestinationFolder.OrderItem;
            else if (url.ToLower().Contains("proposal/item"))
                return UploadDestinationFolder.OrderItem;
            else if (url.ToLower().Contains("orders"))
                return UploadDestinationFolder.Order;
            else if (url.ToLower().Contains("proposals"))
                return UploadDestinationFolder.Order;
            else if (url.ToLower().Contains("enterprise"))
                return UploadDestinationFolder.Enterprises;
            else if (url.ToLower().Contains("user"))
                return UploadDestinationFolder.Users;
            else if (url.ToLower().Contains("establishments"))
                return UploadDestinationFolder.Establishments;

            return UploadDestinationFolder.NotIdentified;
        }
    }
}
