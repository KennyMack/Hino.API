﻿using System;

namespace Hino.API.Infra.Cross.Utils.Files
{
    [Flags]
    public enum UploadDestinationFolder
    {
        NotIdentified = -1,
        Users = 0,
        Products = 1,
        Enterprises = 2,
        Establishments = 3,
        Order = 4,
        OrderItem = 5
    }
}
