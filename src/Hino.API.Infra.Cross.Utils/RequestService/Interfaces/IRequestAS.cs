﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Infra.Cross.Utils.RequestService.Interfaces
{
    public interface IRequestAS
    {
        RestClient CreateClient();
        RestRequest CreateRequest(string pUri, Method pMethod);
    }
}
