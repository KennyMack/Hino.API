﻿using Hino.API.Infra.Cross.Utils.RequestService.Interfaces;
using RestSharp;

namespace Hino.API.Infra.Cross.Utils.RequestService
{
    public abstract class RequestAS : IRequestAS
    {
        public abstract RestClient CreateClient();
        public abstract RestClient CreateClientFind();
        public abstract RestRequest CreateRequest(string pUri, Method pMethod);
    }
}
