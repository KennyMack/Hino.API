﻿using System;

namespace Hino.API.Infra.Cross.Utils
{
    public static class DateUtils
    {
        #region Numero da semana
        public static int MonthWeekNumber(this DateTime date)
        {
            var numSemana = 1;
            var numDias = DateTime.DaysInMonth(date.Year, date.Month);
            var diaSemana = (int)date.DayOfWeek;

            for (int i = 0; i < numDias; i++)
            {
                if (i == date.Day)
                {
                    break;
                }

                diaSemana++;

                if (diaSemana > 7)
                {
                    numSemana++;
                    diaSemana = 1;
                }
            }

            return numSemana;
        }
        #endregion

        #region Primeiro dia do mes
        /// <summary>
        /// Retorna o primeiro dia do mês
        /// </summary>
        /// <param name="date">data</param>
        /// <returns>DateTime</returns>
        public static DateTime FirstDay(this DateTime date)
        {
            return new DateTime(date.Year, date.Month, 1, date.Hour, date.Minute, date.Second, date.Millisecond);
        }
        #endregion

        #region Ultimo dia do mes
        /// <summary>
        /// Retorna o ultimo dia do mês
        /// </summary>
        /// <param name="date">data</param>
        /// <returns>DateTime</returns>
        public static DateTime LastDay(this DateTime date)
        {
            return new DateTime(date.Year, date.Month,
                        DateTime.DaysInMonth(date.Year, date.Month),
                       date.Hour, date.Minute, date.Second, date.Millisecond);
        }
        #endregion

        #region Ultimo hora do dia
        /// <summary>
        /// Retorna a ultima hora dia
        /// </summary>
        /// <param name="date">data</param>
        /// <returns>DateTime</returns>
        public static DateTime LastHour(this DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day,
                       23, 59, 59, 59);
        }
        #endregion

        #region Primeira hora do dia
        /// <summary>
        /// Retorna a primeira hora dia
        /// </summary>
        /// <param name="date">data</param>
        /// <returns>DateTime</returns>
        public static DateTime FirstHour(this DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day,
                        0, 0, 0, 0);
        }
        #endregion

        #region Adiciona Dias Uteis
        public static DateTime AddWorkdays(this DateTime pDate, int pDays)
        {
            int daysToAdd = pDays;
            DateTime dt = pDate;

            while (daysToAdd > 0)
            {
                dt = dt.AddDays(1);

                if (!dt.DayOfWeek.In(DayOfWeek.Saturday, DayOfWeek.Sunday))
                    daysToAdd--;
            }

            return dt;
        }
        #endregion
    }
}
