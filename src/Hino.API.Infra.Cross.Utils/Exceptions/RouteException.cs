﻿using System;

namespace Hino.API.Infra.Cross.Utils.Exceptions
{
    public class RouteException : Exception
    {
        private string _message;
        public override string Message
        {
            get => _message;
        }

        public RouteException(string message)
        {
            _message = message;
        }
    }
}
