﻿using Hino.API.Infra.Cross.Utils.Enums;
using System;
using System.Collections.Generic;

namespace Hino.API.Infra.Cross.Utils.Exceptions
{
    [Serializable]
    public class ModelException
    {
        public int ErrorCode { get; set; }
        public IEnumerable<string> Messages { get; set; }
        public string Field { get; set; }
        public string Value { get; set; }

        public ModelException()
        {
            Messages = new List<string>();
        }

        public static ModelException CreateValidationError(string pMessage, string pField, string pValue) =>
            new()
            {
                ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                Field = pField,
                Value = pValue,
                Messages = new string[] {
                    pMessage
                }
            };

        public static ModelException CreateNotFoundError(string pMessage, string pField, string pValue) =>
            new()
            {
                ErrorCode = (int)EExceptionErrorCodes.RegisterNotFound,
                Field = pField,
                Value = pValue,
                Messages = new string[] {
                    pMessage
                }
            };

        public static ModelException CreateSqlError(Exception e, EExceptionErrorCodes pExceptionCode) =>
            new()
            {
                ErrorCode = (int)pExceptionCode,
                Field = e.HelpLink,
                Value = e.Source,
                Messages = new string[] {
                    e.Message,
                    e?.InnerException?.Message,
                    e?.InnerException?.InnerException?.Message  
                }
            };

        public static ModelException CreateSqlError(Exception e, string pField, string pValue, EExceptionErrorCodes pExceptionCode) =>
            new()
            {
                ErrorCode = (int)pExceptionCode,
                Field = pField,
                Value = pValue,
                Messages = new string[] {
                    e.Message,
                    e?.InnerException?.Message,
                    e?.InnerException?.InnerException?.Message
                }
            };

        public static ModelException CreateSqlError(Exception e, string pField, EExceptionErrorCodes pExceptionCode) =>
            new()
            {
                ErrorCode = (int)pExceptionCode,
                Field = pField,
                Value = "",
                Messages = new string[] {
                    e.Message,
                    e?.InnerException?.Message,
                    e?.InnerException?.InnerException?.Message
                }
            };
    }
}
