﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Infra.Cross.Utils.Settings
{
    public static class AppSettings
    {
        public static bool HasBeenLoaded { get; private set; }
        public static IConfiguration Configuration { get; private set; }
        public static string SecurityKey { get; set; }
        public static string Hino_Id { get; set; }

        public static bool IsDebug { get; set; }
        public static string Audience { get; set; }
        public static string Issuer { get; set; }

        public static string Seconds { get; set; }
        public static string FinalExpiration { get; set; }

        public static EmailAppConfig Email { get; private set; }

        public static RabbitAppConfig Rabbit { get; private set; }

        // public static DataBaseAppConfig DataBase { get; private set; }

        public static AWSAppConfig AWS { get; private set; }

        public static RedisAppConfig Redis { get; private set; }

        public static FirebaseAppConfig Firebase { get; private set; }

        public static string HereMapsKey { get; set; }

        public static string GetConnectionString(string pConnectionName)
        {
            return pConnectionName switch
            {
                _ => Configuration.GetConnectionString("BaseConnection")
            };
        }

        public static string ElasticSearchUrl { get; private set; }

        // public static string PATH_TO_HINO_CONN { get; set; }
        // public static string BaseURL { get; set; }
        // public static string Email { get; set; }
        // public static string Password { get; set; }

        #region Load Settings
        public static void LoadSettingsAPI(IConfiguration settings)
        {
            HasBeenLoaded = true;
            Configuration = settings;
            SecurityKey = settings["SecurityKey"];
            Hino_Id = settings["Hino_Id"];
            IsDebug = ConvertEx.ToBoolean(settings["IsDebug"]);

            Audience = settings["Audience"];
            Issuer = settings["Issuer"];

            Seconds = settings["Seconds"];
            FinalExpiration = settings["FinalExpiration"];

            HereMapsKey = settings["HereMapsKey"];

            ElasticSearchUrl = settings["ElasticConfiguration:Uri"];

            Redis = new RedisAppConfig(settings);
            Email = new EmailAppConfig(settings);
            Rabbit = new RabbitAppConfig(settings);
            // DataBase = new DataBaseAppConfig(settings);
            AWS = new AWSAppConfig(settings);
            Firebase = new FirebaseAppConfig(settings);
        }
        #endregion
    }
}
