﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Infra.Cross.Utils.Settings
{
    public struct RabbitAppConfig
    {
        public RabbitAppConfig(IConfiguration configuration)
        {
            USER = configuration["RABBIT:User"];
            PASS = configuration["RABBIT:Password"];
            URL = configuration["RABBIT:Url"];
            PORT = configuration["RABBIT:Port"];
        }

        public string USER { get; private set; }
        public string PASS { get; private set; }
        public string URL { get; private set; }
        public string PORT { get; private set; }
    }

    public struct EmailAppConfig
    {
        public EmailAppConfig(IConfiguration configuration)
        {
            SMTPHost = configuration["Email:SMTPHost"];
            SMTPPort = configuration["Email:SMTPPort"];
            SMTPUsername = configuration["Email:SMTPUsername"];
            SMTPPassword = configuration["Email:SMTPPassword"];
            UseSSL = configuration["Email:UseSSL"];
            LocalEmail = configuration["Email:LocalEmail"];
        }

        public string SMTPHost { get; private set; }
        public string SMTPPort { get; private set; }
        public string SMTPUsername { get; private set; }
        public string SMTPPassword { get; private set; }
        public string UseSSL { get; private set; }
        public string LocalEmail { get; private set; }
    }

    public struct AWSAppConfig
    {
        public AWSAppConfig(IConfiguration configuration)
        {
            AccessKey = configuration["AWS_AccessKey"];
            SecretAccess = configuration["AWS_SecretAccess"];
            Region = configuration["AWS_Region"];
            Bucket = configuration["AWS_Bucket"];
        }

        public string AccessKey { get; private set; }
        public string SecretAccess { get; private set; }
        public string Region { get; private set; }
        public string Bucket { get; private set; }
    }

    public struct DataBaseAppConfig
    {
        public DataBaseAppConfig(IConfiguration configuration)
        {
            DEFAULTSCHEMA = configuration["Connection:DefaultSchema"];
            UserID = configuration["ORCL_UserID"];
            Password = configuration["ORCL_Password"];
            DataSource = configuration["ORCL_DataSource"];
        }
        public string DEFAULTSCHEMA { get; private set; }
        public string UserID { get; private set; }
        public string Password { get; private set; }
        public string DataSource { get; private set; }
    }

    public struct RedisAppConfig
    {
        public RedisAppConfig(IConfiguration configuration)
        {
            Host = configuration["Redis:Host"];
            Port = configuration["Redis:Port"];
            Password = configuration["Redis:Password"];
            DatabaseID = configuration["Redis:DatabaseID"];
        }

        public string Host { get; private set; }
        public string Port { get; private set; }
        public string Password { get; private set; }
        public string DatabaseID { get; private set; }
    }

    public struct FirebaseAppConfig
    {
        public FirebaseAppConfig(IConfiguration configuration)
        {
            ApiKey = configuration["Jwt:Firebase:ApiKey"];
        }

        public string ApiKey { get; set; }
    }
}
