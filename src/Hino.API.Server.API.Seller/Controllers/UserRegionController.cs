﻿using Hino.API.Application.Interfaces.Services.Seller;
using Hino.API.Application.MapperModels;
using Hino.API.Application.ViewModel.Seller;
using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Seller.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using Hino.API.Infra.Cross.Utils.Paging;
using Hino.API.Server.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.API.Server.API.Seller.Controllers
{
    [Route("api/Seller/Region/{pEstablishmentKey}")]
    [ApiController]
    [Authorize]
    public class UserRegionController : BaseApiController
    {
        private readonly IVEUserRegionAS _IVEUserRegionAS;

        public UserRegionController(IVEUserRegionAS pIVEUserRegionAS)
        {
            _IVEUserRegionAS = pIVEUserRegionAS;

            Services = new IErrorBaseService[]
            {
                _IVEUserRegionAS
            };
        }

        [Route("all")]
        [HttpGet]
        public async Task<IActionResult> GetAll(string pEstablishmentKey)
        {
            try
            {
                var Results =
                    Mapper.Map<PagedResult<VEUserRegion>, PagedResult<VEUserRegionVM>>
                    (
                        await _IVEUserRegionAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                            r => r.EstablishmentKey == pEstablishmentKey,
                            s => s.GEUsers,
                            t => t.VESaleWorkRegion
                        )
                    );

                if (Results == null)
                    throw new HinoException(_IVEUserRegionAS.Errors.FirstOrDefault());

                return RequestOK(Results);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IActionResult> GetById(string pEstablishmentKey, long pId)
        {
            try
            {
                var Result =
                    Mapper.Map<VEUserRegion, VEUserRegionVM>
                    (
                        await _IVEUserRegionAS.FirstOrDefaultAsync(
                            r => r.Id == pId &&
                                 r.EstablishmentKey == pEstablishmentKey,
                            s => s.GEUsers,
                            t => t.VESaleWorkRegion)
                    );

                if (Result == null)
                    return NotFoundRequest();

                return RequestOK(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [Route("user/{pUserId}/all")]
        [HttpGet]
        public async Task<IActionResult> GetByUser(string pEstablishmentKey, long pUserId)
        {
            try
            {
                var Result =
                    Mapper.Map<VEUserRegion, VEUserRegionVM>
                    (
                        await _IVEUserRegionAS.FirstOrDefaultAsync(
                            r => r.UserId == pUserId &&
                                 r.EstablishmentKey == pEstablishmentKey,
                            s => s.GEUsers,
                            t => t.VESaleWorkRegion)
                    );

                if (Result == null)
                    return NotFoundRequest();

                return RequestOK(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [Route("user/{pUser}/cep/{pZipCode}")]
        [HttpGet]
        public IActionResult GetExistsZipCode(string pEstablishmentKey, long pUser, string pZipCode)
        {
            try
            {
                var Result =_IVEUserRegionAS.ZipCodeExists(pEstablishmentKey, pUser, pZipCode);

                return RequestResult(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }


        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        public async Task<IActionResult> Put(string pUniqueKey, long id, [FromBody] VEUserRegionVM pVEUserRegionVM)
        {
            _IVEUserRegionAS.Errors.Clear();
            pVEUserRegionVM.Id = id;
            pVEUserRegionVM.UniqueKey = pUniqueKey;

            ValidateModelState(pVEUserRegionVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pVEUserRegionVM, ModelState);

            try
            {
                var Result = _IVEUserRegionAS.Update(
                    Mapper.Map<VEUserRegionVM, VEUserRegion>(pVEUserRegionVM));

                if (Result != null && !_IVEUserRegionAS.Errors.Any())
                    await _IVEUserRegionAS.SaveChanges();

                return RequestResult(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpPost]
        [Route("save")]
        public async Task<IActionResult> Post(string pEstablishmentKey, [FromBody] VEUserRegionVM pVEUserRegionVM)
        {
            _IVEUserRegionAS.Errors.Clear();
            pVEUserRegionVM.EstablishmentKey = pEstablishmentKey;
            ValidateModelState(pVEUserRegionVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pVEUserRegionVM, ModelState);

            try
            {
                var Result = _IVEUserRegionAS.Add(
                    Mapper.Map<VEUserRegionVM, VEUserRegion>(pVEUserRegionVM));

                if (Result != null && !_IVEUserRegionAS.Errors.Any())
                    await _IVEUserRegionAS.SaveChanges();

                return RequestResult(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        public async Task<IActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IVEUserRegionAS.Errors.Clear();

            try
            {
                var Result = await _IVEUserRegionAS.RemoveById(pId, pEstablishmentKey, pUniqueKey);

                if (Result != null && !_IVEUserRegionAS.Errors.Any())
                    await _IVEUserRegionAS.SaveChanges();

                return RequestResult(Mapper.Map<VEUserRegion, VEUserRegionVM>(Result));
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpPost]
        [Route("list/delete")]
        public async Task<IActionResult> PostDelete(string pEstablishmentKey, [FromBody] VEUserRegionListVM pVEUserRegionVM)
        {
            _IVEUserRegionAS.Errors.Clear();
            ValidateModelState(pVEUserRegionVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pVEUserRegionVM, ModelState);

            try
            {
                var Result = _IVEUserRegionAS.RemoveListAsync(pEstablishmentKey, pVEUserRegionVM.results);

                if (Result != null && !_IVEUserRegionAS.Errors.Any())
                    await _IVEUserRegionAS.SaveChanges();

                return RequestResult(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpPost]
        [Route("list/save")]
        public async Task<IActionResult> PostList(string pEstablishmentKey, [FromBody] VEUserRegionListVM pVEUserRegionVM)
        {
            _IVEUserRegionAS.Errors.Clear();
            ValidateModelState(pVEUserRegionVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pVEUserRegionVM, ModelState);

            try
            {
                var Result = _IVEUserRegionAS.CreateOrUpdateListAsync(pEstablishmentKey, pVEUserRegionVM.results);

                if (Result != null && !_IVEUserRegionAS.Errors.Any())
                    await _IVEUserRegionAS.SaveChanges();

                return RequestResult(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IVEUserRegionAS.Dispose();
            }
        }
    }
}
