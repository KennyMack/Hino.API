using Hino.API.Domain.Establishments.Interfaces.Repositories;
using Hino.API.Domain.Establishments.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using Hino.API.Infra.Data.Repositories.Base.Repositories;
using Hino.API.Infra.Data.Repositories.Establishments.Context;
using System.Linq;
using DefaultMessage = Hino.API.Infra.Cross.Resources.MessagesResource;

namespace Hino.API.Infra.Data.Repositories.Establishments.Repositories
{
    public class GEEstabDevicesRepository : BaseRepository<GEEstabDevices>, IGEEstabDevicesRepository
    {
        public GEEstabDevicesRepository(EstablishmentsDbContext appDbContext) : base(appDbContext)
        {
        }

        public int DevicesCount(long pIdEstablishment, string pEstablishmentKey)
        {
            var result = QueryAsync(r =>
                  r.GEEstabID == pIdEstablishment &&
                  r.EstablishmentKey == pEstablishmentKey &&
                  r.IsActive).Result;
            return result.Count();
        }

        public ModelException ValidateNewDeviceCount(long pGEEstabID, string pEstablishmentKey, int pDevices)
        {
            if ((DevicesCount(pGEEstabID, pEstablishmentKey) + 1) > pDevices)
                return ModelException.CreateValidationError(DefaultMessage.TotalDeviceIsGreater, "Devices", pDevices.ToString());

            return null;
        }

        public ModelException CanChangeDeviceCount(long pGEEstabID, string pEstablishmentKey, int pDevices)
        {
            if (DevicesCount(pGEEstabID, pEstablishmentKey) > pDevices)
                return ModelException.CreateValidationError(DefaultMessage.TotalDeviceIsGreater, "Devices", pDevices.ToString());

            return null;
        }

    }
}
