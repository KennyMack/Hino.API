using Hino.API.Domain.Establishments.Interfaces.Repositories;
using Hino.API.Domain.Establishments.Models;
using Hino.API.Infra.Data.Repositories.Base.Repositories;
using Hino.API.Infra.Data.Repositories.Establishments.Context;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.API.Infra.Data.Repositories.Establishments.Repositories
{
    public class GEEstabMenuRepository : BaseRepository<GEEstabMenu>, IGEEstabMenuRepository
    {
        public GEEstabMenuRepository(EstablishmentsDbContext appDbContext) : base(appDbContext)
        {
        }

        public void SaveEstablishmentMenu(GEEstablishments pEstablishment, IEnumerable<GEEstabMenu> pMenus)
        {
            foreach (var item in pMenus)
            {
                var estabMenu = new GEEstabMenu();
                if (item.Id > 0)
                    estabMenu = GetByIdToUpdate(item.Id, item.EstablishmentKey, item.UniqueKey);

                estabMenu.GEEstabID = pEstablishment.Id;
                estabMenu.EstablishmentKey = pEstablishment.EstablishmentKey;
                estabMenu.Name = item.Name;
                estabMenu.Menu = item.Menu;
                estabMenu.Description = item.Description;
                estabMenu.Visible = item.Visible;

                if (item.Id <= 0)
                    Add(estabMenu);
                else
                {
                    estabMenu.UniqueKey = item.UniqueKey;

                    Update(estabMenu);
                }
            }
        }
    }
}
