using Hino.API.Domain.Establishments.Interfaces.Repositories;
using Hino.API.Domain.Establishments.Models;
using Hino.API.Infra.Data.Repositories.Base;
using Hino.API.Infra.Data.Repositories.Base.Repositories;
using Hino.API.Infra.Data.Repositories.Establishments.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.API.Infra.Data.Repositories.Establishments.Repositories
{
    public class GEEstablishmentsRepository : BaseRepository<GEEstablishments>, IGEEstablishmentsRepository
    {
        private readonly EstablishmentsDbContext AppDbContext;
        public GEEstablishmentsRepository(EstablishmentsDbContext appDbContext) : base(appDbContext)
        {
            AppDbContext = appDbContext;
        }

        public async Task<bool> ExistsEstablishmentAsync(string pEstablishmentKey)
        {
            var Exists = await this.QueryAsync(r =>
                          r.EstablishmentKey == pEstablishmentKey &&
                          r.IsActive);

            return Exists.Any();
        }

        public async Task<IEnumerable<GEEstablishments>> GetByIdAndEstablishmentKeyAsync(long pId, string pEstablishmentKey) =>
            await this.QueryAsync(r =>
                       r.Id == pId &&
                       r.EstablishmentKey == pEstablishmentKey);

        public async Task<bool> HasEstablishmentDependencyAsync(string pEstablishmentKey)
        {
            var sql = @"SELECT SUM(CONTAGEM) CONTAGEM
                          FROM (SELECT COUNT(1) CONTAGEM
                                  FROM GEENTERPRISES
                                 WHERE ESTABLISHMENTKEY = :pESTABLISHMENTKEY1
                                 UNION ALL
                                SELECT COUNT(1) CONTAGEM
                                  FROM GEPRODUCTS
                                 WHERE ESTABLISHMENTKEY = :pESTABLISHMENTKEY2
                                 UNION ALL
                                SELECT COUNT(1) CONTAGEM
                                  FROM GEPAYMENTCONDITION
                                 WHERE ESTABLISHMENTKEY = :pESTABLISHMENTKEY3
                                 UNION ALL  
                                SELECT COUNT(1) CONTAGEM
                                  FROM GEPAYMENTTYPE
                                 WHERE ESTABLISHMENTKEY = :pESTABLISHMENTKEY4
                                 UNION ALL  
                                SELECT COUNT(1) CONTAGEM
                                  FROM VEORDERS
                                 WHERE ESTABLISHMENTKEY = :pESTABLISHMENTKEY5
                                 UNION ALL  
                                SELECT COUNT(1) CONTAGEM
                                 FROM VEORDERITEMS
                                WHERE ESTABLISHMENTKEY = :pESTABLISHMENTKEY6) TBESTAB";

            return Convert.ToInt32(await AppDbContext.RawSqlQueryAsync(sql,
                new Oracle.ManagedDataAccess.Client.OracleParameter("pESTABLISHMENTKEY1", pEstablishmentKey),
                new Oracle.ManagedDataAccess.Client.OracleParameter("pESTABLISHMENTKEY2", pEstablishmentKey),
                new Oracle.ManagedDataAccess.Client.OracleParameter("pESTABLISHMENTKEY3", pEstablishmentKey),
                new Oracle.ManagedDataAccess.Client.OracleParameter("pESTABLISHMENTKEY4", pEstablishmentKey),
                new Oracle.ManagedDataAccess.Client.OracleParameter("pESTABLISHMENTKEY5", pEstablishmentKey),
                new Oracle.ManagedDataAccess.Client.OracleParameter("pESTABLISHMENTKEY6", pEstablishmentKey))) > 0;

        }

        public async Task<IEnumerable<GEEstabDevices>> GetDevicesEstablishment(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            var Establishment = await GetByIdAsync(pId, pEstablishmentKey, pUniqueKey, r => r.GEEstabDevices);

            return Establishment.GEEstabDevices;
        }

        public async Task<GEEstablishments> GetByEstablishmentKeyAsync(string pEstablishmentKey) =>
            (await QueryAsync(r => r.EstablishmentKey == pEstablishmentKey, f => f.FSFiscalOper)).FirstOrDefault();
    }
}
