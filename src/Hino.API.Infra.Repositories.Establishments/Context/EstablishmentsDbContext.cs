﻿using Hino.API.Domain.Establishments.Models;
using Hino.API.Infra.Data.Repositories.Base.Context;
using Hino.API.Infra.Data.Repositories.Establishments.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Infra.Data.Repositories.Establishments.Context
{
    public class EstablishmentsDbContext : BaseDbContext
    {
        public virtual DbSet<GEEstabDevices> GEEstabDevices { get; set; }
        public virtual DbSet<GEEstabFat> GEEstabFat { get; set; }
        public virtual DbSet<GEEstablishments> GEEstablishments { get; set; }
        public virtual DbSet<GEEstabMenu> GEEstabMenu { get; set; }
        public virtual DbSet<GEEstabPay> GEEstabPay { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new GEEstabDevicesConfiguration("GEESTABDEVICES"));
            modelBuilder.ApplyConfiguration(new GEEstabFatConfiguration("GEESTABFAT"));
            modelBuilder.ApplyConfiguration(new GEEstablishmentsConfiguration("GEESTABLISHMENTS"));
            modelBuilder.ApplyConfiguration(new GEEstabMenuConfiguration("GEESTABMENU"));
            modelBuilder.ApplyConfiguration(new GEEstabPayConfiguration("GEESTABPAY"));
        }
    }
}
