﻿using Hino.API.Domain.Establishments.Models;
using Hino.API.Infra.Data.Repositories.Base.Configurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Hino.API.Infra.Data.Repositories.Establishments.EntityConfigurations
{
    public class GEEstabFatConfiguration : BaseEntityConfigurarion<GEEstabFat>
    {
        public GEEstabFatConfiguration(string Table) : base(Table)
        {
        }

        public override void Configure(EntityTypeBuilder<GEEstabFat> config)
        {
            base.Configure(config);

            config.Property(b => b.EstablishmentKey)
                .HasMaxLength(36)
                .IsRequired();
            config.Property(b => b.UniqueKey)
                .HasMaxLength(36)
                .IsRequired();
            config.Property(b => b.EstabId)
                .IsRequired();
            config.Property(b => b.Period)
                .IsRequired();
            config.Property(b => b.Value)
                .HasDefaultValue(0)
                .IsRequired();
            config.Property(b => b.IsActive)
                .HasDefaultValue(true)
                .IsRequired();

            config.HasIndex("EstablishmentKey");
            config.HasIndex("UniqueKey")
              .IsUnique(true);
            config.HasIndex("Period");

            config.HasIndex("IsActive");
        }
    }
}
