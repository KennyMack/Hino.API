﻿using Hino.API.Domain.Establishments.Models;
using Hino.API.Infra.Data.Repositories.Base.Configurations;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Hino.API.Infra.Data.Repositories.Establishments.EntityConfigurations
{
    public class GEEstablishmentsConfiguration : BaseEntityConfigurarion<GEEstablishments>
    {
        public GEEstablishmentsConfiguration(string Table) : base(Table)
        {
        }

        public override void Configure(EntityTypeBuilder<GEEstablishments> config)
        {
        }
    }
}
