﻿using Hino.API.Domain.Establishments.Models;
using Hino.API.Infra.Data.Repositories.Base.Configurations;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Hino.API.Infra.Data.Repositories.Establishments.EntityConfigurations
{
    public class GEEstabMenuConfiguration : BaseEntityConfigurarion<GEEstabMenu>
    {
        public GEEstabMenuConfiguration(string Table) : base(Table)
        {
        }

        public override void Configure(EntityTypeBuilder<GEEstabMenu> config)
        {
        }
    }
}
