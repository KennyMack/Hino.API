﻿using Hino.API.Domain.Base.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.API.Domain.Demograph.Models
{
    public class GEStates : BaseEntity
    {
        public GEStates()
        {
            this.GECities = new HashSet<GECities>();
        }

        public string Name { get; set; }
        public string Initials { get; set; }
        public string IBGE { get; set; }
        public decimal Lat { get; set; }
        public decimal Lng { get; set; }
        public string CodeFIPS { get; set; }

        [ForeignKey("GECountries")]
        public long CountryID { get; set; }
        public long IdERP { get; set; }
        public virtual GECountries GECountries { get; set; }

        public virtual ICollection<GECities> GECities { get; set; }
    }
}
