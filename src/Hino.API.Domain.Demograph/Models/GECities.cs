﻿using Hino.API.Domain.Base.Models;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.API.Domain.Demograph.Models
{
    public class GECities : BaseEntity
    {
        public string Name { get; set; }
        public string IBGE { get; set; }
        public string DDD { get; set; }

        [ForeignKey("GEStates")]
        public long StateID { get; set; }
        public virtual GEStates GEStates { get; set; }

        public long IdERP { get; set; }
    }
}
