﻿using System.Collections.Generic;

namespace Hino.API.Domain.Demograph.Models
{
    public class CountriesLoad
    {
        public CountriesLoad()
        {
            callingCodes = new List<string>();
            latlng = new List<decimal>();
            translations = new CountriesLoadTranslations();
        }

        public string name { get; set; }
        public string alpha2Code { get; set; }
        public string alpha3Code { get; set; }
        public string numericCode { get; set; }
        public string flag { get; set; }
        public List<string> callingCodes { get; set; }
        public List<decimal> latlng { get; set; }
        public CountriesLoadTranslations translations { get; set; }
    }
}
