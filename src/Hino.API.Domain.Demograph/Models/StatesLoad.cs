﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Domain.Demograph.Models
{
    public class StatesLoad
    {
        public StatesLoadBody body { get; set; }
    }

    public class StatesLoadBody
    {
        public StatesLoadBody()
        {
            data = new List<StatesLoadContent>();
        }

        public List<StatesLoadContent> data { get; set; }
    }

    public class StatesLoadContent
    {
        public string countryCode { get; set; }
        public string fipsCode { get; set; }
        public string isoCode { get; set; }
        public string name { get; set; }
        public string wikiDataId { get; set; }
    }
}
