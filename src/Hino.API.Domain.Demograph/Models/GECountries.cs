﻿using Hino.API.Domain.Base.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.API.Domain.Demograph.Models
{
    [Table("GECOUNTRIES")]
    public class GECountries : BaseEntity
    {
        public GECountries()
        {
            this.GEStates = new HashSet<GEStates>();
        }
        public string Initials { get; set; }
        public string Name { get; set; }
        public string BACEN { get; set; }
        public string DDI { get; set; }
        public string LocalName { get; set; }
        public string CIOC { get; set; }
        public string NumericCode { get; set; }
        public decimal Lat { get; set; }
        public decimal Lng { get; set; }
        public string Flag { get; set; }

        public long IdERP { get; set; }

        public virtual ICollection<GEStates> GEStates { get; set; }
    }
}
