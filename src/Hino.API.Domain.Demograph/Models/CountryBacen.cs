﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Domain.Demograph.Models
{
    public class CountryBacen
    {
        public string Bacen { get; set; }
        public string Description { get; set; }

        public static List<CountryBacen> GetContries()
        {
            return new()
            {
                new CountryBacen
                {
                    Bacen = "0132",
                    Description = "Afeganistão"
                },
                new CountryBacen
                {
                    Bacen = "7560",
                    Description = "África do Sul"
                },
                new CountryBacen
                {
                    Bacen = "0175",
                    Description = "Albânia, República da"
                },
                new CountryBacen
                {
                    Bacen = "0230",
                    Description = "Alemanha"
                },
                new CountryBacen
                {
                    Bacen = "0370",
                    Description = "Andorra"
                },
                new CountryBacen
                {
                    Bacen = "0400",
                    Description = "Angola"
                },
                new CountryBacen
                {
                    Bacen = "0418",
                    Description = "Anguilla"
                },
                new CountryBacen
                {
                    Bacen = "0434",
                    Description = "Antigua e Barbuda"
                },
                new CountryBacen
                {
                    Bacen = "0477",
                    Description = "Antilhas Holandesas"
                },
                new CountryBacen
                {
                    Bacen = "0531",
                    Description = "Arábia Saudita"
                },
                new CountryBacen
                {
                    Bacen = "0590",
                    Description = "Argélia"
                },
                new CountryBacen
                {
                    Bacen = "0639",
                    Description = "Argentina"
                },
                new CountryBacen
                {
                    Bacen = "0647",
                    Description = "Armênia, República da"
                },
                new CountryBacen
                {
                    Bacen = "0655",
                    Description = "Aruba"
                },
                new CountryBacen
                {
                    Bacen = "0698",
                    Description = "Austrália"
                },
                new CountryBacen
                {
                    Bacen = "0728",
                    Description = "Áustria"
                },
                new CountryBacen
                {
                    Bacen = "0736",
                    Description = "Azerbaijão, República do"
                },
                new CountryBacen
                {
                    Bacen = "0779",
                    Description = "Bahamas, Ilhas"
                },
                new CountryBacen
                {
                    Bacen = "0809",
                    Description = "Bahrein, Ilhas"
                },
                new CountryBacen
                {
                    Bacen = "0817",
                    Description = "Bangladesh"
                },
                new CountryBacen
                {
                    Bacen = "0833",
                    Description = "Barbados"
                },
                new CountryBacen
                {
                    Bacen = "0850",
                    Description = "Belarus"
                },
                new CountryBacen
                {
                    Bacen = "0876",
                    Description = "Bélgica"
                },
                new CountryBacen
                {
                    Bacen = "0884",
                    Description = "Belize"
                },
                new CountryBacen
                {
                    Bacen = "2291",
                    Description = "Benin"
                },
                new CountryBacen
                {
                    Bacen = "0906",
                    Description = "Bermudas"
                },
                new CountryBacen
                {
                    Bacen = "0973",
                    Description = "Bolívia"
                },
                new CountryBacen
                {
                    Bacen = "0981",
                    Description = "Bósnia-Herzegovina"
                },
                new CountryBacen
                {
                    Bacen = "1015",
                    Description = "Botsuana"
                },
                new CountryBacen
                {
                    Bacen = "1058",
                    Description = "Brasil"
                },
                new CountryBacen
                {
                    Bacen = "1082",
                    Description = "Brunei"
                },
                new CountryBacen
                {
                    Bacen = "1112",
                    Description = "Bulgária, República da"
                },
                new CountryBacen
                {
                    Bacen = "0310",
                    Description = "Burkina Faso"
                },
                new CountryBacen
                {
                    Bacen = "1155",
                    Description = "Burundi"
                },
                new CountryBacen
                {
                    Bacen = "1198",
                    Description = "Butão"
                },
                new CountryBacen
                {
                    Bacen = "1279",
                    Description = "Cabo Verde, República de"
                },
                new CountryBacen
                {
                    Bacen = "1457",
                    Description = "Camarões"
                },
                new CountryBacen
                {
                    Bacen = "1414",
                    Description = "Camboja"
                },
                new CountryBacen
                {
                    Bacen = "1490",
                    Description = "Canadá"
                },
                new CountryBacen
                {
                    Bacen = "1504",
                    Description = "Canal, Ilhas do (Jersey e Guernsey)"
                },
                new CountryBacen
                {
                    Bacen = "1511",
                    Description = "Canárias, Ilhas"
                },
                new CountryBacen
                {
                    Bacen = "1546",
                    Description = "Catar "
                },
                new CountryBacen
                {
                    Bacen = "1376",
                    Description = "Cayman, Ilhas"
                },
                new CountryBacen
                {
                    Bacen = "1538",
                    Description = "Cazaquistão, República do"
                },
                new CountryBacen
                {
                    Bacen = "7889",
                    Description = "Chade"
                },
                new CountryBacen
                {
                    Bacen = "1589",
                    Description = "Chile"
                },
                new CountryBacen
                {
                    Bacen = "1600",
                    Description = "China, República Popular da"
                },
                new CountryBacen
                {
                    Bacen = "1635",
                    Description = "Chipre"
                },
                new CountryBacen
                {
                    Bacen = "5118",
                    Description = "Christmas, Ilha (Navidad)"
                },
                new CountryBacen
                {
                    Bacen = "7412",
                    Description = "Cingapura"
                },
                new CountryBacen
                {
                    Bacen = "1651",
                    Description = "Cocos (Keeling), Ilhas"
                },
                new CountryBacen
                {
                    Bacen = "1694",
                    Description = "Colômbia"
                },
                new CountryBacen
                {
                    Bacen = "1732",
                    Description = "Comores, Ilhas"
                },
                new CountryBacen
                {
                    Bacen = "8885",
                    Description = "Congo, República Democrática do"
                },
                new CountryBacen
                {
                    Bacen = "1775",
                    Description = "Congo, República do"
                },
                new CountryBacen
                {
                    Bacen = "1830",
                    Description = "Cook, Ilhas"
                },
                new CountryBacen
                {
                    Bacen = "1872",
                    Description = "Coréia, Rep. Pop. Democrática da"
                },
                new CountryBacen
                {
                    Bacen = "1902",
                    Description = "Coréia, República da"
                },
                new CountryBacen
                {
                    Bacen = "1937",
                    Description = "Costa do Marfim"
                },
                new CountryBacen
                {
                    Bacen = "1961",
                    Description = "Costa Rica"
                },
                new CountryBacen
                {
                    Bacen = "1988",
                    Description = "Coveite"
                },
                new CountryBacen
                {
                    Bacen = "1953",
                    Description = "Croácia, República da"
                },
                new CountryBacen
                {
                    Bacen = "1996",
                    Description = "Cuba"
                },
                new CountryBacen
                {
                    Bacen = "2321",
                    Description = "Dinamarca"
                },
                new CountryBacen
                {
                    Bacen = "7838",
                    Description = "Djibuti"
                },
                new CountryBacen
                {
                    Bacen = "2356",
                    Description = "Dominica, Ilha"
                },
                new CountryBacen
                {
                    Bacen = "402",
                    Description = "Egito"
                },
                new CountryBacen
                {
                    Bacen = "6874",
                    Description = "El Salvador"
                },
                new CountryBacen
                {
                    Bacen = "2445",
                    Description = "Emirados Árabes Unidos"
                },
                new CountryBacen
                {
                    Bacen = "2399",
                    Description = "Equador"
                },
                new CountryBacen
                {
                    Bacen = "2437",
                    Description = "Eritréia"
                },
                new CountryBacen
                {
                    Bacen = "6289",
                    Description = "Escócia"
                },
                new CountryBacen
                {
                    Bacen = "2470",
                    Description = "Eslovaca, República"
                },
                new CountryBacen
                {
                    Bacen = "2461",
                    Description = "Eslovênia, República da"
                },
                new CountryBacen
                {
                    Bacen = "2453",
                    Description = "Espanha"
                },
                new CountryBacen
                {
                    Bacen = "2496",
                    Description = "Estados Unidos"
                },
                new CountryBacen
                {
                    Bacen = "2518",
                    Description = "Estônia, República da"
                },
                new CountryBacen
                {
                    Bacen = "2534",
                    Description = "Etiópia"
                },
                new CountryBacen
                {
                    Bacen = "2550",
                    Description = "Falkland (Ilhas Malvinas)"
                },
                new CountryBacen
                {
                    Bacen = "2593",
                    Description = "Feroe, Ilhas"
                },
                new CountryBacen
                {
                    Bacen = "8702",
                    Description = "Fiji"
                },
                new CountryBacen
                {
                    Bacen = "2674",
                    Description = "Filipinas"
                },
                new CountryBacen
                {
                    Bacen = "2712",
                    Description = "Finlândia"
                },
                new CountryBacen
                {
                    Bacen = "1619",
                    Description = "Formosa (Taiwan)"
                },
                new CountryBacen
                {
                    Bacen = "2755",
                    Description = "França "
                },
                new CountryBacen
                {
                    Bacen = "2810",
                    Description = "Gabão"
                },
                new CountryBacen
                {
                    Bacen = "6289",
                    Description = "Gales, País de"
                },
                new CountryBacen
                {
                    Bacen = "2852",
                    Description = "Gâmbia"
                },
                new CountryBacen
                {
                    Bacen = "2895",
                    Description = "Gana"
                },
                new CountryBacen
                {
                    Bacen = "2917",
                    Description = "Geórgia, República da"
                },
                new CountryBacen
                {
                    Bacen = "2933",
                    Description = "Gibraltar"
                },
                new CountryBacen
                {
                    Bacen = "6289",
                    Description = "Grã-Bretanha"
                },
                new CountryBacen
                {
                    Bacen = "2976",
                    Description = "Granada"
                },
                new CountryBacen
                {
                    Bacen = "3018",
                    Description = "Grécia"
                },
                new CountryBacen
                {
                    Bacen = "3050",
                    Description = "Groenlândia"
                },
                new CountryBacen
                {
                    Bacen = "3093",
                    Description = "Guadalupe"
                },
                new CountryBacen
                {
                    Bacen = "3131",
                    Description = "Guam"
                },
                new CountryBacen
                {
                    Bacen = "3174",
                    Description = "Guatemala"
                },
                new CountryBacen
                {
                    Bacen = "3379",
                    Description = "Guiana"
                },
                new CountryBacen
                {
                    Bacen = "3255",
                    Description = "Guiana Francesa"
                },
                new CountryBacen
                {
                    Bacen = "3298",
                    Description = "Guiné"
                },
                new CountryBacen
                {
                    Bacen = "3344",
                    Description = "Guiné-Bissau"
                },
                new CountryBacen
                {
                    Bacen = "3310",
                    Description = "Guiné-Equatorial"
                },
                new CountryBacen
                {
                    Bacen = "3417",
                    Description = "Haiti"
                },
                new CountryBacen
                {
                    Bacen = "5738",
                    Description = "Holanda (Países Baixos)"
                },
                new CountryBacen
                {
                    Bacen = "3450",
                    Description = "Honduras"
                },
                new CountryBacen
                {
                    Bacen = "3514",
                    Description = "Hong Kong, Região Adm. Especial"
                },
                new CountryBacen
                {
                    Bacen = "3557",
                    Description = "Hungria, República da"
                },
                new CountryBacen
                {
                    Bacen = "3573",
                    Description = "Iêmen"
                },
                new CountryBacen
                {
                    Bacen = "3611",
                    Description = "Índia"
                },
                new CountryBacen
                {
                    Bacen = "3654",
                    Description = "Indonésia"
                },
                new CountryBacen
                {
                    Bacen = "6289",
                    Description = "Inglaterra"
                },
                new CountryBacen
                {
                    Bacen = "3727",
                    Description = "Irã, República Islâmica do"
                },
                new CountryBacen
                {
                    Bacen = "3697",
                    Description = "Iraque"
                },
                new CountryBacen
                {
                    Bacen = "3751",
                    Description = "Irlanda"
                },
                new CountryBacen
                {
                    Bacen = "6289",
                    Description = "Irlanda do Norte"
                },
                new CountryBacen
                {
                    Bacen = "3794",
                    Description = "Islândia"
                },
                new CountryBacen
                {
                    Bacen = "3832",
                    Description = "Israel"
                },
                new CountryBacen
                {
                    Bacen = "3867",
                    Description = "Itália"
                },
                new CountryBacen
                {
                    Bacen = "3883",
                    Description = "Iugoslávia, República Fed. da"
                },
                new CountryBacen
                {
                    Bacen = "3913",
                    Description = "Jamaica"
                },
                new CountryBacen
                {
                    Bacen = "3999",
                    Description = "Japão"
                },
                new CountryBacen
                {
                    Bacen = "3964",
                    Description = "Johnston, Ilhas"
                },
                new CountryBacen
                {
                    Bacen = "4030",
                    Description = "Jordânia"
                },
                new CountryBacen
                {
                    Bacen = "4111",
                    Description = "Kiribati"
                },
                new CountryBacen
                {
                    Bacen = "4200",
                    Description = "Laos, Rep. Pop. Democrática do"
                },
                new CountryBacen
                {
                    Bacen = "4235",
                    Description = "Lebuan"
                },
                new CountryBacen
                {
                    Bacen = "4260",
                    Description = "Lesoto "
                },
                new CountryBacen
                {
                    Bacen = "4278",
                    Description = "Letônia, República da"
                },
                new CountryBacen
                {
                    Bacen = "4316",
                    Description = "Líbano"
                },
                new CountryBacen
                {
                    Bacen = "4340",
                    Description = "Libéria"
                },
                new CountryBacen
                {
                    Bacen = "4383",
                    Description = "Líbia"
                },
                new CountryBacen
                {
                    Bacen = "4405",
                    Description = "Liechtenstein"
                },
                new CountryBacen
                {
                    Bacen = "4421",
                    Description = "Lituânia, República da"
                },
                new CountryBacen
                {
                    Bacen = "4456",
                    Description = "Luxemburgo"
                },
                new CountryBacen
                {
                    Bacen = "4472",
                    Description = "Macau"
                },
                new CountryBacen
                {
                    Bacen = "4499",
                    Description = "Macedônia"
                },
                new CountryBacen
                {
                    Bacen = "4502",
                    Description = "Madagascar"
                },
                new CountryBacen
                {
                    Bacen = "4525",
                    Description = "Madeira, Ilha da"
                },
                new CountryBacen
                {
                    Bacen = "4553",
                    Description = "Malásia"
                },
                new CountryBacen
                {
                    Bacen = "4588",
                    Description = "Malavi"
                },
                new CountryBacen
                {
                    Bacen = "4618",
                    Description = "Maldivas"
                },
                new CountryBacen
                {
                    Bacen = "4642",
                    Description = "Máli"
                },
                new CountryBacen
                {
                    Bacen = "4677",
                    Description = "Malta"
                },
                new CountryBacen
                {
                    Bacen = "3595",
                    Description = "Man, Ilhas"
                },
                new CountryBacen
                {
                    Bacen = "4723",
                    Description = "Marianas do Norte"
                },
                new CountryBacen
                {
                    Bacen = "4740",
                    Description = "Marrocos"
                },
                new CountryBacen
                {
                    Bacen = "4766",
                    Description = "Marshall, Ilhas"
                },
                new CountryBacen
                {
                    Bacen = "4774",
                    Description = "Martinica"
                },
                new CountryBacen
                {
                    Bacen = "4855",
                    Description = "Maurício"
                },
                new CountryBacen
                {
                    Bacen = "4880",
                    Description = "Mauritânia"
                },
                new CountryBacen
                {
                    Bacen = "4936",
                    Description = "México"
                },
                new CountryBacen
                {
                    Bacen = "0930",
                    Description = "Mianmar (Birmânia)"
                },
                new CountryBacen
                {
                    Bacen = "4995",
                    Description = "Micronésia"
                },
                new CountryBacen
                {
                    Bacen = "4901",
                    Description = "Midway, Ilhas"
                },
                new CountryBacen
                {
                    Bacen = "5053",
                    Description = "Moçambique"
                },
                new CountryBacen
                {
                    Bacen = "4944",
                    Description = "Moldávia, República da"
                },
                new CountryBacen
                {
                    Bacen = "4952",
                    Description = "Mônaco"
                },
                new CountryBacen
                {
                    Bacen = "4979",
                    Description = "Mongólia"
                },
                new CountryBacen
                {
                    Bacen = "5010",
                    Description = "Montserrat, Ilhas"
                },
                new CountryBacen
                {
                    Bacen = "5070",
                    Description = "Namíbia"
                },
                new CountryBacen
                {
                    Bacen = "5088",
                    Description = "Nauru"
                },
                new CountryBacen
                {
                    Bacen = "5177",
                    Description = "Nepal"
                },
                new CountryBacen
                {
                    Bacen = "5215",
                    Description = "Nicarágua"
                },
                new CountryBacen
                {
                    Bacen = "5258",
                    Description = "Niger"
                },
                new CountryBacen
                {
                    Bacen = "5282",
                    Description = "Nigéria"
                },
                new CountryBacen
                {
                    Bacen = "5312",
                    Description = "Niue, Ilha"
                },
                new CountryBacen
                {
                    Bacen = "5355",
                    Description = "Norfolk, Ilha"
                },
                new CountryBacen
                {
                    Bacen = "5380",
                    Description = "Noruega"
                },
                new CountryBacen
                {
                    Bacen = "5428",
                    Description = "Nova Caledônia"
                },
                new CountryBacen
                {
                    Bacen = "5487",
                    Description = "Nova Zelândia "
                },
                new CountryBacen
                {
                    Bacen = "5568",
                    Description = "Omã"
                },
                new CountryBacen
                {
                    Bacen = "5738",
                    Description = "Países Baixos (Holanda)"
                },
                new CountryBacen
                {
                    Bacen = "5754",
                    Description = "Palau"
                },
                new CountryBacen
                {
                    Bacen = "5800",
                    Description = "Panamá"
                },
                new CountryBacen
                {
                    Bacen = "5452",
                    Description = "Papua Nova Guiné"
                },
                new CountryBacen
                {
                    Bacen = "5762",
                    Description = "Paquistão"
                },
                new CountryBacen
                {
                    Bacen = "5860",
                    Description = "Paraguai"
                },
                new CountryBacen
                {
                    Bacen = "5894",
                    Description = "Peru"
                },
                new CountryBacen
                {
                    Bacen = "5932",
                    Description = "Pitcairn, Ilha"
                },
                new CountryBacen
                {
                    Bacen = "5991",
                    Description = "Polinésia Francesa"
                },
                new CountryBacen
                {
                    Bacen = "6033",
                    Description = "Polônia, República da"
                },
                new CountryBacen
                {
                    Bacen = "6114",
                    Description = "Porto Rico"
                },
                new CountryBacen
                {
                    Bacen = "6076",
                    Description = "Portugal"
                },
                new CountryBacen
                {
                    Bacen = "6238",
                    Description = "Quênia"
                },
                new CountryBacen
                {
                    Bacen = "6254",
                    Description = "Quirguiz, República"
                },
                new CountryBacen
                {
                    Bacen = "6289",
                    Description = "Reino Unido"
                },
                new CountryBacen
                {
                    Bacen = "6408",
                    Description = "República Centro-Africana"
                },
                new CountryBacen
                {
                    Bacen = "6475",
                    Description = "República Dominicana"
                },
                new CountryBacen
                {
                    Bacen = "6602",
                    Description = "Reunião, Ilha"
                },
                new CountryBacen
                {
                    Bacen = "6700",
                    Description = "Romênia"
                },
                new CountryBacen
                {
                    Bacen = "6750",
                    Description = "Ruanda"
                },
                new CountryBacen
                {
                    Bacen = "6769",
                    Description = "Rússia"
                },
                new CountryBacen
                {
                    Bacen = "6858",
                    Description = "Saara Ocidental"
                },
                new CountryBacen
                {
                    Bacen = "6777",
                    Description = "Salomão, Ilhas"
                },
                new CountryBacen
                {
                    Bacen = "6904",
                    Description = "Samoa"
                },
                new CountryBacen
                {
                    Bacen = "6912",
                    Description = "Samoa Americana"
                },
                new CountryBacen
                {
                    Bacen = "6971",
                    Description = "San Marino"
                },
                new CountryBacen
                {
                    Bacen = "7102",
                    Description = "Santa Helena"
                },
                new CountryBacen
                {
                    Bacen = "7153",
                    Description = "Santa Lúcia"
                },
                new CountryBacen
                {
                    Bacen = "6955",
                    Description = "São Cristóvão e Neves"
                },
                new CountryBacen
                {
                    Bacen = "7005",
                    Description = "São Pedro e Miquelon"
                },
                new CountryBacen
                {
                    Bacen = "7200",
                    Description = "São Tomé e Príncipe, Ilhas"
                },
                new CountryBacen
                {
                    Bacen = "7056",
                    Description = "São Vicente e Granadinas"
                },
                new CountryBacen
                {
                    Bacen = "7285",
                    Description = "Senegal"
                },
                new CountryBacen
                {
                    Bacen = "7358",
                    Description = "Serra Leoa"
                },
                new CountryBacen
                {
                    Bacen = "7315",
                    Description = "Seychelles"
                },
                new CountryBacen
                {
                    Bacen = "7447",
                    Description = "Síria, República Árabe da"
                },
                new CountryBacen
                {
                    Bacen = "7480",
                    Description = "Somália"
                },
                new CountryBacen
                {
                    Bacen = "7501",
                    Description = "Sri Lanka"
                },
                new CountryBacen
                {
                    Bacen = "7544",
                    Description = "Suazilândia"
                },
                new CountryBacen
                {
                    Bacen = "7595",
                    Description = "Sudão"
                },
                new CountryBacen
                {
                    Bacen = "7641",
                    Description = "Suécia"
                },
                new CountryBacen
                {
                    Bacen = "7676",
                    Description = "Suíça "
                },
                new CountryBacen
                {
                    Bacen = "7706",
                    Description = "Suriname"
                },
                new CountryBacen
                {
                    Bacen = "7722",
                    Description = "Tadjiquistão"
                },
                new CountryBacen
                {
                    Bacen = "7765",
                    Description = "Tailândia"
                },
                new CountryBacen
                {
                    Bacen = "7803",
                    Description = "Tanzânia, República Unida da"
                },
                new CountryBacen
                {
                    Bacen = "7919",
                    Description = "Tcheca, República"
                },
                new CountryBacen
                {
                    Bacen = "7820",
                    Description = "Território Britânico Oc. Índico"
                },
                new CountryBacen
                {
                    Bacen = "7951",
                    Description = "Timor Leste"
                },
                new CountryBacen
                {
                    Bacen = "8001",
                    Description = "Togo"
                },
                new CountryBacen
                {
                    Bacen = "8109",
                    Description = "Tonga"
                },
                new CountryBacen
                {
                    Bacen = "8052",
                    Description = "Toquelau, Ilhas"
                },
                new CountryBacen
                {
                    Bacen = "8150",
                    Description = "Trinidad e Tobago"
                },
                new CountryBacen
                {
                    Bacen = "8206",
                    Description = "Tunísia"
                },
                new CountryBacen
                {
                    Bacen = "8230",
                    Description = "Turcas e Caicos, Ilhas"
                },
                new CountryBacen
                {
                    Bacen = "8249",
                    Description = "Turcomenistão, República do"
                },
                new CountryBacen
                {
                    Bacen = "8273",
                    Description = "Turquia"
                },
                new CountryBacen
                {
                    Bacen = "8281",
                    Description = "Tuvalu"
                },
                new CountryBacen
                {
                    Bacen = "8311",
                    Description = "Ucrânia"
                },
                new CountryBacen
                {
                    Bacen = "8338",
                    Description = "Uganda"
                },
                new CountryBacen
                {
                    Bacen = "8451",
                    Description = "Uruguai"
                },
                new CountryBacen
                {
                    Bacen = "8478",
                    Description = "Uzbequistão, República do"
                },
                new CountryBacen
                {
                    Bacen = "5517",
                    Description = "Vanuatu"
                },
                new CountryBacen
                {
                    Bacen = "8486",
                    Description = "Vaticano, Estado da Cidade do"
                },
                new CountryBacen
                {
                    Bacen = "8508",
                    Description = "Venezuela"
                },
                new CountryBacen
                {
                    Bacen = "8583",
                    Description = "Vietnã"
                },
                new CountryBacen
                {
                    Bacen = "8630",
                    Description = "Virgens, Ilhas (Britânicas)"
                },
                new CountryBacen
                {
                    Bacen = "8664",
                    Description = "Virgens, Ilhas (E.U.A.)"
                },
                new CountryBacen
                {
                    Bacen = "8737",
                    Description = "Wake, Ilha"
                },
                new CountryBacen
                {
                    Bacen = "8753",
                    Description = "Wallis e Futuna, Ilhas"
                },
                new CountryBacen
                {
                    Bacen = "8907",
                    Description = "Zâmbia"
                },
                new CountryBacen
                {
                    Bacen = "6653",
                    Description = "Zimbábue"
                },
                new CountryBacen
                {
                    Bacen = "8958",
                    Description = "Zona do Canal do Panamá"
                }
            };
        }
    }
}
