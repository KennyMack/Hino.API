﻿using Hino.API.Domain.Base.Services;
using Hino.API.Domain.Demograph.Interfaces.Repositories;
using Hino.API.Domain.Demograph.Interfaces.Services;
using Hino.API.Domain.Demograph.Models;
using Hino.API.Infra.Cross.Utils.Enums;
using Hino.API.Infra.Cross.Utils.Exceptions;
using System;
using System.Linq;
using System.Threading.Tasks;
using DefaultMessages = Hino.API.Infra.Cross.Resources.MessagesResource;

namespace Hino.API.Domain.Demograph.Services
{
    public class GECountriesService : BaseService<GECountries>, IGECountriesService
    {
        private readonly IGECountriesRepository _IGECountriesRepository;

        public GECountriesService(IGECountriesRepository pIGECountriesRepository) :
            base(pIGECountriesRepository)
        {
            _IGECountriesRepository = pIGECountriesRepository;
        }

        public override void Remove(GECountries model) =>
            Errors.Add(ModelException.CreateValidationError(DefaultMessages.RemoveNotAllowed, "Id", model.Id.ToString()));

        public override Task<GECountries> RemoveById(long id, string pEstablishmentKey, string pUniqueKey)
        {
            Errors.Add(ModelException.CreateValidationError(DefaultMessages.RemoveNotAllowed, "Id", id.ToString()));

            return Task.FromResult<GECountries>(null);
        }

        public async Task LoadCountriesAsync(CountriesLoad[] pLoad)
        {
            var LocalName = "";
            var Lat = 0M;
            var Lng = 0M;
            var DDI = "";
            var lstCountries = CountryBacen.GetContries();
            foreach (var item in pLoad)
            {
                Lat = 0M;
                Lng = 0M;

                if (item.translations != null)
                    LocalName = (item.translations?.br ?? item.translations?.pt) ?? item.name;

                var bacen = lstCountries.FirstOrDefault(r => r.Description.Contains(LocalName))?.Bacen ?? "";

                try
                {

                    Lat = item.latlng[0];
                    Lng = item.latlng[1];
                }
                catch
                {
                    Lat = 0M;
                    Lng = 0M;
                }

                try
                {
                    DDI = item.callingCodes.First();
                }
                catch
                {
                    DDI = "";
                }

                try
                {
                    _IGECountriesRepository.Add(new GECountries
                    {
                        EstablishmentKey = "85db3229-73a2-4989-b2f3-9caa542443fe",
                        CIOC = item.alpha3Code,
                        Initials = item.alpha2Code,
                        Name = LocalName,
                        LocalName = item.name,
                        Lat = Lat,
                        Lng = Lng,
                        NumericCode = item.numericCode,
                        DDI = DDI,
                        BACEN = bacen.PadLeft(5, '0'),
                        Flag = item.flag
                    });

                }
                catch (Exception ex)
                {
                    Errors.Add(ModelException.CreateSqlError(ex, "GECountries", EExceptionErrorCodes.InsertSQLError));
                }
            }

            if (!Errors.Any())
                await _IGECountriesRepository.SaveChanges();
        }

        public GECountries GetByInitials(string pInitial) =>
            _IGECountriesRepository.GetByInitials(pInitial);
    }
}
