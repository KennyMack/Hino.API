using Hino.API.Domain.Base.Services;
using Hino.API.Domain.Demograph.Interfaces.Repositories;
using Hino.API.Domain.Demograph.Interfaces.Services;
using Hino.API.Domain.Demograph.Models;
using Hino.API.Infra.Cross.Utils.Enums;
using Hino.API.Infra.Cross.Utils.Exceptions;
using System;
using System.Linq;
using System.Threading.Tasks;
using DefaultMessages = Hino.API.Infra.Cross.Resources.MessagesResource;

namespace Hino.API.Domain.Demograph.Services
{
    public class GEStatesService : BaseService<GEStates>, IGEStatesService
    {
        private readonly IGEStatesRepository _IGEStatesRepository;
        private readonly IGECountriesRepository _IGECountriesRepository;

        public GEStatesService(IGEStatesRepository pIGEStatesRepository,
            IGECountriesRepository pIGECountriesRepository) :
             base(pIGEStatesRepository)
        {
            _IGEStatesRepository = pIGEStatesRepository;
            _IGECountriesRepository = pIGECountriesRepository;
        }

        public async Task LoadStatesAsync(StatesLoad[] pLoad)
        {
            foreach (var content in pLoad)
            {
                foreach (var item in content.body.data)
                {
                    try
                    {
                        var country = await _IGECountriesRepository.FirstOrDefaultAsync(r => r.Initials == item.countryCode);

                        _IGEStatesRepository.Add(new GEStates
                        {
                            EstablishmentKey = "85db3229-73a2-4989-b2f3-9caa542443fe",
                            CountryID = country.Id,
                            CodeFIPS = item.fipsCode,
                            Name = item.name,
                            Initials = item.isoCode
                        });

                    }
                    catch (Exception ex)
                    {
                        Errors.Add(ModelException.CreateSqlError(ex, "GECountries", EExceptionErrorCodes.InsertSQLError));
                    }
                }
            }

            if (!Errors.Any())
                await _IGEStatesRepository.SaveChanges();
        }

        public override void Remove(GEStates model) =>
            Errors.Add(ModelException.CreateValidationError(DefaultMessages.RemoveNotAllowed, "Id", model.Id.ToString()));

        public override Task<GEStates> RemoveById(long id, string pEstablishmentKey, string pUniqueKey)
        {
            Errors.Add(ModelException.CreateValidationError(DefaultMessages.RemoveNotAllowed, "Id", id.ToString()));

            return Task.FromResult<GEStates>(null);
        }
    }
}
