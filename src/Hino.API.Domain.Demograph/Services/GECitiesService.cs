using Hino.API.Domain.Base.Services;
using Hino.API.Domain.Demograph.Interfaces.Repositories;
using Hino.API.Domain.Demograph.Interfaces.Services;
using Hino.API.Domain.Demograph.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using System.Threading.Tasks;
using DefaultMessages = Hino.API.Infra.Cross.Resources.MessagesResource;

namespace Hino.API.Domain.Demograph.Services
{
    public class GECitiesService : BaseService<GECities>, IGECitiesService
    {
        private readonly IGECitiesRepository _IGECitiesRepository;

        public GECitiesService(IGECitiesRepository pIGECitiesRepository) :
             base(pIGECitiesRepository)
        {
            _IGECitiesRepository = pIGECitiesRepository;
        }

        public override void Remove(GECities model) =>
            Errors.Add(ModelException.CreateValidationError(DefaultMessages.RemoveNotAllowed, "Id", model.Id.ToString()));

        public override Task<GECities> RemoveById(long id, string pEstablishmentKey, string pUniqueKey)
        {
            Errors.Add(ModelException.CreateValidationError(DefaultMessages.RemoveNotAllowed, "Id", id.ToString()));

            return Task.FromResult<GECities>(null);
        }
    }
}
