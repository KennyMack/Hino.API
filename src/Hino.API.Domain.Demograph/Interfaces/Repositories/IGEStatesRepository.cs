﻿using Hino.API.Domain.Base.Interfaces.Repositories;
using Hino.API.Domain.Demograph.Models;

namespace Hino.API.Domain.Demograph.Interfaces.Repositories
{
    public interface IGEStatesRepository : IBaseRepository<GEStates>
    {
    }
}
