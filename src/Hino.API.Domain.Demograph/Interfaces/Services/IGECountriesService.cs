﻿using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Demograph.Models;
using System.Threading.Tasks;

namespace Hino.API.Domain.Demograph.Interfaces.Services
{
    public interface IGECountriesService : IBaseService<GECountries>
    {
        Task LoadCountriesAsync(CountriesLoad[] pLoad);
        GECountries GetByInitials(string pInitial);
    }
}
