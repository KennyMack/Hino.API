using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Demograph.Models;

namespace Hino.API.Domain.Demograph.Interfaces.Services
{
    public interface IGEStatesService : IBaseService<GEStates>
    {
    }
}
