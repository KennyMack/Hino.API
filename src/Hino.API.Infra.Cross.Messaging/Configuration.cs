﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hino.API.Infra.Cross.Utils;
using Hino.API.Infra.Cross.Utils.Settings;
using Microsoft.Extensions.Configuration;

namespace Hino.API.Infra.Cross.Messaging
{
    public class Configuration
    {
        public string HostName { get; init; }
        public string UserName { get; init; }
        public string Password { get; init; }
        public int Port { get; init; }

        public Configuration()
        {
           
        }

        public static Configuration GetRabbitConfiguration()
        {
            return new Configuration
            {
                HostName = AppSettings.Rabbit.URL,
                UserName = AppSettings.Rabbit.USER,
                Password = AppSettings.Rabbit.PASS,
                Port = ConvertEx.ToInt32(AppSettings.Rabbit.PORT) ?? 15672
            };
        }
    }
}
