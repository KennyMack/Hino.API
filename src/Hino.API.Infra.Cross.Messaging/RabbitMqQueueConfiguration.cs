﻿using Hino.API.Domain.Base.Interfaces.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Infra.Cross.Messaging
{
    public class RabbitMqQueueConfiguration : IQueueConfiguration
    {
        public string Name { get; set; }
        public bool Exclusive { get; set; }
        public bool Durable { get; set; }
        public bool AutoDelete { get; set; }
        public bool Persistent { get; set; }
    }
}
