﻿using Hino.API.Domain.Base.Interfaces.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Infra.Cross.Messaging
{
    public class RabbitMqExchangeConfiguration : IExchangeConfiguration
    {
        public string Name { get; set; }
        public string RoutingKey { get; set; }
        public string Type { get; set; }
        public bool Durable { get; set; }
        public bool AutoDelete { get; set; }
    }
}
