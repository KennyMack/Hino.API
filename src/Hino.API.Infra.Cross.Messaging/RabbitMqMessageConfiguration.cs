﻿using Hino.API.Domain.Base.Interfaces.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Infra.Cross.Messaging
{
    public class RabbitMqMessageConfiguration : IMessageConfiguration
    {
        public IQueueConfiguration QueueConfig { get; set; }
        public IExchangeConfiguration ExchangeConfig { get; set; }

        public static RabbitMqMessageConfiguration GetDefault()
        {
            return new RabbitMqMessageConfiguration
            {
                QueueConfig = new RabbitMqQueueConfiguration
                {
                    Name = "QUEUE",
                    Exclusive = false,
                    AutoDelete = true,
                    Durable = true,
                    Persistent = true
                },
                ExchangeConfig = new RabbitMqExchangeConfiguration
                {
                    AutoDelete = true,
                    Durable = true,
                    Name = "EXCHANGE",
                    RoutingKey = "*",
                    Type = "topic"
                }
            };
        }
    }
}
