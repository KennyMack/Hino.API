﻿using Hino.API.Domain.Base.Interfaces.Messaging;
using Hino.API.Domain.Base.Interfaces.Models;
using Hino.API.Domain.Base.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Xml;

namespace Hino.API.Infra.Cross.Messaging
{
    public class RabbitMqMessagePublisher: IMessageService
    {
        readonly Configuration RabbitBaseConfig;
        readonly ConnectionFactory FactoryConn;
        public RabbitMqMessagePublisher()
        {
            RabbitBaseConfig = Configuration.GetRabbitConfiguration();
            FactoryConn = new ConnectionFactory
            {
                HostName = RabbitBaseConfig.HostName,
                UserName = RabbitBaseConfig.UserName,
                Password = RabbitBaseConfig.Password,
                ContinuationTimeout = new TimeSpan(0, 0, 3, 0),
                RequestedConnectionTimeout = new TimeSpan(0, 0, 3, 0),
                HandshakeContinuationTimeout = new TimeSpan(0, 0, 3, 0),
                SocketReadTimeout = new TimeSpan(0, 0, 3, 0),
                SocketWriteTimeout = new TimeSpan(0, 0, 3, 0)
            };
        }

        public void PublishMessage(IMessageConfiguration Configuration, IQueueItem Item)
        {
            try
            {
                using var conn = FactoryConn.CreateConnection();
                using var channel = conn.CreateModel();
                var exchangeName = Configuration.ExchangeConfig.Name;
                channel.ExchangeDeclare(
                    exchange: exchangeName,
                    type: Configuration.ExchangeConfig.Type,
                    durable: Configuration.ExchangeConfig.Durable, 
                    autoDelete: Configuration.ExchangeConfig.AutoDelete);

                var queue = Configuration.QueueConfig.Name;
                channel.QueueDeclare(
                   queue: queue,
                   durable: Configuration.QueueConfig.Durable,
                   exclusive: Configuration.QueueConfig.Exclusive,
                   autoDelete: Configuration.QueueConfig.AutoDelete,
                   arguments: null);

                channel.QueueBind(
                    queue: queue,
                    exchange: exchangeName,
                    routingKey: Configuration.ExchangeConfig.RoutingKey);

                var properties = channel.CreateBasicProperties();
                properties.Persistent = Configuration.QueueConfig.Persistent;
                properties.DeliveryMode = (byte)(Configuration.QueueConfig.Persistent ? 2 : 1);

                Item.DateTimePublished = DateTime.Now;
                var obj = JsonSerializer.Serialize(Item, typeof(IQueueItem),
                    new JsonSerializerOptions
                    {
                        MaxDepth = 4
                    });
                var body = Encoding.UTF8.GetBytes(obj);

                channel.BasicPublish(
                    exchange: exchangeName,
                    routingKey: Configuration.ExchangeConfig.RoutingKey,
                    basicProperties: properties,
                    body: body);
            }
            catch (Exception e)
            {
                Logging.Exception(e);
            }
        }

        public void PublishMessage(string pRoutingKey, string pQueue, IQueueItem Item)
        {
            try
            {
                var EstablishmentKey = Item.EstablishmentKey;

                using var conn = FactoryConn.CreateConnection();
                using var channel = conn.CreateModel();
                var exchangeName = EstablishmentKey;
                channel.ExchangeDeclare(exchange: exchangeName, 
                    type: "topic", 
                    durable: true, autoDelete: true);

                var queue = $"{EstablishmentKey}_{pQueue}";
                channel.QueueDeclare(
                   queue: queue,
                   durable: true,
                   exclusive: false,
                   autoDelete: true,
                   arguments: null);

                channel.QueueBind(
                    queue: queue,
                    exchange: exchangeName,
                    routingKey: pRoutingKey);

                var properties = channel.CreateBasicProperties();
                properties.Persistent = true;
                properties.DeliveryMode = 2;

                Item.DateTimePublished = DateTime.Now;
                var obj = JsonSerializer.Serialize(Item, typeof(IQueueItem), 
                    new JsonSerializerOptions
                    {
                        MaxDepth = 4
                    });
                var body = Encoding.UTF8.GetBytes(obj);

                channel.BasicPublish(
                    exchange: exchangeName,
                    routingKey: pRoutingKey,
                    basicProperties: properties,
                    body: body);
            }
            catch (Exception e)
            {
                Logging.Exception(e);
            }
        }
    }
}
