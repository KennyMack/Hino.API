﻿using Hino.API.Domain.Demograph.Models;
using Hino.API.Infra.Data.Repositories.Base.Context;
using Hino.API.Infra.Data.Repositories.Demograph.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Infra.Data.Repositories.Demograph.Context
{
    public class DemographDbContext : BaseDbContext
    {
        public virtual DbSet<GECountries> GECountries { get; set; }
        public virtual DbSet<GEStates> GEStates { get; set; }
        public virtual DbSet<GECities> GECities { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new GEStatesConfiguration("GESTATES"));
            modelBuilder.ApplyConfiguration(new GECitiesConfiguration("GECITIES"));
            modelBuilder.ApplyConfiguration(new GECountriesConfiguration("GECOUNTRIES"));
        }
    }
}
