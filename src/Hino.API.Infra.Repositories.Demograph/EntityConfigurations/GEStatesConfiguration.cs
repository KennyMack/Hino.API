﻿using Hino.API.Domain.Demograph.Models;
using Hino.API.Infra.Data.Repositories.Base.Configurations;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Hino.API.Infra.Data.Repositories.Demograph.EntityConfigurations
{
    class GEStatesConfiguration : BaseEntityConfigurarion<GEStates>
    {
        public GEStatesConfiguration(string Table) : base(Table)
        {
        }

        public override void Configure(EntityTypeBuilder<GEStates> config)
        {
        }
    }
}
