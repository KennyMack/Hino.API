﻿using Hino.API.Domain.Demograph.Models;
using Hino.API.Infra.Data.Repositories.Base.Configurations;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Hino.API.Infra.Data.Repositories.Demograph.EntityConfigurations
{
    public class GECitiesConfiguration : BaseEntityConfigurarion<GECities>
    {
        public GECitiesConfiguration(string Table) : base(Table)
        {
        }

        public override void Configure(EntityTypeBuilder<GECities> config)
        {
        }
    }
}
