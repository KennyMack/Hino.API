﻿using Hino.API.Domain.Demograph.Models;
using Hino.API.Infra.Data.Repositories.Base.Configurations;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Hino.API.Infra.Data.Repositories.Demograph.EntityConfigurations
{
    class GECountriesConfiguration : BaseEntityConfigurarion<GECountries>
    {
        public GECountriesConfiguration(string Table) : base(Table)
        {
        }

        public override void Configure(EntityTypeBuilder<GECountries> config)
        {
        }
    }
}
