﻿using Hino.API.Domain.Demograph.Interfaces.Repositories;
using Hino.API.Domain.Demograph.Models;
using Hino.API.Infra.Data.Repositories.Base.Repositories;
using Hino.API.Infra.Data.Repositories.Demograph.Context;

namespace Hino.API.Infra.Data.Repositories.Demograph.Repositories
{
    public class GEStatesRepository : BaseRepository<GEStates>, IGEStatesRepository
    {
        public GEStatesRepository(DemographDbContext appDbContext) : base(appDbContext)
        {

        }
    }
}
