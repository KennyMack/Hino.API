﻿using Hino.API.Domain.Demograph.Interfaces.Repositories;
using Hino.API.Domain.Demograph.Models;
using Hino.API.Infra.Data.Repositories.Base.Repositories;
using Hino.API.Infra.Data.Repositories.Demograph.Context;
using System.Threading.Tasks;

namespace Hino.API.Infra.Data.Repositories.Demograph.Repositories
{
    public class GECountriesRepository : BaseRepository<GECountries>, IGECountriesRepository
    {
        public GECountriesRepository(DemographDbContext appDbContext) : base(appDbContext)
        {
        }

        public GECountries GetByInitials(string pInitial) =>
            Task.Run(async () => await FirstOrDefaultAsync(r => r.Initials == pInitial)).Result;
    }
}
