﻿using Hino.API.Application.Interfaces.Services.Demograph;
using Hino.API.Application.MapperModels;
using Hino.API.Application.ViewModel.Demograph;
using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Demograph.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using Hino.API.Infra.Cross.Utils.Paging;
using Hino.API.Server.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.API.Server.API.Demograph.Controllers
{
    [Route("api/Cities/{pEstablishmentKey}")]
    [ApiController]
    [Authorize]
    public class CitiesController : BaseApiController
    {
        private readonly IGECitiesAS _IGECitiesAS;
        public CitiesController(IGECitiesAS pIGECitiesAS)
        {
            _IGECitiesAS = pIGECitiesAS;

            Services = new IErrorBaseService[]
            {
                _IGECitiesAS
            };
        }

        [Route("all")]
        [HttpGet]
        public async Task<IActionResult> Get(string pEstablishmentKey)
        {
            try
            {
                var filter = GetQueryFilter();
                var Results =
                    Mapper.Map<PagedResult<GECities>, PagedResult<GECitiesVM>>
                    (
                        await _IGECitiesAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                            r => r.EstablishmentKey == pEstablishmentKey
                        )
                    );

                if (Results == null)
                    throw new HinoException(_IGECitiesAS.Errors.FirstOrDefault());

                return RequestOK(Results);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IActionResult> GetById(string pEstablishmentKey, long pId)
        {
            try
            {
                var filter = GetQueryFilter();
                var Results =
                    Mapper.Map<PagedResult<GECities>, PagedResult<GECitiesVM>>
                    (
                        await _IGECitiesAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                            r => r.EstablishmentKey == pEstablishmentKey &&
                                r.Id == pId
                        )
                    );

                if (Results == null)
                    throw new HinoException(_IGECitiesAS.Errors.FirstOrDefault());

                return RequestOK(Results);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IGECitiesAS.Dispose();
            }
        }
    }
}
