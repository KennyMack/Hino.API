﻿using Hino.API.Application.Interfaces.Services.Demograph;
using Hino.API.Application.MapperModels;
using Hino.API.Application.ViewModel.Demograph;
using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Demograph.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using Hino.API.Infra.Cross.Utils.Paging;
using Hino.API.Server.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.API.Server.API.Demograph.Controllers
{
    [Route("api/Countries/{pEstablishmentKey}")]
    [ApiController]
    [Authorize]
    public class CountriesController : BaseApiController
    {
        private readonly IGECountriesAS _IGECountriesAS;
        public CountriesController(IGECountriesAS pIGECountriesAS)
        {
            _IGECountriesAS = pIGECountriesAS;

            Services = new IErrorBaseService[]
            {
                _IGECountriesAS
            };
        }

        [Route("all")]
        [HttpGet]
        public async Task<IActionResult> Get(string pEstablishmentKey)
        {
            try
            {
                var filter = GetQueryFilter();
                var Results =
                    Mapper.Map<PagedResult<GECountries>, PagedResult<GECountriesVM>>
                    (
                        await _IGECountriesAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                            r => r.EstablishmentKey == pEstablishmentKey
                        )
                    );

                if (Results == null)
                    throw new HinoException(_IGECountriesAS.Errors.FirstOrDefault());

                return RequestOK(Results);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IActionResult> GetById(string pEstablishmentKey, long pId)
        {
            try
            {
                var filter = GetQueryFilter();
                var Results =
                    Mapper.Map<PagedResult<GECountries>, PagedResult<GECountriesVM>>
                    (
                        await _IGECountriesAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                            r => r.EstablishmentKey == pEstablishmentKey &&
                                r.Id == pId
                        )
                    );

                if (Results == null)
                    throw new HinoException(_IGECountriesAS.Errors.FirstOrDefault());

                return RequestOK(Results);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [Route("load")]
        [HttpPost]
        public async Task<IActionResult> PostLoadCountries(string pEstablishmentKey, [FromBody] CountriesLoadVM[] loadCountries)
        {
            try
            {
                _IGECountriesAS.Errors.Clear();

                await _IGECountriesAS.LoadCountriesAsync(
                    Mapper.Map<CountriesLoadVM[], CountriesLoad[]>(loadCountries)
                );

                return RequestResult(true);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IGECountriesAS.Dispose();
            }
        }
    }
}
