﻿using Hino.API.Application.Interfaces.Services.Demograph;
using Hino.API.Application.MapperModels;
using Hino.API.Application.ViewModel.Demograph;
using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Demograph.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using Hino.API.Infra.Cross.Utils.Paging;
using Hino.API.Server.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.API.Server.API.Demograph.Controllers
{
    [Route("api/States/{pEstablishmentKey}")]
    [ApiController]
    [Authorize]
    public class StatesController : BaseApiController
    {
        private readonly IGEStatesAS _IGEStatesAS;
        public StatesController(IGEStatesAS pIGEStatesAS)
        {
            _IGEStatesAS = pIGEStatesAS;

            Services = new IErrorBaseService[]
            {
                _IGEStatesAS
            };
        }

        [Route("all")]
        [HttpGet]
        public async Task<IActionResult> Get(string pEstablishmentKey)
        {
            try
            {
                var filter = GetQueryFilter();
                var Results =
                    Mapper.Map<PagedResult<GEStates>, PagedResult<GEStatesVM>>
                    (
                        await _IGEStatesAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                            r => r.EstablishmentKey == pEstablishmentKey
                        )
                    );

                if (Results == null)
                    throw new HinoException(_IGEStatesAS.Errors.FirstOrDefault());

                return RequestOK(Results);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IActionResult> GetById(string pEstablishmentKey, long pId)
        {
            try
            {
                var filter = GetQueryFilter();
                var Results =
                    Mapper.Map<PagedResult<GEStates>, PagedResult<GEStatesVM>>
                    (
                        await _IGEStatesAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                            r => r.EstablishmentKey == pEstablishmentKey &&
                                r.Id == pId
                        )
                    );

                if (Results == null)
                    throw new HinoException(_IGEStatesAS.Errors.FirstOrDefault());

                return RequestOK(Results);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IGEStatesAS.Dispose();
            }
        }
    }
}
