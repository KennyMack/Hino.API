﻿using Hino.API.Application.Interfaces.Services.Customers;
using Hino.API.Application.MapperModels;
using Hino.API.Application.ViewModel.Customers;
using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Customers.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using Hino.API.Infra.Cross.Utils.Paging;
using Hino.API.Server.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.API.Server.API.Customers.Controllers
{
    [Route("api/Groups/{pEstablishmentKey}")]
    [ApiController]
    [Authorize]
    public class EnterpriseGroupController : BaseApiController
    {
        private readonly IGEEnterpriseGroupAS _IGEEnterpriseGroupAS;

        public EnterpriseGroupController(IGEEnterpriseGroupAS pIGEEnterpriseGroupAS)
        {
            _IGEEnterpriseGroupAS = pIGEEnterpriseGroupAS;

            Services = new IErrorBaseService[]
            {
                _IGEEnterpriseGroupAS
            };
        }

        [Route("all")]
        [HttpGet]
        public async Task<IActionResult> GetAll(string pEstablishmentKey)
        {
            try
            {
                var Results =
                    Mapper.Map<PagedResult<GEEnterpriseGroup>, PagedResult<GEEnterpriseGroupVM>>
                    (
                        await _IGEEnterpriseGroupAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                            r => r.EstablishmentKey == pEstablishmentKey
                        )
                    );

                if (Results == null)
                    throw new HinoException(_IGEEnterpriseGroupAS.Errors.FirstOrDefault());

                return RequestOK(Results);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IActionResult> GetById(string pEstablishmentKey, long pId)
        {
            try
            {
                var Result =
                    Mapper.Map<GEEnterpriseGroup, GEEnterpriseGroupVM>
                    (
                        await _IGEEnterpriseGroupAS.FirstOrDefaultAsync(
                            r => r.Id == pId &&
                                 r.EstablishmentKey == pEstablishmentKey)
                    );

                if (Result == null)
                    return NotFoundRequest();

                return RequestOK(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        public async Task<IActionResult> Put(string pUniqueKey, long id, [FromBody] GEEnterpriseGroupVM pGEEnterpriseGroupVM)
        {
            _IGEEnterpriseGroupAS.Errors.Clear();
            pGEEnterpriseGroupVM.Id = id;
            pGEEnterpriseGroupVM.UniqueKey = pUniqueKey;

            ValidateModelState(pGEEnterpriseGroupVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEEnterpriseGroupVM, ModelState);

            try
            {
                var Result = _IGEEnterpriseGroupAS.Add(
                    Mapper.Map<GEEnterpriseGroupVM, GEEnterpriseGroup>(pGEEnterpriseGroupVM));

                if (Result == null || _IGEEnterpriseGroupAS.Errors.Any())
                    throw new HinoException(_IGEEnterpriseGroupAS.Errors.FirstOrDefault());

                await _IGEEnterpriseGroupAS.SaveChanges();

                return RequestResult(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpPost]
        [Route("save")]
        public async Task<IActionResult> Post(string pEstablishmentKey, [FromBody] GEEnterpriseGroupVM pGEEnterpriseGroupVM)
        {
            _IGEEnterpriseGroupAS.Errors.Clear();
            pGEEnterpriseGroupVM.EstablishmentKey = pEstablishmentKey;
            ValidateModelState(pGEEnterpriseGroupVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEEnterpriseGroupVM, ModelState);

            try
            {
                var Result = _IGEEnterpriseGroupAS.Add(
                    Mapper.Map<GEEnterpriseGroupVM, GEEnterpriseGroup>(pGEEnterpriseGroupVM));

                if (Result == null || _IGEEnterpriseGroupAS.Errors.Any())
                    throw new HinoException(_IGEEnterpriseGroupAS.Errors.FirstOrDefault());

                await _IGEEnterpriseGroupAS.SaveChanges();

                return RequestResult(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        public async Task<IActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IGEEnterpriseGroupAS.Errors.Clear();

            try
            {
                var Result = await _IGEEnterpriseGroupAS.RemoveById(pId, pEstablishmentKey, pUniqueKey);

                if (Result == null || _IGEEnterpriseGroupAS.Errors.Any())
                    throw new HinoException(_IGEEnterpriseGroupAS.Errors.FirstOrDefault());

                await _IGEEnterpriseGroupAS.SaveChanges();
                return RequestResult(Mapper.Map<GEEnterpriseGroup, GEEnterpriseGroupVM>(Result));
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IGEEnterpriseGroupAS.Dispose();
            }
        }
    }
}
