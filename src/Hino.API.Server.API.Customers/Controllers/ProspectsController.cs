﻿using Hino.API.Application.Interfaces.Services.Customers;
using Hino.API.Application.MapperModels;
using Hino.API.Application.ViewModel.Customers;
using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Customers.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using Hino.API.Infra.Cross.Utils.Paging;
using Hino.API.Server.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;


namespace Hino.API.Server.API.Customers.Controllers
{
    [Route("api/Prospects/{pEstablishmentKey}")]
    [ApiController]
    [Authorize]
    public class ProspectsController : BaseApiController
    {
        private readonly IGEEnterprisesAS _IGEEnterprisesAS;

        public ProspectsController(IGEEnterprisesAS pIGEEnterprisesAS)
        {
            _IGEEnterprisesAS = pIGEEnterprisesAS;

            Services = new IErrorBaseService[]
            {
                _IGEEnterprisesAS
            };
        }

        [Route("all")]
        [HttpGet]
        public async Task<IActionResult> GetAll(string pEstablishmentKey)
        {
            try
            {
                var Results =
                    Mapper.Map<PagedResult<GEEnterprises>, PagedResult<GEEnterprisesVM>>
                    (
                        await _IGEEnterprisesAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                            r => r.EstablishmentKey == pEstablishmentKey &&
                                r.Classification == Domain.Customers.Enums.EEnterpriseClassification.Prospect
                        // GEEnterprisesVM.GetDefaultFilter(pEstablishmentKey, filter, column)
                        ,
                        s => s.GEPaymentCondition,
                        x => x.GEPaymentCondition.GEPaymentType,
                        g => g.GEEnterpriseGeo,
                        t => t.GEEnterpriseContacts,
                        j => j.GEEnterpriseCategory,
                        o => o.GEEnterpriseGroup,
                        h => h.GEEnterpriseFiscalGroup)
                    );

                if (Results == null)
                    throw new HinoException(_IGEEnterprisesAS.Errors.FirstOrDefault());

                return RequestOK(Results);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IActionResult> GetById(string pEstablishmentKey, long pId)
        {
            try
            {
                var Result =
                    Mapper.Map<GEEnterprises, GEEnterprisesVM>
                    (
                        await _IGEEnterprisesAS.FirstOrDefaultAsync(
                            r => r.Id == pId &&
                                 r.EstablishmentKey == pEstablishmentKey,
                            s => s.GEPaymentCondition,
                            x => x.GEPaymentCondition.GEPaymentType,
                            g => g.GEEnterpriseGeo,
                            t => t.GEEnterpriseContacts,
                            j => j.GEEnterpriseCategory,
                            o => o.GEEnterpriseGroup,
                            h => h.GEEnterpriseFiscalGroup)
                    );

                if (Result == null)
                    return NotFoundRequest();

                return RequestOK(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        public async Task<IActionResult> Put(string pUniqueKey, long id, [FromBody] GEEnterprisesVM pGEEnterprisesVM)
        {
            _IGEEnterprisesAS.Errors.Clear();
            pGEEnterprisesVM.Id = id;
            pGEEnterprisesVM.UniqueKey = pUniqueKey;

            ValidateModelState(pGEEnterprisesVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEEnterprisesVM, ModelState);

            try
            {
                var Result = await _IGEEnterprisesAS.ChangeAsync(pGEEnterprisesVM);

                if (Result == null)
                    throw new HinoException(_IGEEnterprisesAS.Errors.FirstOrDefault());

                return RequestResult(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpPost]
        [Route("save")]
        public async Task<IActionResult> Post(string pEstablishmentKey, [FromBody] GEEnterprisesVM pGEEnterprisesVM)
        {
            _IGEEnterprisesAS.Errors.Clear();
            pGEEnterprisesVM.EstablishmentKey = pEstablishmentKey;

            ValidateModelState(pGEEnterprisesVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEEnterprisesVM, ModelState);

            try
            {
                var Result = await _IGEEnterprisesAS.CreateAsync(pGEEnterprisesVM);

                if (Result == null)
                    throw new HinoException(_IGEEnterprisesAS.Errors.FirstOrDefault());

                return RequestResult(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        public async Task<IActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IGEEnterprisesAS.Errors.Clear();

            try
            {
                var Result = await _IGEEnterprisesAS.RemoveById(pId, pEstablishmentKey, pUniqueKey);

                return RequestResult(Mapper.Map<GEEnterprises, GEEnterprisesVM>(Result));
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/convert")]
        public async Task<IActionResult> PutConvert(string pEstablishmentKey, string pUniqueKey, long id)
        {
            try
            {
                _IGEEnterprisesAS.Errors.Clear();

                var Enterprise = await _IGEEnterprisesAS.UpdateToClient(
                    pEstablishmentKey, 
                    pUniqueKey, id);

                if (Enterprise == null)
                    return NotFoundRequest();

                if (_IGEEnterprisesAS.Errors.Any())
                    return InvalidRequest(Enterprise, _IGEEnterprisesAS.Errors);

                return RequestOK(Enterprise);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IGEEnterprisesAS.Dispose();
            }
        }
    }
}
