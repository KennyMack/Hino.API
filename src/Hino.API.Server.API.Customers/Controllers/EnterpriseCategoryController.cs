﻿using Hino.API.Application.Interfaces.Services.Customers;
using Hino.API.Application.MapperModels;
using Hino.API.Application.ViewModel.Customers;
using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Customers.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using Hino.API.Infra.Cross.Utils.Paging;
using Hino.API.Server.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.API.Server.API.Customers.Controllers
{
    [Route("api/Categories/{pEstablishmentKey}")]
    [ApiController]
    [Authorize]
    public class EnterpriseCategoryController : BaseApiController
    {
        private readonly IGEEnterpriseCategoryAS _IGEEnterpriseCategoryAS;

        public EnterpriseCategoryController(IGEEnterpriseCategoryAS pIGEEnterpriseCategoryAS)
        {
            _IGEEnterpriseCategoryAS = pIGEEnterpriseCategoryAS;

            Services = new IErrorBaseService[]
            {
                _IGEEnterpriseCategoryAS
            };
        }

        [Route("all")]
        [HttpGet]
        public async Task<IActionResult> GetAll(string pEstablishmentKey)
        {
            try
            {
                var Results =
                    Mapper.Map<PagedResult<GEEnterpriseCategory>, PagedResult<GEEnterpriseCategoryVM>>
                    (
                        await _IGEEnterpriseCategoryAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                            r => r.EstablishmentKey == pEstablishmentKey
                        )
                    );

                if (Results == null)
                    throw new HinoException(_IGEEnterpriseCategoryAS.Errors.FirstOrDefault());

                return RequestOK(Results);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IActionResult> GetById(string pEstablishmentKey, long pId)
        {
            try
            {
                var Result =
                    Mapper.Map<GEEnterpriseCategory, GEEnterpriseCategoryVM>
                    (
                        await _IGEEnterpriseCategoryAS.FirstOrDefaultAsync(
                            r => r.Id == pId &&
                                 r.EstablishmentKey == pEstablishmentKey)
                    );

                if (Result == null)
                    return NotFoundRequest();

                return RequestOK(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        public async Task<IActionResult> Put(string pUniqueKey, long id, [FromBody] GEEnterpriseCategoryVM pGEEnterpriseCategoryVM)
        {
            _IGEEnterpriseCategoryAS.Errors.Clear();
            pGEEnterpriseCategoryVM.Id = id;
            pGEEnterpriseCategoryVM.UniqueKey = pUniqueKey;

            ValidateModelState(pGEEnterpriseCategoryVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEEnterpriseCategoryVM, ModelState);

            try
            {
                var Result = _IGEEnterpriseCategoryAS.Add(
                    Mapper.Map<GEEnterpriseCategoryVM, GEEnterpriseCategory>(pGEEnterpriseCategoryVM));

                if (Result == null || _IGEEnterpriseCategoryAS.Errors.Any())
                    throw new HinoException(_IGEEnterpriseCategoryAS.Errors.FirstOrDefault());

                await _IGEEnterpriseCategoryAS.SaveChanges();

                return RequestResult(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpPost]
        [Route("save")]
        public async Task<IActionResult> Post(string pEstablishmentKey, [FromBody] GEEnterpriseCategoryVM pGEEnterpriseCategoryVM)
        {
            _IGEEnterpriseCategoryAS.Errors.Clear();
            pGEEnterpriseCategoryVM.EstablishmentKey = pEstablishmentKey;
            ValidateModelState(pGEEnterpriseCategoryVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEEnterpriseCategoryVM, ModelState);

            try
            {
                var Result = _IGEEnterpriseCategoryAS.Add(
                    Mapper.Map<GEEnterpriseCategoryVM, GEEnterpriseCategory>(pGEEnterpriseCategoryVM));

                if (Result == null || _IGEEnterpriseCategoryAS.Errors.Any())
                    throw new HinoException(_IGEEnterpriseCategoryAS.Errors.FirstOrDefault());

                await _IGEEnterpriseCategoryAS.SaveChanges();

                return RequestResult(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        public async Task<IActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IGEEnterpriseCategoryAS.Errors.Clear();

            try
            {
                var Result = await _IGEEnterpriseCategoryAS.RemoveById(pId, pEstablishmentKey, pUniqueKey);

                if (Result == null || _IGEEnterpriseCategoryAS.Errors.Any())
                    throw new HinoException(_IGEEnterpriseCategoryAS.Errors.FirstOrDefault());

                await _IGEEnterpriseCategoryAS.SaveChanges();
                return RequestResult(Mapper.Map<GEEnterpriseCategory, GEEnterpriseCategoryVM>(Result));
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IGEEnterpriseCategoryAS.Dispose();
            }
        }
    }
}
