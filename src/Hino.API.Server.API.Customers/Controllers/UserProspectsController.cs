﻿using Hino.API.Application.Interfaces.Services.Customers;
using Hino.API.Application.MapperModels;
using Hino.API.Application.ViewModel.Customers;
using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Customers.Enums;
using Hino.API.Domain.Customers.Models;
using Hino.API.Infra.Cross.Utils.Paging;
using Hino.API.Server.Base;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Hino.API.Server.API.Customers.Controllers
{
    [Route("api/Prospects/{pEstablishmentKey}")]
    [ApiController]
    public class UserProspectsController : BaseApiController
    {
        private readonly IGEEnterprisesAS _IGEEnterprisesAS;
        private readonly IGEUserEnterprisesAS _IGEUserEnterprisesAS;

        public UserProspectsController(IGEEnterprisesAS pIGEEnterprisesAS,
            IGEUserEnterprisesAS pIGEUserEnterprisesAS)
        {
            _IGEUserEnterprisesAS = pIGEUserEnterprisesAS;
            _IGEEnterprisesAS = pIGEEnterprisesAS;

            Services = new IErrorBaseService[]
            {
                _IGEEnterprisesAS,
                _IGEUserEnterprisesAS
            };
        }

        [Route("user/{pUserId}/all")]
        [HttpGet]
        public async Task<IActionResult> GetProspectsByUserId(string pEstablishmentKey, long pUserId)
        {
            try
            {
                var UserResults = await _IGEUserEnterprisesAS.QueryPagedAsync(
                    GetPageNumber(), GetLimitNumber(),
                    r => (r.EstablishmentKey == pEstablishmentKey &&
                          r.UserId == pUserId &&
                          r.GEEnterprises.EstablishmentKey == pEstablishmentKey &&
                          r.GEEnterprises.Classification == EEnterpriseClassification.Prospect),
                    s => s.GEEnterprises.GEPaymentCondition,
                    x => x.GEEnterprises.GEPaymentCondition.GEPaymentType,
                    g => g.GEEnterprises.GEEnterpriseGeo,
                    t => t.GEEnterprises.GEEnterpriseContacts,
                    j => j.GEEnterprises.GEEnterpriseCategory,
                    o => o.GEEnterprises.GEEnterpriseGroup,
                    h => h.GEEnterprises.GEEnterpriseFiscalGroup
                );

                var Results = new PagedResult<GEEnterprises>
                {
                    CurrentPage = UserResults.CurrentPage,
                    PageCount = UserResults.PageCount,
                    PageSize = UserResults.PageSize,
                    RowCount = UserResults.RowCount
                };

                foreach (var item in UserResults.Results)
                    Results.Results.Add(item.GEEnterprises);

                if (Results == null)
                    return InvalidRequest(null, _IGEEnterprisesAS.Errors);

                return RequestOK(Mapper.Map<PagedResult<GEEnterprises>, PagedResult<GEEnterprisesVM>>(Results));
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IGEUserEnterprisesAS.Dispose();
                _IGEEnterprisesAS.Dispose();
            }
        }
    }
}
