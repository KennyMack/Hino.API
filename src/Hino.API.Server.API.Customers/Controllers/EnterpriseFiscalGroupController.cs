﻿using Hino.API.Application.Interfaces.Services.Customers;
using Hino.API.Application.MapperModels;
using Hino.API.Application.ViewModel.Customers;
using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Customers.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using Hino.API.Infra.Cross.Utils.Paging;
using Hino.API.Server.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.API.Server.API.Customers.Controllers
{
    [Route("api/Fiscal/Groups/{pEstablishmentKey}")]
    [ApiController]
    [Authorize]
    public class EnterpriseFiscalGroupController : BaseApiController
    {
        private readonly IGEEnterpriseFiscalGroupAS _GEEnterpriseFiscalGroupAS;

        public EnterpriseFiscalGroupController(IGEEnterpriseFiscalGroupAS pIGEEnterpriseFiscalGroupAS)
        {
            _GEEnterpriseFiscalGroupAS = pIGEEnterpriseFiscalGroupAS;

            Services = new IErrorBaseService[]
            {
                _GEEnterpriseFiscalGroupAS
            };
        }

        [Route("all")]
        [HttpGet]
        public async Task<IActionResult> GetAll(string pEstablishmentKey)
        {
            try
            {
                var Results =
                    Mapper.Map<PagedResult<GEEnterpriseFiscalGroup>, PagedResult<GEEnterpriseFiscalGroupVM>>
                    (
                        await _GEEnterpriseFiscalGroupAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                            r => r.EstablishmentKey == pEstablishmentKey
                        )
                    );

                if (Results == null)
                    throw new HinoException(_GEEnterpriseFiscalGroupAS.Errors.FirstOrDefault());

                return RequestOK(Results);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IActionResult> GetById(string pEstablishmentKey, long pId)
        {
            try
            {
                var Result =
                    Mapper.Map<GEEnterpriseFiscalGroup, GEEnterpriseFiscalGroupVM>
                    (
                        await _GEEnterpriseFiscalGroupAS.FirstOrDefaultAsync(
                            r => r.Id == pId &&
                                 r.EstablishmentKey == pEstablishmentKey)
                    );

                if (Result == null)
                    return NotFoundRequest();

                return RequestOK(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        public async Task<IActionResult> Put(string pUniqueKey, long id, [FromBody] GEEnterpriseFiscalGroupVM pGEEnterpriseFiscalGroupVM)
        {
            _GEEnterpriseFiscalGroupAS.Errors.Clear();
            pGEEnterpriseFiscalGroupVM.Id = id;
            pGEEnterpriseFiscalGroupVM.UniqueKey = pUniqueKey;

            ValidateModelState(pGEEnterpriseFiscalGroupVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEEnterpriseFiscalGroupVM, ModelState);

            try
            {
                var Result = _GEEnterpriseFiscalGroupAS.Add(
                    Mapper.Map<GEEnterpriseFiscalGroupVM, GEEnterpriseFiscalGroup>(pGEEnterpriseFiscalGroupVM));

                if (Result == null || _GEEnterpriseFiscalGroupAS.Errors.Any())
                    throw new HinoException(_GEEnterpriseFiscalGroupAS.Errors.FirstOrDefault());

                await _GEEnterpriseFiscalGroupAS.SaveChanges();

                return RequestResult(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpPost]
        [Route("save")]
        public async Task<IActionResult> Post(string pEstablishmentKey, [FromBody] GEEnterpriseFiscalGroupVM pGEEnterpriseFiscalGroupVM)
        {
            _GEEnterpriseFiscalGroupAS.Errors.Clear();
            pGEEnterpriseFiscalGroupVM.EstablishmentKey = pEstablishmentKey;
            ValidateModelState(pGEEnterpriseFiscalGroupVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEEnterpriseFiscalGroupVM, ModelState);

            try
            {
                var Result = _GEEnterpriseFiscalGroupAS.Add(
                    Mapper.Map<GEEnterpriseFiscalGroupVM, GEEnterpriseFiscalGroup>(pGEEnterpriseFiscalGroupVM));

                if (Result == null || _GEEnterpriseFiscalGroupAS.Errors.Any())
                    throw new HinoException(_GEEnterpriseFiscalGroupAS.Errors.FirstOrDefault());

                await _GEEnterpriseFiscalGroupAS.SaveChanges();

                return RequestResult(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        public async Task<IActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _GEEnterpriseFiscalGroupAS.Errors.Clear();

            try
            {
                var Result = await _GEEnterpriseFiscalGroupAS.RemoveById(pId, pEstablishmentKey, pUniqueKey);

                if (Result == null || _GEEnterpriseFiscalGroupAS.Errors.Any())
                    throw new HinoException(_GEEnterpriseFiscalGroupAS.Errors.FirstOrDefault());

                await _GEEnterpriseFiscalGroupAS.SaveChanges();
                return RequestResult(Mapper.Map<GEEnterpriseFiscalGroup, GEEnterpriseFiscalGroupVM>(Result));
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                _GEEnterpriseFiscalGroupAS.Dispose();
            }
        }
    }
}

