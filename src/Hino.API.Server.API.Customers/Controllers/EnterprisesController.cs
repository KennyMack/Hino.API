﻿using Hino.API.Application.Integrations.Enterprises;
using Hino.API.Application.Interfaces.Services.Customers;
using Hino.API.Application.Interfaces.Services.Establishments;
using Hino.API.Application.MapperModels;
using Hino.API.Application.ViewModel.Customers;
using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Customers.Enums;
using Hino.API.Domain.Customers.Models;
using Hino.API.Infra.Cross.Utils;
using Hino.API.Infra.Cross.Utils.Exceptions;
using Hino.API.Infra.Cross.Utils.Paging;
using Hino.API.Server.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.API.Server.API.Customers.Controllers
{
    [Route("api/Enterprises/{pEstablishmentKey}")]
    [ApiController]
    [Authorize]
    public class EnterprisesController : BaseApiController
    {
        private readonly IGEEnterprisesAS _IGEEnterprisesAS;
        private readonly IGEEstablishmentsAS _IGEEstablishmentsAS;
        private readonly IEnterpriseSearchAS _IEnterpriseSearchAS;

        public EnterprisesController(IGEEnterprisesAS pIGEEnterprisesAS,
             IEnterpriseSearchAS pIEnterpriseSearchAS,
             IGEEstablishmentsAS pIGEEstablishmentsAS)
        {
            _IGEEstablishmentsAS = pIGEEstablishmentsAS;
            _IGEEnterprisesAS = pIGEEnterprisesAS;
            _IEnterpriseSearchAS = pIEnterpriseSearchAS;

            Services = new IErrorBaseService[]
            {
                _IGEEnterprisesAS
            };
        }

        [Route("all")]
        [HttpGet]
        public async Task<IActionResult> GetAll(string pEstablishmentKey)
        {
            try
            {
                var Results =
                    Mapper.Map<PagedResult<GEEnterprises>, PagedResult<GEEnterprisesVM>>
                    (
                        await _IGEEnterprisesAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                            r => r.EstablishmentKey == pEstablishmentKey
                        // GEEnterprisesVM.GetDefaultFilter(pEstablishmentKey, filter, column)
                        ,
                        s => s.GEPaymentCondition,
                        x => x.GEPaymentCondition.GEPaymentType,
                        g => g.GEEnterpriseGeo,
                        t => t.GEEnterpriseContacts,
                        j => j.GEEnterpriseCategory,
                        o => o.GEEnterpriseGroup,
                        h => h.GEEnterpriseFiscalGroup)
                    );

                if (Results == null)
                    throw new HinoException(_IGEEnterprisesAS.Errors.FirstOrDefault());

                return RequestOK(Results);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IActionResult> GetById(string pEstablishmentKey, long pId)
        {
            try
            {
                var Result =
                    Mapper.Map<GEEnterprises, GEEnterprisesVM>
                    (
                        await _IGEEnterprisesAS.FirstOrDefaultAsync(
                            r => r.Id == pId &&
                                 r.EstablishmentKey == pEstablishmentKey,
                            s => s.GEPaymentCondition,
                            x => x.GEPaymentCondition.GEPaymentType,
                            g => g.GEEnterpriseGeo,
                            t => t.GEEnterpriseContacts,
                            j => j.GEEnterpriseCategory,
                            o => o.GEEnterpriseGroup,
                            h => h.GEEnterpriseFiscalGroup)
                    );

                if (Result == null)
                    return NotFoundRequest();

                return RequestOK(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpGet]
        [Route("search/cnpj/{pCNPJ}")]
        public async Task<IActionResult> SearchEnterpriseData(string pEstablishmentKey, string pCNPJ)
        {
            try
            {
                _IGEEnterprisesAS.Errors.Clear();

                var Establishment = await _IGEEstablishmentsAS.GetByEstablishmentKeyAsync(pEstablishmentKey);

                var Result = await _IEnterpriseSearchAS.GetEnterpriseAsync(pCNPJ, Establishment.TokenCNPJ);

                if (Result == null)
                    return NotFoundRequest();

                var EnterpriseNew = new GEEnterprisesVM
                {
                    Id = 0,
                    NomeFantasia = Result.nomefantasia.ToLower().ToTitleCase(),
                    RazaoSocial = Result.nome.ToLower().ToTitleCase(),
                    CNPJCPF = Result.cnpj,
                    IE = "N/INFO",
                    Type = EEnterpriseType.PessoaJuridica,
                    Status = EStatusEnterprise.New,
                    StatusSinc = EStatusSinc.Waiting,
                    Created = DateTime.Now,
                    Modified = DateTime.Now,
                    IsActive = true,
                    EstablishmentKey = pEstablishmentKey
                };

                var Address = Result.Address.CEPAddress;

                if (Address.ZipCode != null)
                {
                    EnterpriseNew.GEEnterpriseGeo.Add(new GEEnterpriseGeoVM
                    {
                        Created = DateTime.Now,
                        Modified = DateTime.Now,
                        IsActive = true,
                        EstablishmentKey = pEstablishmentKey,
                        Type = EAddressType.Commercial,
                        EnterpriseId = 0,
                        IE = EnterpriseNew.IE,
                        CNPJCPF = EnterpriseNew.CNPJCPF,
                        Phone = Result.primeirotelefone,
                        Email = Result.email,
                        Address = Address.Street,
                        Complement = Address.Complement,
                        District = Address.District,
                        Num = Result.numero,
                        ZipCode = Address.ZipCode,
                        CountryIni = Result.Address.Country.CIOC,
                        CountryCode = Result.Address.Country.NumericCode,
                        CountryName = Result.Address.Country.Name,
                        UF = Result.Address.uf,
                        StateName = Result.Address.MapResult.Address.StateName,
                        IBGE = Result.Address.ibge,
                        DisplayLat = 0,
                        DisplayLng = 0,
                        NavLat = 0,
                        NavLng = 0,
                        CityName = Result.Address.MapResult.Address.City
                    });
                }

                if (_IGEEnterprisesAS.Errors.Any())
                    return InvalidRequest(pCNPJ, _IGEEnterprisesAS.Errors);

                if (Result != null)
                    return RequestOK(EnterpriseNew);

                return InvalidRequest(pCNPJ, Infra.Cross.Resources.ValidationMessagesResource.InvalidCNPJ);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IGEEnterprisesAS.Dispose();
            }
        }
    }
}
