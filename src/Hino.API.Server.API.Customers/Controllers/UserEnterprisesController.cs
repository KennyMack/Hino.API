﻿using Hino.API.Application.Interfaces.Services.Customers;
using Hino.API.Application.MapperModels;
using Hino.API.Application.ViewModel.Customers;
using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Customers.Models;
using Hino.API.Infra.Cross.Utils.Paging;
using Hino.API.Server.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.API.Server.API.Customers.Controllers
{
    [Route("api/User/Enterprises/{pEstablishmentKey}")]
    [ApiController]
    [Authorize]
    public class UserEnterprisesController : BaseApiController
    {
        private readonly IGEUserEnterprisesAS _IGEUserEnterprisesAS;
        public UserEnterprisesController(IGEUserEnterprisesAS pIGEUserEnterprisesAS)
        {
            _IGEUserEnterprisesAS = pIGEUserEnterprisesAS;

            Services = new IErrorBaseService[]
            {
                _IGEUserEnterprisesAS
            };
        }

        [Route("user/{pUserId}/all")]
        [HttpGet]
        public async Task<IActionResult> GetByUser(string pEstablishmentKey, long pUserId)
        {
            var filter = GetQueryFilter();

            var Results =
                Mapper.Map<PagedResult<GEUserEnterprises>, PagedResult<GEUserEnterprisesVM>>
                (
                await _IGEUserEnterprisesAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.EstablishmentKey == pEstablishmentKey &&
                     r.UserId == pUserId &&
                   (
                     (
                        (r.Id.ToString().Contains(filter)) ||
                        (r.UserId.ToString().Contains(filter)) ||
                        (r.EnterpriseId.ToString().Contains(filter))
                     ) ||
                     (filter == "")
                   ),
                s => s.GEEnterprises,
                t => t.GEUsers));

            if (Results == null)
                return InvalidRequest(null, _IGEUserEnterprisesAS.Errors);

            return RequestOK(Results);
        }

        [Route("all")]
        [HttpGet]
        public async Task<IActionResult> Get(string pEstablishmentKey)
        {
            var compareInfo = CultureInfo.InvariantCulture.CompareInfo;
            var filter = GetQueryFilter();

            var Results =
                Mapper.Map<PagedResult<GEUserEnterprises>, PagedResult<GEUserEnterprisesVM>>
                (
                await _IGEUserEnterprisesAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.EstablishmentKey == pEstablishmentKey &&
                   (
                     (
                        (r.Id.ToString().Contains(filter)) ||
                        (r.UserId.ToString().Contains(filter)) ||
                        (r.EnterpriseId.ToString().Contains(filter))
                     ) ||
                     (filter == "")
                   ),
                s => s.GEEnterprises,
                t => t.GEUsers));

            if (Results == null)
                return InvalidRequest(null, _IGEUserEnterprisesAS.Errors);

            return RequestOK(Results);
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IActionResult> GetById(string pEstablishmentKey, long pId)
        {
            var Results =
                Mapper.Map<PagedResult<GEUserEnterprises>, PagedResult<GEUserEnterprisesVM>>
                (
                await _IGEUserEnterprisesAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.Id == pId &&
                r.EstablishmentKey == pEstablishmentKey));

            if (Results == null)
                return InvalidRequest(null, _IGEUserEnterprisesAS.Errors);

            if (Results.Results.Count <= 0)
                return NotFoundRequest();

            return RequestOK(Results);
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        public async Task<IActionResult> Put(string pUniqueKey, long id, GEUserEnterprisesVM pGEUserEnterprisesVM)
        {
            _IGEUserEnterprisesAS.Errors.Clear();
            pGEUserEnterprisesVM.Id = id;
            pGEUserEnterprisesVM.UniqueKey = pUniqueKey;

            ValidateModelState(pGEUserEnterprisesVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEUserEnterprisesVM, ModelState);

            var Enterprise = _IGEUserEnterprisesAS.Update(Mapper.Map<GEUserEnterprisesVM, GEUserEnterprises>(pGEUserEnterprisesVM));

            if (_IGEUserEnterprisesAS.Errors.Count > 0)
                return InvalidRequest(Enterprise, _IGEUserEnterprisesAS.Errors);
            else
                await _IGEUserEnterprisesAS.SaveChanges();

            return RequestOK(Enterprise);
        }

        [HttpPost]
        [Route("save")]
        public async Task<IActionResult> Post(string pEstablishmentKey, [FromBody] GEUserEnterprisesVM pGEUserEnterprisesVM)
        {
            _IGEUserEnterprisesAS.Errors.Clear();
            ValidateModelState(pGEUserEnterprisesVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEUserEnterprisesVM, ModelState);

            var Enterprise = await _IGEUserEnterprisesAS.CreateOrUpdateAsync(pGEUserEnterprisesVM);

            if (_IGEUserEnterprisesAS.Errors.Count > 0)
                return InvalidRequest(Enterprise, _IGEUserEnterprisesAS.Errors);
            else
                await _IGEUserEnterprisesAS.SaveChanges();

            return RequestOK(Enterprise);
        }

        [HttpPost]
        [Route("list/save")]
        public async Task<IActionResult> PostList(string pEstablishmentKey, [FromBody] GEUserEnterprisesListVM pGEUserEnterprisesVM)
        {
            _IGEUserEnterprisesAS.Errors.Clear();
            ValidateModelState(pGEUserEnterprisesVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEUserEnterprisesVM, ModelState);

            var Enterprise = await _IGEUserEnterprisesAS.CreateOrUpdateListAsync(pEstablishmentKey, pGEUserEnterprisesVM.results);

            if (_IGEUserEnterprisesAS.Errors.Count > 0)
                return InvalidRequest(Enterprise, _IGEUserEnterprisesAS.Errors);
            else
                await _IGEUserEnterprisesAS.SaveChanges();

            return RequestOK(Enterprise);
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        public async Task<IActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IGEUserEnterprisesAS.Errors.Clear();
            var Result = await _IGEUserEnterprisesAS.RemoveById(pId, pEstablishmentKey, pUniqueKey);

            if (Result != null && !_IGEUserEnterprisesAS.Errors.Any())
            {
                await _IGEUserEnterprisesAS.SaveChanges();
                return RequestOK(Mapper.Map<GEUserEnterprises, GEUserEnterprisesVM>(Result));
            }

            return InvalidRequest(Result, _IGEUserEnterprisesAS.Errors);
        }

        [HttpPost]
        [Route("list/delete")]
        public async Task<IActionResult> PostDelete(string pEstablishmentKey, [FromBody] GEUserEnterprisesListVM pGEUserEnterprisesVM)
        {
            _IGEUserEnterprisesAS.Errors.Clear();
            var Result = await _IGEUserEnterprisesAS.RemoveListAsync(pEstablishmentKey, pGEUserEnterprisesVM.results);

            if (Result != null && !_IGEUserEnterprisesAS.Errors.Any())
            {
                await _IGEUserEnterprisesAS.SaveChanges();
                return RequestOK(Result);
            }

            return InvalidRequest(Result, _IGEUserEnterprisesAS.Errors);
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IGEUserEnterprisesAS.Dispose();
            }
        }
    }
}
