﻿using Hino.API.Domain.Base.Models;
using Hino.API.Infra.Cross.Utils.Attributes;
using Hino.API.Infra.Cross.Utils.Exceptions;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using DefaultMessages = Hino.API.Infra.Cross.Resources.MessagesResource;

namespace Hino.API.Domain.Payments.Models
{
    [Queue("GEPaymentCondition")]
    public class GEPaymentCondition : BaseEntity
    {
        public GEPaymentCondition()
        {
            this.GEPaymentCondInstallments = new HashSet<GEPaymentCondInstallments>();
        }

        [ForeignKey("GEPaymentType")]
        public long TypePayID { get; set; }
        public virtual GEPaymentType GEPaymentType { get; set; }

        public string Description { get; set; }
        public short Installments { get; set; }
        public decimal PercDiscount { get; set; }
        public decimal PercIncrease { get; set; }
        public long IdERP { get; set; }

        public virtual ICollection<GEPaymentCondInstallments> GEPaymentCondInstallments { get; set; }

        public ModelException ValidateInstallments()
        {
            if (GEPaymentCondInstallments.Sum(r => r.Percent) != 100)
            {
                return ModelException.CreateValidationError(
                    DefaultMessages.InvalidPercentualInstallments,
                    "GEPaymentCondInstallments",
                    ""
                );
            }

            if (GEPaymentCondInstallments.Count != Installments)
            {
                return ModelException.CreateValidationError(
                    DefaultMessages.InvalidCountInstallments,
                    "GEPaymentCondInstallments",
                    ""
                );
            }

            return null;
        }
    }
}
