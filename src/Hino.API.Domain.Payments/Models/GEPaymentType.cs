﻿using Hino.API.Domain.Base.Models;
using Hino.API.Infra.Cross.Utils.Attributes;
using Hino.API.Infra.Cross.Utils.Enums;
using Hino.API.Infra.Cross.Utils.Exceptions;
using System.Collections.Generic;
using System.Linq;
using ValidationMessages = Hino.API.Infra.Cross.Resources.ValidationMessagesResource;

namespace Hino.API.Domain.Payments.Models
{
    [Queue("GEPaymentType")]
    public class GEPaymentType : BaseEntity
    {
        public GEPaymentType()
        {
            GEPaymentCondition = new HashSet<GEPaymentCondition>();
            GEOrders = new HashSet<GEOrders>();
            GEUserPayType = new HashSet<GEUserPayType>();
        }

        public string Description { get; set; }
        public long IdERP { get; set; }
        public bool Boleto { get; set; }

        public virtual ICollection<GEUserPayType> GEUserPayType { get; set; }
        public virtual ICollection<GEPaymentCondition> GEPaymentCondition { get; set; }
        public virtual ICollection<GEOrders> GEOrders { get; set; }

        public ModelException ValidateChilds()
        {
            if (GEUserPayType.Any())
            {
                return new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { ValidationMessages.UserPayChild }
                };
            }
            if (GEPaymentCondition.Any())
            {
                return new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { ValidationMessages.ConditionPayChild }
                };
            }
            if (GEOrders.Any())
            {
                return new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { ValidationMessages.OrderPayChild }
                };
            }

            return null;
        }
    }
}
