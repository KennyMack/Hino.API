﻿using Hino.API.Domain.Base.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Domain.Payments.Models
{
    [Table("VEORDERS")]
    public class GEOrders : BaseEntity
    {
        [ForeignKey("GEEnterprises")]
        [Column("ENTERPRISEID")]
        public long EnterpriseID { get; set; }

        [ForeignKey("GECarriers")]
        [Column("CARRIERID")]
        public long? CarrierID { get; set; }

        [ForeignKey("GERedispatch")]
        [Column("REDISPATCHID")]
        public long? RedispatchID { get; set; }

        [ForeignKey("GEUsers")]
        public long UserID { get; set; }

        [ForeignKey("GEPaymentType")]
        public long TypePaymentID { get; set; }
        public virtual GEPaymentType GEPaymentType { get; set; }

        [ForeignKey("GEPaymentCondition")]
        public long PayConditionID { get; set; }
        public virtual GEPaymentCondition GEPaymentCondition { get; set; }

        public long IdERP { get; set; }
    }
}
