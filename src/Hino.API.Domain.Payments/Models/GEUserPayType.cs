using System.ComponentModel.DataAnnotations.Schema;
using Hino.API.Domain.Base.Models;

namespace Hino.API.Domain.Payments.Models
{
    public class GEUserPayType : BaseEntity
    {
        [ForeignKey("GEPaymentType")]
        public long PaymentTypeId { get; set; }
        public virtual GEPaymentType GEPaymentType { get; set; }
        
        [ForeignKey("GEUsers")]
        public long UserId { get; set; }
        public virtual GESeller GEUsers { get; set; }
    }
}
