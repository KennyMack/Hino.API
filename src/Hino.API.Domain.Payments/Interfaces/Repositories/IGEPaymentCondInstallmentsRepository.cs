using Hino.API.Domain.Base.Interfaces.Repositories;
using Hino.API.Domain.Payments.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.API.Domain.Payments.Interfaces.Repositories
{
    public interface IGEPaymentCondInstallmentsRepository : IBaseRepository<GEPaymentCondInstallments>
    {
        Task ClearInstallmentsAsync(string pEstablishmentKey, long pCondPayId);
        Task SaveInstallmentsAsync(GEPaymentCondition pModel, IEnumerable<GEPaymentCondInstallments> pInstallments);
    }
}
