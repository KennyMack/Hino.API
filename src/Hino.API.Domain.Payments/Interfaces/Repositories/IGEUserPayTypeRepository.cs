using Hino.API.Domain.Base.Interfaces.Repositories;
using Hino.API.Domain.Payments.Models;

namespace Hino.API.Domain.Payments.Interfaces.Repositories
{
    public interface IGEUserPayTypeRepository : IBaseRepository<GEUserPayType>
    {
    }
}
