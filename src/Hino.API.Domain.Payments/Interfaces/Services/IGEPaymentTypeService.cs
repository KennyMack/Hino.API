using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Payments.Models;

namespace Hino.API.Domain.Payments.Interfaces.Services
{
    public interface IGEPaymentTypeService : IBaseService<GEPaymentType>
    {
    }
}
