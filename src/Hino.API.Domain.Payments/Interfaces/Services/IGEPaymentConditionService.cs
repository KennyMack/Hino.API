using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Payments.Models;
using System.Threading.Tasks;

namespace Hino.API.Domain.Payments.Interfaces.Services
{
    public interface IGEPaymentConditionService : IBaseService<GEPaymentCondition>
    {
        Task<GEPaymentCondition> ChangeAsync(GEPaymentCondition model);
    }
}
