using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Payments.Models;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.API.Domain.Payments.Interfaces.Services
{
    public interface IGEUserPayTypeService : IBaseService<GEUserPayType>
    {
        Task<GEUserPayType> CreateOrUpdateAsync(GEUserPayType paymentType);
        Task<GEUserPayType[]> CreateOrUpdateListAsync(string pEstablishmentKey, GEUserPayType[] paymentType);
        Task<GEUserPayType[]> RemoveListAsync(string pEstablishmentKey, GEUserPayType[] paymentType);
    }
}
