using Hino.API.Domain.Payments.Interfaces.Services;
using Hino.API.Domain.Payments.Models;
using Hino.API.Domain.Base.Services;
using Hino.API.Domain.Payments.Interfaces.Repositories;
using System.Threading.Tasks;
using Hino.API.Infra.Cross.Utils.Exceptions;
using ValidationMessage = Hino.API.Infra.Cross.Resources.ValidationMessagesResource;

namespace Hino.API.Domain.Payments.Services
{
    public class GEPaymentTypeService : BaseService<GEPaymentType>, IGEPaymentTypeService
    {
        private readonly IGEPaymentTypeRepository _IGEPaymentTypeRepository;

        public GEPaymentTypeService(IGEPaymentTypeRepository pIGEPaymentTypeRepository) :
             base(pIGEPaymentTypeRepository)
        {
            _IGEPaymentTypeRepository = pIGEPaymentTypeRepository;
        }

        public override async Task<GEPaymentType> RemoveById(long id, string pEstablishmentKey, string pUniqueKey)
        {
            var PaymentType = await _IGEPaymentTypeRepository.GetByIdAsync(id, pEstablishmentKey, pUniqueKey,
                s => s.GEPaymentCondition,
                v => v.GEOrders,
                u => u.GEUserPayType);

            if (PaymentType == null)
            {
                Errors.Add(ModelException.CreateNotFoundError(ValidationMessage.NotFound, "GEPaymentType", id.ToString()));
                return null;
            }

            var Error = PaymentType.ValidateChilds();

            if (Error == null)
            {
                Errors.Add(Error);
                return null;
            }

            _IGEPaymentTypeRepository.Remove(PaymentType);
            await _IGEPaymentTypeRepository.SaveChanges();
            return PaymentType;
        }
    }
}
