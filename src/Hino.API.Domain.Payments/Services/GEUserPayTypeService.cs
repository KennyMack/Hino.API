using Hino.API.Domain.Payments.Interfaces.Services;
using Hino.API.Domain.Payments.Models;
using Hino.API.Domain.Base.Services;
using Hino.API.Domain.Payments.Interfaces.Repositories;
using System.Threading.Tasks;
using System.Linq;

namespace Hino.API.Domain.Payments.Services
{
    public class GEUserPayTypeService : BaseService<GEUserPayType>, IGEUserPayTypeService
    {
        private readonly IGEUserPayTypeRepository _IGEUserPayTypeRepository;

        public GEUserPayTypeService(IGEUserPayTypeRepository pIGEUserPayTypeRepository) : 
             base(pIGEUserPayTypeRepository)
        {
            _IGEUserPayTypeRepository = pIGEUserPayTypeRepository;
        }

        public async Task<GEUserPayType> CreateOrUpdateAsync(GEUserPayType paymentType)
        {
            var ent = await _IGEUserPayTypeRepository.QueryAsync(r =>
                r.EstablishmentKey == paymentType.EstablishmentKey &&
                r.UserId == paymentType.UserId &&
                r.PaymentTypeId == paymentType.PaymentTypeId);

            if (!ent.Any())
            {
                var entNew = new GEUserPayType
                {
                    EstablishmentKey = paymentType.EstablishmentKey,
                    UserId = paymentType.UserId,
                    PaymentTypeId = paymentType.PaymentTypeId
                };

                _IGEUserPayTypeRepository.Add(entNew);

                await _IGEUserPayTypeRepository.SaveChanges();

                return entNew;
            }

            return paymentType;
        }

        public async Task<GEUserPayType[]> CreateOrUpdateListAsync(string pEstablishmentKey, GEUserPayType[] paymentType)
        {

            for (int i = 0, length = paymentType.Length; i < length; i++)
            {
                if (paymentType[i].Id < 0)
                    paymentType[i].Id = 0;

                paymentType[i].EstablishmentKey = pEstablishmentKey;
                paymentType[i].GEPaymentType = null;
                paymentType[i].GEUsers = null;

                paymentType[i] = await CreateOrUpdateAsync(paymentType[i]);
            }

            return paymentType;
        }

        public async Task<GEUserPayType[]> RemoveListAsync(string pEstablishmentKey, GEUserPayType[] paymentType)
        {
            for (int i = 0, length = paymentType.Length; i < length; i++)
            {
                if (paymentType[i].Id < 0)
                    paymentType[i].Id = 0;

                paymentType[i] = await _IGEUserPayTypeRepository.RemoveById(
                    paymentType[i].Id,
                    pEstablishmentKey,
                    paymentType[i].UniqueKey
                );
            }

            return paymentType;
        }
    }
}
