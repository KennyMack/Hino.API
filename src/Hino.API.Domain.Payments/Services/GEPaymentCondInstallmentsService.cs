using Hino.API.Domain.Payments.Interfaces.Services;
using Hino.API.Domain.Payments.Models;
using Hino.API.Domain.Base.Services;
using Hino.API.Domain.Payments.Interfaces.Repositories;

namespace Hino.API.Domain.Payments.Services
{
    public class GEPaymentCondInstallmentsService : BaseService<GEPaymentCondInstallments>, IGEPaymentCondInstallmentsService
    {
        private readonly IGEPaymentCondInstallmentsRepository _IGEPaymentCondInstallmentsRepository;

        public GEPaymentCondInstallmentsService(IGEPaymentCondInstallmentsRepository pIGEPaymentCondInstallmentsRepository) :
             base(pIGEPaymentCondInstallmentsRepository)
        {
            _IGEPaymentCondInstallmentsRepository = pIGEPaymentCondInstallmentsRepository;
        }
    }
}
