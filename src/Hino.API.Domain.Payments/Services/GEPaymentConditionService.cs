using Hino.API.Domain.Payments.Interfaces.Services;
using Hino.API.Domain.Payments.Models;
using Hino.API.Domain.Base.Services;
using Hino.API.Domain.Payments.Interfaces.Repositories;
using System.Threading.Tasks;
using System;
using Hino.API.Infra.Cross.Utils.Enums;
using Hino.API.Infra.Cross.Utils.Exceptions;
using ValidationMessage = Hino.API.Infra.Cross.Resources.ValidationMessagesResource;

namespace Hino.API.Domain.Payments.Services
{
    public class GEPaymentConditionService : BaseService<GEPaymentCondition>, IGEPaymentConditionService
    {
        private readonly IGEPaymentConditionRepository _IGEPaymentConditionRepository;
        private readonly IGEPaymentCondInstallmentsRepository _IGEPaymentCondInstallmentsRepository;

        public GEPaymentConditionService(IGEPaymentConditionRepository pIGEPaymentConditionRepository,
            IGEPaymentCondInstallmentsRepository pIGEPaymentCondInstallmentsRepository) :
             base(pIGEPaymentConditionRepository)
        {
            _IGEPaymentConditionRepository = pIGEPaymentConditionRepository;
            _IGEPaymentCondInstallmentsRepository = pIGEPaymentCondInstallmentsRepository;
        }

        public override GEPaymentCondition Add(GEPaymentCondition model)
        {
            foreach (var item in model.GEPaymentCondInstallments)
                item.EstablishmentKey = model.EstablishmentKey;

            var InstallmentsError = model.ValidateInstallments();
            if (InstallmentsError != null)
            {
                Errors.Add(InstallmentsError);
                return null;
            }

            return base.Add(model);
        }

        public async Task<GEPaymentCondition> ChangeAsync(GEPaymentCondition model)
        {
            var InstallmentsError = model.ValidateInstallments();
            if (InstallmentsError != null)
            {
                Errors.Add(InstallmentsError);
                return null;
            }

            var dbModel = await GetByIdAsync(model.Id, model.EstablishmentKey, model.UniqueKey);

            try
            {
                await _IGEPaymentCondInstallmentsRepository.ClearInstallmentsAsync(dbModel.EstablishmentKey, dbModel.Id);
                await _IGEPaymentCondInstallmentsRepository.SaveInstallmentsAsync(dbModel, model.GEPaymentCondInstallments);
            }
            catch (Exception ex)
            {
                Errors.Add(ModelException.CreateSqlError(ex, "GEPaymentCondInstallments", EExceptionErrorCodes.InsertSQLError));
                return null;
            }

            dbModel.Description = model.Description;
            dbModel.Installments = model.Installments;
            dbModel.TypePayID = model.TypePayID;
            dbModel.IdERP = model.IdERP;
            dbModel.GEPaymentType = null;
            dbModel.PercDiscount = model.PercDiscount;
            dbModel.PercIncrease = model.PercIncrease;

            base.Update(dbModel);

            await _IGEPaymentConditionRepository.SaveChanges();

            return dbModel;
        }

        public override async Task<GEPaymentCondition> RemoveById(long id, string pEstablishmentKey, string pUniqueKey)
        {
            var PaymentCondition = await _IGEPaymentConditionRepository.GetByIdAsync(id, pEstablishmentKey, pUniqueKey);

            if (PaymentCondition == null)
            {
                Errors.Add(ModelException.CreateNotFoundError(ValidationMessage.NotFound, "GEPaymentCondition", id.ToString()));
                return null;
            }

            try
            {
                await _IGEPaymentCondInstallmentsRepository.ClearInstallmentsAsync(PaymentCondition.EstablishmentKey, PaymentCondition.Id);
            }
            catch (Exception ex)
            {
                Errors.Add(ModelException.CreateSqlError(ex, "GEPaymentCondInstallments", EExceptionErrorCodes.InsertSQLError));
                return null;
            }

            _IGEPaymentConditionRepository.Remove(PaymentCondition);
            await _IGEPaymentConditionRepository.SaveChanges();
            return PaymentCondition;
        }

        public async override Task GenerateEntryQueueAsync()
        {
            /*
            var queue = new MessageService<GEPaymentCondition>();

            foreach (var item in AddedOrUpdatedItems)
            {
                var dbItem = await GetByIdAsync(item.Id, item.EstablishmentKey, item.UniqueKey,
                    r => r.GEPaymentType,
                    s => s.GEPaymentCondInstallments);
                queue.SendCreatedOrUpdatedQueue(dbItem);
            }

            foreach (var item in RemovedItems)
                queue.SendRemovedQueue(item);
            */
            await Task.Delay(1);
            AddedOrUpdatedItems.Clear();
            RemovedItems.Clear();
        }
    }
}
