﻿using Hino.API.Domain.Customers.Models;
using Hino.API.Infra.Data.Repositories.Base.Context;
using Hino.API.Infra.Data.Repositories.Customers.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Hino.API.Infra.Data.Repositories.Customers.Context
{
    public class CustomerDbContext : BaseDbContext
    {
        public virtual DbSet<GEEnterprises> GEEnterprises { get; set; }
        public virtual DbSet<GEEnterpriseGeo> GEEnterpriseGeo { get; set; }
        public virtual DbSet<GEEnterpriseContacts> GEEnterpriseContacts { get; set; }
        public virtual DbSet<GEEnterpriseFiscalGroup> GEEnterpriseFiscalGroup { get; set; }
        public virtual DbSet<GEEnterpriseCategory> GEEnterpriseCategory { get; set; }
        public virtual DbSet<GEEnterpriseGroup> GEEnterpriseGroup { get; set; }
        public virtual DbSet<GEUserEnterprises> GEUserEnterprises { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new GEEnterprisesConfiguration("GEENTERPRISES"));
        }
    }
}
