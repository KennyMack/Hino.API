﻿using Hino.API.Domain.Customers.Models;
using Hino.API.Infra.Data.Repositories.Base.Configurations;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Infra.Data.Repositories.Customers.EntityConfigurations
{
    class GEEnterprisesConfiguration : BaseEntityConfigurarion<GEEnterprises>
    {
        public GEEnterprisesConfiguration(string Table) : base(Table)
        {
        }

        public override void Configure(EntityTypeBuilder<GEEnterprises> config)
        {
        }
    }
}
