using Hino.API.Domain.Customers.Interfaces.Repositories;
using Hino.API.Domain.Customers.Models;
using Hino.API.Infra.Data.Repositories.Base.Repositories;
using Hino.API.Infra.Data.Repositories.Customers.Context;

namespace Hino.API.Infra.Data.Repositories.Customers.Repositories
{
    public class GEEnterpriseCategoryRepository : BaseRepository<GEEnterpriseCategory>, IGEEnterpriseCategoryRepository
    {
        public GEEnterpriseCategoryRepository(CustomerDbContext CustomerDbContext) : base(CustomerDbContext)
        {
        }
    }
}
