using Hino.API.Domain.Customers.Interfaces.Repositories;
using Hino.API.Domain.Customers.Models;
using Hino.API.Infra.Data.Repositories.Base.Repositories;
using Hino.API.Infra.Data.Repositories.Customers.Context;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.API.Infra.Data.Repositories.Customers.Repositories
{
    public class GEEnterpriseContactsRepository : BaseRepository<GEEnterpriseContacts>, IGEEnterpriseContactsRepository
    {
        public GEEnterpriseContactsRepository(CustomerDbContext CustomerDbContext) : base(CustomerDbContext)
        {
        }

        public async Task ClearEnterpriseContactsAsync(string pEstablishmentKey, long pEnterpriseId)
        {
            var contacts = await QueryAsync(r => r.EstablishmentKey == pEstablishmentKey && r.EnterpriseId == pEnterpriseId);

            foreach (var item in contacts)
                await RemoveById(item.Id, pEstablishmentKey, item.UniqueKey);

            await SaveChanges();
        }

        public async Task SaveEnterpriseContactsAsync(GEEnterprises pModel, IEnumerable<GEEnterpriseContacts> pEnterpriseContacts)
        {
            foreach (var item in pEnterpriseContacts)
            {
                item.EstablishmentKey = pModel.EstablishmentKey;
                item.EnterpriseId = pModel.Id;

                Add(new GEEnterpriseContacts
                {
                    EnterpriseId = item.EnterpriseId,
                    ReceptivityIndex = item.ReceptivityIndex,
                    Sector = item.Sector,
                    Email = item.Email,
                    Contact = item.Contact,
                    Ramal = item.Ramal,
                    Phone = item.Phone,
                    Note = item.Note,
                    EstablishmentKey = item.EstablishmentKey,
                    UniqueKey = item.UniqueKey,
                    Created = item.Created,
                    Modified = item.Modified,
                    Id = item.Id
                });
            }
            await SaveChanges();
        }
    }
}
