using Hino.API.Domain.Customers.Interfaces.Repositories;
using Hino.API.Domain.Customers.Models;
using Hino.API.Infra.Cross.Utils.Paging;
using Hino.API.Infra.Data.Repositories.Base.Repositories;
using Hino.API.Infra.Data.Repositories.Customers.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hino.API.Infra.Data.Repositories.Customers.Repositories
{
    public class GEUserEnterprisesRepository : BaseRepository<GEUserEnterprises>, IGEUserEnterprisesRepository
    {
        public GEUserEnterprisesRepository(CustomerDbContext CustomerDbContext) : base(CustomerDbContext)
        {
        }

        public async Task<PagedResult<GEUserEnterprises>> QuerySearchPagedAsync(int page, int pageSize,
            Expression<Func<GEUserEnterprises, bool>> predicate,
            params Expression<Func<GEUserEnterprises, object>>[] includeProperties) =>
            await PaginateQueryAsync(LocalAddQueryProperties(DbEntity
                .Include(r => r.GEEnterprises)
                .Include(r => r.GEUsers), includeProperties)
                .AsNoTracking()
                .Where(predicate), page, pageSize);

        public async override Task<PagedResult<GEUserEnterprises>> QueryPagedAsync(int page, int pageSize,
            Expression<Func<GEUserEnterprises, bool>> predicate,
            params Expression<Func<GEUserEnterprises, object>>[] includeProperties) =>
            await PaginateQueryAsync(LocalAddQueryProperties(DbEntity
                .Include(r => r.GEEnterprises)
                .Include(r => r.GEUsers)
                .Include("GEEnterprises.GEPaymentCondition")
                .Include("GEEnterprises.GEPaymentCondition.GEPaymentType")
                .Include("GEEnterprises.GEEnterpriseGeo")
                .Include("GEEnterprises.GEEnterpriseContacts")
                .Include("GEEnterprises.GEEnterpriseCategory")
                .Include("GEEnterprises.GEEnterpriseGroup")
                .Include("GEEnterprises.GEEnterpriseFiscalGroup"), includeProperties)
                .AsNoTracking()
                .Where(predicate), page, pageSize);

        public override async Task<PagedResult<GEUserEnterprises>> GetAllPagedAsync(int page, int pageSize,
                params Expression<Func<GEUserEnterprises, object>>[] includeProperties) =>
            await PaginateQueryAsync(LocalAddQueryProperties(DbEntity
                .Include(r => r.GEEnterprises)
                .Include(r => r.GEUsers)
                .Include("GEEnterprises.GEPaymentCondition")
                .Include("GEEnterprises.GEPaymentCondition.GEPaymentType")
                .Include("GEEnterprises.GEEnterpriseGeo")
                .Include("GEEnterprises.GEEnterpriseContacts")
                .Include("GEEnterprises.GEEnterpriseCategory")
                .Include("GEEnterprises.GEEnterpriseGroup")
                .Include("GEEnterprises.GEEnterpriseFiscalGroup"), includeProperties)
                .AsNoTracking(), page, pageSize);

        public override async Task<IEnumerable<GEUserEnterprises>> QueryAsync(
            Expression<Func<GEUserEnterprises, bool>> predicate,
            params Expression<Func<GEUserEnterprises, object>>[] includeProperties) =>
            await LocalAddQueryProperties(DbEntity
                .Include(r => r.GEEnterprises)
                .Include(r => r.GEUsers)
                .Include("GEEnterprises.GEPaymentCondition")
                .Include("GEEnterprises.GEPaymentCondition.GEPaymentType")
                .Include("GEEnterprises.GEEnterpriseGeo")
                .Include("GEEnterprises.GEEnterpriseContacts")
                .Include("GEEnterprises.GEEnterpriseCategory")
                .Include("GEEnterprises.GEEnterpriseGroup")
                .Include("GEEnterprises.GEEnterpriseFiscalGroup"), includeProperties)
                .Where(predicate)
                .AsNoTracking()
                .ToListAsync();

        public override async Task<IEnumerable<GEUserEnterprises>> GetAllAsync(
            params Expression<Func<GEUserEnterprises, object>>[] includeProperties) =>
            await LocalAddQueryProperties(DbEntity
                .Include(r => r.GEEnterprises)
                .Include(r => r.GEUsers)
                .Include("GEEnterprises.GEPaymentCondition")
                .Include("GEEnterprises.GEPaymentCondition.GEPaymentType")
                .Include("GEEnterprises.GEEnterpriseGeo")
                .Include("GEEnterprises.GEEnterpriseContacts")
                .Include("GEEnterprises.GEEnterpriseCategory")
                .Include("GEEnterprises.GEEnterpriseGroup")
                .Include("GEEnterprises.GEEnterpriseFiscalGroup"), includeProperties).AsNoTracking().ToListAsync();

        public override async Task<GEUserEnterprises> GetByIdAsync(long id, string pEstablishmentKey, string pUniqueKey,
             params Expression<Func<GEUserEnterprises, object>>[] includeProperties) =>
             await DbEntity
                .Include(r => r.GEEnterprises)
                .Include(r => r.GEUsers)
                .Include("GEEnterprises.GEPaymentCondition")
                .Include("GEEnterprises.GEPaymentCondition.GEPaymentType")
                .Include("GEEnterprises.GEEnterpriseGeo")
                .Include("GEEnterprises.GEEnterpriseContacts")
                .Include("GEEnterprises.GEEnterpriseCategory")
                .Include("GEEnterprises.GEEnterpriseGroup")
                .Include("GEEnterprises.GEEnterpriseFiscalGroup")
                .AsNoTracking().Where(r =>
                       r.Id == id &&
                       r.EstablishmentKey == pEstablishmentKey &&
                       r.UniqueKey == pUniqueKey).FirstOrDefaultAsync();

        static IQueryable<GEUserEnterprises> LocalAddQueryProperties(IQueryable<GEUserEnterprises> query,
            params Expression<Func<GEUserEnterprises, object>>[] includeProperties)
        {
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        public override GEUserEnterprises GetById(long id, string pEstablishmentKey, string pUniqueKey,
            params Expression<Func<GEUserEnterprises, object>>[] includeProperties) =>
            LocalAddQueryProperties(DbEntity
                .Include(r => r.GEEnterprises)
                .Include(r => r.GEUsers)
                .Include("GEEnterprises.GEPaymentCondition")
                .Include("GEEnterprises.GEPaymentCondition.GEPaymentType")
                .Include("GEEnterprises.GEEnterpriseGeo")
                .Include("GEEnterprises.GEEnterpriseContacts")
                .Include("GEEnterprises.GEEnterpriseCategory")
                .Include("GEEnterprises.GEEnterpriseGroup")
                .Include("GEEnterprises.GEEnterpriseFiscalGroup"), includeProperties)
                .Where(r =>
                       r.Id == id &&
                       r.EstablishmentKey == pEstablishmentKey &&
                       r.UniqueKey == pUniqueKey)
                .AsNoTracking().FirstOrDefault();
    }
}
