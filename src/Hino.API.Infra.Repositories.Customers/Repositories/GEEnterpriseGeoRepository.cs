using Hino.API.Domain.Customers.Enums;
using Hino.API.Domain.Customers.Interfaces.Repositories;
using Hino.API.Domain.Customers.Models;
using Hino.API.Infra.Data.Repositories.Base.Repositories;
using Hino.API.Infra.Data.Repositories.Customers.Context;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.API.Infra.Data.Repositories.Customers.Repositories
{
    public class GEEnterpriseGeoRepository : BaseRepository<GEEnterpriseGeo>, IGEEnterpriseGeoRepository
    {
        public GEEnterpriseGeoRepository(CustomerDbContext CustomerDbContext) : base(CustomerDbContext)
        {
        }

        public async Task ClearEnterpriseGeoAsync(string pEstablishmentKey, long pEnterpriseId)
        {
            var geo = await QueryAsync(r => r.EstablishmentKey == pEstablishmentKey && r.Enterpriseid == pEnterpriseId);

            foreach (var item in geo)
                await RemoveById(item.Id, pEstablishmentKey, item.UniqueKey);

            await SaveChanges();
        }

        public async Task SaveEnterpriseGeoAsync(GEEnterprises pModel, IEnumerable<GEEnterpriseGeo> pEnterpriseGeo)
        {
            foreach (var item in pEnterpriseGeo)
            {
                item.Enterpriseid = pModel.Id;
                item.EstablishmentKey = pModel.EstablishmentKey;

                item.IE = pModel.FormatIE(item.UF);
                if (item.Type == EAddressType.Commercial)
                    pModel.IE = item.IE;

                Add(new GEEnterpriseGeo
                {
                    Enterpriseid = item.Enterpriseid,
                    Type = item.Type,
                    CNPJCPF = item.CNPJCPF,
                    IE = item.IE,
                    RG = item.RG,
                    CellPhone = item.CellPhone,
                    Phone = item.Phone,
                    Email = item.Email,
                    Address = item.Address,
                    Complement = item.Complement,
                    District = item.District,
                    Num = item.Num,
                    ZipCode = item.ZipCode,
                    Site = item.Site,
                    CountryIni = item.CountryIni,
                    CountryCode = item.CountryCode,
                    CountryName = item.CountryName,
                    UF = item.UF,
                    StateName = item.StateName,
                    IBGE = item.IBGE,
                    CityName = item.CityName,
                    DisplayLat = item.DisplayLat,
                    DisplayLng = item.DisplayLng,
                    NavLat = item.NavLat,
                    NavLng = item.NavLng,
                    EstablishmentKey = item.EstablishmentKey,
                    UniqueKey = item.UniqueKey,
                    Created = item.Created,
                    Modified = item.Modified,
                    Id = item.Id
                });
            }
            await SaveChanges();
        }
    }
}
