using Hino.API.Domain.Customers.Enums;
using Hino.API.Domain.Customers.Interfaces.Repositories;
using Hino.API.Domain.Customers.Models;
using Hino.API.Infra.Cross.Utils;
using Hino.API.Infra.Data.Repositories.Base;
using Hino.API.Infra.Data.Repositories.Base.Repositories;
using Hino.API.Infra.Data.Repositories.Customers.Context;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.API.Infra.Data.Repositories.Customers.Repositories
{
    public class GEEnterprisesRepository : BaseRepository<GEEnterprises>, IGEEnterprisesRepository
    {
        public GEEnterprisesRepository(CustomerDbContext CustomerDbContext) : base(CustomerDbContext)
        {
        }

        public async Task UpdateEnterpriseRegionIdAsync(string pEstablishmentKey, string pUF, long pRegionId)
        {
            var sql = @"UPDATE GEENTERPRISES
                  SET GEENTERPRISES.REGIONID         = :pREGIONID
                WHERE GEENTERPRISES.ESTABLISHMENTKEY = :pESTABLISHMENTKEY
                  AND EXISTS (SELECT 1
                                FROM GEENTERPRISEGEO
                               WHERE GEENTERPRISEGEO.ESTABLISHMENTKEY = GEENTERPRISES.ESTABLISHMENTKEY
                                 AND GEENTERPRISEGEO.ENTERPRISEID     = GEENTERPRISES.ID
                                 AND GEENTERPRISEGEO.TYPE             = 0
                                 AND GEENTERPRISEGEO.UF               = :pUF)";

            await DbConn.ExecuteSqlCommandAsync(sql,
                new OracleParameter("pREGIONID", OracleDbType.Int64, pRegionId, ParameterDirection.Input),
                new OracleParameter("pESTABLISHMENTKEY", OracleDbType.Varchar2, pEstablishmentKey, ParameterDirection.Input),
                new OracleParameter("pUF", OracleDbType.Varchar2, pUF, ParameterDirection.Input)
            );
        }

        public override bool Add(GEEnterprises model)
        {
            if (model.Id == 0)
                model.Id = NextSequence();
            model.Search = $"{model.Id}|{model.RazaoSocial}|{model.NomeFantasia}|{model.CNPJCPF}|{model.IE}|{model.ClassifEmpresa}"
                .RemoveAccent()
                .ToUpper();
            return base.Add(model);
        }

        public override bool Update(GEEnterprises model)
        {
            model.Search = $"{model.Id}|{model.RazaoSocial}|{model.NomeFantasia}|{model.CNPJCPF}|{model.IE}|{model.ClassifEmpresa}"
                .RemoveAccent()
                .ToUpper();
            return base.Update(model);
        }

        public async Task<bool> EnterpriseExists(string pEstablishmentKey, string pCNPJCPF, EEnterpriseClassification pClassification, long idActual)
        {
            var EnterpriseDuplicated = await QueryAsync(r => r.EstablishmentKey == pEstablishmentKey &&
                                                                r.CNPJCPF == pCNPJCPF &&
                                                                r.Classification == pClassification &&
                                                                r.Id != idActual);
            return EnterpriseDuplicated.Any();
        }
    }
}
