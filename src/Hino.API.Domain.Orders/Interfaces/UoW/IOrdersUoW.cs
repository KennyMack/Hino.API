﻿using Hino.API.Domain.Base.Interfaces.UoW;
using Hino.API.Domain.Orders.Interfaces.Repositories;
using Hino.API.Domain.Orders.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Domain.Orders.Interfaces.UoW
{
    public interface IOrdersUoW: IBaseUnitOfWork
    {
        public IVEOrdersRepository OrdersRepository { get; }
        public IVEOrderItemsRepository OrderItemsRepository { get; }
        public IVEOrderTaxesRepository OrderTaxesRepository { get; }
        public IVESalePriceRepository SalePriceRepository { get; }

    }
}
