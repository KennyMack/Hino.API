using Hino.API.Domain.Base.Interfaces.Repositories;
using Hino.API.Domain.Orders.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.API.Domain.Orders.Interfaces.Repositories
{
    public interface IVEOrdersRepository : IBaseRepository<VEOrders>
    {
        Task<IEnumerable<VEOrders>> GetOrdersLinkedTreeAsync(string pEstablishmentKey, long id);
        Task<IEnumerable<VEOrders>> GetOrdersLinkedAsync(string pEstablishmentKey, long id);
        Task<IEnumerable<VEOrders>> GetOrderDirectLinkedAsync(string pEstablishmentKey, long id);
        Task<VEOrders> CanConvertToOrderAsync(string pEstablishmentKey, long id);
    }
}
