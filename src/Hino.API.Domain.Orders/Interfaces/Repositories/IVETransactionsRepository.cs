using Hino.API.Domain.Base.Interfaces.Repositories;
using Hino.API.Domain.Orders.Models;
using System.Threading.Tasks;

namespace Hino.API.Domain.Orders.Interfaces.Repositories
{
    public interface IVETransactionsRepository : IBaseRepository<VETransactions>
    {
        Task<VETransactions> GetByAuthCode(string pEstablishmentKey, string pAuthCode);
        Task<VETransactions> GetByNsuHost(string pEstablishmentKey, string pTerminalId, string pNsuHost);
        Task<VETransactions> SearchTransationByNSUAsync(string pEstablishmentKey, string NSU);
    }
}
