using Hino.API.Domain.Base.Interfaces.Repositories;
using Hino.API.Domain.Orders.Models;
using System.Threading.Tasks;

namespace Hino.API.Domain.Orders.Interfaces.Repositories
{
    public interface IVEOrderTaxesRepository : IBaseRepository<VEOrderTaxes>
    {
        Task ClearOrderTaxesAsync(string pEstablishmentKey, long pOrderId);
    }
}
