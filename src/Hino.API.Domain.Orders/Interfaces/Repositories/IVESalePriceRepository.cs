﻿using Hino.API.Domain.Base.Interfaces.Repositories;
using Hino.API.Domain.Orders.Models;
using Hino.API.Infra.Cross.Utils.Paging;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.API.Domain.Orders.Interfaces.Repositories
{
    public interface IVESalePriceRepository : IBaseRepository<VESalePrice>
    {
    }
}
