using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Orders.Models;
using System.Threading.Tasks;

namespace Hino.API.Domain.Orders.Interfaces.Services
{
    public interface IVEOrderTaxesService : IBaseService<VEOrderTaxes>
    {
        Task ClearOrderTaxesAsync(string EstablishmentKey, long pOrderId);
    }
}
