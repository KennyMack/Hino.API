﻿using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Orders.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Domain.Orders.Interfaces.Services
{
    public interface IRemoveOrderService: IErrorBaseService, IDisposable
    {
        Task<VEOrders> RemoveByIdAsync(long id, string pEstablishmentKey, string pUniqueKey);
    }
}
