using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Orders.Models;
using System.Threading.Tasks;

namespace Hino.API.Domain.Orders.Interfaces.Services
{
    public interface IVETransactionsService : IBaseService<VETransactions>
    {
        Task<VETransactions> GetByAuthCode(string pEstablishmentKey, string pAuthCode);
        Task<VETransactions> GetByNsuHost(string pEstablishmentKey, string pTerminalId, string pNsuHost);
        Task<VETransactions> SearchTransationByNSUAsync(string pEstablishmentKey, string NSU);
        Task<VETransactions> CreateTransaction(VETransactions pTransaction);
        Task<VETransactions> UpdateTransaction(VETransactions pTransaction);
    }
}
