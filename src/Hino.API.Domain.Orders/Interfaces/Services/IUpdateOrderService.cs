﻿using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Orders.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Domain.Orders.Interfaces.Services
{
    public interface IUpdateOrderService : IErrorBaseService, IDisposable
    {
        Task<VEOrders> UpdateOrderAsync(VEOrders pOrder);
    }
}
