using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Orders.Models;
using Hino.API.Domain.Orders.Enums;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Hino.API.Domain.Orders.Interfaces.Services
{
    public interface IVEOrdersService : IBaseService<VEOrders>
    {
        VEOrders CreateOrder(VEOrders pOrder);
        Task<VEOrders> CanConvertToOrderAsync(string pEstablishmentKey, long id);
        Task<IEnumerable<VEOrders>> GetOrdersLinkedTreeAsync(string pEstablishmentKey, long id);
        Task<IEnumerable<VEOrders>> GetOrdersLinkedAsync(string pEstablishmentKey, long id);
        Task<IEnumerable<VEOrders>> GetOrderDirectLinkedAsync(string pEstablishmentKey, long id);
    }
}
