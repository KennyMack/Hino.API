﻿using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Orders.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Domain.Orders.Interfaces.Services
{
    public interface ICreateOrderService : IErrorBaseService, IDisposable
    {
        Task<VEOrders> CreateOrderAsync(VEOrders pOrder);
        Task<VEOrders> GenerateOrderRevisionAsync(VEOrderStatus pOrder);
        Task<VEOrders> ChangeStatusAsync(VEOrderStatus pOrder);
    }
}
