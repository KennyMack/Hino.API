using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Orders.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.API.Domain.Orders.Interfaces.Services
{
    public interface IVEOrderItemsService : IBaseService<VEOrderItems>
    {
        IEnumerable<VEOrderItems> GetItemsByOrderId(long OrderId, string pEstablishmentKey);
        Task<bool> RemoveAllItemsAsync(long pId, string pEstablishmentKey);
    }
}
