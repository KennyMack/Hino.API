﻿using Hino.API.Domain.Base.Services;
using Hino.API.Domain.Orders.Enums;
using Hino.API.Domain.Orders.Interfaces.Services;
using Hino.API.Domain.Orders.Interfaces.UoW;
using Hino.API.Domain.Orders.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DefaultMessages = Hino.API.Infra.Cross.Resources.MessagesResource;
using ValidationMessages = Hino.API.Infra.Cross.Resources.ValidationMessagesResource;

namespace Hino.API.Domain.Orders.Services
{
    public class RemoveOrderService : BaseUoWService<VEOrders>, IRemoveOrderService
    {
        private readonly IOrdersUoW _IOrdersUoW;
        public RemoveOrderService(IOrdersUoW pIOrdersUoW)
        {
            _IOrdersUoW = pIOrdersUoW;
        }

        public async Task<VEOrders> RemoveByIdAsync(long id, string pEstablishmentKey, string pUniqueKey)
        {
            var Order = await _IOrdersUoW.OrdersRepository.GetByIdAsync(id, pEstablishmentKey, pUniqueKey);

            if (Order == null)
            {
                Errors.Add(ModelException.CreateValidationError(ValidationMessages.NotFound, "VEOrders", id.ToString()));
                return null;
            }

            var OrderError = Order.AllowRemove();
            if (OrderError != null)
            {
                Errors.Add(OrderError);
                return null;
            }

            var OrdersLinked = await _IOrdersUoW.OrdersRepository.GetOrderDirectLinkedAsync(Order.EstablishmentKey, Order.Id);
            if (OrdersLinked.Any(r => r.Id != Order.Id))
            {
                Errors.Add(ModelException.CreateValidationError(DefaultMessages.OrderIsLinked, "VEOrders", id.ToString()));
                return null;
            }

            try
            {
                await _IOrdersUoW.OrderItemsRepository.RemoveAllItemsAsync(Order.Id, Order.EstablishmentKey);

                await _IOrdersUoW.OrdersRepository.RemoveById(Order.Id, Order.EstablishmentKey, Order.UniqueKey);

                _IOrdersUoW.Commit();
            }
            catch (Exception ex)
            {
                Errors.Add(ModelException.CreateValidationError(ex.Message, "VEOrders", id.ToString()));
                _IOrdersUoW.Rollback();
                return null;
            }

            return Order;
        }

        public void Dispose()
        {
            _IOrdersUoW.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
