﻿using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Base.Services;
using Hino.API.Domain.Orders.Enums;
using Hino.API.Domain.Orders.Interfaces.Services;
using Hino.API.Domain.Orders.Interfaces.UoW;
using Hino.API.Domain.Orders.Models;
using Hino.API.Infra.Cross.Utils;
using Hino.API.Infra.Cross.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DefaultMessages = Hino.API.Infra.Cross.Resources.MessagesResource;
using ValidationMessages = Hino.API.Infra.Cross.Resources.ValidationMessagesResource;

namespace Hino.API.Domain.Orders.Services
{
    public class CreateOrderService: BaseUoWService<VEOrders>, ICreateOrderService
    {
        private IOrdersUoW _IOrdersUoW;

        public CreateOrderService(IOrdersUoW pIOrdersUoW)
        {
            _IOrdersUoW = pIOrdersUoW;
        }

        public Task<VEOrders> CreateOrderAsync(VEOrders pOrder)
        {
            pOrder.GEEnterprises = null;
            pOrder.GEPaymentCondition = null;
            pOrder.GEPaymentType = null;
            pOrder.GEUserDigitizer = null;
            List<VEOrderItems> items = new();

            pOrder.CalculateDeliveryDate();
            if (pOrder.VEOrderItems != null)
            {
                items = pOrder.VEOrderItems.ToList();
                pOrder.VEOrderItems = null;
            }

            pOrder.CalculatePaymentDue(pOrder.DaysPayment);
            pOrder.DeliveryDate = pOrder.DeliveryDate.Date;
            pOrder.DigitizerID = pOrder.DigitizerID <= 0 ? pOrder.UserCreatedID : pOrder.DigitizerID;

            try
            {
                _IOrdersUoW.OrdersRepository.Add(pOrder);
                pOrder.DistributeCommission();
                pOrder.DistributeDiscount();

                foreach (var item in items)
                {
                    item.Id = 0;
                    item.EstablishmentKey = pOrder.EstablishmentKey;
                    item.OrderID = pOrder.Id;
                    item.DeliveryDate = item.DeliveryDate.Date;

                    _IOrdersUoW.OrderItemsRepository.Add(item);
                }

                _IOrdersUoW.Commit();

                var NewOrder = _IOrdersUoW.OrdersRepository.GetById(pOrder.Id, pOrder.EstablishmentKey, pOrder.UniqueKey);
                AddedOrUpdatedItems.Add(NewOrder);
                
                return Task.FromResult(NewOrder);
            }
            catch (Exception ex)
            {
                Errors.Add(ModelException.CreateValidationError(ex.Message, "VEOrders", pOrder.Id.ToString()));
                _IOrdersUoW.Rollback();
                return null;
            }
        }

        public async Task<VEOrders> GenerateOrderRevisionAsync(VEOrderStatus pOrder)
        {
            var OrderDB = await _IOrdersUoW.OrdersRepository.GetByIdAsync(pOrder.OrderID, pOrder.EstablishmentKey, pOrder.UniqueKey,
                r => r.GEEnterprises);

            if (OrderDB == null)
            {
                Errors.Add(ModelException.CreateValidationError(ValidationMessages.NotFound, "VEOrders", pOrder.OrderID.ToString()));
                return null;
            }

            try
            {
                int LastOrderVersion = 0;
                var MainOrderId = OrderDB.MainOrderID ?? OrderDB.Id;

                var OrdersLinked = await _IOrdersUoW.OrdersRepository.GetOrdersLinkedAsync(pOrder.EstablishmentKey, MainOrderId);

                LastOrderVersion = (OrdersLinked.Any() ? OrdersLinked.Max(r => r.OrderVersion) : LastOrderVersion) + 1;

                var OrderDestiny = new VEOrders();

                OrderDestiny.CopyProperties(OrderDB);

                OrderDB.RevisionReason = pOrder.RevisionReason;

                OrderDestiny.DigitizerID = pOrder.DigitizerID;
                OrderDestiny.RevisionReason = "";
                OrderDestiny.Id = 0;
                OrderDestiny.IdERP = 0;
                OrderDestiny.OriginOrderID = OrderDB.Id;
                OrderDestiny.MainOrderID = OrderDB.MainOrderID ?? OrderDB.Id;
                OrderDestiny.OrderVersion = LastOrderVersion;
                OrderDestiny.StatusSinc = EStatusSinc.Sincronized;
                OrderDestiny.StatusCRM = "P";
                OrderDestiny.Status = "P";
                OrderDestiny.Created = DateTime.Now;
                OrderDestiny.Modified = DateTime.Now;

                OrderDestiny.CalculatePaymentDue(pOrder.DaysPayment);

                OrderDestiny.IsActive = true;
                OrderDestiny.EstablishmentKey = pOrder.EstablishmentKey;
                OrderDestiny.UniqueKey = Guid.NewGuid().ToString();

                OrderDestiny.GEEnterprises = null;
                OrderDestiny.GEPaymentCondition = null;
                OrderDestiny.GEPaymentType = null;
                OrderDestiny.GERedispatch = null;
                OrderDestiny.GECarriers = null;
                OrderDestiny.GEEnterprises = null;
                OrderDestiny.GEUsers = null;
                OrderDestiny.VEOrderItems = null;
                OrderDestiny.GEUserDigitizer = null;
                OrderDestiny.MainFiscalOper = null;
                OrderDestiny.VEOrderItems = new List<VEOrderItems>();

                long RegionId = 0;

                if (pOrder.CanChangeValue && OrderDestiny.IsProposal)
                    RegionId = OrderDB.GEEnterprises.RegionId;

                foreach (var item in OrderDB.VEOrderItems)
                {
                    var itemDB = new VEOrderItems();

                    itemDB.CopyProperties(item);

                    itemDB.Id = 0;
                    itemDB.IdERP = 0;
                    itemDB.Created = DateTime.Now;
                    itemDB.Modified = DateTime.Now;
                    itemDB.IsActive = true;
                    itemDB.UniqueKey = Guid.NewGuid().ToString();
                    itemDB.EstablishmentKey = pOrder.EstablishmentKey;

                    if (pOrder.CanChangeValue && RegionId > 0 && OrderDestiny.IsProposal)
                    {
                        var Price = await _IOrdersUoW.SalePriceRepository.FirstOrDefaultAsync(r =>
                            r.EstablishmentKey == OrderDB.EstablishmentKey &&
                            r.RegionId == RegionId &&
                            r.ProductId == item.ProductID);

                        if (Price != null)
                        {
                            itemDB.Value = (float)Price.Value;
                            itemDB.TableValue = Price.Value;
                            itemDB.PercDiscount = 0;
                        }
                    }

                    itemDB.VEOrders = null;
                    itemDB.GEProducts = null;
                    itemDB.FSFiscalOper = null;
                    itemDB.VEOrderTaxes = null;

                    OrderDestiny.VEOrderItems.Add(itemDB);
                }

                OrderDestiny.UserCreatedID = OrderDestiny.DigitizerID;

                var Created = await CreateOrderAsync(OrderDestiny);

                if (this.Errors.Any())
                    throw new Exception("Não foi possível gerar o pedido.");


                await ChangeStatusAsync(new VEOrderStatus
                {
                    EstablishmentKey = OrderDB.EstablishmentKey,
                    OrderID = OrderDB.Id,
                    Status = OrderDB.Status,
                    UniqueKey = OrderDB.UniqueKey,
                    RevisionReason = pOrder.RevisionReason
                });

                _IOrdersUoW.Commit();

                return Created;
            }
            catch (Exception ex)
            {
                Errors.Add(ModelException.CreateValidationError(ex.Message, "VEOrders", pOrder.OrderID.ToString()));
                _IOrdersUoW.Rollback();
                return null;
            }
        }

        public async Task<VEOrders> ChangeStatusAsync(VEOrderStatus pOrder)
        {
            if (!pOrder.Status.In("A", "E", "M", "P", "R", "C", "O", "G"))
            {
                Errors.Add(ModelException.CreateValidationError(ValidationMessages.InvalidOrderStatus, "VEOrders", pOrder.OrderID.ToString()));
                return null;
            }
            try
            {
                var OrderDB = _IOrdersUoW.OrdersRepository.GetById(pOrder.OrderID, pOrder.EstablishmentKey, pOrder.UniqueKey);

                if (OrderDB == null)
                {
                    Errors.Add(ModelException.CreateValidationError(ValidationMessages.NotFound, "VEOrders", pOrder.OrderID.ToString()));
                    return null;
                }

                OrderDB.RevisionReason = pOrder.RevisionReason;
                OrderDB.StatusCRM = pOrder.Status;
                if (OrderDB.Status == "P" && pOrder.Status.In("R", "C"))
                    OrderDB.Status = pOrder.Status;

                OrderDB.GEEnterprises = null;
                OrderDB.GECarriers = null;
                OrderDB.GERedispatch = null;
                OrderDB.GEEnterprises = null;
                OrderDB.GEPaymentCondition = null;
                OrderDB.GEPaymentType = null;
                OrderDB.GEUsers = null;
                OrderDB.MainFiscalOper = null;
                OrderDB.GEUserDigitizer = null;
                OrderDB.VEOrderItems = null;

                _IOrdersUoW.OrdersRepository.Update(OrderDB);

                if (pOrder.Status == "A")
                {
                    var OrdersLinked = await _IOrdersUoW.OrdersRepository.GetOrdersLinkedTreeAsync(OrderDB.EstablishmentKey, OrderDB.MainOrderID ?? OrderDB.Id);

                    foreach (var item in OrdersLinked)
                    {
                        if (item.Id != OrderDB.Id)
                        {
                            var orderToReprove = _IOrdersUoW.OrdersRepository.GetByIdToUpdate(item.Id, item.EstablishmentKey, item.UniqueKey);

                            orderToReprove.GEEnterprises = null;
                            orderToReprove.GECarriers = null;
                            orderToReprove.GERedispatch = null;
                            orderToReprove.GEEnterprises = null;
                            orderToReprove.GEPaymentCondition = null;
                            orderToReprove.GEPaymentType = null;
                            orderToReprove.GEUsers = null;
                            orderToReprove.MainFiscalOper = null;
                            orderToReprove.GEUserDigitizer = null;
                            orderToReprove.VEOrderItems = null;
                            orderToReprove.StatusCRM = "R";
                            _IOrdersUoW.OrdersRepository.Update(orderToReprove);
                        }
                    }
                }

                return OrderDB;
            }
            catch (Exception ex)
            {
                Errors.Add(ModelException.CreateValidationError(ex.Message, "VEOrders", pOrder.OrderID.ToString()));
                _IOrdersUoW.Rollback();
                return null;
            }
        }

        public void Dispose()
        {
            _IOrdersUoW.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
