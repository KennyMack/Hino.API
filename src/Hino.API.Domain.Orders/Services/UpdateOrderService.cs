﻿using Hino.API.Domain.Base.Services;
using Hino.API.Domain.Orders.Interfaces.Services;
using Hino.API.Domain.Orders.Interfaces.UoW;
using Hino.API.Domain.Orders.Models;
using Hino.API.Infra.Cross.Utils;
using Hino.API.Infra.Cross.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Domain.Orders.Services
{
    public class UpdateOrderService : BaseUoWService<VEOrders>, IUpdateOrderService
    {
        private IOrdersUoW _IOrdersUoW;
        public UpdateOrderService(IOrdersUoW pIOrdersUoW)
        {
            _IOrdersUoW = pIOrdersUoW;
        }

        public async Task<VEOrders> UpdateOrderAsync(VEOrders pOrder)
        {
            pOrder.GEEnterprises = null;
            pOrder.GEPaymentCondition = null;
            pOrder.GEPaymentType = null;

            List<VEOrderItems> items = new List<VEOrderItems>();
            List<VEOrderTaxes> taxes = new List<VEOrderTaxes>();

            if (pOrder.VEOrderItems != null)
            {
                items = pOrder.VEOrderItems.ToList();
                pOrder.VEOrderItems = null;
            }

            try
            {
                var OrderDB = _IOrdersUoW.OrdersRepository.GetByIdToUpdate(pOrder.Id, pOrder.EstablishmentKey, pOrder.UniqueKey);

                OrderDB.GEEnterprises = null;

                OrderDB.CarrierID = pOrder.CarrierID;
                OrderDB.GECarriers = null;

                OrderDB.RedispatchID = pOrder.RedispatchID;
                OrderDB.GERedispatch = null;

                OrderDB.GEUsers = null;
                OrderDB.GEUserDigitizer = null;

                OrderDB.TypePaymentID = pOrder.TypePaymentID;
                OrderDB.GEPaymentType = null;

                OrderDB.PayConditionID = pOrder.PayConditionID;
                OrderDB.GEPaymentCondition = null;

                OrderDB.MainFiscalOperID = pOrder.MainFiscalOperID;
                OrderDB.MainFiscalOper = null;

                OrderDB.FinancialTaxes = pOrder.FinancialTaxes;
                OrderDB.OnlyOnDate = pOrder.OnlyOnDate;
                OrderDB.AllowPartial = pOrder.AllowPartial;
                OrderDB.RevisionReason = pOrder.RevisionReason;
                OrderDB.CodPedVenda = pOrder.CodPedVenda;
                OrderDB.NumPedMob = pOrder.NumPedMob;
                OrderDB.DeliveryDate = pOrder.DeliveryDate.Date;
                OrderDB.Note = pOrder.Note;
                OrderDB.InnerNote = pOrder.InnerNote;
                OrderDB.DetailedNote = pOrder.DetailedNote;
                OrderDB.StatusCRM = pOrder.StatusCRM;
                OrderDB.Status = pOrder.Status;
                if ((pOrder.Status != "P" && pOrder.StatusCRM == "P") || pOrder.Status == "O")
                    OrderDB.StatusCRM = OrderDB.Status;

                OrderDB.StatusSinc = pOrder.StatusSinc;
                OrderDB.IsProposal = pOrder.IsProposal;
                OrderDB.FreightPaidBy = pOrder.FreightPaidBy;
                OrderDB.RedispatchPaidBy = pOrder.RedispatchPaidBy;
                OrderDB.FreightValue = pOrder.FreightValue;
                OrderDB.ClientOrder = pOrder.ClientOrder;
                OrderDB.IdERP = pOrder.IdERP;
                OrderDB.OriginOrderID = pOrder.OriginOrderID;
                OrderDB.OrderVersion = pOrder.OrderVersion;
                OrderDB.PercCommission = pOrder.PercCommission;
                OrderDB.PercDiscount = pOrder.PercDiscount;
                OrderDB.ContactPhone = pOrder.ContactPhone;
                OrderDB.ContactEmail = pOrder.ContactEmail;
                OrderDB.Contact = pOrder.Contact;
                OrderDB.Sector = pOrder.Sector;
                OrderDB.InPerson = pOrder.InPerson;
                OrderDB.PaymentDueDate = pOrder.PaymentDueDate;

                OrderDB.CalculateDeliveryDate(items);

                if (pOrder.Converted)
                {
                    OrderDB.Note = OrderDB.DetailedNote;

                    pOrder.CalculatePaymentDue(pOrder.DaysPayment);
                }

                OrderDB.VEOrderItems = null;
                var itemsDB = _IOrdersUoW.OrderItemsRepository.GetItemsByOrderId(pOrder.Id, pOrder.EstablishmentKey);

                _IOrdersUoW.OrdersRepository.Update(OrderDB);

                await _IOrdersUoW.OrderTaxesRepository.ClearOrderTaxesAsync(pOrder.EstablishmentKey, pOrder.Id);

                OrderDB.DistributeCommission();
                OrderDB.DistributeDiscount();

                foreach (var item in items)
                {
                    item.DeliveryDate = item.DeliveryDate.Date;
                    taxes = item.VEOrderTaxes.ToList();

                    item.GEProducts = null;
                    item.FSFiscalOper = null;
                    item.VEOrderTaxes = null;

                    VEOrderItems itemDB = _IOrdersUoW.OrderItemsRepository.GetByIdToUpdate(item.Id, item.EstablishmentKey, item.UniqueKey);

                    if (itemDB == null)
                        itemDB = new VEOrderItems();

                    itemDB.OrderID = OrderDB.Id;
                    itemDB.GEProducts = null;
                    itemDB.VEOrders = null;
                    itemDB.FSFiscalOper = null;
                    itemDB.VEOrderTaxes = null;
                    itemDB.FiscalOperID = item.FiscalOperID;
                    itemDB.TableValue = item.TableValue;
                    itemDB.Value = item.Value;
                    itemDB.Quantity = item.Quantity;
                    itemDB.QuantityReference = item.QuantityReference;
                    itemDB.ShippingDays = item.ShippingDays;
                    itemDB.PercDiscount = item.PercDiscount;
                    itemDB.Note = item.Note;
                    itemDB.Item = item.Item;
                    itemDB.ItemLevel = item.ItemLevel;
                    itemDB.IdERP = item.IdERP;
                    itemDB.ClientOrder = item.ClientOrder;
                    itemDB.ClientItem = item.ClientItem;
                    itemDB.DeliveryDate = item.DeliveryDate;
                    itemDB.PercDiscountHead = item.PercDiscountHead;
                    itemDB.PercCommissionHead = item.PercCommissionHead;
                    itemDB.PercCommission = item.PercCommission;
                    itemDB.AltDescription = item.AltDescription;
                    itemDB.QuantityReturned = item.QuantityReturned;
                    itemDB.EstablishmentKey = pOrder.EstablishmentKey;
                    itemDB.UniqueKey = item.Id == 0 ? "" : item.UniqueKey;
                    itemDB.ProductID = item.ProductID;

                    if (item.Id == 0)
                        _IOrdersUoW.OrderItemsRepository.Add(item);
                    else
                        _IOrdersUoW.OrderItemsRepository.Update(itemDB);

                    foreach (var tax in taxes)
                    {
                        if (item.Id > 0)
                        {
                            tax.OrderItemID = item.Id;
                            _IOrdersUoW.OrderTaxesRepository.Add(tax);
                        }
                    }
                }

                foreach (var item in itemsDB)
                {
                    if (!items.Any(r => r.Id == item.Id))
                        await _IOrdersUoW.OrderTaxesRepository.RemoveById(item.Id, item.EstablishmentKey, item.UniqueKey);
                }

                _IOrdersUoW.Commit();
                var NewOrder = _IOrdersUoW.OrdersRepository.GetById(pOrder.Id, pOrder.EstablishmentKey, pOrder.UniqueKey);
                AddedOrUpdatedItems.Add(NewOrder);

                return NewOrder;
            }
            catch (Exception ex)
            {
                Errors.Add(ModelException.CreateValidationError(ex.Message, "VEOrders", pOrder.Id.ToString()));
                _IOrdersUoW.Rollback();
                return null;
            }
        }

        public void Dispose()
        {
            _IOrdersUoW.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
