using Hino.API.Domain.Base.Services;
using Hino.API.Domain.Orders.Enums;
using Hino.API.Domain.Orders.Interfaces.Repositories;
using Hino.API.Domain.Orders.Interfaces.Services;
using Hino.API.Domain.Orders.Models;
using Hino.API.Infra.Cross.Utils.Enums;
using Hino.API.Infra.Cross.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DefaultMessages = Hino.API.Infra.Cross.Resources.MessagesResource;
using ValidationMessages = Hino.API.Infra.Cross.Resources.ValidationMessagesResource;

namespace Hino.API.Domain.Orders.Services
{
    public class VEOrdersService : BaseService<VEOrders>, IVEOrdersService
    {
        private readonly IVEOrdersRepository _IVEOrdersRepository;

        public VEOrdersService(IVEOrdersRepository pIVEOrdersRepository) :
             base(pIVEOrdersRepository)
        {
            _IVEOrdersRepository = pIVEOrdersRepository;
        }

        public async Task<VEOrders> CanConvertToOrderAsync(string pEstablishmentKey, long id)
        {
            try
            {
                return await _IVEOrdersRepository.CanConvertToOrderAsync(pEstablishmentKey, id);
            }
            catch (Exception ex)
            {
                Errors.Add(ModelException.CreateValidationError(ex.Message, "VEOrders", id.ToString()));
                return null;
            }
        }

        public VEOrders CreateOrder(VEOrders pOrder)
        {
            try
            {
                _IVEOrdersRepository.Add(pOrder);
                AddedOrUpdatedItems.Add(pOrder);
            }
            catch (Exception e)
            {
                Logging.Exception(e);
                Errors.Add(ModelException.CreateSqlError(e, EExceptionErrorCodes.InsertSQLError));
            }

            return pOrder;
        }

        public async Task<IEnumerable<VEOrders>> GetOrderDirectLinkedAsync(string pEstablishmentKey, long id) =>
            await _IVEOrdersRepository.GetOrderDirectLinkedAsync(pEstablishmentKey, id);

        public async Task<IEnumerable<VEOrders>> GetOrdersLinkedAsync(string pEstablishmentKey, long id) =>
            await _IVEOrdersRepository.GetOrdersLinkedAsync(pEstablishmentKey, id);

        public async Task<IEnumerable<VEOrders>> GetOrdersLinkedTreeAsync(string pEstablishmentKey, long id) =>
            await _IVEOrdersRepository.GetOrdersLinkedTreeAsync(pEstablishmentKey, id);
    }
}
