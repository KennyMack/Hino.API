using Hino.API.Domain.Base.Services;
using Hino.API.Domain.Orders.Enums;
using Hino.API.Domain.Orders.Interfaces.Repositories;
using Hino.API.Domain.Orders.Interfaces.Services;
using Hino.API.Domain.Orders.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DefaultMessages = Hino.API.Infra.Cross.Resources.MessagesResource;
using ValidationMessages = Hino.API.Infra.Cross.Resources.ValidationMessagesResource;

namespace Hino.API.Domain.Orders.Services
{
    public class VETransactionsService : BaseService<VETransactions>, IVETransactionsService
    {
        private readonly IVETransactionsRepository _IVETransactionsRepository;

        public VETransactionsService(IVETransactionsRepository pIVETransactionsRepository) :
             base(pIVETransactionsRepository)
        {
            _IVETransactionsRepository = pIVETransactionsRepository;
        }


        public async Task<VETransactions> CreateTransaction(VETransactions pTransaction)
        {
            try
            {
                var CreateTransaction = pTransaction;

                _IVETransactionsRepository.Add(CreateTransaction);
                await _IVETransactionsRepository.SaveChanges();

                return CreateTransaction;
            }
            catch (Exception ex)
            {
                Logging.Exception(ex);
                Errors.Add(ModelException.CreateValidationError(ex.Message, "VETransactions", pTransaction.Id.ToString()));
                return null;
            }
        }

        public async Task<VETransactions> UpdateTransaction(VETransactions pTransaction)
        {
            try
            {
                _IVETransactionsRepository.Update(pTransaction);
                await _IVETransactionsRepository.SaveChanges();

                return pTransaction;
            }
            catch (Exception ex)
            {
                Logging.Exception(ex);
                Errors.Add(ModelException.CreateValidationError(ex.Message, "VETransactions", pTransaction.Id.ToString()));
                return null;
            }
        }

        public override async Task<VETransactions> RemoveById(long id, string pEstablishmentKey, string pUniqueKey)
        {
            try
            {
                var transaction = await _IVETransactionsRepository.GetByIdAsync(id, pEstablishmentKey, pUniqueKey);

                if (transaction?.Status == ETransactionStatus.Done)
                {
                    Errors.Add(ModelException.CreateValidationError(DefaultMessages.RemoveNotAllowed, "VETransactions", id.ToString()));
                    return null;
                }

                await _IVETransactionsRepository.RemoveById(id, pEstablishmentKey, pUniqueKey);
                await _IVETransactionsRepository.SaveChanges();
                return transaction;
            }
            catch (Exception ex)
            {
                Logging.Exception(ex);
                Errors.Add(ModelException.CreateValidationError(ex.Message, "VETransactions", id.ToString()));
                return null;
            }
        }

        public async Task<VETransactions> SearchTransationByNSUAsync(string pEstablishmentKey, string NSU) =>
            await _IVETransactionsRepository.SearchTransationByNSUAsync(pEstablishmentKey, NSU);

        public async Task<IEnumerable<VETransactions>> SyncData(string pEstablishmentKey, VETransactions[] pRegisters)
        {
            try
            {
                List<ModelException> SyncErrors = new List<ModelException>();

                for (int i = 0, length = pRegisters.Length; i < length; i++)
                {
                    VETransactions result;

                    try
                    {
                        if (pRegisters[i].Id <= 0)
                            result = await CreateTransaction(pRegisters[i]);
                        else
                            result = await UpdateTransaction(pRegisters[i]);
                       
                        pRegisters[i] = result;
                    }
                    catch (Exception)
                    {
                        pRegisters[i].Id = 0;
                        SyncErrors.Add(Errors[0]);
                    }
                }

                await SaveChanges();

                return pRegisters;
            }
            catch (Exception ex)
            {
                Logging.Exception(ex);
                Errors.Add(ModelException.CreateValidationError(ex.Message, "VETransactions", ""));
                return null;
            }
        }

        public async Task<VETransactions> GetByAuthCode(string pEstablishmentKey, string pAuthCode) =>
            await _IVETransactionsRepository.GetByAuthCode(pEstablishmentKey, pAuthCode);

        public async Task<VETransactions> GetByNsuHost(string pEstablishmentKey, string pTerminalId, string pNsuHost) =>
            await _IVETransactionsRepository.GetByNsuHost(pEstablishmentKey, pTerminalId, pNsuHost);
    }
}
