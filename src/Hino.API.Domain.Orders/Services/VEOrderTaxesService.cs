using Hino.API.Domain.Base.Services;
using Hino.API.Domain.Orders.Interfaces.Repositories;
using Hino.API.Domain.Orders.Interfaces.Services;
using Hino.API.Domain.Orders.Models;
using System.Threading.Tasks;

namespace Hino.API.Domain.Orders.Services
{
    public class VEOrderTaxesService : BaseService<VEOrderTaxes>, IVEOrderTaxesService
    {
        private readonly IVEOrderTaxesRepository _IVEOrderTaxesRepository;

        public VEOrderTaxesService(IVEOrderTaxesRepository pIVEOrderTaxesRepository) :
             base(pIVEOrderTaxesRepository)
        {
            _IVEOrderTaxesRepository = pIVEOrderTaxesRepository;
        }

        public async Task ClearOrderTaxesAsync(string pEstablishmentKey, long pOrderId)
        {
            var Taxes = await _IVEOrderTaxesRepository.QueryAsync(r =>
                r.VEOrderItems.OrderID == pOrderId &&
                r.EstablishmentKey == pEstablishmentKey);

            foreach (var tax in Taxes)
                await _IVEOrderTaxesRepository.RemoveById(tax.Id, tax.EstablishmentKey, tax.UniqueKey);
        }
    }
}
