using Hino.API.Domain.Base.Services;
using Hino.API.Domain.Orders.Interfaces.Repositories;
using Hino.API.Domain.Orders.Interfaces.Services;
using Hino.API.Domain.Orders.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.API.Domain.Orders.Services
{
    public class VEOrderItemsService : BaseService<VEOrderItems>, IVEOrderItemsService
    {
        private readonly IVEOrderItemsRepository _IVEOrderItemsRepository;

        public VEOrderItemsService(IVEOrderItemsRepository pIVEOrderItemsRepository) :
             base(pIVEOrderItemsRepository)
        {
            _IVEOrderItemsRepository = pIVEOrderItemsRepository;
        }

        public IEnumerable<VEOrderItems> GetItemsByOrderId(long OrderId, string pEstablishmentKey) =>
            _IVEOrderItemsRepository.GetItemsByOrderId(OrderId, pEstablishmentKey);

        public async Task<bool> RemoveAllItemsAsync(long pId, string pEstablishmentKey)
        {
            var Items = await _IVEOrderItemsRepository.QueryAsync(
                r => r.OrderID == pId &&
                     r.EstablishmentKey == pEstablishmentKey);

            foreach (VEOrderItems item in Items)
                await _IVEOrderItemsRepository.RemoveById(item.Id, item.EstablishmentKey, item.UniqueKey);

            return true;
        }
    }
}
