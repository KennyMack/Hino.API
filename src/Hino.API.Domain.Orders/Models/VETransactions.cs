﻿using Hino.API.Domain.Base.Models;
using Hino.API.Domain.Orders.Enums;
using Hino.API.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Domain.Orders.Models
{
    [Queue("VETransactions")]
    public class VETransactions : BaseEntity
    {
        [ForeignKey("VEOrders")]
        public long OrderID { get; set; }
        public virtual VEOrders VEOrders { get; set; }
        public ETransactionStatus Status { get; set; }
        public ETransactionType Type { get; set; }
        public string AuthCode { get; set; }
        public DateTime? AuthDate { get; set; }
        public string AcquirerName { get; set; }
        public string NSU { get; set; }
        public string NSUHost { get; set; }
        public string AdminCode { get; set; }
        public string CardBrand { get; set; }
        public string CardLastDigits { get; set; }
        public decimal? Installments { get; set; }
        public decimal? Amount { get; set; }
        public string Description { get; set; }
        public string CustomerReceipt { get; set; }
        public string MerchantReceipt { get; set; }
        public string TerminalId { get; set; }
        public string Operator { get; set; }
        public string StoreId { get; set; }
        public bool Reversed { get; set; }
        public long IdERP { get; set; }
        public EStatusSinc StatusSinc { get; set; }
        public ETransactionOrigin Origin { get; set; }
    }
}
