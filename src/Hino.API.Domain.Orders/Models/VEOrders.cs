﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hino.API.Domain.Base.Models;
using Hino.API.Domain.Orders.Enums;
using Hino.API.Infra.Cross.Utils;
using Hino.API.Infra.Cross.Utils.Attributes;
using Hino.API.Infra.Cross.Utils.Exceptions;
using DefaultMessages = Hino.API.Infra.Cross.Resources.MessagesResource;
using ValidationMessages = Hino.API.Infra.Cross.Resources.ValidationMessagesResource;

namespace Hino.API.Domain.Orders.Models
{
    [Queue("VEOrders")]
    public class VEOrders : BaseEntity
    {
        public VEOrders()
        {
            this.VEOrderItems = new HashSet<VEOrderItems>();
            this.VETransactions = new HashSet<VETransactions>();
            // this.GEFilesPath = new HashSet<GEFilesPath>();
        }

        [ForeignKey("GEEnterprises")]
        [Column("ENTERPRISEID")]
        public long EnterpriseID { get; set; }
        public virtual VECustomer GEEnterprises { get; set; }

        [ForeignKey("GECarriers")]
        [Column("CARRIERID")]
        public long? CarrierID { get; set; }
        public virtual VECarrier GECarriers { get; set; }

        [ForeignKey("GERedispatch")]
        [Column("REDISPATCHID")]
        public long? RedispatchID { get; set; }
        public virtual VECarrier GERedispatch { get; set; }

        [ForeignKey("GEUsers")]
        public long UserID { get; set; }
        public virtual VESallesman GEUsers { get; set; }

        [ForeignKey("GEPaymentType")]
        public long TypePaymentID { get; set; }
        public virtual VEPaymentType GEPaymentType { get; set; }

        [ForeignKey("GEPaymentCondition")]
        public long PayConditionID { get; set; }
        public virtual VEPaymentCondition GEPaymentCondition { get; set; }

        public long CodPedVenda { get; set; }
        public long NumPedMob { get; set; }
        public System.DateTime DeliveryDate { get; set; }
        public string Note { get; set; }
        public string DetailedNote { get; set; }
        public string InnerNote { get; set; }
        public string Status { get; set; }
        public string StatusCRM { get; set; }
        public EStatusSinc StatusSinc { get; set; }
        public bool IsProposal { get; set; }
        public EFreightPaidBy FreightPaidBy { get; set; }
        public EFreightPaidBy RedispatchPaidBy { get; set; }
        public decimal FreightValue { get; set; }
        public string ClientOrder { get; set; }

        public decimal FinancialTaxes { get; set; }
        public bool OnlyOnDate { get; set; }
        public bool AllowPartial { get; set; }

        public long IdERP { get; set; }
        public long? OriginOrderID { get; set; }
        public long? MainOrderID { get; set; }
        public int OrderVersion { get; set; }
        public string RevisionReason { get; set; }

        [ForeignKey("MainFiscalOper")]
        public long? MainFiscalOperID { get; set; }
        public virtual VEFiscalOper MainFiscalOper { get; set; }

        [ForeignKey("GEUserDigitizer")]
        [Column("DIGITIZERID")]
        public long DigitizerID { get; set; }
        public virtual VESallesman GEUserDigitizer { get; set; }

        public float PercDiscount { get; set; }
        public float PercCommission { get; set; }

        public string ContactPhone { get; set; }
        public string ContactEmail { get; set; }
        public string Contact { get; set; }
        public EContactSectors Sector { get; set; }

        public bool InPerson { get; set; }

        public DateTime? PaymentDueDate { get; set; }

        public virtual ICollection<VEOrderItems> VEOrderItems { get; set; }
        public virtual ICollection<VETransactions> VETransactions { get; set; }
        // public virtual ICollection<GEFilesPath> GEFilesPath { get; set; }

        public ModelException AllowRemove()
        {
            if (StatusSinc == EStatusSinc.Integrated)
                return ModelException.CreateValidationError(DefaultMessages.StatusSincNotAllowRemove, "VEOrders", Id.ToString());

            if (Status != "P")
                return ModelException.CreateValidationError(DefaultMessages.OrderStatusNotAllowRemove, "VEOrders", Id.ToString());

            return null;
        }

        public void DistributeCommission()
        {
            if (PercCommission > 0)
            {
                foreach (var item in VEOrderItems)
                    item.PercCommissionHead = PercCommission;
            }
        }

        public void DistributeDiscount()
        {
            if (PercDiscount > 0)
            {
                foreach (var item in VEOrderItems)
                    item.PercDiscountHead = PercDiscount;
            }
        }

        public void CalculatePaymentDue(int pDaysPayment)
        {
            PaymentDueDate = DateTime.Now;
            if (!IsProposal)
                PaymentDueDate = DateTime.Now.AddWorkdays(pDaysPayment);
        }

        public void CalculateDeliveryDate(ICollection<VEOrderItems> pItems = null)
        {
            if (!IsProposal && !Converted)
                return;

            if (pItems == null)
                pItems = VEOrderItems;

            var Days = pItems?.Max(r => r.ShippingDays) ?? 0;

            DeliveryDate = DateTime.Now.AddWorkdays(Days);
            foreach (var item in VEOrderItems)
                item.DeliveryDate = DateTime.Now.AddWorkdays(item.ShippingDays);
        }

        [NotMapped]
        public long UserCreatedID { get; set; }

        [NotMapped]
        public int DaysPayment { get; set; }

        [NotMapped]
        public bool Converted { get; set; }

        [NotMapped]
        public float TotalValue
        {
            get
            {
                if (VEOrderItems != null)
                {
                    return VEOrderItems.Where(r => r.ItemLevel == 0).Sum(r =>
                        (r.Quantity * r.Value)
                    );
                }

                return 0;
            }
        }

        [NotMapped]
        public float TotalValueDiscount
        {
            get
            {
                if (VEOrderItems != null)
                {
                    return this.TotalValue - VEOrderItems.Where(r => r.ItemLevel == 0).Sum(r =>
                        (r.Quantity * r.Value) * (r.PercDiscount / 100)
                    ); ;
                }

                return 0;
            }
        }

        [NotMapped]
        public float TotalValueWithDiscount
        {
            get => TotalValueDiscount - (TotalValueDiscount * (PercDiscount / 100));
        }

        [NotMapped]
        public float TotalValueCommission
        {
            //get => (TotalValueWithDiscount * (PercCommission / 100));
            // Alterei para calcular comissão pelos itens para ficar igual ao ERP
            get
            {
                if (VEOrderItems != null && VEOrderItems.Any())
                {
                    return VEOrderItems.Where(r => r.ItemLevel == 0).Sum(r => (r.TotalValue - (r.TotalValue * (r.PercDiscount / 100))) * (r.PercCommission / 100));
                }

                return 0;
            }
        }


        [NotMapped]
        public float TotalValueWithDiscountAndTaxes
        {
            get
            {
                if (VEOrderItems != null && VEOrderItems.Any())
                {
                    return (float)((decimal)TotalValueWithDiscount +
                        VEOrderItems.Sum(r => r.TotalIPI + r.TotalICMSST));
                }

                return TotalValueWithDiscount;
            }
        }

        [NotMapped]
        public decimal TotalICMSDiferencial
        {
            get
            {
                if (VEOrderItems != null && VEOrderItems.Any())
                {
                    return VEOrderItems.Sum(r => r.TotalICMSST);
                }

                return 0;
            }
        }
    }
}
