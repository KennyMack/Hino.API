﻿using Hino.API.Domain.Base.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Domain.Orders.Models
{
    [Table("GEUSERS")]
    public class VESallesman: BaseEntity
    {
        public string Name { get; set; }

        public VESallesman()
        {
            this.VEOrders = new HashSet<VEOrders>();
            this.GEUserDigitizer = new HashSet<VEOrders>();
        }

        public virtual ICollection<VEOrders> VEOrders { get; set; }
        [InverseProperty("GEUserDigitizer")]
        public virtual ICollection<VEOrders> GEUserDigitizer { get; set; }
    }
}
