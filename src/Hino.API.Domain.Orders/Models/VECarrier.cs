﻿using Hino.API.Domain.Base.Models;
using Hino.API.Domain.Orders.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Domain.Orders.Models
{
    [Table("GEENTERPRISES")]
    public class VECarrier : BaseEntity
    {
        public VECarrier()
        {
            VEOrdersCarrier = new HashSet<VEOrders>();
            VEOrdersRedispatch = new HashSet<VEOrders>();
        }

        public string RazaoSocial { get; set; }
        public string NomeFantasia { get; set; }
        public string CNPJCPF { get; set; }
        public string IE { get; set; }
        public EEnterpriseType Type { get; set; }
        public EStatusEnterprise Status { get; set; }
        public EEnterpriseClassification Classification { get; set; }
        public long RegionId { get; set; }

        [InverseProperty("GECarriers")]
        public virtual ICollection<VEOrders> VEOrdersCarrier { get; set; }

        [InverseProperty("GERedispatch")]
        public virtual ICollection<VEOrders> VEOrdersRedispatch { get; set; }
    }
}
