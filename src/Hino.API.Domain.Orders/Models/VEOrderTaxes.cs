﻿using Hino.API.Domain.Base.Models;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.API.Domain.Orders.Models
{
    public class VEOrderTaxes : BaseEntity
    {
        [ForeignKey("VEOrderItems")]
        public long OrderItemID { get; set; }
        public virtual VEOrderItems VEOrderItems { get; set; }
        public short Type { get; set; }
        public decimal FreeZone { get; set; }
        public decimal Aliquot { get; set; }
        public decimal Basis { get; set; }
        public decimal Value { get; set; }
        public decimal ExemptValue { get; set; }
        public decimal OtherValue { get; set; }
        public decimal NoteValue { get; set; }
        public decimal MVA { get; set; }
        public decimal PerRedBC { get; set; }
    }
}
