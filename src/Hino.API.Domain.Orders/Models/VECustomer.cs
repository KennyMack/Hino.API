﻿using Hino.API.Domain.Base.Models;
using Hino.API.Domain.Orders.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Domain.Orders.Models
{
    [Table("GEENTERPRISES")]
    public class VECustomer: BaseEntity
    {
        public VECustomer()
        {
            VEOrders = new HashSet<VEOrders>();
        }

        public string RazaoSocial { get; set; }
        public string NomeFantasia { get; set; }
        public string CNPJCPF { get; set; }
        public string IE { get; set; }
        public EEnterpriseType Type { get; set; }
        public EStatusEnterprise Status { get; set; }
        public EEnterpriseClassification Classification { get; set; }
        public long RegionId { get; set; }

        [InverseProperty("GEEnterprises")]
        public virtual ICollection<VEOrders> VEOrders { get; set; }
    }
}
