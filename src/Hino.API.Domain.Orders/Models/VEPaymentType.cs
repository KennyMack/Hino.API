﻿using Hino.API.Domain.Base.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Domain.Orders.Models
{
    [Table("GEPAYMENTTYPE")]
    public class VEPaymentType : BaseEntity
    {
        public string Description { get; set; }
    }
}
