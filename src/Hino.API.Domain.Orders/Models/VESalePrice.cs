﻿using Hino.API.Domain.Base.Models;
using Hino.API.Infra.Cross.Utils.Attributes;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.API.Domain.Orders.Models
{
    public class VESalePrice : BaseEntity
    {
        public VESalePrice()
        {

        }

        public long CodPrVenda { get; set; }
        public long RegionId { get; set; }

        public string Description { get; set; }

        public long ProductId { get; set; }

        public virtual VEProducts GEProducts { get; set; }

        public decimal Value { get; set; }
    }
}
