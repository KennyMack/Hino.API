﻿using Hino.API.Domain.Orders.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Domain.Orders.Utils
{
    public static class ConvertFreightPaidBy
    {
        public static EFreightPaidBy ToFreightPaidBy(this int pStatus)
        {
            return pStatus switch
            {
                0 => EFreightPaidBy.Emitente,
                1 => EFreightPaidBy.Destinatario,
                2 => EFreightPaidBy.Terceiros,
                3 => EFreightPaidBy.ProprioEmitente,
                4 => EFreightPaidBy.ProprioDestinatario,
                9 => EFreightPaidBy.SemFrete,
                _ => EFreightPaidBy.SemFrete,
            };
        }
    }
}
