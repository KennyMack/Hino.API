﻿using Hino.API.Domain.Orders.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Domain.Orders.Utils
{
    public static class ConvertStatusOrder
    {
        public static EStatusSinc ToStatusSinc(this int pStatus)
        {
            return pStatus switch
            {
                0 => EStatusSinc.Waiting,
                1 => EStatusSinc.Sincronized,
                2 => EStatusSinc.Integrated,
                _ => EStatusSinc.Waiting,
            };
        }
    }
}
