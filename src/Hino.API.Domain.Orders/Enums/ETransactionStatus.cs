﻿using System.ComponentModel.DataAnnotations;

namespace Hino.API.Domain.Orders.Enums
{
    public enum ETransactionStatus
    {
        [Display(Description = "Pendente")]
        Pending = 0,
        [Display(Description = "Concluído")]
        Done = 1,
        [Display(Description = "Estornado")]
        Reversed = 2
    }
}
