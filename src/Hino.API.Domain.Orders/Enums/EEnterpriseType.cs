﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Domain.Orders.Enums
{
    /// <summary>
    /// 0 - Pessoa Física 1 - Pessoa Jurídica
    /// </summary>
    public enum EEnterpriseType
    {
        PessoaFisica = 0,
        PessoaJuridica = 1
    }
}
