﻿using System.ComponentModel.DataAnnotations;

namespace Hino.API.Domain.Orders.Enums
{
    public enum EEnterpriseClassification
    {
        [Display(Description = "Cliente")]
        Client = 0,
        [Display(Description = "Prospect")]
        Prospect = 1,
        [Display(Description = "Transportadora")]
        Carrier = 2
    }
}
