﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Domain.Orders.Enums
{
    /// <summary>
    /// Tipos de transação
    /// 0 - Cartão Débito
    /// 1 - Cartão Crédito
    /// 2 - Estorno
    /// </summary>
    public enum ETransactionType
    {
        Debit = 0,
        Credit = 1,
        Reversal = 2
    }
}
