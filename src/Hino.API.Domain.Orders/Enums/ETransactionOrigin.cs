﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Domain.Orders.Enums
{
    public enum ETransactionOrigin
    {
        Tef = 1,
        Pos = 0
    }
}
