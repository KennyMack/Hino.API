﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Domain.Orders.Enums
{
    public enum EStatusSinc
    {
        [Display(Description = "Aguardando sincronização")]
        Waiting = 0,
        [Display(Description = "Sincronizado")]
        Sincronized = 1,
        [Display(Description = "Integrado")]
        Integrated = 2
    }
}
