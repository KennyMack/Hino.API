﻿using Hino.API.Domain.Base.Models;

namespace Hino.API.Domain.Taxes.Models
{
    public class FSNCM : BaseEntity
    {
        public string Category { get; set; }
        public string NCM { get; set; }
        public string Description { get; set; }
        public decimal IPI { get; set; }
        public decimal AliquotaII { get; set; }
    }
}
