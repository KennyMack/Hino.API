using Hino.API.Domain.Base.Models;

namespace Hino.API.Domain.Taxes.Models
{
    public class FSImpostos : BaseEntity
    {
        public long CodImposto { get; set; }
        public short Classificacao { get; set; }
        public bool Status { get; set; }
    }
}
