﻿using Hino.API.Domain.Base.Models;
using Hino.API.Infra.Cross.Utils.Attributes;

namespace Hino.API.Domain.Taxes.Models
{
    public class FSNCMLoad: BaseEntity
    {
        [RequiredField]
        [DisplayField]
        [Max255LengthField]
        public string Category { get; set; }

        [RequiredField]
        [DisplayField]
        [Max8LengthField]
        public string NCM { get; set; }

        [RequiredField]
        [DisplayField]
        [Max255LengthField]
        public string Description { get; set; }

        [RequiredField]
        public string IPI { get; set; }

        [RequiredField]
        public decimal AliquotaII { get; set; }

    }
}
