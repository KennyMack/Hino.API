﻿using Hino.API.Domain.Base.Models;
using System.Collections.Generic;

namespace Hino.API.Domain.Taxes.Models
{
    public class FSFiscalOper : BaseEntity
    {
        public FSFiscalOper()
        {
        }

        public string Description { get; set; }
        public long IdERP { get; set; }
    }
}
