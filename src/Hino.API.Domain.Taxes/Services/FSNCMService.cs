﻿using Hino.API.Domain.Base.Services;
using Hino.API.Domain.Taxes.Interfaces.Repositories;
using Hino.API.Domain.Taxes.Interfaces.Services;
using Hino.API.Domain.Taxes.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using System.Threading.Tasks;
using DefaultMessages = Hino.API.Infra.Cross.Resources.MessagesResource;
using System.Linq;
using System;
using Hino.API.Infra.Cross.Utils;

namespace Hino.API.Domain.Taxes.Services
{
    public class FSNCMService : BaseService<FSNCM>, IFSNCMService
    {
        private readonly IFSNCMRepository _IFSNCMRepository;

        public FSNCMService(IFSNCMRepository repo) : base(repo)
        {
            _IFSNCMRepository = repo;
        }

        public override void Remove(FSNCM model) =>
            Errors.Add(ModelException.CreateValidationError(DefaultMessages.RemoveNotAllowed, "Id", model.Id.ToString()));

        public override Task<FSNCM> RemoveById(long id, string pEstablishmentKey, string pUniqueKey)
        {
            Errors.Add(ModelException.CreateValidationError(DefaultMessages.RemoveNotAllowed, "Id", id.ToString()));

            return Task.FromResult<FSNCM>(null);
        }

        public async Task LoadNCMsAsync(FSNCMLoad[] pLoad)
        {
            foreach (var item in pLoad)
            {
                var ncm = await FirstOrDefaultAsync(r => r.NCM == item.NCM);
                if (ncm == null)
                {
                    Add(new FSNCM
                    {
                        EstablishmentKey = "85db3229-73a2-4989-b2f3-9caa542443fe",
                        NCM = item.NCM,
                        Description = item.Description,
                        IPI = item.IPI == "NT" || item.IPI.IsEmpty() || item.IPI == "-" ? -1 : 
                            Convert.ToDecimal(item.IPI)
                    });
                }
                else
                {
                    Update(new FSNCM
                    {
                        Id = ncm.Id,
                        UniqueKey = ncm.UniqueKey,
                        EstablishmentKey = "85db3229-73a2-4989-b2f3-9caa542443fe",
                        NCM = ncm.NCM,
                        Description = item.Description,
                        IPI = item.IPI == "NT" || item.IPI.IsEmpty() || item.IPI == "-" ? -1 : 
                            Convert.ToDecimal(item.IPI)
                    });
                }
            }

            if (!Errors.Any())
                await SaveChanges();
        }
    }
}
