using Hino.API.Domain.Base.Services;
using Hino.API.Domain.Taxes.Interfaces.Repositories;
using Hino.API.Domain.Taxes.Interfaces.Services;
using Hino.API.Domain.Taxes.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using DefaultMessages = Hino.API.Infra.Cross.Resources.MessagesResource;
using System.Threading.Tasks;

namespace Hino.API.Domain.Taxes.Services
{
    public class FSFiscalOperService : BaseService<FSFiscalOper>, IFSFiscalOperService
    {
        private readonly IFSFiscalOperRepository _IFSFiscalOperRepository;

        public FSFiscalOperService(IFSFiscalOperRepository pIFSFiscalOperRepository) :
             base(pIFSFiscalOperRepository)
        {
            _IFSFiscalOperRepository = pIFSFiscalOperRepository;
        }

        public override void Remove(FSFiscalOper model) =>
            Errors.Add(ModelException.CreateValidationError(DefaultMessages.RemoveNotAllowed, "Id", model.Id.ToString()));

        public override Task<FSFiscalOper> RemoveById(long id, string pEstablishmentKey, string pUniqueKey)
        {
            Errors.Add(ModelException.CreateValidationError(DefaultMessages.RemoveNotAllowed, "Id", id.ToString()));

            return Task.FromResult<FSFiscalOper>(null);
        }
    }
}
