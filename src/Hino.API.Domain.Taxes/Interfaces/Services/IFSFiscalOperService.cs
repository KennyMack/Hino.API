using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Taxes.Models;

namespace Hino.API.Domain.Taxes.Interfaces.Services
{
    public interface IFSFiscalOperService : IBaseService<FSFiscalOper>
    {
    }
}
