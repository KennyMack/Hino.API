﻿using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Taxes.Models;
using System.Threading.Tasks;

namespace Hino.API.Domain.Taxes.Interfaces.Services
{
    public interface IFSNCMService : IBaseService<FSNCM>
    {
        Task LoadNCMsAsync(FSNCMLoad[] pLoad);
    }
}
