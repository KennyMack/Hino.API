﻿using Hino.API.Domain.Base.Interfaces.Repositories;
using Hino.API.Domain.Taxes.Models;

namespace Hino.API.Domain.Taxes.Interfaces.Repositories
{
    public interface IFSNCMRepository : IBaseRepository<FSNCM>
    {
    }
}
