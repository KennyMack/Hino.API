﻿using Hino.API.Application.Interfaces.Services.Seller;
using Hino.API.Application.Services.Seller;
using Hino.API.Domain.Seller.Interfaces.Repositories;
using Hino.API.Domain.Seller.Interfaces.Services;
using Hino.API.Domain.Seller.Services;
using Hino.API.Infra.Data.Repositories.Seller.Context;
using Hino.API.Infra.Data.Repositories.Seller.Context.Repositories;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Infra.Cross.IoC
{
    public static class BindingSeller
    {
        public static void AddSellerInstances(this IServiceCollection services)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            services.AddScoped<IVEUserRegionRepository, VEUserRegionRepository>();

            services.AddScoped<IVEUserRegionService, VEUserRegionService>();

            services.AddScoped<IVEUserRegionAS, VEUserRegionAS>();

            services.AddDbContext<SellerDbContext>();
        }
    }
}
