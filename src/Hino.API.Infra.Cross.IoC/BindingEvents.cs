﻿using Hino.API.Application.Interfaces.Services.Events;
using Hino.API.Application.Services.Events;
using Hino.API.Domain.Events.Interfaces.Repositories;
using Hino.API.Domain.Events.Interfaces.Services;
using Hino.API.Domain.Events.Services;
using Hino.API.Infra.Data.Repositories.Events.Context;
using Hino.API.Infra.Data.Repositories.Events.Repositories;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Infra.Cross.IoC
{
    public static class BindingEvents
    {
        public static void AddEventsInstances(this IServiceCollection services)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            services.AddScoped<IGEEnterpriseEventRepository, GEEnterpriseEventRepository>();
            services.AddScoped<IGEEstabCalendarRepository, GEEstabCalendarRepository>();
            services.AddScoped<IGEEventsClassificationRepository, GEEventsClassificationRepository>();
            services.AddScoped<IGEEventsRepository, GEEventsRepository>();
            services.AddScoped<IGEUserCalendarRepository, GEUserCalendarRepository>();

            services.AddScoped<IGEEnterpriseEventService, GEEnterpriseEventService>();
            services.AddScoped<IGEEstabCalendarService, GEEstabCalendarService>();
            services.AddScoped<IGEEventsClassificationService, GEEventsClassificationService>();
            services.AddScoped<IGEEventsService, GEEventsService>();
            services.AddScoped<IGEUserCalendarService, GEUserCalendarService>();

            services.AddScoped<IGEEnterpriseEventAS, GEEnterpriseEventAS>();
            services.AddScoped<IGEEstabCalendarAS, GEEstabCalendarAS>();
            services.AddScoped<IGEEventsClassificationAS, GEEventsClassificationAS>();
            services.AddScoped<IGEEventsAS, GEEventsAS>();
            services.AddScoped<IGEUserCalendarAS, GEUserCalendarAS>();

            services.AddDbContext<EventsDbContext>();
        }
    }
}
