﻿using Hino.API.Domain.Base.Interfaces.Messaging;
using Hino.API.Infra.Cross.Messaging;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Infra.Cross.IoC
{
    public static class BindingIoC
    {
        public static void RegisterServices(this IServiceCollection services)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            services.AddDemographInstances();
            services.AddEstablishmentsInstances();
            services.AddProductsInstances();
            services.AddMordorInstances();
            services.AddPaymentsInstances();
            services.AddTaxesInstances();
            services.AddRoutingInstances();
            services.AddEventsInstances();
            services.AddSellerInstances();
            services.AddOrdersInstances();
            services.AddSalePricesInstances();

            services.AddSingleton<IMessageService, RabbitMqMessagePublisher>();
        }

    }
}
