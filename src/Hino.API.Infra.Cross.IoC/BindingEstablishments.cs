﻿using Hino.API.Application.Interfaces.Services.Establishments;
using Hino.API.Application.Services.Establishments;
using Hino.API.Domain.Establishments.Interfaces.Repositories;
using Hino.API.Domain.Establishments.Interfaces.Services;
using Hino.API.Domain.Establishments.Services;
using Hino.API.Infra.Data.Repositories.Establishments.Context;
using Hino.API.Infra.Data.Repositories.Establishments.Repositories;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Hino.API.Infra.Cross.IoC
{
    public static class BindingEstablishments
    {
        public static void AddEstablishmentsInstances(this IServiceCollection services)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            services.AddScoped<IGEEstabDevicesRepository, GEEstabDevicesRepository>();
            services.AddScoped<IGEEstablishmentsRepository, GEEstablishmentsRepository>();
            services.AddScoped<IGEEstabMenuRepository, GEEstabMenuRepository>();

            services.AddScoped<IGEEstabDevicesService, GEEstabDevicesService>();
            services.AddScoped<IGEEstablishmentsService, GEEstablishmentsService>();
            services.AddScoped<IGEEstabMenuService, GEEstabMenuService>();

            services.AddScoped<IGEEstabDevicesAS, GEEstabDevicesAS>();
            services.AddScoped<IGEEstablishmentsAS, GEEstablishmentsAS>();
            services.AddScoped<IGEEstabMenuAS, GEEstabMenuAS>();

            services.AddDbContext<EstablishmentsDbContext>();
        }
    }
}
