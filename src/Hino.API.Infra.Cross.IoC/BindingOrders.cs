﻿using Hino.API.Application.Interfaces.Services.Orders;
using Hino.API.Application.Services.Orders;
using Hino.API.Domain.Orders.Interfaces.Repositories;
using Hino.API.Domain.Orders.Interfaces.Services;
using Hino.API.Domain.Orders.Interfaces.UoW;
using Hino.API.Domain.Orders.Services;
using Hino.API.Infra.Data.Repositories.Orders;
using Hino.API.Infra.Data.Repositories.Orders.Context;
using Hino.API.Infra.Data.Repositories.Orders.UoW;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Infra.Cross.IoC
{
    public static class BindingOrders
    {
        public static void AddOrdersInstances(this IServiceCollection services)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            services.AddScoped<IVEOrderItemsRepository, VEOrderItemsRepository>();
            services.AddScoped<IVEOrdersRepository, VEOrdersRepository>();
            services.AddScoped<IVEOrderTaxesRepository, VEOrderTaxesRepository>();
            services.AddScoped<IVESalePriceRepository, VESalePriceRepository>();
            services.AddScoped<IVETransactionsRepository, VETransactionsRepository>();

            services.AddScoped<IVEOrderItemsService, VEOrderItemsService>();
            services.AddScoped<IVEOrdersService, VEOrdersService>();
            services.AddScoped<IVEOrderTaxesService, VEOrderTaxesService>();
            services.AddScoped<IVETransactionsService, VETransactionsService>();
            services.AddScoped<ICreateOrderService, CreateOrderService>();
            services.AddScoped<IRemoveOrderService, RemoveOrderService>();
            services.AddScoped<IUpdateOrderService, UpdateOrderService>();

            services.AddScoped<IVEOrdersAS, VEOrdersAS>();
            services.AddScoped<IVETransactionsAS, VETransactionsAS>();

            services.AddScoped<IOrdersUoW, OrdersUoW>();

            services.AddDbContext<OrdersDbContext>();
        }
    }
}
