﻿
using Hino.API.Application.Interfaces.Services.Routing;
using Hino.API.Application.Services.Routing;
using Hino.API.Domain.Routing.Interfaces.Repositories;
using Hino.API.Domain.Routing.Interfaces.Services;
using Hino.API.Domain.Routing.Services;
using Hino.API.Infra.Data.Repositories.Products.Context;
using Hino.API.Infra.Data.Repositories.Routing;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Infra.Cross.IoC
{
    public static class BindingRouting
    {
        public static void AddRoutingInstances(this IServiceCollection services)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            services.AddScoped<ILORoutesRepository, LORoutesRepository>();
            services.AddScoped<ILOTripsRepository, LOTripsRepository>();
            services.AddScoped<ILOWaypointsRepository, LOWaypointsRepository>();

            services.AddScoped<ILORoutesService, LORoutesService>();
            services.AddScoped<ILOTripsService, LOTripsService>();
            services.AddScoped<ILOWaypointsService, LOWaypointsService>();

            services.AddScoped<ILORoutesAS, LORoutesAS>();
            services.AddScoped<ILOTripsAS, LOTripsAS>();
            services.AddScoped<ILOWaypointsAS, LOWaypointsAS>();

            services.AddDbContext<RoutingDbContext>();
        }
    }
}
