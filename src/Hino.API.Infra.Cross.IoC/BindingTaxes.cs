﻿using Hino.API.Application.Interfaces.Services.Taxes;
using Hino.API.Application.Services.Taxes;
using Hino.API.Domain.Taxes.Interfaces.Repositories;
using Hino.API.Domain.Taxes.Interfaces.Services;
using Hino.API.Domain.Taxes.Services;
using Hino.API.Infra.Data.Repositories.Taxes.Context;
using Hino.API.Infra.Data.Repositories.Taxes.Repositories;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Infra.Cross.IoC
{
    public static class BindingTaxes
    {
        public static void AddTaxesInstances(this IServiceCollection services)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            services.AddScoped<IFSFiscalOperRepository, FSFiscalOperRepository>();
            services.AddScoped<IFSNCMRepository, FSNCMRepository>();

            services.AddScoped<IFSFiscalOperService, FSFiscalOperService>();
            services.AddScoped<IFSNCMService, FSNCMService>();

            services.AddScoped<IFSNCMAS, FSNCMAS>();

            services.AddDbContext<TaxesDbContext>();
        }
    }
}
