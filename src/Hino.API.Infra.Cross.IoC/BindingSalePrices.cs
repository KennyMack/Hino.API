﻿using Hino.API.Application.Interfaces.Services.SalePrices;
using Hino.API.Application.Services.SalePrices;
using Hino.API.Domain.SalePrices.Repositories;
using Hino.API.Domain.SalePrices.Services;
using Hino.API.Infra.Data.Repositories.SalePrices.Context;
using Hino.API.Infra.Data.Repositories.SalePrices.Repositories;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Infra.Cross.IoC
{
    public static class BindingSalePrices
    {
        public static void AddSalePricesInstances(this IServiceCollection services)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            services.AddScoped<IVERegionSaleRepository, VERegionSaleRepository>();
            services.AddScoped<IVERegionSaleUFRepository, VERegionSaleUFRepository>();
            services.AddScoped<IVESalePriceRepository, VESalePriceRepository>();
            services.AddScoped<IVESaleWorkRegionRepository, VESaleWorkRegionRepository>();

            services.AddScoped<IVERegionSaleService, VERegionSaleService>();
            services.AddScoped<IVERegionSaleUFService, VERegionSaleUFService>();
            services.AddScoped<IVESalePriceService, VESalePriceService>();
            services.AddScoped<IVESaleWorkRegionService, VESaleWorkRegionService>();

            services.AddScoped<IVERegionSaleAS, VERegionSaleAS>();
            services.AddScoped<IVERegionSaleUFAS, VERegionSaleUFAS>();
            services.AddScoped<IVESalePriceAS, VESalePriceAS>();
            services.AddScoped<IVESaleWorkRegionAS, VESaleWorkRegionAS>();

            services.AddDbContext<SalePricesDbContext>();
        }
    }
}
