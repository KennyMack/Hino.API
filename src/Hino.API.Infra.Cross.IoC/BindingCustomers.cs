﻿using Hino.API.Application.Interfaces.Services.Customers;
using Hino.API.Application.Services.Customers;
using Hino.API.Domain.Customers.Interfaces.Repositories;
using Hino.API.Domain.Customers.Interfaces.Services;
using Hino.API.Domain.Customers.Services;
using Hino.API.Infra.Data.Repositories.Customers.Context;
using Hino.API.Infra.Data.Repositories.Customers.Repositories;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Infra.Cross.IoC
{
    public static class BindingCustomers
    {
        public static void AddCustomersInstances(this IServiceCollection services)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            services.AddScoped<IGEEnterpriseCategoryRepository, GEEnterpriseCategoryRepository>();
            services.AddScoped<IGEEnterpriseContactsRepository, GEEnterpriseContactsRepository>();
            services.AddScoped<IGEEnterpriseFiscalGroupRepository, GEEnterpriseFiscalGroupRepository>();
            services.AddScoped<IGEEnterpriseGeoRepository, GEEnterpriseGeoRepository>();
            services.AddScoped<IGEEnterpriseGroupRepository, GEEnterpriseGroupRepository>();
            services.AddScoped<IGEEnterprisesRepository, GEEnterprisesRepository>();
            services.AddScoped<IGEUserEnterprisesRepository, GEUserEnterprisesRepository>();

            services.AddScoped<IGEEnterpriseCategoryService, GEEnterpriseCategoryService>();
            services.AddScoped<IGEEnterpriseContactsService, GEEnterpriseContactsService>();
            services.AddScoped<IGEEnterpriseFiscalGroupService, GEEnterpriseFiscalGroupService>();
            services.AddScoped<IGEEnterpriseGeoService, GEEnterpriseGeoService>();
            services.AddScoped<IGEEnterpriseGroupService, GEEnterpriseGroupService>();
            services.AddScoped<IGEEnterprisesService, GEEnterprisesService>();
            services.AddScoped<IGEUserEnterprisesService, GEUserEnterprisesService>();

            services.AddScoped<IGEEnterpriseCategoryAS, GEEnterpriseCategoryAS>();
            services.AddScoped<IGEEnterpriseContactsAS, GEEnterpriseContactsAS>();
            services.AddScoped<IGEEnterpriseFiscalGroupAS, GEEnterpriseFiscalGroupAS>();
            services.AddScoped<IGEEnterpriseGeoAS, GEEnterpriseGeoAS>();
            services.AddScoped<IGEEnterpriseGroupAS, GEEnterpriseGroupAS>();
            services.AddScoped<IGEEnterprisesAS, GEEnterprisesAS>();
            services.AddScoped<IGEUserEnterprisesAS, GEUserEnterprisesAS>();

            services.AddDbContext<CustomerDbContext>();
        }
    }
}
