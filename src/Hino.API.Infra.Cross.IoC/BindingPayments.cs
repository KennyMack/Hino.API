﻿using Hino.API.Application.Interfaces.Services.Payments;
using Hino.API.Application.Services.Payments;
using Hino.API.Domain.Payments.Interfaces.Repositories;
using Hino.API.Domain.Payments.Interfaces.Services;
using Hino.API.Domain.Payments.Services;
using Hino.API.Infra.Data.Repositories.Payments.Context;
using Hino.API.Infra.Data.Repositories.Payments.Repositories;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Hino.API.Infra.Cross.IoC
{
    public static class BindingPayments
    {
        public static void AddPaymentsInstances(this IServiceCollection services)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            services.AddScoped<IGEPaymentCondInstallmentsRepository, GEPaymentCondInstallmentsRepository>();
            services.AddScoped<IGEPaymentConditionRepository, GEPaymentConditionRepository>();
            services.AddScoped<IGEPaymentTypeRepository, GEPaymentTypeRepository>();
            services.AddScoped<IGEUserPayTypeRepository, GEUserPayTypeRepository>();

            services.AddScoped<IGEPaymentCondInstallmentsService, GEPaymentCondInstallmentsService>();
            services.AddScoped<IGEPaymentConditionService, GEPaymentConditionService>();
            services.AddScoped<IGEPaymentTypeService, GEPaymentTypeService>();
            services.AddScoped<IGEUserPayTypeService, GEUserPayTypeService>();

            services.AddScoped<IGEPaymentConditionAS, GEPaymentConditionAS>();
            services.AddScoped<IGEPaymentTypeAS, GEPaymentTypeAS>();
            // services.AddScoped<IGEUserPayTypeAS, GEUserPayTypeAS>();

            services.AddDbContext<PaymentsDbContext>();
        }
    }
}
