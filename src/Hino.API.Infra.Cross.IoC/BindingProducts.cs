﻿using Hino.API.Infra.Data.Repositories.Products.Context;
using Hino.API.Infra.Data.Repositories.Products.Repositories;
using Hino.API.Domain.Products.Interfaces.Repositories;
using Hino.API.Domain.Products.Interfaces.Services;
using Hino.API.Domain.Products.Services;
using Hino.API.Application.Interfaces.Services.Products;
using Hino.API.Application.Services.Products;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Infra.Cross.IoC
{
    public static class BindingProducts
    {
        public static void AddProductsInstances(this IServiceCollection services)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            services.AddScoped<IGEProductAplicRepository, GEProductAplicRepository>();
            services.AddScoped<IGEProductsFamilyRepository, GEProductsFamilyRepository>();
            services.AddScoped<IGEProductsRepository, GEProductsRepository>();
            services.AddScoped<IGEProductsTypeRepository, GEProductsTypeRepository>();
            services.AddScoped<IGEProductsUnitRepository, GEProductsUnitRepository>();

            services.AddScoped<IGEProductAplicService, GEProductAplicService>();
            services.AddScoped<IGEProductsFamilyService, GEProductsFamilyService>();
            services.AddScoped<IGEProductsService, GEProductsService>();
            services.AddScoped<IGEProductsTypeService, GEProductsTypeService>();
            services.AddScoped<IGEProductsUnitService, GEProductsUnitService>();

            services.AddScoped<IGEProductAplicAS, GEProductAplicAS>();
            services.AddScoped<IGEProductsFamilyAS, GEProductsFamilyAS>();
            services.AddScoped<IGEProductsAS, GEProductsAS>();
            services.AddScoped<IGEProductsTypeAS, GEProductsTypeAS>();
            services.AddScoped<IGEProductsUnitAS, GEProductsUnitAS>();

            services.AddDbContext<ProductsDbContext>();
        }
    }
}
