﻿using Hino.API.Application.Interfaces.Services.Mordor;
using Hino.API.Application.Services.Mordor;
using Hino.API.Domain.Mordor.Interfaces.Repositories;
using Hino.API.Domain.Mordor.Interfaces.Services;
using Hino.API.Domain.Mordor.Services;
using Hino.API.Infra.Data.Repositories.Mordor.Context;
using Hino.API.Infra.Data.Repositories.Mordor.Repositories;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Infra.Cross.IoC
{
    public static class BindingMordor
    {
        public static void AddMordorInstances(this IServiceCollection services)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            services.AddScoped<IGEUsersRepository, GEUsersRepository>();

            services.AddScoped<IFirebaseMordorService, FirebaseMordorService>();
            services.AddScoped<IGEUsersService, GEUsersService>();

            services.AddScoped<IAuthenticationAS, AuthenticationAS>();

            services.AddDbContext<MordorDbContext>();
        }
    }
}
