﻿using Hino.API.Application.Interfaces.Services.Demograph;
using Hino.API.Application.Services.Demograph;
using Hino.API.Domain.Demograph.Interfaces.Repositories;
using Hino.API.Domain.Demograph.Interfaces.Services;
using Hino.API.Domain.Demograph.Services;
using Hino.API.Infra.Data.Repositories.Demograph.Context;
using Hino.API.Infra.Data.Repositories.Demograph.Repositories;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Infra.Cross.IoC
{
    public static class BindingDemograph
    {
        public static void AddDemographInstances(this IServiceCollection services)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            services.AddScoped<IGECountriesRepository, GECountriesRepository>();
            services.AddScoped<IGEStatesRepository, GEStatesRepository>();
            services.AddScoped<IGECitiesRepository, GECitiesRepository>();

            services.AddScoped<IGECountriesService, GECountriesService>();
            services.AddScoped<IGEStatesService, GEStatesService>();
            services.AddScoped<IGECitiesService, GECitiesService>();

            services.AddScoped<IGECountriesAS, GECountriesAS>();
            services.AddScoped<IGEStatesAS, GEStatesAS>();
            services.AddScoped<IGECitiesAS, GECitiesAS>();

            services.AddDbContext<DemographDbContext>();
        }
    }
}
