﻿using Hino.API.Application.Integrations.Locations.Models;
using Hino.API.Application.Interfaces.Services.Routing;
using Hino.API.Application.MapperModels;
using Hino.API.Application.ViewModel.Routing;
using Hino.API.Domain.Routing.Interfaces.Services;
using Hino.API.Domain.Routing.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.API.Application.Services.Routing
{
    public class LORoutesAS : BaseAppService<LORoutes>, ILORoutesAS
    {
        private readonly ILORoutesService _ILORoutesService;
        private readonly ILOWaypointsService _ILOWaypointsService;
        public LORoutesAS(ILORoutesService pILORoutesService,
            ILOWaypointsService pILOWaypointsService) :
             base(pILORoutesService)
        {
            _ILORoutesService = pILORoutesService;
            _ILOWaypointsService = pILOWaypointsService;
        }

        public async Task<LORoutesVM> SaveTripAsync(IOptimizedVM pLocations, IFindSequenceResultVM pSequence)
        {
            var route = new LORoutes
            {
                Id = await _ILORoutesService.NextSequenceAsync(),
                EstablishmentKey = pLocations.EstablishmentKey,
                Weigth = 0,
                Duration = pSequence.Duration,
                Distance = pSequence.Distance,
                Note = ""
            };

            foreach (var item in pSequence.Waypoints)
            {
                ILocationVM locationItem = null;
                if (item.Name == "Origem" ||
                    item.Name == "Destino")
                    locationItem = pLocations.Location.First();
                else
                    locationItem = pLocations.Location.FirstOrDefault(r => r.Name == item.Name);

                if (locationItem != null)
                {
                    var WayPoint = new LOWaypoints
                    {
                        Id = await _ILOWaypointsService.NextSequenceAsync(),
                        EstablishmentKey = pLocations.EstablishmentKey,
                        RouteID = route.Id,
                        Distance = item?.Distance ?? 0,
                        Name = locationItem.Name,
                        Address = locationItem.Address,
                        WayPointIndex = item.Sequence,
                        TripIndex = 0,
                        Lat = item.Lat,
                        Lng = item.Lng,
                        Created = DateTime.Now,
                        Modified = DateTime.Now,
                        IsActive = true,
                        UniqueKey = Guid.NewGuid().ToString()
                    };

                    route.LOWaypoints.Add(WayPoint);
                }
            }
            _ILORoutesService.Add(route);
            await _ILORoutesService.SaveChanges();

            return Mapper.Map<LORoutesVM>(route);
        }
    }
}
