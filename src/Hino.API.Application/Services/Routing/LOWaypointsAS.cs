﻿using Hino.API.Application.Integrations.Locations.Models;
using Hino.API.Application.Interfaces.Services.Routing;
using Hino.API.Domain.Routing.Interfaces.Services;
using Hino.API.Domain.Routing.Models;
using System.Threading.Tasks;

namespace Hino.API.Application.Services.Routing
{
    public class LOWaypointsAS : BaseAppService<LOWaypoints>, ILOWaypointsAS
    {
        public LOWaypointsAS(ILOWaypointsService pILOWaypointsService) :
             base(pILOWaypointsService)
        {

        }

    }
}
