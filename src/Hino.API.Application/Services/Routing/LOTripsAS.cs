﻿using Hino.API.Application.Integrations.Locations.Models;
using Hino.API.Application.Interfaces.Services.Routing;
using Hino.API.Application.ViewModel.Routing;
using Hino.API.Domain.Routing.Interfaces.Services;
using Hino.API.Domain.Routing.Models;
using System.Threading.Tasks;
using System;

namespace Hino.API.Application.Services.Routing
{
    public class LOTripsAS : BaseAppService<LOTrips>, ILOTripsAS
    {
        public LOTripsAS(ILOTripsService pILOTripsService) :
             base(pILOTripsService)
        {

        }
    }
}
