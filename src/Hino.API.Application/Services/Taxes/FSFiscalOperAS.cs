﻿using Hino.API.Application.Interfaces.Services.Taxes;
using Hino.API.Domain.Taxes.Interfaces.Services;
using Hino.API.Domain.Taxes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.Services.Taxes
{
    public class FSFiscalOperAS : BaseAppService<FSFiscalOper>, IFSFiscalOperAS
    {
        public FSFiscalOperAS(IFSFiscalOperService pIFSFiscalOperService) :
             base(pIFSFiscalOperService)
        {

        }
    }
}
