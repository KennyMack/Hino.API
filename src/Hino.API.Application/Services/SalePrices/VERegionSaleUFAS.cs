﻿using System;
using Hino.API.Application.Interfaces.Services.SalePrices;
using Hino.API.Domain.SalePrices.Models;
using Hino.API.Domain.SalePrices.Services;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.Services.SalePrices
{
    public class VERegionSaleUFAS : BaseAppService<VERegionSaleUF>, IVERegionSaleUFAS
    {
        public VERegionSaleUFAS(IVERegionSaleUFService pService) :
             base(pService)
        {
        }
    }
}
