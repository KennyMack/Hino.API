﻿using System;
using Hino.API.Application.Interfaces.Services.SalePrices;
using Hino.API.Domain.SalePrices.Models;
using Hino.API.Domain.SalePrices.Services;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hino.API.Infra.Cross.Utils.Paging;
using Hino.API.Application.ViewModel.SalePrices;
using Hino.API.Domain.Establishments.Interfaces.Services;
using Hino.API.Domain.Payments.Interfaces.Services;
using Hino.API.Domain.Mordor.Interfaces.Services;
using Hino.API.Infra.Cross.Utils.Exceptions;
using Hino.API.Application.MapperModels;

namespace Hino.API.Application.Services.SalePrices
{
    public class VESalePriceAS : BaseAppService<VESalePrice>, IVESalePriceAS
    {
        readonly IVESalePriceService _IVESalePriceService;
        readonly IGEUsersService _IGEUsersService;
        readonly IGEEstablishmentsService _IGEEstablishmentsService;
        readonly IGEPaymentConditionService _IGEPaymentConditionService;

        public VESalePriceAS(IVESalePriceService pService,
                            IGEEstablishmentsService pIGEEstablishmentsService,
                            IGEUsersService pIGEUsersService,
                            IGEPaymentConditionService pIGEPaymentConditionService) :
             base(pService)
        {
            _IGEUsersService = pIGEUsersService;
            _IVESalePriceService = pService;
            _IGEEstablishmentsService = pIGEEstablishmentsService;
            _IGEPaymentConditionService = pIGEPaymentConditionService;
        }

        public async Task<PagedResult<VESalePriceVM>> GetAllSalePrice(int pageNumber, int limit, string pEstablishmentKey, long pRegionId, long pPaymentConditionId, long pUserId, string filter)
        {
            var establishment = await _IGEEstablishmentsService.GetByEstablishmentKeyAsync(pEstablishmentKey);

            PagedResult<VESalePrice> salesPrices;

            if (establishment.OnlyWithStock)
                salesPrices = await _IVESalePriceService.GetAllPagedSearchProductWithStock(pageNumber, limit, pEstablishmentKey, pRegionId, filter);
            else
                salesPrices = await _IVESalePriceService.GetAllPagedSearchProduct(pageNumber, limit, pEstablishmentKey, pRegionId, filter);

            if (salesPrices == null)
                return new PagedResult<VESalePriceVM>();

            var user = await _IGEUsersService.FirstOrDefaultAsync(r => r.EstablishmentKey == pEstablishmentKey &&
                                                                       r.Id == pUserId);

            if (user?.PercDiscountPrice > 0)
            {
                decimal percDiscountPrice = user.PercDiscountPrice / 100;

                foreach (var salePrice in salesPrices.Results)
                    salePrice.Value -= Math.Round((salePrice.Value * percDiscountPrice), 2);
            }
            else if (user?.PercIncreasePrice > 0)
            {
                decimal percIncreasePrice = user.PercIncreasePrice / 100;

                foreach (var salePrice in salesPrices.Results)
                    salePrice.Value += Math.Round((salePrice.Value * percIncreasePrice), 2);
            }

            var paymentCondition = await _IGEPaymentConditionService.FirstOrDefaultAsync(r => r.Id == pPaymentConditionId &&
                                                                                              r.EstablishmentKey == pEstablishmentKey);

            decimal conditionDiscount = Math.Max(paymentCondition?.PercDiscount ?? 0, 0) / 100;
            decimal conditionIncrease = Math.Max(paymentCondition?.PercIncrease ?? 0, 0) / 100;

            if (conditionDiscount > 0)
            {
                foreach (var salePrice in salesPrices.Results)
                    salePrice.Value -= (salePrice.Value * conditionDiscount);
            }
            else if (conditionIncrease > 0)
            {
                foreach (var salePrice in salesPrices.Results)
                    salePrice.Value += (salePrice.Value * conditionIncrease);
            }

            return Mapper.Map<PagedResult<VESalePrice>, PagedResult<VESalePriceVM>>(salesPrices);
        }

        public async Task<PagedResult<VESalePriceVM>> GetProductSalePrice(string pEstablishmentKey, long pProdutctId, long pRegionId, long pPaymentConditionId, long pUserId)
        {
            var salesPrices = (await QueryAsync(r => r.EstablishmentKey == pEstablishmentKey &&
                                                    r.RegionId == pRegionId &&
                                                    r.ProductId == pProdutctId,
                                                    s => s.VERegionSale));

            if (salesPrices == null)
                return new PagedResult<VESalePriceVM>();

            var user = await _IGEUsersService.FirstOrDefaultAsync(r => r.EstablishmentKey == pEstablishmentKey &&
                                                                       r.Id == pUserId);

            if (user?.PercDiscountPrice > 0)
            {
                decimal percDiscountPrice = user.PercDiscountPrice / 100;

                foreach (var salePrice in salesPrices)
                    salePrice.Value -= (salePrice.Value * percDiscountPrice);
            }
            else if (user?.PercIncreasePrice > 0)
            {
                decimal percIncreasePrice = user.PercIncreasePrice / 100;

                foreach (var salePrice in salesPrices)
                    salePrice.Value += (salePrice.Value * percIncreasePrice);
            }

            var paymentCondition = await _IGEPaymentConditionService.FirstOrDefaultAsync(r => r.Id == pPaymentConditionId &&
                                                                                              r.EstablishmentKey == pEstablishmentKey);

            decimal conditionDiscount = Math.Max(paymentCondition?.PercDiscount ?? 0, 0) / 100;
            decimal conditionIncrease = Math.Max(paymentCondition?.PercIncrease ?? 0, 0) / 100;

            if (conditionDiscount > 0)
            {
                foreach (var salePrice in salesPrices)
                    salePrice.Value -= Math.Round((salePrice.Value * conditionDiscount), 2);
            }
            else if (conditionIncrease > 0)
            {
                foreach (var salePrice in salesPrices)
                    salePrice.Value += Math.Round((salePrice.Value * conditionIncrease), 2);
            }

            return new PagedResult<VESalePriceVM>(
                Mapper.Map<List<VESalePrice>, List<VESalePriceVM>>(salesPrices.ToList())
                );
        }

        public async Task<VESalePriceVM> CreateSalePrice(VESalePriceVM pSalePrice) =>
            Mapper.Map<VESalePriceVM>(
                await _IVESalePriceService.CreateSalePrice(
                    Mapper.Map<VESalePrice>(pSalePrice)
                )
            );

        public async Task<VESalePriceVM> UpdateSalePrice(VESalePriceVM pSalePrice) =>
            Mapper.Map<VESalePriceVM>(
                await _IVESalePriceService.UpdateSalePrice(
                    Mapper.Map<VESalePrice>(pSalePrice)
                )
            );

        public override void Dispose()
        {
            _IVESalePriceService.Dispose();
            _IGEEstablishmentsService.Dispose();
            _IGEPaymentConditionService.Dispose();
            base.Dispose();
        }
    }
}
