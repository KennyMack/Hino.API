﻿using Hino.API.Application.Interfaces.Services.Mordor;
using Hino.API.Application.MapperModels;
using Hino.API.Application.ViewModel.Mordor;
using Hino.API.Domain.Establishments.Interfaces.Services;
using Hino.API.Domain.Mordor.Interfaces.Services;
using Hino.API.Domain.Mordor.Models;
using Hino.API.Infra.Cross.Utils.Enums;
using Hino.API.Infra.Cross.Utils.Exceptions;
using Hino.API.Infra.Cross.Utils.Settings;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DefaultMessage = Hino.API.Infra.Cross.Resources.MessagesResource;
using ValidationMessage = Hino.API.Infra.Cross.Resources.ValidationMessagesResource;

namespace Hino.API.Application.Services.Mordor
{
    public class AuthenticationAS : IAuthenticationAS
    {
        public List<ModelException> Errors { get; set; }
        private readonly IFirebaseMordorService _IFirebaseMordorService;
        private readonly IGEEstablishmentsService _IGEEstablishmentsService;
        private readonly IGEUsersService _IGEUsersService;

        public AuthenticationAS(IFirebaseMordorService pIFirebaseMordorService,
            IGEEstablishmentsService pIGEEstablishmentsService,
            IGEUsersService pIGEUsersService)
        {
            Errors = new List<ModelException>();
            _IFirebaseMordorService = pIFirebaseMordorService;
            _IGEEstablishmentsService = pIGEEstablishmentsService;
            _IGEUsersService = pIGEUsersService;
        }

        public async Task<SignUpResult> CreateUserAsync(CreateUserVM pUser)
        {
            var Establishment = await _IGEEstablishmentsService.GetByEstablishmentKeyAsync(pUser.EstablishmentKey);
            if (Establishment == null)
            {
                Errors.Add(ModelException.CreateNotFoundError(DefaultMessage.InvalidEstablishmentKeyOrNull,
                    "EstablishmentKey",
                    pUser.EstablishmentKey));
                return null;
            }

            var Error = Establishment.IsStatusActive();
            if (Error != null)
            {
                Errors.Add(Error);
                return null;
            }

            var userValid = Mapper.Map<CreateUserVM, GEUsers>(pUser);

            Error = userValid.IsValid(_IGEUsersService);
            if (Error != null)
            {
                Errors.Add(Error);
                return null;
            }

            try
            {
                var NewUserFirebase = await _IFirebaseMordorService.SignUpAsync(userValid);
                _IGEUsersService.Create(new GEUsers(
                    0, 
                    pUser.EstablishmentKey,
                    pUser.UniqueKey,
                    NewUserFirebase.UserData.Uid,
                    pUser.UserName,
                    pUser.Email,
                    pUser.Password,
                    pUser.UserType,
                    0,
                    0,
                    0,
                    0,
                    "",
                    "",
                    pUser.UserKey,
                    false,
                    pUser.Role));

                return NewUserFirebase;
            }
            catch (Exception ex)
            {
                Logging.Exception(ex);
                Errors.Add(ModelException.CreateNotFoundError(ex.Message,
                    "User",
                    pUser.Email));
                return null;
            }

        }

        public async Task<SignInResult> SignInWithEmailAsync(SignInUserVM pUser)
        {
            if (AppSettings.Hino_Id != pUser.EstablishmentKey)
            {
                var Establishment = await _IGEEstablishmentsService.GetByEstablishmentKeyAsync(pUser.EstablishmentKey);
                if (Establishment == null)
                {
                    Errors.Add(ModelException.CreateNotFoundError(DefaultMessage.InvalidEstablishmentKeyOrNull,
                        "EstablishmentKey",
                        pUser.EstablishmentKey));
                    return null;
                }

                var Error = Establishment.IsStatusActive();
                if (Error != null)
                {
                    Errors.Add(Error);
                    return null;
                }
            }

            try
            {
                var User = new GEUsers(pUser.EstablishmentKey, pUser.Email, pUser.Password);

                var UserDB = _IGEUsersService.GetByEmail(User.EstablishmentKey, User.Email);

                if (UserDB == null)
                    throw new HinoException(ModelException
                        .CreateValidationError(ValidationMessage.AuthUserNotFound,
                        "User", "")
                    );

                var Error = UserDB.IsValidToAuth();
                if (Error != null)
                    throw new HinoException(Error);

                if (AppSettings.Hino_Id != pUser.EstablishmentKey &&
                    pUser.EstablishmentKey != UserDB.EstablishmentKey)
                    throw new HinoException(ModelException
                        .CreateValidationError(ValidationMessage.UserDoesNotBelongsToEstab,
                        "User", "")
                    );

                return await _IFirebaseMordorService.SignInWithEmailAsync(UserDB);
            }
            catch (HinoException ex)
            {
                if (ex.ExceptionDetails != null)
                    Errors.Add(ex.ExceptionDetails);
                else
                    Errors.Add(ModelException.CreateSqlError(ex, "Email", EExceptionErrorCodes.InvalidRequest));
            }
            catch (Exception ex)
            {
                // Logging.Exception(ex);
                Errors.Add(ModelException.CreateSqlError(ex, "Email", EExceptionErrorCodes.InvalidRequest));
            }
            return null;
        }
    }
}
