﻿using Hino.API.Application.Interfaces.Services;
using Hino.API.Domain.Base.Interfaces.Models;
using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Base.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using Hino.API.Infra.Cross.Utils.Paging;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hino.API.Application.Services
{
    public class BaseAppService<T> : IBaseAppService<T>, IDisposable where T : BaseEntity
    {
        public bool DontSendToQueue
        {
            get
            {
                return _BaseService.DontSendToQueue;
            }
            set
            {
                _BaseService.DontSendToQueue = value;
            }
        }

        private readonly IBaseService<T> _BaseService;
        public List<ModelException> Errors
        {
            get
            {
                return _BaseService.Errors;
            }
            set
            {
                _BaseService.Errors = value;
            }
        }

        public BaseAppService(IBaseService<T> baseService)
        {
            _BaseService = baseService;
        }

        public virtual T Add(T model)
        {
            _BaseService.Add(model);
            return model;
        }

        public virtual T Update(T model)
        {
            _BaseService.Update(model);
            return model;
        }

        public async virtual Task<IEnumerable<T>> SyncData(string pEstablishmentKey, T[] pRegisters)
        {
            List<ModelException> SyncErrors = new();

            for (int i = 0, length = pRegisters.Length; i < length; i++)
            {
                _BaseService.Errors.Clear();

                T result;
                if (((IBaseEntity)pRegisters[i]).Id <= 0)
                    result = _BaseService.Add(pRegisters[i]);
                else
                    result = _BaseService.Update(pRegisters[i]);

                if (Errors.Count > 0)
                {
                    ((IBaseEntity)pRegisters[i]).Id = 0;
                    SyncErrors.Add(Errors[0]);
                    _BaseService.RollBackChanges();
                }
                else
                {
                    pRegisters[i] = result;
                    await SaveChanges();
                }
            }

            await SaveChanges();

            return pRegisters;
        }

        public virtual void Remove(T model)
        {
            _BaseService.Remove(model);
        }

        public virtual async Task<T> RemoveById(long id, string pEstablishmentKey, string pUniqueKey) =>
            await _BaseService.RemoveById(id, pEstablishmentKey, pUniqueKey);

        public async virtual Task<IEnumerable<T>> GetAllAsync(params Expression<Func<T, object>>[] includeProperties) =>
            await _BaseService.GetAllAsync(includeProperties);

        public async virtual Task<PagedResult<T>> GetAllPagedAsync(int page, int pageSize, params Expression<Func<T, object>>[] includeProperties) =>
           await _BaseService.GetAllPagedAsync(page, pageSize, includeProperties);

        public async virtual Task<T> GetByIdAsync(long id, string pEstablishmentKey, string pUniqueKey, params Expression<Func<T, object>>[] includeProperties) =>
            await _BaseService.GetByIdAsync(id, pEstablishmentKey, pUniqueKey, includeProperties);

        public virtual T GetById(long id, string pEstablishmentKey, string pUniqueKey, params Expression<Func<T, object>>[] includeProperties) =>
             _BaseService.GetById(id, pEstablishmentKey, pUniqueKey, includeProperties);

        public async virtual Task<IEnumerable<T>> QueryAsync(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties) =>
           await _BaseService.QueryAsync(predicate, includeProperties);

        public async virtual Task<PagedResult<T>> QueryPagedAsync(int page, int pageSize, Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties) =>
           await _BaseService.QueryPagedAsync(page, pageSize, predicate, includeProperties);

        public virtual long NextSequence() =>
            _BaseService.NextSequence();

        public virtual async Task<long> NextSequenceAsync() =>
            await _BaseService.NextSequenceAsync();

        public virtual async Task<int> SaveChanges() =>
            await _BaseService.SaveChanges();

        public void RollBackChanges() =>
            _BaseService.RollBackChanges();

        public virtual void Dispose()
        {
            _BaseService.Dispose();
            GC.SuppressFinalize(this);
        }

        public T GetByIdToUpdate(long id, string pEstablishmentKey, string pUniqueKey) =>
            _BaseService.GetByIdToUpdate(id, pEstablishmentKey, pUniqueKey);

        public async Task<T> FirstOrDefaultAsync(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties) =>
            await _BaseService.FirstOrDefaultAsync(predicate, includeProperties);
    }
}
