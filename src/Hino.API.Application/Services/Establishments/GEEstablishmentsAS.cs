﻿using Hino.API.Application.Interfaces.Services.Establishments;
using Hino.API.Application.MapperModels;
using Hino.API.Application.ViewModel.Establishments;
using Hino.API.Domain.Establishments.Interfaces.Services;
using Hino.API.Domain.Establishments.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.API.Application.Services.Establishments
{
    public class GEEstablishmentsAS : BaseAppService<GEEstablishments>, IGEEstablishmentsAS
    {
        readonly IGEEstablishmentsService _IGEEstablishmentsService;
        public GEEstablishmentsAS(IGEEstablishmentsService pIGEEstablishmentsService) :
             base(pIGEEstablishmentsService)
        {
            _IGEEstablishmentsService = pIGEEstablishmentsService;
        }

        public async Task<bool> ExistsEstablishmentAsync(string pEstablishmentKey) =>
            await _IGEEstablishmentsService.ExistsEstablishmentAsync(pEstablishmentKey);

        public async Task<GEEstablishments> GetByEstablishmentKeyAsync(string pEstablishmentKey) =>
            await _IGEEstablishmentsService.GetByEstablishmentKeyAsync(pEstablishmentKey);

        public async Task<IEnumerable<GEEstablishments>> GetByIdAndEstablishmentKeyAsync(long pId, string pEstablishmentKey) =>
            await _IGEEstablishmentsService.GetByIdAndEstablishmentKeyAsync(pId, pEstablishmentKey);

        public async Task<IEnumerable<GEEstabDevices>> GetDevicesEstablishment(string pEstablishmentKey, string pUniqueKey, long pId) =>
            await _IGEEstablishmentsService.GetDevicesEstablishment(pEstablishmentKey, pUniqueKey, pId);

        public async Task<bool> HasEstablishmentDependencyAsync(string pEstablishmentKey) =>
            await _IGEEstablishmentsService.HasEstablishmentDependencyAsync(pEstablishmentKey);

        public async Task<GEEstablishments> UpdateEstablishmentAsync(GEEstablishments pEstab) =>
            await _IGEEstablishmentsService.UpdateEstablishmentAsync(pEstab);

        public async Task<GEEstablishments> CreateEstablishmentAsync(GEEstablishmentsCreateVM pEstab)
        {
            var Establishment = Mapper.Map<GEEstablishmentsCreateVM, GEEstablishments>(pEstab);

            return await _IGEEstablishmentsService.CreateEstablishmentAsync(Establishment);
        }

        public async Task<GEEstablishments> UpdateEstablishmentAsync(GEEstablishmentsCreateVM pEstab)
        {
            var Establishment = Mapper.Map<GEEstablishmentsCreateVM, GEEstablishments>(pEstab);

            return await _IGEEstablishmentsService.UpdateEstablishmentAsync(Establishment);
        }
    }
}
