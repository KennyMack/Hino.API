﻿using Hino.API.Application.Interfaces.Services.Establishments;
using Hino.API.Domain.Establishments.Interfaces.Services;
using Hino.API.Domain.Establishments.Models;

namespace Hino.API.Application.Services.Establishments
{
    public class GEEstabMenuAS : BaseAppService<GEEstabMenu>, IGEEstabMenuAS
    {
        public GEEstabMenuAS(IGEEstabMenuService pIGEEstabMenuService) :
             base(pIGEEstabMenuService)
        {

        }
    }
}
