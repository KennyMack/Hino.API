﻿using System;
using Hino.API.Application.Interfaces.Services.Products;
using Hino.API.Domain.Products.Interfaces.Services;
using Hino.API.Domain.Products.Models;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.Services.Products
{
    public class GEProductsFamilyAS : BaseAppService<GEProductsFamily>, IGEProductsFamilyAS
    {
        public GEProductsFamilyAS(IGEProductsFamilyService pService) :
               base(pService)
        {
        }
    }
}
