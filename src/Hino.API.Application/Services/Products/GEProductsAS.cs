﻿using System;
using Hino.API.Application.Interfaces.Services.Products;
using Hino.API.Domain.Products.Interfaces.Services;
using Hino.API.Domain.Products.Models;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hino.API.Application.ViewModel.Products;
using Hino.API.Application.MapperModels;

namespace Hino.API.Application.Services.Products
{
    public class GEProductsAS : BaseAppService<GEProducts>, IGEProductsAS
    {
        readonly IGEProductsService _IGEProductsService;
        public GEProductsAS(IGEProductsService pService) :
               base(pService)
        {
            _IGEProductsService = pService;
        }

        public async Task<GEProductsVM> ChangeAsync(GEProductsVM pProduct) =>
            Mapper.Map<GEProductsVM>(
                await _IGEProductsService.ChangeAsync(Mapper.Map<GEProducts>(pProduct))
            );

        public async Task<GEProductsVM> CreateAsync(GEProductsVM pProduct) =>
            Mapper.Map<GEProductsVM>(
                await _IGEProductsService.CreateAsync(Mapper.Map<GEProducts>(pProduct))
            );

        public async Task<bool> ExistsProductKeyAsync(string pEstablishmentKey, string pProductKey) =>
            await _IGEProductsService.ExistsProductKeyAsync(pEstablishmentKey, pProductKey);

    }
}
