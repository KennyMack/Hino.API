﻿using System;
using Hino.API.Application.Interfaces.Services.Products;
using Hino.API.Domain.Products.Interfaces.Services;
using Hino.API.Domain.Products.Models;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.Services.Products
{
    public class GEProductAplicAS : BaseAppService<GEProductAplic>, IGEProductAplicAS
    {
        public GEProductAplicAS(IGEProductAplicService pService) :
               base(pService)
        {
        }
    }
}
