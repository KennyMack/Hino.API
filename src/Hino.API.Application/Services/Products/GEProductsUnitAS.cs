﻿using System;
using Hino.API.Application.Interfaces.Services.Products;
using Hino.API.Domain.Products.Interfaces.Services;
using Hino.API.Domain.Products.Models;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hino.API.Domain.Base.Interfaces.Messaging;

namespace Hino.API.Application.Services.Products
{
    public class GEProductsUnitAS : BaseAppService<GEProductsUnit>, IGEProductsUnitAS
    {
        readonly IMessageService _IMessageService;
        public GEProductsUnitAS(IGEProductsUnitService pService,
            IMessageService pIMessageService) :
               base(pService)
        {
            _IMessageService = pIMessageService;
        }

        public void PublishMessage(string pEstablishmentKey, string pMessage)
        {
            _IMessageService.PublishMessage("Exception", $"{pEstablishmentKey}_EXCEPTIONS",
                new QueueItem()
                {
                    EstablishmentKey = pEstablishmentKey,
                    EntryName = "Exception",
                    DateTimePublished = DateTime.Now,
                    Tag = new Domain.Base.Models.BaseEntity
                    {
                        UniqueKey = pMessage
                    }
                });
        }
    }
}
