﻿using System;
using System.Collections.Generic;
using Hino.API.Domain.Customers.Models;
using System.Linq;
using Hino.API.Domain.Customers.Interfaces.Services;
using System.Text;
using System.Threading.Tasks;
using Hino.API.Application.Interfaces.Services.Customers;
using Hino.API.Application.ViewModel.Customers;
using Hino.API.Domain.Customers.Enums;
using Hino.API.Application.MapperModels;
using Hino.API.Application.Integrations.Enterprises.Models;

namespace Hino.API.Application.Services.Customers
{
    public class GEEnterprisesAS : BaseAppService<GEEnterprises>, IGEEnterprisesAS
    {
        readonly IGEEnterprisesService _IGEEnterprisesService;
        public GEEnterprisesAS(IGEEnterprisesService pService) :
               base(pService)
        {
            _IGEEnterprisesService = pService;
        }

        public async Task<GEEnterprisesVM> ChangeAsync(GEEnterprisesVM model)
        {
            var result = await _IGEEnterprisesService.ChangeAsync(Mapper.Map<GEEnterprises>(model));

            return Mapper.Map<GEEnterprisesVM>(result);
        }

        public async Task<GEEnterprisesVM> CreateAsync(GEEnterprisesVM model)
        {
            var result = await _IGEEnterprisesService.CreateAsync(Mapper.Map<GEEnterprises>(model));

            return Mapper.Map<GEEnterprisesVM>(result);
        }

        public async Task<bool> EnterpriseExists(string pEstablishmentKey, string pCNPJ, EEnterpriseClassification classification, long idActual) =>
            await _IGEEnterprisesService.EnterpriseExists(pEstablishmentKey, pCNPJ, classification, idActual);

        public async Task UpdateEnterpriseRegionId(string pEstablishmentKey, string pUF, long pRegionId) =>
            await _IGEEnterprisesService.UpdateEnterpriseRegionId(pEstablishmentKey, pUF, pRegionId);

        public async Task UpdateEnterpriseRegionIdAsync(string pEstablishmentKey, string pUF, long pRegionId) =>
            await _IGEEnterprisesService.UpdateEnterpriseRegionIdAsync(pEstablishmentKey, pUF, pRegionId);

        public async Task<GEEnterprisesVM> UpdateToClient(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            var result = await _IGEEnterprisesService.UpdateToClient(pEstablishmentKey, pUniqueKey, pId);

            return Mapper.Map<GEEnterprisesVM>(result);
        }
    }
}
