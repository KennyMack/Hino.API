﻿using System;
using System.Collections.Generic;
using Hino.API.Domain.Customers.Models;
using Hino.API.Application.Interfaces.Services.Customers;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hino.API.Domain.Customers.Interfaces.Services;

namespace Hino.API.Application.Services.Customers
{
    public class GEEnterpriseFiscalGroupAS : BaseAppService<GEEnterpriseFiscalGroup>, IGEEnterpriseFiscalGroupAS
    {
        public GEEnterpriseFiscalGroupAS(IGEEnterpriseFiscalGroupService pService) :
               base(pService)
        {
        }
    }
}
