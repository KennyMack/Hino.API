﻿using System;
using System.Collections.Generic;
using Hino.API.Domain.Customers.Models;
using System.Linq;
using Hino.API.Domain.Customers.Interfaces.Services;
using System.Text;
using System.Threading.Tasks;
using Hino.API.Application.Interfaces.Services.Customers;
using Hino.API.Infra.Cross.Utils.Paging;
using System.Linq.Expressions;
using Hino.API.Application.ViewModel.Customers;
using Hino.API.Application.MapperModels;

namespace Hino.API.Application.Services.Customers
{
    public class GEUserEnterprisesAS : BaseAppService<GEUserEnterprises>, IGEUserEnterprisesAS
    {
        readonly IGEUserEnterprisesService _IGEUserEnterprisesService;
        public GEUserEnterprisesAS(IGEUserEnterprisesService pService) :
               base(pService)
        {
            _IGEUserEnterprisesService = pService;
        }

        public async Task<GEUserEnterprisesVM> CreateOrUpdateAsync(GEUserEnterprisesVM enterprise) =>
            Mapper.Map<GEUserEnterprisesVM>(
            await _IGEUserEnterprisesService.CreateOrUpdateAsync(
                Mapper.Map<GEUserEnterprises>(enterprise)));

        public async Task<GEUserEnterprisesVM[]> CreateOrUpdateListAsync(string pEstablishmentKey, GEUserEnterprisesVM[] enterprise) =>
            Mapper.Map<GEUserEnterprisesVM[]>(
            await _IGEUserEnterprisesService.CreateOrUpdateListAsync(pEstablishmentKey, 
                Mapper.Map<GEUserEnterprises[]>(enterprise)));

        public async Task<PagedResult<GEUserEnterprises>> QuerySearchPagedAsync(int page, int pageSize, Expression<Func<GEUserEnterprises, bool>> predicate, params Expression<Func<GEUserEnterprises, object>>[] includeProperties) =>
            await _IGEUserEnterprisesService.QuerySearchPagedAsync(page, pageSize, predicate, includeProperties);

        public async Task<GEUserEnterprisesVM[]> RemoveListAsync(string pEstablishmentKey, GEUserEnterprisesVM[] enterprise) =>
            Mapper.Map<GEUserEnterprisesVM[]>(
            await _IGEUserEnterprisesService.RemoveListAsync(pEstablishmentKey, 
                Mapper.Map<GEUserEnterprises[]>(enterprise)));
    }
}
