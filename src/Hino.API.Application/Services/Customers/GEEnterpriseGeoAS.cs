﻿using System;
using System.Collections.Generic;
using Hino.API.Domain.Customers.Models;
using System.Linq;
using Hino.API.Application.Interfaces.Services.Customers;
using System.Text;
using Hino.API.Domain.Customers.Interfaces.Services;
using System.Threading.Tasks;

namespace Hino.API.Application.Services.Customers
{
    public class GEEnterpriseGeoAS : BaseAppService<GEEnterpriseGeo>, IGEEnterpriseGeoAS
    {
        public GEEnterpriseGeoAS(IGEEnterpriseGeoService pService) :
               base(pService)
        {
        }
    }
}
