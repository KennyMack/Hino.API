﻿using System;
using System.Collections.Generic;
using System.Linq;
using Hino.API.Application.Interfaces.Services.Customers;
using Hino.API.Domain.Customers.Models;
using System.Text;
using System.Threading.Tasks;
using Hino.API.Domain.Customers.Interfaces.Services;

namespace Hino.API.Application.Services.Customers
{
    public class GEEnterpriseContactsAS : BaseAppService<GEEnterpriseContacts>, IGEEnterpriseContactsAS
    {
        public GEEnterpriseContactsAS(IGEEnterpriseContactsService pService) :
               base(pService)
        {
        }
    }
}
