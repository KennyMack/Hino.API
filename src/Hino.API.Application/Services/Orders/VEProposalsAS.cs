﻿using Hino.API.Application.Interfaces.Services.Orders;
using Hino.API.Application.MapperModels;
using Hino.API.Application.ViewModel.Orders;
using Hino.API.Domain.Establishments.Interfaces.Services;
using Hino.API.Domain.Orders.Interfaces.Services;
using Hino.API.Domain.Orders.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.Services.Orders
{
    public class VEProposalsAS : BaseAppService<VEOrders>, IVEProposalsAS
    {
        readonly IVEOrdersService _IVEOrdersService;
        readonly ICreateOrderService _ICreateOrderService;
        readonly IUpdateOrderService _IUpdateOrderService;
        readonly IGEEstablishmentsService _IGEEstablishmentsService;
        readonly IRemoveOrderService _IRemoveOrderService;
        public VEProposalsAS(IVEOrdersService pService,
            ICreateOrderService pICreateOrderService,
            IUpdateOrderService pIUpdateOrderService,
            IGEEstablishmentsService pIGEEstablishmentsService,
            IRemoveOrderService pIRemoveOrderService) :
               base(pService)
        {
            _IGEEstablishmentsService = pIGEEstablishmentsService;
            _IUpdateOrderService = pIUpdateOrderService;
            _IVEOrdersService = pService;
            _ICreateOrderService = pICreateOrderService;
            _IRemoveOrderService = pIRemoveOrderService;
        }

        public async Task<VEOrdersVM> CanConvertToOrderAsync(string pEstablishmentKey, long id) =>
            Mapper.Map<VEOrdersVM>(
                await _IVEOrdersService.CanConvertToOrderAsync(pEstablishmentKey, id)
           );

        public async Task<VEOrdersVM> CreateOrderAsync(VEOrdersVM pOrder)
        {
            var Order = Mapper.Map<VEOrders>(pOrder);
            var Estab = await _IGEEstablishmentsService.GetByEstablishmentKeyAsync(pOrder.EstablishmentKey);

            Order.DaysPayment = Estab.DaysPayment;

            var Result = await _ICreateOrderService.CreateOrderAsync(Order);

            if (_ICreateOrderService.Errors.Any())
            {
                Errors.AddRange(_ICreateOrderService.Errors);
                return null;
            }

            return Mapper.Map<VEOrdersVM>(Result);
        }

        public async Task<VEOrdersVM> RemoveByIdAsync(long id, string pEstablishmentKey, string pUniqueKey) =>
            Mapper.Map<VEOrdersVM>(
                await _IRemoveOrderService.RemoveByIdAsync(id, pEstablishmentKey, pUniqueKey)
           );

        public async Task<VEOrdersVM> UpdateOrderAsync(VEOrdersVM pOrder)
        {
            var Order = Mapper.Map<VEOrders>(pOrder);
            var Estab = await _IGEEstablishmentsService.GetByEstablishmentKeyAsync(pOrder.EstablishmentKey);

            Order.DaysPayment = Estab.DaysPayment;

            var Result = await _IUpdateOrderService.UpdateOrderAsync(Order);

            if (_IUpdateOrderService.Errors.Any())
            {
                Errors.AddRange(_IUpdateOrderService.Errors);
                return null;
            }

            return Mapper.Map<VEOrdersVM>(Result);
        }
    }
}
