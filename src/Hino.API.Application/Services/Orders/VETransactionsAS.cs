﻿using Hino.API.Application.Interfaces.Services.Orders;
using Hino.API.Application.MapperModels;
using Hino.API.Application.ViewModel.Orders;
using Hino.API.Domain.Orders.Interfaces.Services;
using Hino.API.Domain.Orders.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.Services.Orders
{
    public class VETransactionsAS : BaseAppService<VETransactions>, IVETransactionsAS
    {
        readonly IVETransactionsService _IVETransactionsService;
        public VETransactionsAS(IVETransactionsService pService) :
               base(pService)
        {
            _IVETransactionsService = pService;
        }

        public async Task<VETransactionsVM> CreateTransaction(VETransactionsVM pTransaction) =>
            Mapper.Map<VETransactionsVM>(
                await _IVETransactionsService.CreateTransaction(
                    Mapper.Map<VETransactions>(pTransaction)
                )
            );

        public async Task<VETransactionsVM> GetByAuthCode(string pEstablishmentKey, string pAuthCode) =>
            Mapper.Map<VETransactionsVM>(
                await _IVETransactionsService.GetByAuthCode(pEstablishmentKey, pAuthCode)
            );

        public async Task<VETransactionsVM> GetByNsuHost(string pEstablishmentKey, string pTerminalId, string pNsuHost) =>
            Mapper.Map<VETransactionsVM>(
                await _IVETransactionsService.GetByNsuHost(pEstablishmentKey, pTerminalId, pNsuHost)
            );

        public async Task<VETransactionsVM> SearchTransationByNSUAsync(string pEstablishmentKey, string NSU) =>
            Mapper.Map<VETransactionsVM>(
                await _IVETransactionsService.SearchTransationByNSUAsync(pEstablishmentKey, NSU)
            );

        public async Task<VETransactionsVM> UpdateTransaction(VETransactionsVM pTransaction) =>
            Mapper.Map<VETransactionsVM>(
                await _IVETransactionsService.UpdateTransaction(
                    Mapper.Map<VETransactions>(pTransaction)
                )
            );
    }
}
