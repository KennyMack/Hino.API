﻿using Hino.API.Application.Interfaces.Services.Demograph;
using Hino.API.Domain.Demograph.Interfaces.Services;
using Hino.API.Domain.Demograph.Models;

namespace Hino.API.Application.Services.Demograph
{
    public class GEStatesAS : BaseAppService<GEStates>, IGEStatesAS
    {
        public GEStatesAS(IGEStatesService pIGEStatesService) : base(pIGEStatesService)
        {
        }
    }
}
