﻿using Hino.API.Application.Interfaces.Services.Demograph;
using Hino.API.Application.MapperModels;
using Hino.API.Application.ViewModel.Demograph;
using Hino.API.Domain.Demograph.Interfaces.Services;
using Hino.API.Domain.Demograph.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.Services.Demograph
{
    public class GECountriesAS : BaseAppService<GECountries>, IGECountriesAS
    {
        private readonly IGECountriesService _IGECountriesService;
        public GECountriesAS(IGECountriesService pIGECountriesService) : base(pIGECountriesService)
        {
            _IGECountriesService = pIGECountriesService;
        }

        public GECountriesVM GetByInitials(string pInitial) =>
            Mapper.Map<GECountriesVM>(_IGECountriesService.GetByInitials(pInitial));

        public async Task LoadCountriesAsync(CountriesLoad[] pLoad) =>
            await _IGECountriesService.LoadCountriesAsync(pLoad);

    }
}
