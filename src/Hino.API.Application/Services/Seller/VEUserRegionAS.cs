﻿using Hino.API.Application.Interfaces.Services.Seller;
using Hino.API.Application.MapperModels;
using Hino.API.Application.ViewModel.Seller;
using Hino.API.Domain.Seller.Interfaces.Services;
using Hino.API.Domain.Seller.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.Services.Seller
{
    public class VEUserRegionAS : BaseAppService<VEUserRegion>, IVEUserRegionAS
    {
        readonly IVEUserRegionService _IVEUserRegionService;
        public VEUserRegionAS(IVEUserRegionService pService) :
             base(pService)
        {
            _IVEUserRegionService = pService;
        }

        public async Task<VEUserRegionVM> CreateOrUpdateAsync(VEUserRegion userRegion) =>
            Mapper.Map<VEUserRegionVM>(
                await _IVEUserRegionService.CreateOrUpdateAsync(
                    Mapper.Map<VEUserRegion>(userRegion)
                )
            );

        public async Task<VEUserRegionVM[]> CreateOrUpdateListAsync(string pEstablishmentKey, VEUserRegionVM[] userRegion) =>
            Mapper.Map<VEUserRegionVM[]>(
                await _IVEUserRegionService.CreateOrUpdateListAsync(
                    pEstablishmentKey,
                    Mapper.Map<VEUserRegionVM[], VEUserRegion[]>(userRegion)
                )
            );

        public async Task<VEUserRegionVM[]> RemoveListAsync(string pEstablishmentKey, VEUserRegionVM[] userRegion) =>
            Mapper.Map<VEUserRegionVM[]>(
                await _IVEUserRegionService.RemoveListAsync(
                    pEstablishmentKey,
                    Mapper.Map<VEUserRegionVM[], VEUserRegion[]>(userRegion)
                )
            );

        public bool ZipCodeExists(string pEstablishmentKey, long pUserId, string pZipCode) =>
            _IVEUserRegionService.ZipCodeExists(pEstablishmentKey, pUserId, pZipCode);
    }
}
