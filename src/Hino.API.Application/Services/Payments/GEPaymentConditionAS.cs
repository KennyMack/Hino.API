﻿using Hino.API.Application.Interfaces.Services.Payments;
using Hino.API.Domain.Base.Services;
using Hino.API.Domain.Payments.Interfaces.Services;
using Hino.API.Domain.Payments.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.Services.Payments
{
    public class GEPaymentConditionAS : BaseAppService<GEPaymentCondition>, IGEPaymentConditionAS
    {
        private readonly IGEPaymentConditionService _IGEPaymentConditionService;

        public GEPaymentConditionAS(IGEPaymentConditionService pIGEPaymentConditionService) :
             base(pIGEPaymentConditionService)
        {
            _IGEPaymentConditionService = pIGEPaymentConditionService;
        }

        public async Task<GEPaymentCondition> ChangeAsync(GEPaymentCondition model) =>
            await _IGEPaymentConditionService.ChangeAsync(model);
    }
}
