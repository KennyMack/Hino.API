﻿using Hino.API.Application.Interfaces.Services.Payments;
using Hino.API.Domain.Payments.Interfaces.Services;
using Hino.API.Domain.Payments.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.Services.Payments
{
    public class GEPaymentTypeAS : BaseAppService<GEPaymentType>, IGEPaymentTypeAS
    {
        private readonly IGEPaymentTypeService _IGEPaymentTypeService;

        public GEPaymentTypeAS(IGEPaymentTypeService pIGEPaymentTypeService) :
             base(pIGEPaymentTypeService)
        {
            _IGEPaymentTypeService = pIGEPaymentTypeService;
        }
    }
}
