﻿using Hino.API.Application.Interfaces.Services.Events;
using Hino.API.Domain.Events.Interfaces.Services;
using Hino.API.Domain.Events.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.Services.Events
{
    public class GEUserCalendarAS : BaseAppService<GEUserCalendar>, IGEUserCalendarAS
    {
        public GEUserCalendarAS(IGEUserCalendarService pService) :
             base(pService)
        {
        }
    }
}
