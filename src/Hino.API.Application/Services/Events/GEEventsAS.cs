﻿using Hino.API.Application.Interfaces.Services.Events;
using Hino.API.Application.MapperModels;
using Hino.API.Application.ViewModel.Events;
using Hino.API.Domain.Events.Interfaces.Services;
using Hino.API.Domain.Events.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.Services.Events
{
    public class GEEventsAS : BaseAppService<GEEvents>, IGEEventsAS
    {
        readonly IGEEventsService _IGEEventsService;
        public GEEventsAS(IGEEventsService pService) :
               base(pService)
        {
            _IGEEventsService = pService;
        }

        public async Task<GEEventsVM> ChangeEventStatusAsync(long pId, string pEstablishmentKey, string pUniqueKey, GEEventsStatusVM pEventStatus)
        {
            var result = await _IGEEventsService.ChangeEventStatusAsync(pId, pEstablishmentKey, pUniqueKey, Mapper.Map<GEEventsStatus>(pEventStatus));

            return Mapper.Map<GEEventsVM>(result);
        }

        public async Task<GEEventsVM> CreateEventAsync(string pEstablishmentKey, GECreateEventsVM pGEEventsVM)
        {
            var result = await _IGEEventsService.CreateEventAsync(pEstablishmentKey, Mapper.Map<GECreateEvents>(pGEEventsVM));

            return Mapper.Map<GEEventsVM>(result);
        }

        public async Task<GEEventsVM> GenerateEventChildAsync(long pId, string pEstablishmentKey, string pUniqueKey, GEEventsChildVM pEventsChild)
        {
            var result = await _IGEEventsService.GenerateEventChildAsync(pId, pEstablishmentKey, pUniqueKey, Mapper.Map<GEEventsChild>(pEventsChild));

            return Mapper.Map<GEEventsVM>(result);
        }

        public async Task<GEEventsVM> RemoveEventIdAsync(long pId, string pEstablishmentKey, string pUniqueKey)
        {
            var result = await _IGEEventsService.RemoveEventIdAsync(pId, pEstablishmentKey, pUniqueKey);

            return Mapper.Map<GEEventsVM>(result);
        }

        public async Task<GEEventsVM> SaveReferenceAsync(string pEstablishmentKey, GEEventsVM Event, GECreateEventsVM pGEEventsVM)
        {
            var result = await _IGEEventsService.SaveReferenceAsync(pEstablishmentKey, Mapper.Map<GEEvents>(Event), Mapper.Map<GECreateEvents>(pGEEventsVM));

            return Mapper.Map<GEEventsVM>(result);
        }

        public async Task<GEEventsVM> UpdateEventAsync(string pEstablishmentKey, GECreateEventsVM pGEEventsVM)
        {
            var result = await _IGEEventsService.UpdateEventAsync(pEstablishmentKey, Mapper.Map<GECreateEvents>(pGEEventsVM));

            return Mapper.Map<GEEventsVM>(result);
        }
    }
}
