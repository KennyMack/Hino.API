﻿using Hino.API.Application.Interfaces.Services.Events;
using Hino.API.Domain.Events.Interfaces.Services;
using Hino.API.Domain.Events.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.Services.Events
{
    public class GEEstabCalendarAS : BaseAppService<GEEstabCalendar>, IGEEstabCalendarAS
    {
        public GEEstabCalendarAS(IGEEstabCalendarService pService) :
               base(pService)
        {
        }
    }
}
