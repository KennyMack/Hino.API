﻿using System.Collections.Generic;

namespace Hino.API.Application.ViewModel.SalePrices
{
    public class VERegionSaleVM : BaseVM
    {
        public VERegionSaleVM()
        {
            this.VERegionSaleUF = new HashSet<VERegionSaleUFVM>();
        }

        public string Description { get; set; }
        public long IdERP { get; set; }

        public virtual ICollection<VERegionSaleUFVM> VERegionSaleUF { get; set; }

    }
}
