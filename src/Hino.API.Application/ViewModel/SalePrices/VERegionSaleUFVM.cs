﻿namespace Hino.API.Application.ViewModel.SalePrices
{
    public class VERegionSaleUFVM : BaseVM
    {
        public long RegionId { get; set; }
        public VERegionSaleVM VERegionSale { get; set; }

        public string UF { get; set; }
    }
}
