﻿namespace Hino.API.Application.ViewModel.SalePrices
{
    public class VESalePriceVM : BaseVM
    {
        public VESalePriceVM()
        {

        }

        public long CodPrVenda { get; set; }
        public long RegionId { get; set; }
        public VERegionSaleVM VERegionSale { get; set; }

        public string Description { get; set; }

        public long ProductId { get; set; }

        public virtual VEProductsVM GEProducts { get; set; }

        public decimal Value { get; set; }

        public string ProductKey { get; set; }
    }
}
