﻿namespace Hino.API.Application.ViewModel.SalePrices
{
    public class VEProductsVM : BaseVM
    {
        public string ProductKey { get; set; }
        public string Name { get; set; }
        public decimal PercIPI { get; set; }
        public int Status { get; set; }
        public string NCM { get; set; }
        public decimal StockBalance { get; set; }
    }
}
