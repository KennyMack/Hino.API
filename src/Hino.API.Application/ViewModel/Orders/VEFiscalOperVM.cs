﻿using Hino.API.Domain.Base.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.ViewModel.Orders
{
    [Table("FSFiscalOper")]
    public class VEFiscalOperVM : BaseVM
    {
        public VEFiscalOperVM()
        {
        }

        public string Description { get; set; }
    }
}
