﻿using Hino.API.Domain.Base.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.ViewModel.Orders
{
    [Table("GEPAYMENTTYPE")]
    public class VEPaymentTypeVM : BaseVM
    {
        public string Description { get; set; }
    }
}
