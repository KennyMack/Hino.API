﻿using Hino.API.Domain.Base.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.ViewModel.Orders
{
    [Table("GEUSERS")]
    public class VESallesmanVM: BaseVM
    {
        public string Name { get; set; }

        public VESallesmanVM()
        {
            this.VEOrders = new HashSet<VEOrdersVM>();
            this.GEUserDigitizer = new HashSet<VEOrdersVM>();
        }

        public virtual ICollection<VEOrdersVM> VEOrders { get; set; }
        [InverseProperty("GEUserDigitizer")]
        public virtual ICollection<VEOrdersVM> GEUserDigitizer { get; set; }
    }
}
