﻿using Hino.API.Domain.Base.Models;
using Hino.API.Infra.Cross.Utils.Attributes;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.API.Application.ViewModel.Orders
{
    public class VESalePriceVM : BaseVM
    {
        public VESalePriceVM()
        {

        }

        public long CodPrVenda { get; set; }
        public long RegionId { get; set; }

        public string Description { get; set; }

        public long ProductId { get; set; }

        public virtual VEProductsVM GEProducts { get; set; }

        public decimal Value { get; set; }
    }
}
