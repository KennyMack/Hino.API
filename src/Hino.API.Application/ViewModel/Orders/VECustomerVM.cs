﻿using Hino.API.Domain.Base.Models;
using Hino.API.Domain.Orders.Enums;
using Hino.API.Domain.Orders.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.ViewModel.Orders
{
    public class VECustomerVM : BaseVM
    {
        public VECustomerVM()
        {
            VEOrders = new HashSet<VEOrdersVM>();
        }

        public string RazaoSocial { get; set; }
        public string NomeFantasia { get; set; }
        public string CNPJCPF { get; set; }
        public string IE { get; set; }
        public EEnterpriseType Type { get; set; }
        public EStatusEnterprise Status { get; set; }
        public EEnterpriseClassification Classification { get; set; }
        public long RegionId { get; set; }

        [InverseProperty("GEEnterprises")]
        public virtual ICollection<VEOrdersVM> VEOrders { get; set; }
    }
}
