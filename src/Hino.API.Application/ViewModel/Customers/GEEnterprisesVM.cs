﻿using Hino.API.Domain.Base.Models;
using Hino.API.Domain.Customers.Enums;
using Hino.API.Infra.Cross.Utils;
using Hino.API.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.ViewModel.Customers
{
    public class GEEnterprisesVM : BaseVM
    {
        public GEEnterprisesVM()
        {
            VEOrders = new HashSet<GEOrdersVM>();
            VEOrdersCarrier = new HashSet<GEOrdersVM>();
            VEOrdersRedispatch = new HashSet<GEOrdersVM>();
            GEEnterpriseGeo = new HashSet<GEEnterpriseGeoVM>();
            GEEnterpriseContacts = new HashSet<GEEnterpriseContactsVM>();
            GEUserEnterprises = new HashSet<GEUserEnterprisesVM>();
        }

        public string RazaoSocial { get; set; }
        public string NomeFantasia { get; set; }
        public string CNPJCPF { get; set; }
        public string IE { get; set; }
        public EEnterpriseType Type { get; set; }
        public EStatusEnterprise Status { get; set; }
        public EStatusSinc StatusSinc { get; set; }
        public long IdERP { get; set; }
        public long RegionId { get; set; }
        public string Search { get; set; }
        public decimal CreditLimit { get; set; }
        public decimal CreditUsed { get; set; }
        public DateTime? BirthDate { get; set; }
        public EEnterpriseClassification Classification { get; set; }
        public string ClassifEmpresa { get; set; }

        public long? CreatedById { get; set; }
        public virtual GEUsersVM GEUsersCreatedBy { get; set; }

        public long? FiscalGroupId { get; set; }
        public GEEnterpriseFiscalGroupVM GEEnterpriseFiscalGroup { get; set; }

        public long? PayConditionId { get; set; }
        public GEPaymentConditionVM GEPaymentCondition { get; set; }

        public long? CategoryId { get; set; }
        public GEEnterpriseCategoryVM GEEnterpriseCategory { get; set; }

        public long? GroupId { get; set; }
        public GEEnterpriseGroupVM GEEnterpriseGroup { get; set; }

        public virtual ICollection<GEOrdersVM> VEOrders { get; set; }

        public virtual ICollection<GEOrdersVM> VEOrdersCarrier { get; set; }

        public virtual ICollection<GEOrdersVM> VEOrdersRedispatch { get; set; }

        public virtual ICollection<GEEnterpriseGeoVM> GEEnterpriseGeo { get; set; }

        public virtual ICollection<GEEnterpriseContactsVM> GEEnterpriseContacts { get; set; }

        public virtual ICollection<GEUserEnterprisesVM> GEUserEnterprises { get; set; }

        #region Formata IE
        public string FormatIE(string pUF)
        {
            try
            {
                if (!IE.IsEmpty() && IE != "ISENTO" && IE != "N/INFO" &&
                    pUF.ValidarUF())
                    return IE.CleanSpecialChar().FormatarIE(pUF);
            }
            catch (Exception)
            {
            }
            return IE;
        }
        #endregion
    }
}
