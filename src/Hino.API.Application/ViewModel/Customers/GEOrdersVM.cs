﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.API.Application.ViewModel.Customers
{
    public class GEOrdersVM : BaseVM
    {
        public GEOrdersVM()
        {
        }

        public long EnterpriseID { get; set; }
        public GEEnterprisesVM GEEnterprises { get; set; }

        public long? CarrierID { get; set; }
        public GEEnterprisesVM GECarriers { get; set; }

        public long? RedispatchID { get; set; }
        public GEEnterprisesVM GERedispatch { get; set; }

        public long UserID { get; set; }
        public GEUsersVM GEUsers { get; set; }

        public long TypePaymentID { get; set; }
        public GEPaymentTypeVM GEPaymentType { get; set; }

        public long PayConditionID { get; set; }
        public GEPaymentConditionVM GEPaymentCondition { get; set; }

        public long CodPedVenda { get; set; }
        public long NumPedMob { get; set; }
        public DateTime DeliveryDate { get; set; }
        public string Note { get; set; }
        public string DetailedNote { get; set; }
        public string InnerNote { get; set; }
        public string Status { get; set; }
        public string StatusCRM { get; set; }
        public Domain.Customers.Enums.EStatusSinc StatusSinc { get; set; }
        public bool IsProposal { get; set; }
        public string ClientOrder { get; set; }

        public long IdERP { get; set; }
    }
}
