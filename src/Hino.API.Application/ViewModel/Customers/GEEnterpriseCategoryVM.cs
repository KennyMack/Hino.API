﻿using Hino.API.Domain.Base.Models;
using Hino.API.Infra.Cross.Utils.Attributes;

namespace Hino.API.Application.ViewModel.Customers
{
    public class GEEnterpriseCategoryVM : BaseVM
    {
        public string Description { get; set; }
        public string Identifier { get; set; }
        public long IdERP { get; set; }
    }
}
