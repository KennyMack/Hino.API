﻿using Hino.API.Domain.Base.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.API.Application.ViewModel.Customers
{
    public class GEEnterpriseFiscalGroupVM : BaseVM
    {
        public string Description { get; set; }
        public string Type { get; set; }
        public long IdERP { get; set; }
    }
}
