﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.ViewModel.Customers
{
    public class GEUserEnterprisesListVM
    {
        public GEUserEnterprisesVM[] results { get; set; }
    }
}
