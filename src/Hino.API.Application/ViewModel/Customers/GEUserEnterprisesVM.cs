﻿using Hino.API.Domain.Base.Models;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.API.Application.ViewModel.Customers
{
    public class GEUserEnterprisesVM : BaseVM
    {
        public long UserId { get; set; }
        public GEUsersVM GEUsers { get; set; }

        public long EnterpriseId { get; set; }
        public GEEnterprisesVM GEEnterprises { get; set; }
    }
}
