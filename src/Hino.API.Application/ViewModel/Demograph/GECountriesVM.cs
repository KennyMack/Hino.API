﻿using Hino.API.Domain.Base.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.API.Application.ViewModel.Demograph
{
    public class GECountriesVM : BaseVM
    {
        public GECountriesVM()
        {
            this.GEStates = new HashSet<GEStatesVM>();
        }
        public string Initials { get; set; }
        public string Name { get; set; }
        public string BACEN { get; set; }
        public string DDI { get; set; }
        public string LocalName { get; set; }
        public string CIOC { get; set; }
        public string NumericCode { get; set; }
        public decimal Lat { get; set; }
        public decimal Lng { get; set; }
        public string Flag { get; set; }

        public long IdERP { get; set; }

        public ICollection<GEStatesVM> GEStates { get; set; }
    }
}
