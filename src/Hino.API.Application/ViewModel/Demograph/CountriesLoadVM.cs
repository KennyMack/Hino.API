﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.ViewModel.Demograph
{
    public class CountriesLoadVM
    {
        public CountriesLoadVM()
        {
            callingCodes = new List<string>();
            latlng = new List<decimal>();
            translations = new CountriesLoadTranslationsVM();
        }

        public string name { get; set; }
        public string alpha2Code { get; set; }
        public string alpha3Code { get; set; }
        public string numericCode { get; set; }
        public string flag { get; set; }
        public List<string> callingCodes { get; set; }
        public List<decimal> latlng { get; set; }
        public CountriesLoadTranslationsVM translations { get; set; }
    }
}
