﻿using Hino.API.Domain.Base.Models;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.API.Application.ViewModel.Demograph
{
    public class GECitiesVM : BaseVM
    {
        public string Name { get; set; }
        public string IBGE { get; set; }
        public string DDD { get; set; }

        public long StateID { get; set; }
        public GEStatesVM GEStates { get; set; }

        public long IdERP { get; set; }
    }
}
