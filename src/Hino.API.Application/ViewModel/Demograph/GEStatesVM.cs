﻿using Hino.API.Domain.Base.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.API.Application.ViewModel.Demograph
{
    public class GEStatesVM : BaseVM
    {
        public GEStatesVM()
        {
            this.GECities = new HashSet<GECitiesVM>();
        }

        public string Name { get; set; }
        public string Initials { get; set; }
        public string IBGE { get; set; }
        public decimal Lat { get; set; }
        public decimal Lng { get; set; }
        public string CodeFIPS { get; set; }

        public long CountryID { get; set; }
        public long IdERP { get; set; }
        public GECountriesVM GECountries { get; set; }

        public ICollection<GECitiesVM> GECities { get; set; }
    }
}
