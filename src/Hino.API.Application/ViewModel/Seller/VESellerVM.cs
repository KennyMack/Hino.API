﻿namespace Hino.API.Application.ViewModel.Seller
{
    public class VESellerVM : BaseVM
    {
        public string Name { get; set; }
        public decimal PercDiscount { get; set; }
        public decimal PercCommission { get; set; }
        public decimal PercDiscountPrice { get; set; }
        public decimal PercIncreasePrice { get; set; }
        public string StoreId { get; set; }
        public string TerminalId { get; set; }
        public string UserKey { get; set; }
        public bool IsBlockedByPay { get; set; }
    }
}
