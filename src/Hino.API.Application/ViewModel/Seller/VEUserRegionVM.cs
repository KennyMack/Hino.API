﻿namespace Hino.API.Application.ViewModel.Seller
{
    public class VEUserRegionVM : BaseVM
    {
        public long UserId { get; set; }
        public VESellerVM GEUsers { get; set; }

        public long SaleWorkId { get; set; }
        public VESaleWorkRegionVM VESaleWorkRegion { get; set; }
    }
}
