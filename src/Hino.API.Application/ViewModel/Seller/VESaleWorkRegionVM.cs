﻿using Hino.API.Infra.Cross.Utils;

namespace Hino.API.Application.ViewModel.Seller
{
    public class VESaleWorkRegionVM : BaseVM
    {
        public string Description { get; set; }
        public string ZIPCodeStart { get; set; }
        public string ZIPCodeStartCleaned { get => ZIPCodeStart.OnlyNumbers(); }
        public string ZIPCodeEnd { get; set; }
        public string ZIPCodeEndCleaned { get => ZIPCodeEnd.OnlyNumbers(); }
    }
}
