﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.ViewModel.Seller
{
    public class VEUserRegionListVM
    {
        public VEUserRegionVM[] results { get; set; }
    }
}
