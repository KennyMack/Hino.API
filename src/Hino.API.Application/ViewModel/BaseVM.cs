﻿using Hino.API.Application.Interfaces.ViewModels;
using Hino.API.Infra.Cross.Utils.Attributes;
using System;
using System.ComponentModel.DataAnnotations;

namespace Hino.API.Application.ViewModel
{
    public class BaseVM : IBaseVM
    {
        [Key]
        [DisplayField]
        public long Id { get; set; }
        [Min36LengthField]
        [Max36LengthField]
        [RequiredField]
        [DisplayField]
        public string EstablishmentKey { get; set; }
        [RequiredField]
        [Min36LengthField]
        [Max36LengthField]
        [DisplayField]
        public string UniqueKey { get; set; }
        [DisplayField]
        public DateTime Created { get; set; }
        [DisplayField]
        public DateTime Modified { get; set; }
        [DisplayField]
        public bool IsActive { get; set; }
    }
}
