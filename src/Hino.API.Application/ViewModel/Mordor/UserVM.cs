﻿using Hino.API.Domain.Mordor.Enums;
using Hino.API.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.ViewModel.Mordor
{
    public class UserVM: BaseVM
    {
        public string Uid { get; set; }
        [RequiredField]
        [DisplayField]
        public string UserName { get; set; }
        public string Name { get; set; }
        [RequiredField]
        [DisplayField]
        public string Email { get; set; }
        [RequiredField]
        [DisplayField]
        public string Password { get; set; }
        public DateTime LastLogin { get; set; }
        public EUserType UserType { get; set; }
        public decimal PercDiscount { get; set; }
        public decimal PercCommission { get; set; }
        public decimal PercDiscountPrice { get; set; }
        public decimal PercIncreasePrice { get; set; }
        public string StoreId { get; set; }
        public string TerminalId { get; set; }
        [RequiredField]
        [DisplayField]
        public string UserKey { get; set; }
        public bool IsBlockedByPay { get; set; }

        public string Role { get; set; }
        public bool Active { get; set; }

        public UserVM()
        {

        }

        public UserVM(string Email, string Password)
        {
            this.Email = Email;
            this.Password = Password;

        }

        public UserVM(
            long Id,
            string EstablishmentKey,
            string UniqueKey,
            string Uid,
            string UserName,
            string Email,
            string Password,
            EUserType UserType,
            decimal PercDiscount,
            decimal PercCommission,
            decimal PercDiscountPrice,
            decimal PercIncreasePrice,
            string StoreId,
            string TerminalId,
            string UserKey,
            bool IsBlockedByPay,
            string Role,
            bool Active)
        {
            this.Id = Id;
            this.Uid = Uid;
            this.EstablishmentKey = EstablishmentKey;
            this.UniqueKey = UniqueKey;
            this.UserName = UserName;
            this.UserType = UserType;
            this.PercDiscount = PercDiscount;
            this.PercCommission = PercCommission;
            this.PercDiscountPrice = PercDiscountPrice;
            this.PercIncreasePrice = PercIncreasePrice;
            this.StoreId = StoreId;
            this.TerminalId = TerminalId;
            this.UserKey = UserKey;
            this.IsBlockedByPay = IsBlockedByPay;

            this.Email = Email;
            this.Password = Password;
            this.Role = Role;
            this.Active = Active;
        }
    }
}
