﻿using Hino.API.Domain.Mordor.Enums;
using Hino.API.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.ViewModel.Mordor
{
    public class CreateUserVM: BaseVM
    {
        [DisplayField]
        public string Uid { get; set; }
        [DisplayField]
        [RequiredField]
        public string UserName { get; set; }
        [DisplayField]
        [RequiredField]
        public string Email { get; set; }
        [DisplayField]
        [RequiredField]
        public string Password { get; set; }
        [DisplayField]
        [RequiredField]
        public string Role { get; set; }
        [DisplayField]
        public bool Active { get; set; }
        [DisplayField]
        public EUserType UserType { get; set; }
        [RequiredField]
        [DisplayField]
        public string UserKey { get; set; }
    }
}
