﻿using AutoMapper;
using Hino.API.Domain.Base.Models;
using Hino.API.Domain.Payments.Models;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.API.Application.ViewModel.Payments
{
    [AutoMap(typeof(GEPaymentCondInstallments))]
    public class GEPaymentCondInstallmentsVM : BaseVM
    {
        public long CondPayId { get; set; }
        public virtual GEPaymentConditionVM GEPaymentCondition { get; set; }

        public decimal Percent { get; set; }
        public int Days { get; set; }
    }
}
