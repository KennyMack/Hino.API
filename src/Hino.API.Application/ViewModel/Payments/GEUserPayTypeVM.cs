using System.ComponentModel.DataAnnotations.Schema;
using Hino.API.Domain.Base.Models;

namespace Hino.API.Application.ViewModel.Payments
{
    public class GEUserPayTypeVM : BaseVM
    {
        public long PaymentTypeId { get; set; }
        public GEPaymentTypeVM GEPaymentType { get; set; }
        
        public long UserId { get; set; }
        public GESellerVM GEUsers { get; set; }
    }
}
