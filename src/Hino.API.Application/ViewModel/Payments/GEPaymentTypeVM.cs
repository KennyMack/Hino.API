﻿using Hino.API.Domain.Base.Models;
using Hino.API.Infra.Cross.Utils.Attributes;
using System.Collections.Generic;

namespace Hino.API.Application.ViewModel.Payments
{
    public class GEPaymentTypeVM : BaseVM
    {
        public GEPaymentTypeVM()
        {
            this.GEPaymentCondition = new HashSet<GEPaymentConditionVM>();
        }

        public string Description { get; set; }
        public long IdERP { get; set; }
        public bool Boleto { get; set; }

        public ICollection<GEUserPayTypeVM> GEUserPayType { get; set; }
        public ICollection<GEPaymentConditionVM> GEPaymentCondition { get; set; }

    }
}
