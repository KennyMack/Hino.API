﻿using Hino.API.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.ViewModel.Payments
{
    public class GEPaymentConditionCreateVM : BaseVM
    {
        public GEPaymentConditionCreateVM()
        {
            this.GEPaymentCondInstallments = new HashSet<GEPaymentCondInstallmentsVM>();
        }

        [RequiredField]
        [DisplayField]
        public long TypePayID { get; set; }
        public GEPaymentTypeVM GEPaymentType { get; set; }

        [RequiredField]
        [DisplayField]
        public string Description { get; set; }
        public short Installments { get; set; }

        [RequiredField]
        [DisplayField]
        public decimal PercDiscount { get; set; }

        [RequiredField]
        [DisplayField]
        public decimal PercIncrease { get; set; }

        public virtual ICollection<GEPaymentCondInstallmentsVM> GEPaymentCondInstallments { get; set; }

        public long IdERP { get; set; }

        public string GetDescription
        {
            get
            {
                if (GEPaymentType != null)
                    return $"{GEPaymentType.Id} - {GEPaymentType.Description}@{Id} - {Description}";

                return $"{Id} - {Description}";
            }
        }
    }
}
