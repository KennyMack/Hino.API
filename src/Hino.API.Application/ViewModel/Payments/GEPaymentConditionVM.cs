﻿using Hino.API.Domain.Base.Models;
using Hino.API.Infra.Cross.Utils.Attributes;
using Hino.API.Infra.Cross.Utils.Exceptions;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using AutoMapper;
using DefaultMessages = Hino.API.Infra.Cross.Resources.MessagesResource;

namespace Hino.API.Application.ViewModel.Payments
{
    public class GEPaymentConditionVM : BaseVM
    {
        public GEPaymentConditionVM()
        {
            this.GEPaymentCondInstallments = new HashSet<GEPaymentCondInstallmentsVM>();
        }

        [RequiredField]
        [DisplayField]
        public long TypePayID { get; set; }
        public GEPaymentTypeVM GEPaymentType { get; set; }

        [RequiredField]
        [DisplayField]
        public string Description { get; set; }
        public short Installments { get; set; }

        [RequiredField]
        [DisplayField]
        public decimal PercDiscount { get; set; }

        [RequiredField]
        [DisplayField]
        public decimal PercIncrease { get; set; }
        public long IdERP { get; set; }

        public virtual ICollection<GEPaymentCondInstallmentsVM> GEPaymentCondInstallments { get; set; }
    }
}
