using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Hino.API.Domain.Base.Models;

namespace Hino.API.Application.ViewModel.Events
{
    public class GEUserCalendarVM : BaseVM
    {
        [ForeignKey("GEUsers")]
        public long UserID { get; set; }
        public virtual GEUsersVM GEUsers { get; set; }

        [ForeignKey("GEEvents")]
        public long EventID { get; set; }
        public virtual GEEventsVM GEEvents { get; set; }
    }
}
