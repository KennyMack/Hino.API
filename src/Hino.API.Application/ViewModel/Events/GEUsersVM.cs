﻿using Hino.API.Domain.Base.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.ViewModel.Events
{
    public class GEUsersVM: BaseVM
    {
        public string Name { get; set; }
    }
}
