using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Hino.API.Domain.Base.Models;

namespace Hino.API.Application.ViewModel.Events
{
    public class GEEnterpriseEventVM : BaseVM
    {
        public long EventID { get; set; }
        public GEEventsVM GEEvents { get; set; }
        public long EnterpriseID { get; set; }
        public GEEnterprisesVM GEEnterprises { get; set; }
    }
}
