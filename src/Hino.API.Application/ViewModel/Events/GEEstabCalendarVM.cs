using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Hino.API.Domain.Base.Models;

namespace Hino.API.Application.ViewModel.Events
{
    public class GEEstabCalendarVM : BaseVM
    {
        public long EventID { get; set; }
        public GEEventsVM GEEvents { get; set; }
        public long EstabID { get; set; }
    }
}
