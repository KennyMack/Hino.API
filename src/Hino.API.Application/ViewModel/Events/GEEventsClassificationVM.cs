using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Hino.API.Domain.Base.Models;

namespace Hino.API.Application.ViewModel.Events
{
    public class GEEventsClassificationVM : BaseVM
    {
        public GEEventsClassificationVM()
        {
            GEEvents = new HashSet<GEEventsVM>();
        }

        public string Description { get; set; }

        public ICollection<GEEventsVM> GEEvents { get; set; }
    }
}
