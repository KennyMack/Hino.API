﻿using Hino.API.Domain.Base.Models;

namespace Hino.API.Application.ViewModel.Taxes
{
    public class FSNCMVM : BaseVM
    {
        public string Category { get; set; }
        public string NCM { get; set; }
        public string Description { get; set; }
        public decimal IPI { get; set; }
        public decimal AliquotaII { get; set; }
    }
}
