using Hino.API.Domain.Base.Models;

namespace Hino.API.Application.ViewModel.Taxes
{
    public class FSImpostosVM : BaseVM
    {
        public long CodImposto { get; set; }
        public short Classificacao { get; set; }
        public bool Status { get; set; }
    }
}
