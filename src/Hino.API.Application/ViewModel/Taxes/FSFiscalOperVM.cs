﻿using Hino.API.Domain.Base.Models;
using System.Collections.Generic;

namespace Hino.API.Application.ViewModel.Taxes
{
    public class FSFiscalOperVM : BaseVM
    {
        public FSFiscalOperVM()
        {
        }

        public string Description { get; set; }
        public long IdERP { get; set; }
    }
}
