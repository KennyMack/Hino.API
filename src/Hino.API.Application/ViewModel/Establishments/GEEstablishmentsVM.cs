﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Hino.API.Domain.Base.Models;

namespace Hino.API.Application.ViewModel.Establishments
{
    public class GEEstablishmentsVM : BaseVM
    {
        public GEEstablishmentsVM()
        {
            this.GEEstabDevices = new HashSet<GEEstabDevicesVM>();
            this.GEEstabFat = new HashSet<GEEstabFatVM>();
            this.GEEstabPay = new HashSet<GEEstabPayVM>();
            this.GEEstabMenu = new HashSet<GEEstabMenuVM>();
            // this.GEFilesPath = new HashSet<GEFilesPath>();
            // this.GEEstabCalendar = new HashSet<GEEstabCalendar>();
        }

        public string RazaoSocial { get; set; }
        public string NomeFantasia { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string CNPJCPF { get; set; }
        public int Devices { get; set; }
        public decimal PIS { get; set; }
        public decimal COFINS { get; set; }
        public string TokenCNPJ { get; set; }
        public bool AllowEnterprise { get; set; }
        public bool AllowPayment { get; set; }
        public bool AllowChangePrice { get; set; }
        public bool AllowDiscount { get; set; }
        public long? EstabGroup { get; set; }
        public bool FatorR { get; set; }
        public string CapptaKey { get; set; }
        public string SitefIp { get; set; }
        public long? DefaultFiscalOperID { get; set; }
        public virtual GEFiscalOperVM FSFiscalOper { get; set; }
        public string AditionalInfo { get; set; }
        public string DefaultNoteOrder { get; set; }
        public bool OnlyWithStock { get; set; }
        public bool OnlyOnDate { get; set; }
        public long? PfFiscalGroupId { get; set; }
        public GEEnterpriseFiscalGroupVM PfFiscalGroup { get; set; }
        public long? PjFiscalGroupId { get; set; }
        public GEEnterpriseFiscalGroupVM PjFiscalGroup { get; set; }
        public string PfClassifClient { get; set; }
        public string PjClassifClient { get; set; }

        public int DaysPayment { get; set; }

        public ICollection<GEEstabDevicesVM> GEEstabDevices { get; set; }
        public ICollection<GEEstabFatVM> GEEstabFat { get; set; }
        public ICollection<GEEstabPayVM> GEEstabPay { get; set; }
        public ICollection<GEEstabMenuVM> GEEstabMenu { get; set; }
        // public virtual ICollection<GEFilesPath> GEFilesPath { get; set; }
        // public virtual ICollection<GEEstabCalendar> GEEstabCalendar { get; set; }

    }
}
