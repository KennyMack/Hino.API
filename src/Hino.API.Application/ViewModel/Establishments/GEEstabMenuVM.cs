using Hino.API.Domain.Base.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Hino.API.Application.ViewModel.Establishments
{
    public class GEEstabMenuVM : BaseVM
    {
        public short Menu { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Visible { get; set; }
        public long GEEstabID { get; set; }

        [JsonIgnore]
        public GEEstablishmentsVM GEEstablishments { get; set; }
    }
}
