using Hino.API.Domain.Base.Models;
using System;

namespace Hino.API.Application.ViewModel.Establishments
{
    public class GEEstabFatVM : BaseVM
    {
        public long EstabId { get; set; }
        public DateTime Period { get; set; }
        public decimal Value { get; set; }
    }
}
