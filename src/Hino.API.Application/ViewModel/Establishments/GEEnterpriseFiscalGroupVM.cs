﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.ViewModel.Establishments
{
    public class GEEnterpriseFiscalGroupVM: BaseVM
    {
        public string Description { get; set; }
    }
}
