﻿using Hino.API.Domain.Base.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Hino.API.Application.ViewModel.Establishments
{
    public class GEEstabDevicesVM : BaseVM
    {
        public string UserKey { get; set; }
        public string NickName { get; set; }
        public long GEEstabID { get; set; }

        [JsonIgnore]
        public GEEstablishmentsVM GEEstablishments { get; set; }
    }
}
