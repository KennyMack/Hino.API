﻿using Hino.API.Domain.Establishments.Models;
using Hino.API.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.ViewModel.Establishments
{
    public class GEEstablishmentsCreateVM
    {
        public GEEstablishmentsCreateVM()
        {
            GEEstabMenu = new HashSet<GEEstabMenuVM>();
        }

        [RequiredField]
        [Max60LengthField]
        [DisplayField]
        public string RazaoSocial { get; set; }
        [RequiredField]
        [Max60LengthField]
        [DisplayField]
        public string NomeFantasia { get; set; }
        [RequiredField]
        [Max120LengthField]
        [EmailAddress(
            ErrorMessageResourceName = "InvalidEmail",
            ErrorMessageResourceType = typeof(Infra.Cross.Resources.ValidationMessagesResource))]
        [DisplayField]
        public string Email { get; set; }
        [RequiredField]
        [Max20LengthField]
        [Phone(
            ErrorMessageResourceName = "PhoneInvalid",
            ErrorMessageResourceType = typeof(Infra.Cross.Resources.ValidationMessagesResource))]
        [DisplayField]
        public string Phone { get; set; }
        [RequiredField]
        [Max20LengthField]
        [DisplayField]
        public string CNPJCPF { get; set; }
        [DisplayField]
        [Max255LengthField]
        public string TokenCNPJ { get; set; }
        [RequiredField]
        [IntegerRangeField(1, 999)]
        [DisplayField]
        public int Devices { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal PIS { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal COFINS { get; set; }
        public long Id { get; set; }
        public string EstablishmentKey { get; set; }
        public string UniqueKey { get; set; }
        public bool IsActive { get; set; }
        [DisplayField]
        [RequiredField]
        public bool AllowEnterprise { get; set; }
        [DisplayField]
        [RequiredField]
        public bool AllowPayment { get; set; }
        [DisplayField]
        [RequiredField]
        public bool AllowChangePrice { get; set; }
        [DisplayField]
        [RequiredField]
        public bool AllowDiscount { get; set; }
        [DisplayField]
        public long? EstabGroup { get; set; }
        [DisplayField]
        [RequiredField]
        public bool FatorR { get; set; }
        [DisplayField]
        public string CapptaKey { get; set; }
        [DisplayField]
        public string SitefIp { get; set; }
        [DisplayField]
        public string AditionalInfo { get; set; }
        [DisplayField]
        public string DefaultNoteOrder { get; set; }
        [DisplayField]
        public bool OnlyWithStock { get; set; }
        [DisplayField]
        public bool OnlyOnDate { get; set; }
        [DisplayField]
        public int DaysPayment { get; set; }

        [DisplayField]
        public long? DefaultFiscalOperID { get; set; }
        public GEFiscalOperVM FSFiscalOper { get; set; }

        public long? PfFiscalGroupId { get; set; }
        public GEEnterpriseFiscalGroup PfFiscalGroup { get; set; }

        public long? PjFiscalGroupId { get; set; }
        public GEEnterpriseFiscalGroup PjFiscalGroup { get; set; }

        public string PfClassifClient { get; set; }
        public string PjClassifClient { get; set; }

        public virtual ICollection<GEEstabMenuVM> GEEstabMenu { get; set; }
    }
}
