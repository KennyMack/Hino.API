﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Hino.API.Domain.Base.Models;
using Hino.API.Infra.Cross.Utils.Attributes;

namespace Hino.API.Application.ViewModel.Products
{
    public class GEProductsVM : BaseVM
    {
        public GEProductsVM()
        {
           // this.GEFilesPath = new HashSet<GEFilesPath>();
        }

        public string ProductKey { get; set; }
        public string Image { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public long TypeId { get; set; }
        public GEProductsTypeVM GEProductsType { get; set; }

        public long FamilyId { get; set; }
        public GEProductsFamilyVM GEProductsFamily { get; set; }

        public decimal PercIPI { get; set; }
        public decimal PercMaxDiscount { get; set; }
        public decimal Value { get; set; }
        public decimal StockBalance { get; set; }
        public int Status { get; set; }
        public string NCM { get; set; }

        public long? NCMId { get; set; }
        public GENCMVM FSNCM { get; set; }

        public long? UnitId { get; set; }
        public GEProductsUnitVM GEProductsUnit { get; set; }

        public decimal Weight { get; set; }
        public decimal PackageQTD { get; set; }

        public long? SaleUnitId { get; set; }
        public GEProductsUnitVM GEProductsSaleUnit { get; set; }

        public decimal SaleFactor { get; set; }

        public long? AplicationId { get; set; }
        public GEProductAplicVM GEProductAplic { get; set; }

        public int CodOrigMerc { get; set; }

        public string AditionalInfo { get; set; }

        // public ICollection<GEFilesPath> GEFilesPath { get; set; }
    }
}
