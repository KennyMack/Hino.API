using Hino.API.Domain.Base.Models;
using Hino.API.Domain.Products.Enums;
using Hino.API.Infra.Cross.Utils.Attributes;

namespace Hino.API.Application.ViewModel.Products
{
    public class GEProductAplicVM : BaseVM
    {
        [RequiredField]
        [DisplayField]
        public string Description { get; set; }
        [RequiredField]
        [DisplayField]
        public EClassificationProductAplic Classification { get; set; }
        [RequiredField]
        [DisplayField]
        public long IdERP { get; set; }
    }
}
