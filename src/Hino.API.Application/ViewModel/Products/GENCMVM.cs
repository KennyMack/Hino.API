﻿using Hino.API.Domain.Base.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.ViewModel.Products
{
    public class GENCMVM : BaseVM
    {
        public string NCM { get; set; }
        public string Description { get; set; }
        public decimal IPI { get; set; }
        public decimal AliquotaII { get; set; }
    }
}
