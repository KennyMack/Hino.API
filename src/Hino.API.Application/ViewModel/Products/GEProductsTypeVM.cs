﻿using Hino.API.Domain.Base.Models;

namespace Hino.API.Application.ViewModel.Products
{
    public class GEProductsTypeVM : BaseVM
    {
        public string Description { get; set; }
        public string TypeProd { get; set; }
        public int? CodAnexoSimples { get; set; }
    }
}
