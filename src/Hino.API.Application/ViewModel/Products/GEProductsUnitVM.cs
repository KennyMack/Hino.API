﻿using Hino.API.Domain.Base.Models;

namespace Hino.API.Application.ViewModel.Products
{
    public class GEProductsUnitVM : BaseVM
    {
        public string Unit { get; set; }
        public string Description { get; set; }
    }
}
