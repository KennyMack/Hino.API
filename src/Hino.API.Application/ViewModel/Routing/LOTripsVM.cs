﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.ViewModel.Routing
{
    public class LOTripsVM : BaseVM
    {
        public long RouteID { get; set; }
        public LORoutesVM LORoutes { get; set; }

        public decimal Lat { get; set; }
        public decimal Lng { get; set; }
    }
}
