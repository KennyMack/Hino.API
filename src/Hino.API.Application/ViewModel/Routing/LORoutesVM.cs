﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.ViewModel.Routing
{
    public class LORoutesVM : BaseVM
    {
        public LORoutesVM()
        {
            this.LOTrips = new HashSet<LOTripsVM>();
            this.LOWaypoints = new HashSet<LOWaypointsVM>();
        }
        public decimal Weigth { get; set; }
        public decimal Duration { get; set; }
        public decimal Distance { get; set; }
        public string Note { get; set; }


        public ICollection<LOTripsVM> LOTrips { get; set; }
        public ICollection<LOWaypointsVM> LOWaypoints { get; set; }
    }
}
