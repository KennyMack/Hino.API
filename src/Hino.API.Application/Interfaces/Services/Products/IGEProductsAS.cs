﻿using System;
using Hino.API.Domain.Products.Models;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hino.API.Application.ViewModel.Products;

namespace Hino.API.Application.Interfaces.Services.Products
{
    public interface IGEProductsAS : IBaseAppService<GEProducts>
    {
        Task<bool> ExistsProductKeyAsync(string pEstablishmentKey, string pProductKey);
        Task<GEProductsVM> CreateAsync(GEProductsVM pProduct);
        Task<GEProductsVM> ChangeAsync(GEProductsVM pProduct);
    }
}
