﻿using Hino.API.Domain.Products.Models;

namespace Hino.API.Application.Interfaces.Services.Products
{
    public interface IGEProductsTypeAS : IBaseAppService<GEProductsType>
    {
    }
}
