﻿using System;
using Hino.API.Domain.Products.Models;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.Interfaces.Services.Products
{
    public interface IGEProductsFamilyAS : IBaseAppService<GEProductsFamily>
    {
    }
}
