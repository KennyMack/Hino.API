﻿using Hino.API.Application.Integrations.Locations.Models;
using Hino.API.Domain.Routing.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.Interfaces.Services.Routing
{
    public interface ILOWaypointsAS : IBaseAppService<LOWaypoints>
    {
    }
}
