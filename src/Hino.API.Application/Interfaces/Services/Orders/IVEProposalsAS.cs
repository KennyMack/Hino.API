﻿using Hino.API.Application.ViewModel.Orders;
using Hino.API.Domain.Orders.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.Interfaces.Services.Orders
{
    public interface IVEProposalsAS : IBaseAppService<VEOrders>
    {
        Task<VEOrdersVM> CanConvertToOrderAsync(string pEstablishmentKey, long id);
        Task<VEOrdersVM> CreateOrderAsync(VEOrdersVM pOrder);
        Task<VEOrdersVM> UpdateOrderAsync(VEOrdersVM pOrder);
        Task<VEOrdersVM> RemoveByIdAsync(long id, string pEstablishmentKey, string pUniqueKey);
    }
}
