﻿using Hino.API.Application.ViewModel.Orders;
using Hino.API.Domain.Orders.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.Interfaces.Services.Orders
{
    public interface IVETransactionsAS : IBaseAppService<VETransactions>
    {
        Task<VETransactionsVM> GetByAuthCode(string pEstablishmentKey, string pAuthCode);
        Task<VETransactionsVM> GetByNsuHost(string pEstablishmentKey, string pTerminalId, string pNsuHost);
        Task<VETransactionsVM> SearchTransationByNSUAsync(string pEstablishmentKey, string NSU);
        Task<VETransactionsVM> CreateTransaction(VETransactionsVM pTransaction);
        Task<VETransactionsVM> UpdateTransaction(VETransactionsVM pTransaction);
    }
}
