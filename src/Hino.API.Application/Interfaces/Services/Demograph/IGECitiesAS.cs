﻿using Hino.API.Domain.Demograph.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.Interfaces.Services.Demograph
{
    public interface IGECitiesAS : IBaseAppService<GECities>
    {
    }
}
