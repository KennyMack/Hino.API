﻿using Hino.API.Application.ViewModel.Demograph;
using Hino.API.Domain.Demograph.Models;
using System.Threading.Tasks;

namespace Hino.API.Application.Interfaces.Services.Demograph
{
    public interface IGECountriesAS : IBaseAppService<GECountries>
    {
        Task LoadCountriesAsync(CountriesLoad[] pLoad);
        GECountriesVM GetByInitials(string pInitial);
    }
}
