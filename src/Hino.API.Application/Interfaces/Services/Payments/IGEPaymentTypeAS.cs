﻿using Hino.API.Domain.Payments.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.Interfaces.Services.Payments
{
    public interface IGEPaymentTypeAS : IBaseAppService<GEPaymentType>
    {
    }
}
