﻿using System;
using Hino.API.Domain.SalePrices.Models;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hino.API.Infra.Cross.Utils.Paging;
using Hino.API.Application.ViewModel.SalePrices;

namespace Hino.API.Application.Interfaces.Services.SalePrices
{
    public interface IVESalePriceAS : IBaseAppService<VESalePrice>
    {
        Task<PagedResult<VESalePriceVM>> GetAllSalePrice(int pageNumber, int limit, string pEstablishmentKey, long pRegionId, long pPaymentConditionId, long pUserId, string filter);
        Task<PagedResult<VESalePriceVM>> GetProductSalePrice(string pEstablishmentKey, long pProdutctId, long pRegionId, long pPaymentConditionId, long pUserId);
        Task<VESalePriceVM> CreateSalePrice(VESalePriceVM pSalePrice);
        Task<VESalePriceVM> UpdateSalePrice(VESalePriceVM pSalePrice);
    }
}
