﻿using System;
using Hino.API.Domain.SalePrices.Models;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.Interfaces.Services.SalePrices
{
    public interface IVERegionSaleUFAS : IBaseAppService<VERegionSaleUF>
    {
    }
}
