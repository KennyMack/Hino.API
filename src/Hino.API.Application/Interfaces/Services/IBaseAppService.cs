﻿using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Base.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using Hino.API.Infra.Cross.Utils.Paging;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hino.API.Application.Interfaces.Services
{
    public interface IBaseAppService<T> : IErrorBaseService where T : BaseEntity
    {
        bool DontSendToQueue { get; set; }
        Task<IEnumerable<T>> SyncData(string pEstablishmentKey, T[] pRegisters);
        T Add(T model);
        Task<PagedResult<T>> GetAllPagedAsync(int page, int pageSize, params Expression<Func<T, object>>[] includeProperties);
        T GetById(long id, string pEstablishmentKey, string pUniqueKey, params Expression<Func<T, object>>[] includeProperties);
        T GetByIdToUpdate(long id, string pEstablishmentKey, string pUniqueKey);
        Task<T> GetByIdAsync(long id, string pEstablishmentKey, string pUniqueKey, params Expression<Func<T, object>>[] includeProperties);
        Task<IEnumerable<T>> GetAllAsync(params Expression<Func<T, object>>[] includeProperties);
        Task<T> FirstOrDefaultAsync(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties);
        Task<IEnumerable<T>> QueryAsync(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties);
        Task<PagedResult<T>> QueryPagedAsync(int page, int pageSize, Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties);
        T Update(T model);
        void Remove(T model);
        Task<T> RemoveById(long id, string pEstablishmentKey, string pUniqueKey);
        long NextSequence();
        Task<long> NextSequenceAsync();
        Task<int> SaveChanges();
        void RollBackChanges();
        void Dispose();
    }
}
