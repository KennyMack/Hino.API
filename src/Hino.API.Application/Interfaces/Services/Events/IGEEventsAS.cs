﻿using Hino.API.Application.ViewModel.Events;
using Hino.API.Domain.Events.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.Interfaces.Services.Events
{
    public interface IGEEventsAS : IBaseAppService<GEEvents>
    {
        Task<GEEventsVM> SaveReferenceAsync(string pEstablishmentKey, GEEventsVM Event, GECreateEventsVM pGEEventsVM);
        Task<GEEventsVM> UpdateEventAsync(string pEstablishmentKey, GECreateEventsVM pGEEventsVM);
        Task<GEEventsVM> CreateEventAsync(string pEstablishmentKey, GECreateEventsVM pGEEventsVM);
        Task<GEEventsVM> RemoveEventIdAsync(long pId, string pEstablishmentKey, string pUniqueKey);
        Task<GEEventsVM> ChangeEventStatusAsync(long pId, string pEstablishmentKey, string pUniqueKey, GEEventsStatusVM pEventStatus);
        Task<GEEventsVM> GenerateEventChildAsync(long pId, string pEstablishmentKey, string pUniqueKey, GEEventsChildVM pEventsChild);
    }
}
