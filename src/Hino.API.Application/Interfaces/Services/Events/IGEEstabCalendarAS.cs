﻿using Hino.API.Domain.Events.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.Interfaces.Services.Events
{
    public interface IGEEstabCalendarAS : IBaseAppService<GEEstabCalendar>
    {
    }
}
