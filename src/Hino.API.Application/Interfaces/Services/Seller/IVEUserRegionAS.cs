﻿using Hino.API.Application.ViewModel.Seller;
using Hino.API.Domain.Seller.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.Interfaces.Services.Seller
{
    public interface IVEUserRegionAS : IBaseAppService<VEUserRegion>
    {
        bool ZipCodeExists(string pEstablishmentKey, long pUserId, string pZipCode);
        Task<VEUserRegionVM> CreateOrUpdateAsync(VEUserRegion userRegion);
        Task<VEUserRegionVM[]> CreateOrUpdateListAsync(string pEstablishmentKey, VEUserRegionVM[] userRegion);
        Task<VEUserRegionVM[]> RemoveListAsync(string pEstablishmentKey, VEUserRegionVM[] userRegion);
    }
}
