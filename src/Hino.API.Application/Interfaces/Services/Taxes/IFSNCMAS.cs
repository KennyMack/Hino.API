﻿using Hino.API.Domain.Taxes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.Interfaces.Services.Taxes
{
    public interface IFSNCMAS : IBaseAppService<FSNCM>
    {
    }
}
