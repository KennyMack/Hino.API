﻿using Hino.API.Domain.Establishments.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.Interfaces.Services.Establishments
{
    public interface IGEEstabDevicesAS : IBaseAppService<GEEstabDevices>
    {
    }
}
