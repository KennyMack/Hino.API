﻿using Hino.API.Application.ViewModel.Establishments;
using Hino.API.Domain.Establishments.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.Interfaces.Services.Establishments
{
    public interface IGEEstablishmentsAS : IBaseAppService<GEEstablishments>
    {
        Task<bool> HasEstablishmentDependencyAsync(string pEstablishmentKey);
        Task<IEnumerable<GEEstablishments>> GetByIdAndEstablishmentKeyAsync(long pId, string pEstablishmentKey);
        Task<GEEstablishments> UpdateEstablishmentAsync(GEEstablishmentsCreateVM pEstab);
        Task<GEEstablishments> CreateEstablishmentAsync(GEEstablishmentsCreateVM pEstab);
        Task<bool> ExistsEstablishmentAsync(string pEstablishmentKey);
        Task<IEnumerable<GEEstabDevices>> GetDevicesEstablishment(string pEstablishmentKey, string pUniqueKey, long pId);
        Task<GEEstablishments> GetByEstablishmentKeyAsync(string pEstablishmentKey);
    }
}
