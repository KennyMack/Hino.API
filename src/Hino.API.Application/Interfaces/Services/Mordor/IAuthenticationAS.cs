﻿using Hino.API.Application.ViewModel.Mordor;
using Hino.API.Domain.Mordor.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using Hino.API.Domain.Base.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.Interfaces.Services.Mordor
{
    public interface IAuthenticationAS: IErrorBaseService
    {
        Task<SignInResult> SignInWithEmailAsync(SignInUserVM pUser);
        Task<SignUpResult> CreateUserAsync(CreateUserVM pUser);
    }
}
