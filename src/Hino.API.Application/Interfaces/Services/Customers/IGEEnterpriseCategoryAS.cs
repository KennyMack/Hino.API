﻿using System;
using Hino.API.Domain.Customers.Models;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.Interfaces.Services.Customers
{
    public interface IGEEnterpriseCategoryAS : IBaseAppService<GEEnterpriseCategory>
    {
    }
}
