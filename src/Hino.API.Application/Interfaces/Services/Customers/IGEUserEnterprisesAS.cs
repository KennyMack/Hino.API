﻿using System;
using System.Collections.Generic;
using Hino.API.Domain.Customers.Models;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hino.API.Infra.Cross.Utils.Paging;
using System.Linq.Expressions;
using Hino.API.Application.ViewModel.Customers;

namespace Hino.API.Application.Interfaces.Services.Customers
{
    public interface IGEUserEnterprisesAS : IBaseAppService<GEUserEnterprises>
    {
        Task<PagedResult<GEUserEnterprises>> QuerySearchPagedAsync(int page, int pageSize,
            Expression<Func<GEUserEnterprises, bool>> predicate,
            params Expression<Func<GEUserEnterprises, object>>[] includeProperties);
        Task<GEUserEnterprisesVM> CreateOrUpdateAsync(GEUserEnterprisesVM enterprise);
        Task<GEUserEnterprisesVM[]> CreateOrUpdateListAsync(string pEstablishmentKey, GEUserEnterprisesVM[] enterprise);
        Task<GEUserEnterprisesVM[]> RemoveListAsync(string pEstablishmentKey, GEUserEnterprisesVM[] enterprise);
    }
}
