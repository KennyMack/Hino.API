﻿using System;
using System.Collections.Generic;
using Hino.API.Domain.Customers.Models;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.Interfaces.Services.Customers
{
    public interface IGEEnterpriseGroupAS : IBaseAppService<GEEnterpriseGroup>
    {
    }
}
