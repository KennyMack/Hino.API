﻿using System;
using System.Collections.Generic;
using Hino.API.Domain.Customers.Models;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hino.API.Application.ViewModel.Customers;
using Hino.API.Domain.Customers.Enums;
using Hino.API.Application.Integrations.Enterprises.Models;

namespace Hino.API.Application.Interfaces.Services.Customers
{
    public interface IGEEnterprisesAS : IBaseAppService<GEEnterprises>
    {
        Task UpdateEnterpriseRegionIdAsync(string pEstablishmentKey, string pUF, long pRegionId);
        Task<GEEnterprisesVM> UpdateToClient(string pEstablishmentKey, string pUniqueKey, long pId);
        Task<GEEnterprisesVM> CreateAsync(GEEnterprisesVM model);
        Task<GEEnterprisesVM> ChangeAsync(GEEnterprisesVM model);
        Task<bool> EnterpriseExists(string pEstablishmentKey, string pCNPJ, EEnterpriseClassification classification, long idActual);
        Task UpdateEnterpriseRegionId(string pEstablishmentKey, string pUF, long pRegionId);
    }
}
