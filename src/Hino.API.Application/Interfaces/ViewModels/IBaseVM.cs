﻿using System;

namespace Hino.API.Application.Interfaces.ViewModels
{
    public interface IBaseVM
    {
        long Id { get; set; }
        string EstablishmentKey { get; set; }
        string UniqueKey { get; set; }
        DateTime Created { get; set; }
        DateTime Modified { get; set; }
        bool IsActive { get; set; }
    }
}
