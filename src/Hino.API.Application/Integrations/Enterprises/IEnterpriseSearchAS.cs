﻿using Hino.API.Application.Integrations.Enterprises.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.Integrations.Enterprises
{
    public interface IEnterpriseSearchAS
    {
        List<ModelException> Errors { get; set; }
        Task<IReceitaWSEnterpriseVM> GetEnterpriseAsync(string pCNPJ, string pTokenCNPJ);
    }
}
