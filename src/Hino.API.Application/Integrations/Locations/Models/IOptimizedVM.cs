﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.Integrations.Locations.Models
{
    public interface IOptimizedVM
    {
        string EstablishmentKey { get; set; }
        IList<ILocationVM> Location { get; set; }
        string ToString();
        string GetRadiuses();
        List<ILocationVM> GetDestinations();
    }

    public interface ILocationVM
    {
        public string Address { get; set; }
        public string Name { get; set; }
        public decimal Lng { get; set; }
        public decimal Lat { get; set; }
        string ToDestination();
        string ToStart();
        string ToEnd();
    }
}
