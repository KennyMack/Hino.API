﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.Integrations.Locations.Models
{
    public interface IFindSequenceResultVM
    {
        decimal Weigth { get; set; }
        decimal Duration { get; set; }
        decimal Distance { get; set; }
        string Note { get; set; }

        public IList<IFindSequenceLocationVM> Waypoints { get; set; }
    }

    public interface IFindSequenceLocationVM
    {
        string Name { get; set; }
        string Address { get; set; }
        decimal Lat { get; set; }
        decimal Lng { get; set; }
        int Sequence { get; set; }
        string AddressFrom { get; set; }
        string AddressTo { get; set; }
        decimal Distance { get; set; }
        decimal Time { get; set; }
        decimal Rest { get; set; }
        decimal Waiting { get; set; }
    }
}
