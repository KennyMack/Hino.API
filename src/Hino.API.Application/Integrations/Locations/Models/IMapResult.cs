﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.Integrations.Locations.Models
{
    public interface IMapResult
    {
        ICepAddress Address { get; set; }
        IMapPosition Position { get; set; }
        IMapView MapView { get; set; }
        string ReferenceId { get; set; }
    }

    public interface IMapPosition
    {
        float Lat { get; set; }
        float Lng { get; set; }
    }

    public interface IMapView
    {
        float West { get; set; }
        float South { get; set; }
        float East { get; set; }
        float North { get; set; }
    }
}
