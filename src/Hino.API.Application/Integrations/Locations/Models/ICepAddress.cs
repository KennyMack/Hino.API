﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.Integrations.Locations.Models
{
    public interface ICepAddress
    {
        string Label { get; set; }
        string Country { get; set; }
        string CountryCode { get; set; }
        string CountryName { get; set; }
        string State { get; set; }
        string StateName { get; set; }
        string City { get; set; }
        string ZipCode { get; set; }
        string District { get; set; }
        string Street { get; set; }
        string Number { get; set; }
        string Complement { get; set; }

        string ToString();
        string ToAddress();
    }
}
