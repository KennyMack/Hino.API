﻿using Hino.API.Application.ViewModel.Demograph;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.Integrations.Locations.Models
{
    public interface ICepVM
    {
        string cep { get; set; }
        string logradouro { get; set; }
        string complemento { get; set; }
        string bairro { get; set; }
        string localidade { get; set; }
        string uf { get; set; }
        string unidade { get; set; }
        string ibge { get; set; }
        string gia { get; set; }
        
        ICepAddress CEPAddress { get; set; }
        GEStatesVM State { get; set; }
        GECitiesVM City { get; set; }
        GECountriesVM Country { get; set; }

        IMapResult MapResult { get; set; }
    }
}
