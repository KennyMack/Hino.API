﻿using Hino.API.Application.Integrations.Locations.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.Integrations.Locations
{
    public interface IMapAS
    {
        List<ModelException> Errors { get; set; }
        Task<IMapResult> GetAddressAsync(ICepAddress pAddress);
        Task<IFindSequenceResultVM> FindSequenceAsync(IOptimizedVM pLocations);
    }
}
