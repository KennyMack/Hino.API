﻿using Hino.API.Application.Integrations.Enterprises.Models;
using Hino.API.Application.Integrations.Locations.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.Integrations.Locations
{
    public interface ICepAS
    {
        List<ModelException> Errors { get; set; }
        Task<ICepVM> GetCEPAsync(string pCEP);
        Task<ICepVM> GetCEPAddressAsync(string pCEP, string pNumber, string pCountry, bool pAllInfo);
        Task<ICepVM> GetCEPAddressEnterpriseAsync(IReceitaWSEnterpriseVM pEnterprise);
    }
}
