﻿using System;
using System.Collections.Generic;
using AutoMapper;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hino.API.Application.ViewModel.Demograph;
using Hino.API.Domain.Demograph.Models;
using Hino.API.Infra.Cross.Utils.Paging;

namespace Hino.API.Application.MapperModels.Profiles
{
    public class ViewToDomainDemographProfile : Profile
    {
        public ViewToDomainDemographProfile()
        {
            CreateMap<GECitiesVM, GECities>();
            CreateMap<GECountriesVM, GECountries>();
            CreateMap<GEStatesVM, GEStates>();

            CreateMap<PagedResult<GECitiesVM>, PagedResult<GECities>>();
            CreateMap<PagedResult<GECountriesVM>, PagedResult<GECountries>>();
            CreateMap<PagedResult<GEStatesVM>, PagedResult<GEStates>>();
        }
    }
}
