﻿using AutoMapper;
using Hino.API.Application.ViewModel.SalePrices;
using Hino.API.Domain.SalePrices.Models;
using System.Collections.Generic;

namespace Hino.API.Application.MapperModels.Profiles
{
    public class DomainToViewSalePriceProfile: Profile
    {
        public DomainToViewSalePriceProfile()
        {
            CreateMap<VEProducts, VEProductsVM>();
            CreateMap<VERegionSaleUF, VERegionSaleUFVM>();
            CreateMap<VERegionSale, VERegionSaleVM>();
            CreateMap<VESalePrice, VESalePriceVM>();
            CreateMap<VESaleWorkRegion, VESaleWorkRegionVM>();

            // CreateMap<List<VESalePrice>, List<VESalePriceVM>>();
            
        }
    }
}
