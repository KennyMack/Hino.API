﻿using AutoMapper;
using Hino.API.Application.ViewModel.Events;
using Hino.API.Domain.Events.Models;
using Hino.API.Infra.Cross.Utils.Paging;

namespace Hino.API.Application.MapperModels.Profiles
{
    public class DomainToViewEventsProfile: Profile
    {
        public DomainToViewEventsProfile()
        {
            CreateMap<GECreateEvents, GECreateEventsVM>();
            CreateMap<GEEnterpriseEvent, GEEnterpriseEventVM>();
            CreateMap<GEEstabCalendar, GEEstabCalendarVM>();
            CreateMap<GEEventsChild, GEEventsChildVM>();
            CreateMap<GEEventsClassification, GEEventsClassificationVM>();
            CreateMap<GEEventsStatus, GEEventsStatusVM>();
            CreateMap<GEEvents, GEEventsVM>();
            CreateMap<GEUserCalendar, GEUserCalendarVM>();

            CreateMap<PagedResult<GEEvents>, PagedResult<GEEventsVM>>();
        }
    }
}
