﻿using AutoMapper;
using Hino.API.Application.ViewModel.Seller;
using Hino.API.Domain.Seller.Models;
using Hino.API.Infra.Cross.Utils.Paging;

namespace Hino.API.Application.MapperModels.Profiles
{
    public class ViewToDomainSellerProfile : Profile
    {
        public ViewToDomainSellerProfile()
        {
            CreateMap<VESaleWorkRegionVM, VESaleWorkRegion>();
            CreateMap<VESellerVM, VESeller>();
            CreateMap<VEUserRegionVM, VEUserRegion>();
            CreateMap<PagedResult<VEUserRegionVM>, PagedResult<VEUserRegion>>();
            CreateMap<VEUserRegionVM[], VEUserRegion[]>();
        }
    }
}
