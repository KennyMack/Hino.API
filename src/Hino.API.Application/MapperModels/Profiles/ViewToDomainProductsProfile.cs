﻿using AutoMapper;
using Hino.API.Application.ViewModel.Products;
using Hino.API.Domain.Products.Models;
using Hino.API.Infra.Cross.Utils.Paging;

namespace Hino.API.Application.MapperModels.Profiles
{
    public class ViewToDomainProductsProfile : Profile
    {
        public ViewToDomainProductsProfile()
        {
            CreateMap<GENCMVM, GENCM>();
            CreateMap<GEProductAplicVM, GEProductAplic>();
            CreateMap<GEProductsFamilyVM, GEProductsFamily>();
            CreateMap<GEProductsTypeVM, GEProductsType>();
            CreateMap<GEProductsUnitVM, GEProductsUnit>();
            CreateMap<GEProductsVM, GEProducts>();

            CreateMap<PagedResult<GENCMVM>, PagedResult<GENCM>>();
            CreateMap<PagedResult<GEProductAplicVM>, PagedResult<GEProductAplic>>();
            CreateMap<PagedResult<GEProductsFamilyVM>, PagedResult<GEProductsFamily>>();
            CreateMap<PagedResult<GEProductsTypeVM>, PagedResult<GEProductsType>>();
            CreateMap<PagedResult<GEProductsUnitVM>, PagedResult<GEProductsUnit>>();
            CreateMap<PagedResult<GEProductsVM>, PagedResult<GEProducts>>();
        }
    }
}
