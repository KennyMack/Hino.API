﻿using System;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hino.API.Domain.Customers.Models;
using Hino.API.Application.ViewModel.Customers;
using Hino.API.Infra.Cross.Utils.Paging;

namespace Hino.API.Application.MapperModels.Profiles
{
    public class ViewToDomainCustomersProfile: Profile
    {
        public ViewToDomainCustomersProfile()
        {
            CreateMap<GEEnterpriseCategoryVM, GEEnterpriseCategory>();
            CreateMap<GEEnterpriseContactsVM, GEEnterpriseContacts>();
            CreateMap<GEEnterpriseFiscalGroupVM, GEEnterpriseFiscalGroup>();
            CreateMap<GEEnterpriseGeoVM, GEEnterpriseGeo>();
            CreateMap<GEEnterpriseGroupVM, GEEnterpriseGroup>();
            CreateMap<GEEnterprisesVM, GEEnterprises>();
            CreateMap<GEPaymentConditionVM, GEPaymentCondition>();
            CreateMap<GEPaymentTypeVM, GEPaymentType>();
            CreateMap<GEUserEnterprisesVM, GEUserEnterprises>();
            CreateMap<GEUserEnterprisesVM[], GEUserEnterprises[]>();

            CreateMap<PagedResult<GEEnterpriseCategoryVM>, PagedResult<GEEnterpriseCategory>>();
            CreateMap<PagedResult<GEEnterpriseContactsVM>, PagedResult<GEEnterpriseContacts>>();
            CreateMap<PagedResult<GEEnterpriseFiscalGroupVM>, PagedResult<GEEnterpriseFiscalGroup>>();
            CreateMap<PagedResult<GEEnterpriseGeoVM>, PagedResult<GEEnterpriseGeo>>();
            CreateMap<PagedResult<GEEnterpriseGroupVM>, PagedResult<GEEnterpriseGroup>>();
            CreateMap<PagedResult<GEEnterprisesVM>, PagedResult<GEEnterprises>>();
            CreateMap<PagedResult<GEPaymentConditionVM>, PagedResult<GEPaymentCondition>>();
            CreateMap<PagedResult<GEPaymentTypeVM>, PagedResult<GEPaymentType>>();
            CreateMap<PagedResult<GEUserEnterprisesVM>, PagedResult<GEUserEnterprises>>();
            // CreateMap<CreateCustomerVM, Customers>()
            //     .ConstructUsing(c => new Customers(c.Id, c.Name, c.CNPJ, c.Email, c.Active));
        }
    }
}
