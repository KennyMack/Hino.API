﻿using AutoMapper;
using Hino.API.Application.ViewModel.Orders;
using Hino.API.Domain.Orders.Models;

namespace Hino.API.Application.MapperModels.Profiles
{
    public class ViewToDomainOrdersProfile : Profile
    {
        public ViewToDomainOrdersProfile()
        {
            CreateMap<VECustomerVM, VECustomer>();
            CreateMap<VECarrierVM, VECarrier>();
            CreateMap<VEFiscalOperVM, VEFiscalOper>();
            CreateMap<VEOrderItemsVM, VEOrderItems>();
            CreateMap<VEOrderStatusVM, VEOrderStatus>();
            CreateMap<VEOrdersVM, VEOrders>();
            CreateMap<VEOrderTaxesVM, VEOrderTaxes>();
            CreateMap<VEPaymentConditionVM, VEPaymentCondition>();
            CreateMap<VEPaymentTypeVM, VEPaymentType>();
            CreateMap<VEProductsVM, VEProducts>();
            CreateMap<VESalePriceVM, VESalePrice>();
            CreateMap<VECustomerVM, VECustomer>();
            CreateMap<VESallesmanVM, VESallesman>();
            CreateMap<VETransactionsVM, VETransactions>();
        }
    }
}
