﻿using AutoMapper;
using Hino.API.Application.ViewModel.Payments;
using Hino.API.Domain.Payments.Models;

namespace Hino.API.Application.MapperModels.Profiles
{
    public class ViewToDomainPaymentsProfile : Profile
    {
        public ViewToDomainPaymentsProfile()
        {
            CreateMap<GEPaymentCondInstallmentsVM, GEPaymentCondInstallments>();
            CreateMap<GEPaymentConditionVM, GEPaymentCondition>();
            CreateMap<GEPaymentTypeVM, GEPaymentType>();
            CreateMap<GESellerVM, GESeller>();
            CreateMap<GEUserPayTypeVM, GEUserPayType>();
        }
    }
}
