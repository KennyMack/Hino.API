﻿using AutoMapper;
using Hino.API.Application.ViewModel.Taxes;
using Hino.API.Domain.Taxes.Models;
using Hino.API.Infra.Cross.Utils.Paging;

namespace Hino.API.Application.MapperModels.Profiles
{
    public class DomainToViewTaxesProfile: Profile
    {
        public DomainToViewTaxesProfile()
        {
            CreateMap<FSFiscalOper, FSFiscalOperVM>();
            CreateMap<FSImpostos, FSImpostosVM>();
            CreateMap<FSNCM, FSNCMVM>();
            CreateMap<PagedResult<FSNCM>, PagedResult<FSNCMVM>>();
        }
    }
}
