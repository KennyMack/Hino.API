﻿using AutoMapper;
using Hino.API.Application.ViewModel.Products;
using Hino.API.Domain.Products.Models;
using Hino.API.Infra.Cross.Utils.Paging;

namespace Hino.API.Application.MapperModels.Profiles
{
    public class DomainToViewProductsProfile: Profile
    {
        public DomainToViewProductsProfile()
        {
            CreateMap<GENCM, GENCMVM>();
            CreateMap<GEProductAplic, GEProductAplicVM>();
            CreateMap<GEProductsFamily, GEProductsFamilyVM>();
            CreateMap<GEProductsType, GEProductsTypeVM>();
            CreateMap<GEProductsUnit, GEProductsUnitVM>();
            CreateMap<GEProducts, GEProductsVM>();

            CreateMap<PagedResult<GENCM>, PagedResult<GENCMVM>>();
            CreateMap<PagedResult<GEProductAplic>, PagedResult<GEProductAplicVM>>();
            CreateMap<PagedResult<GEProductsFamily>, PagedResult<GEProductsFamilyVM>>();
            CreateMap<PagedResult<GEProductsType>, PagedResult<GEProductsTypeVM>>();
            CreateMap<PagedResult<GEProductsUnit>, PagedResult<GEProductsUnitVM>>();
            CreateMap<PagedResult<GEProducts>, PagedResult<GEProductsVM>>();
        }
    }
}
