﻿using AutoMapper;
using Hino.API.Application.ViewModel.Routing;
using Hino.API.Domain.Routing.Models;

namespace Hino.API.Application.MapperModels.Profiles
{
    public class DomainToViewRoutingProfile: Profile
    {
        public DomainToViewRoutingProfile()
        {
            CreateMap<LORoutes, LORoutesVM>();
            CreateMap<LOTrips, LOTripsVM>();
            CreateMap<LOWaypoints, LOWaypointsVM>();
        }
    }
}
