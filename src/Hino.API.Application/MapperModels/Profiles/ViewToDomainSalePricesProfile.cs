﻿using AutoMapper;
using Hino.API.Application.ViewModel.SalePrices;
using Hino.API.Domain.SalePrices.Models;
using System.Collections.Generic;

namespace Hino.API.Application.MapperModels.Profiles
{
    public class ViewToDomainSalePricesProfile : Profile
    {
        public ViewToDomainSalePricesProfile()
        {
            CreateMap<VEProductsVM, VEProducts>();
            CreateMap<VERegionSaleUFVM, VERegionSaleUF>();
            CreateMap<VERegionSaleVM, VERegionSale>();
            CreateMap<VESalePriceVM, VESalePrice>();
            CreateMap<VESaleWorkRegionVM, VESaleWorkRegion>();
           // CreateMap<List<VESalePriceVM>, List<VESalePrice>>();
        }
    }
}
