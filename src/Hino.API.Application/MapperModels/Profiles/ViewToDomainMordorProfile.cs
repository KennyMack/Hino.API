﻿using AutoMapper;
using Hino.API.Application.ViewModel.Mordor;
using Hino.API.Domain.Mordor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.MapperModels.Profiles
{
    public class ViewToDomainMordorProfile : Profile
    {
        public ViewToDomainMordorProfile()
        {
            CreateMap<UserVM, GEUsers>()
                .ConstructUsing(c => new GEUsers(
                    c.Id,
                    c.EstablishmentKey,
                    c.UniqueKey,
                    c.Uid,
                    c.UserName,
                    c.Email,
                    c.Password,
                    c.UserType,
                    c.PercDiscount,
                    c.PercCommission,
                    c.PercDiscountPrice,
                    c.PercIncreasePrice,
                    c.StoreId,
                    c.TerminalId,
                    c.UserKey,
                    c.IsBlockedByPay,
                    c.Role
                 ));
            CreateMap<CreateUserVM, GEUsers>()
                .ConstructUsing(c => new GEUsers(
                    c.Id,
                    c.EstablishmentKey,
                    c.UniqueKey,
                    c.Uid,
                    c.UserName,
                    c.Email,
                    c.Password,
                    c.UserType,
                    c.UserKey,
                    c.Role
                 ));
            
            /*
            CreateMap<CreateUserVM, GEUsers>()
                .ConstructUsing(c => new GEUsers(
                    c.Id, c.EstablishmentKey, 
                    c.UniqueKey, c.Uid, c.Username, 
                    c.Email, c.Password, c.Role, c.Active));
            */
        }
    }
}
