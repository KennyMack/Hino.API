﻿using AutoMapper;
using Hino.API.Application.ViewModel.Routing;
using Hino.API.Domain.Routing.Models;

namespace Hino.API.Application.MapperModels.Profiles
{
    public class ViewToDomainRoutingProfile : Profile
    {
        public ViewToDomainRoutingProfile()
        {
            CreateMap<LORoutesVM, LORoutes>();
            CreateMap<LOTripsVM, LOTrips>();
            CreateMap<LOWaypointsVM, LOWaypoints>();
        }
    }
}
