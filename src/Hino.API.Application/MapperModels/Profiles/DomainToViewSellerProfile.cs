﻿using AutoMapper;
using Hino.API.Application.ViewModel.Seller;
using Hino.API.Domain.Seller.Models;
using Hino.API.Infra.Cross.Utils.Paging;

namespace Hino.API.Application.MapperModels.Profiles
{
    public class DomainToViewSellerProfile: Profile
    {
        public DomainToViewSellerProfile()
        {
            CreateMap<VESaleWorkRegion, VESaleWorkRegionVM>();
            CreateMap<VESeller, VESellerVM>();
            CreateMap<VEUserRegion, VEUserRegionVM>();
            CreateMap<PagedResult<VEUserRegion>, PagedResult<VEUserRegionVM>>();
            CreateMap<VEUserRegion[], VEUserRegionVM[]>();
        }
    }
}
