﻿using AutoMapper;
using Hino.API.Application.ViewModel.Customers;
using Hino.API.Domain.Customers.Models;
using Hino.API.Infra.Cross.Utils.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.MapperModels.Profiles
{
    public class DomainToViewCustomersProfile : Profile
    {
        public DomainToViewCustomersProfile()
        {
            CreateMap<GEEnterpriseCategory, GEEnterpriseCategoryVM>();
            CreateMap<GEEnterpriseContacts, GEEnterpriseContactsVM>();
            CreateMap<GEEnterpriseFiscalGroup, GEEnterpriseFiscalGroupVM>();
            CreateMap<GEEnterpriseGeo, GEEnterpriseGeoVM>();
            CreateMap<GEEnterpriseGroup, GEEnterpriseGroupVM>();
            CreateMap<GEEnterprises, GEEnterprisesVM>();
            CreateMap<GEPaymentCondition, GEPaymentConditionVM>();
            CreateMap<GEPaymentType, GEPaymentTypeVM>();
            CreateMap<GEUserEnterprises, GEUserEnterprisesVM>();
            CreateMap<GEUserEnterprises[], GEUserEnterprisesVM[]>();

            CreateMap<PagedResult<GEEnterpriseCategory>, PagedResult<GEEnterpriseCategoryVM>>();
            CreateMap<PagedResult<GEEnterpriseContacts>, PagedResult<GEEnterpriseContactsVM>>();
            CreateMap<PagedResult<GEEnterpriseFiscalGroup>, PagedResult<GEEnterpriseFiscalGroupVM>>();
            CreateMap<PagedResult<GEEnterpriseGeo>, PagedResult<GEEnterpriseGeoVM>>();
            CreateMap<PagedResult<GEEnterpriseGroup>, PagedResult<GEEnterpriseGroupVM>>();
            CreateMap<PagedResult<GEEnterprises>, PagedResult<GEEnterprisesVM>>();
            CreateMap<PagedResult<GEPaymentCondition>, PagedResult<GEPaymentConditionVM>>();
            CreateMap<PagedResult<GEPaymentType>, PagedResult<GEPaymentTypeVM>>();
            CreateMap<PagedResult<GEUserEnterprises>, PagedResult<GEUserEnterprisesVM>>();
        }
    }
}
