﻿using AutoMapper;
using Hino.API.Application.ViewModel.Payments;
using Hino.API.Domain.Payments.Models;
using Hino.API.Infra.Cross.Utils.Paging;

namespace Hino.API.Application.MapperModels.Profiles
{
    public class DomainToViewPaymentsProfile: Profile
    {
        public DomainToViewPaymentsProfile()
        {
            CreateMap<GEPaymentCondInstallments, GEPaymentCondInstallmentsVM>();
            CreateMap<GEPaymentCondition, GEPaymentConditionVM>();
            CreateMap<GEPaymentType, GEPaymentTypeVM>();
            CreateMap<GESeller, GESellerVM>();
            CreateMap<GEUserPayType, GEUserPayTypeVM>();

            CreateMap<GEPaymentCondition, GEPaymentConditionCreateVM>();
            CreateMap<GEPaymentConditionCreateVM, GEPaymentCondition>();

            CreateMap<PagedResult<GEPaymentCondition>, PagedResult<GEPaymentConditionVM>>();
            CreateMap<PagedResult<GEPaymentType>, PagedResult<GEPaymentTypeVM>>();
        }
    }
}
