﻿using AutoMapper;
using Hino.API.Application.ViewModel.Mordor;
using Hino.API.Domain.Mordor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.MapperModels.Profiles
{
    public class DomainToViewMordorProfile : Profile
    {
        public DomainToViewMordorProfile()
        {
            CreateMap<GEUsers, CreateUserVM>();

        }
    }
}
