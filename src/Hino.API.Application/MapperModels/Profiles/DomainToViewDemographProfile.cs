﻿using AutoMapper;
using Hino.API.Application.ViewModel.Demograph;
using Hino.API.Domain.Demograph.Models;
using Hino.API.Infra.Cross.Utils.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Application.MapperModels.Profiles
{
    public class DomainToViewDemographProfile: Profile
    {
        public DomainToViewDemographProfile()
        {
            CreateMap<GECities, GECitiesVM>();
            CreateMap<GECountries, GECountriesVM>();
            CreateMap<GEStates, GEStatesVM>();

            CreateMap<PagedResult<GECities>, PagedResult<GECitiesVM>>();
            CreateMap<PagedResult<GECountries>, PagedResult<GECountriesVM>>();
            CreateMap<PagedResult<GEStates>, PagedResult<GEStatesVM>>();

            CreateMap<CountriesLoadVM[], CountriesLoad[]>();
        }
    }
}
