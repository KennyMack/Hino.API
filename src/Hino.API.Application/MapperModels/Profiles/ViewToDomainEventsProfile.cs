﻿using AutoMapper;
using Hino.API.Application.ViewModel.Events;
using Hino.API.Domain.Events.Models;


namespace Hino.API.Application.MapperModels.Profiles
{
    public class ViewToDomainEventsProfile : Profile
    {
        public ViewToDomainEventsProfile()
        {
            CreateMap<GECreateEventsVM, GECreateEvents>();
            CreateMap<GEEnterpriseEventVM, GEEnterpriseEvent>();
            CreateMap<GEEstabCalendarVM, GEEstabCalendar>();
            CreateMap<GEEventsChildVM, GEEventsChild>();
            CreateMap<GEEventsClassificationVM, GEEventsClassification>();
            CreateMap<GEEventsStatusVM, GEEventsStatus>();
            CreateMap<GEEventsVM, GEEvents>();
            CreateMap<GEUserCalendarVM, GEUserCalendar>();
        }
    }
}
