﻿using AutoMapper;
using Hino.API.Application.ViewModel.Taxes;
using Hino.API.Domain.Taxes.Models;

namespace Hino.API.Application.MapperModels.Profiles
{
    public class ViewToDomainTaxesProfile: Profile
    {
        public ViewToDomainTaxesProfile()
        {
            CreateMap<FSFiscalOperVM, FSFiscalOper>();
            CreateMap<FSImpostosVM, FSImpostos>();
            CreateMap<FSNCMVM, FSNCM>();
        }
    }
}
