﻿using AutoMapper;
using Hino.API.Application.ViewModel.Establishments;
using Hino.API.Domain.Establishments.Models;
using Hino.API.Infra.Cross.Utils.Paging;

namespace Hino.API.Application.MapperModels.Profiles
{
    public class DomainToViewEstablishmentsProfile: Profile
    {
        public DomainToViewEstablishmentsProfile()
        {
            CreateMap<GEEstabDevices, GEEstabDevicesVM>();
            CreateMap<GEEstabFat, GEEstabFatVM>();
            CreateMap<GEEstablishments, GEEstablishmentsVM>();
            CreateMap<GEEstabMenu, GEEstabMenuVM>();
            CreateMap<GEEstabPay, GEEstabPayVM>();
            CreateMap<PagedResult<GEEstablishments>, PagedResult<GEEstablishmentsVM>>();
        }
    }
}
