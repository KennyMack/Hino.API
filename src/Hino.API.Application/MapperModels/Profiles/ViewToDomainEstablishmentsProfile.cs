﻿using AutoMapper;
using Hino.API.Application.ViewModel.Establishments;
using Hino.API.Domain.Establishments.Models;

namespace Hino.API.Application.MapperModels.Profiles
{
    public class ViewToDomainEstablishmentsProfile : Profile
    {
        public ViewToDomainEstablishmentsProfile()
        {
            CreateMap<GEEstabDevicesVM, GEEstabDevices>();
            CreateMap<GEEstabFatVM, GEEstabFat>();
            CreateMap<GEEstablishmentsVM, GEEstablishments>();
            CreateMap<GEEstabMenuVM, GEEstabMenu>();
            CreateMap<GEEstabPayVM, GEEstabPay>();
            CreateMap<GEEstablishmentsCreateVM, GEEstablishments>();
        }
    }
}
