﻿using AutoMapper;
using Hino.API.Application.ViewModel.Orders;
using Hino.API.Domain.Orders.Models;

namespace Hino.API.Application.MapperModels.Profiles
{
    public class DomainToViewOrdersProfile: Profile
    {
        public DomainToViewOrdersProfile()
        {
            CreateMap<VECustomer, VECustomerVM>();
            CreateMap<VECarrier, VECarrierVM>();
            CreateMap<VEFiscalOper, VEFiscalOperVM>();
            CreateMap<VEOrderItems, VEOrderItemsVM>();
            CreateMap<VEOrderStatus, VEOrderStatusVM>();
            CreateMap<VEOrders, VEOrdersVM>();
            CreateMap<VEOrderTaxes, VEOrderTaxesVM>();
            CreateMap<VEPaymentCondition, VEPaymentConditionVM>();
            CreateMap<VEPaymentType, VEPaymentTypeVM>();
            CreateMap<VEProducts, VEProductsVM>();
            CreateMap<VESalePrice, VESalePriceVM>();
            CreateMap<VECustomer, VECustomerVM>();
            CreateMap<VESallesman, VESallesmanVM>();
            CreateMap<VETransactions, VETransactionsVM>();
        }
    }
}
