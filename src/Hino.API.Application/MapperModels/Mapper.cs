﻿using AutoMapper;
using System;

namespace Hino.API.Application.MapperModels
{
    public static class Mapper
    {
        private static IMapper _IMapper { get; set; }

        public static void SetMapper(IMapper pMapper)
        {
            if (MapInstance != null)
                throw new ArgumentNullException("Deve ser instanciado apenas uma vez");

            _IMapper = pMapper;
        }

        public static IMapper MapInstance { get => _IMapper; }

        static void CheckInstance()
        {
            if (MapInstance == null)
                throw new ArgumentNullException("Instancia do mapper não encontrada.");
        }

        public static TDestination Map<TDestination>(object source)
        {
            CheckInstance();

            return MapInstance.Map<TDestination>(source);
        }

        public static TDestination Map<TDestination>(object source, Action<IMappingOperationOptions<object, TDestination>> opts)
        {
            CheckInstance();

            return MapInstance.Map(source, opts);
        }

        public static TDestination Map<TSource, TDestination>(TSource source)
        {
            CheckInstance();

            return MapInstance.Map<TSource, TDestination>(source);
        }

        public static TDestination Map<TSource, TDestination>(TSource source, Action<IMappingOperationOptions<TSource, TDestination>> opts)
        {
            CheckInstance();

            return MapInstance.Map(source, opts);
        }

        public static TDestination Map<TSource, TDestination>(TSource source, TDestination destination, Action<IMappingOperationOptions<TSource, TDestination>> opts)
        {
            CheckInstance();

            return MapInstance.Map(source, destination, opts);
        }
    }
}
