﻿using Hino.API.Domain.Base.Models;
using Hino.API.Domain.Orders.Models;
using Hino.API.Infra.Data.Repositories.Base.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Infra.Data.Repositories.Orders.Context
{
    public class OrdersDbContext : BaseDbContext
    {
        public virtual DbSet<VEOrders> VEOrders { get; set; }
        public virtual DbSet<VEOrderItems> VEOrderItems { get; set; }
        public virtual DbSet<VETransactions> VETransactions { get; set; }
        public virtual DbSet<VEOrderTaxes> VEOrderTaxes { get; set; }
        public virtual DbSet<VESalePrice> VESalePrice { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            // modelBuilder.ApplyConfiguration(new GEUsersConfiguration("GEUSERS"));
        }
    }
}
