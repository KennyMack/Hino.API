﻿using Hino.API.Domain.Orders.Interfaces.Repositories;
using Hino.API.Domain.Orders.Interfaces.UoW;
using Hino.API.Domain.Orders.Models;
using Hino.API.Infra.Data.Repositories.Base.UoW;
using Hino.API.Infra.Data.Repositories.Orders.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Infra.Data.Repositories.Orders.UoW
{
    public class OrdersUoW : BaseUnitOfWork, IOrdersUoW
    {
        private OrdersDbContext _OrdersDbContext;
        public IVEOrdersRepository OrdersRepository { get; }
        public IVEOrderItemsRepository OrderItemsRepository { get; }
        public IVEOrderTaxesRepository OrderTaxesRepository { get; }
        public IVESalePriceRepository SalePriceRepository { get; }

        public OrdersUoW(OrdersDbContext pOrdersDbContext):
            base(pOrdersDbContext)
        {
            _OrdersDbContext = pOrdersDbContext;
            OrdersRepository = new VEOrdersRepository(_OrdersDbContext);
            OrderItemsRepository = new VEOrderItemsRepository(_OrdersDbContext);
            OrderTaxesRepository = new VEOrderTaxesRepository(_OrdersDbContext);
            SalePriceRepository = new VESalePriceRepository(_OrdersDbContext);
        }

        public override void Commit()
        {
            base.RowsAffected = _OrdersDbContext.SaveChanges();
        }

        protected override void Dispose(bool disposing)
        {
            OrdersRepository.Dispose();
            OrderItemsRepository.Dispose();
            OrderTaxesRepository.Dispose();
            SalePriceRepository.Dispose();
            _OrdersDbContext.Dispose();
            base.Dispose(disposing);
        }
    }
}
