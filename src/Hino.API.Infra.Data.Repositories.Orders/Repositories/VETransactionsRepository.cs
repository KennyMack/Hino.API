using Hino.API.Domain.Orders.Interfaces.Repositories;
using Hino.API.Domain.Orders.Models;
using Hino.API.Infra.Data.Repositories.Base.Repositories;
using Hino.API.Infra.Data.Repositories.Orders.Context;
using System.Threading.Tasks;

namespace Hino.API.Infra.Data.Repositories.Orders
{
    public class VETransactionsRepository : BaseRepository<VETransactions>, IVETransactionsRepository
    {
        public VETransactionsRepository(OrdersDbContext appDbContext) : base(appDbContext)
        {
        }

        public async Task<VETransactions> GetByAuthCode(string pEstablishmentKey, string pAuthCode) =>
            await FirstOrDefaultAsync(r =>
                r.EstablishmentKey == pEstablishmentKey &&
                r.AuthCode == pAuthCode);

        public async Task<VETransactions> GetByNsuHost(string pEstablishmentKey, string pTerminalId, string pNsuHost) =>
            await FirstOrDefaultAsync(r =>
                r.NSUHost == pNsuHost &&
                r.TerminalId == pTerminalId &&
                r.EstablishmentKey == pEstablishmentKey);

        public async Task<VETransactions> SearchTransationByNSUAsync(string pEstablishmentKey, string NSU) =>
            await FirstOrDefaultAsync(r => 
                r.EstablishmentKey == pEstablishmentKey &&
                r.NSU == NSU);
       
    }
}
