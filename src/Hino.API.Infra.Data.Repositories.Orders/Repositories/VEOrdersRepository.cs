using Hino.API.Domain.Orders.Enums;
using Hino.API.Domain.Orders.Interfaces.Repositories;
using Hino.API.Domain.Orders.Models;
using Hino.API.Infra.Cross.Utils.Paging;
using Hino.API.Infra.Data.Repositories.Base.Repositories;
using Hino.API.Infra.Data.Repositories.Orders.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DefaultMessages = Hino.API.Infra.Cross.Resources.MessagesResource;
using ValidationMessages = Hino.API.Infra.Cross.Resources.ValidationMessagesResource;

namespace Hino.API.Infra.Data.Repositories.Orders
{
    public class VEOrdersRepository : BaseRepository<VEOrders>, IVEOrdersRepository
    {
        public VEOrdersRepository(OrdersDbContext appDbContext) : base(appDbContext)
        {
        }

        public async Task<VEOrders> CanConvertToOrderAsync(string pEstablishmentKey, long id)
        {
            var OrderDB = (await FirstOrDefaultAsync(
                   r => r.EstablishmentKey == pEstablishmentKey &&
                   r.Id == id,
                   e => e.GEEnterprises));

            if (OrderDB == null)
                throw new Exception(ValidationMessages.NotFound);

            if (OrderDB.GEEnterprises.Classification == EEnterpriseClassification.Prospect)
                throw new Exception(DefaultMessages.EnterpriseIsProspect);

            return OrderDB;
        }

        public async Task<IEnumerable<VEOrders>> GetOrdersLinkedTreeAsync(string pEstablishmentKey, long id) =>
            await QueryAsync(r => (r.MainOrderID ?? r.Id) == id &&
                r.EstablishmentKey == pEstablishmentKey);

        public async Task<IEnumerable<VEOrders>> GetOrdersLinkedAsync(string pEstablishmentKey, long id) =>
            await QueryAsync(r => r.MainOrderID == id &&
                r.EstablishmentKey == pEstablishmentKey);

        public async Task<IEnumerable<VEOrders>> GetOrderDirectLinkedAsync(string pEstablishmentKey, long id) =>
            await QueryAsync(r => (r.MainOrderID == id || r.OriginOrderID == id) &&
                r.EstablishmentKey == pEstablishmentKey);

        public async override Task<PagedResult<VEOrders>> QueryPagedAsync(int page, int pageSize,
            Expression<Func<VEOrders, bool>> predicate,
            params Expression<Func<VEOrders, object>>[] includeProperties)
        {
            var count = DbEntity.Count(predicate);

            return await PaginateQueryAsync(LocalAddQueryProperties(
                DbEntity
                .Include(r => r.VEOrderItems)
                .Include("VEOrderItems.GEProducts")
                .Include("VEOrderItems.FSFiscalOper")
                .Include("VEOrderItems.GEProducts.FSNCM")
                .Include("VEOrderItems.VEOrderTaxes")
                .Include(e => e.GEUsers)
                .Include(e => e.GEUserDigitizer)
                .Include(e => e.GEPaymentType)
                .Include(e => e.GEPaymentCondition)
                .Include(e => e.GECarriers)
                .Include(e => e.GERedispatch)
                .Include(e => e.GEEnterprises)
                .Include(e => e.VETransactions)
                .Include(e => e.MainFiscalOper)
                .Include("GEEnterprises.GEEnterpriseGeo")
                .Include("GEEnterprises.GEEnterpriseContacts"), includeProperties)
                .AsNoTracking()
                .Where(predicate), page, pageSize, count);
        }

        public override async Task<PagedResult<VEOrders>> GetAllPagedAsync(int page, int pageSize,
                params Expression<Func<VEOrders, object>>[] includeProperties) =>
            await PaginateQueryAsync(LocalAddQueryProperties(
                DbEntity
                .Include(r => r.VEOrderItems)
                .Include("VEOrderItems.GEProducts")
                .Include("VEOrderItems.FSFiscalOper")
                .Include("VEOrderItems.GEProducts.FSNCM")
                .Include("VEOrderItems.VEOrderTaxes")
                .Include(e => e.GEUsers)
                .Include(e => e.GEUserDigitizer)
                .Include(e => e.GEPaymentType)
                .Include(e => e.GEPaymentCondition)
                .Include(e => e.GECarriers)
                .Include(e => e.GERedispatch)
                .Include(e => e.GEEnterprises)
                .Include(e => e.VETransactions)
                .Include(e => e.MainFiscalOper)
                .Include("GEEnterprises.GEEnterpriseGeo")
                .Include("GEEnterprises.GEEnterpriseContacts"), includeProperties)
                .AsNoTracking(), page, pageSize);

        public override async Task<IEnumerable<VEOrders>> QueryAsync(
            Expression<Func<VEOrders, bool>> predicate,
            params Expression<Func<VEOrders, object>>[] includeProperties) =>
            await LocalAddQueryProperties(DbEntity
                .Include(r => r.VEOrderItems)
                .Include("VEOrderItems.GEProducts")
                .Include("VEOrderItems.FSFiscalOper")
                .Include("VEOrderItems.GEProducts.FSNCM")
                .Include("VEOrderItems.VEOrderTaxes")
                .Include(e => e.GEUsers)
                .Include(e => e.GEUserDigitizer)
                .Include(e => e.GEPaymentType)
                .Include(e => e.GEPaymentCondition)
                .Include(e => e.GECarriers)
                .Include(e => e.GERedispatch)
                .Include(e => e.GEEnterprises)
                .Include(e => e.VETransactions)
                .Include(e => e.MainFiscalOper)
                .Include("GEEnterprises.GEEnterpriseGeo")
                .Include("GEEnterprises.GEEnterpriseContacts"), includeProperties)
            .Where(predicate)
            .AsNoTracking()
            .ToListAsync();

        public override async Task<IEnumerable<VEOrders>> GetAllAsync(
            params Expression<Func<VEOrders, object>>[] includeProperties) =>
            await LocalAddQueryProperties(DbEntity
                .Include(r => r.VEOrderItems)
                .Include("VEOrderItems.GEProducts")
                .Include("VEOrderItems.FSFiscalOper")
                .Include("VEOrderItems.GEProducts.FSNCM")
                .Include("VEOrderItems.VEOrderTaxes")
                .Include(e => e.GEUsers)
                .Include(e => e.GEUserDigitizer)
                .Include(e => e.GEPaymentType)
                .Include(e => e.GEPaymentCondition)
                .Include(e => e.GECarriers)
                .Include(e => e.GERedispatch)
                .Include(e => e.GEEnterprises)
                .Include(e => e.VETransactions)
                .Include(e => e.MainFiscalOper)
                .Include("GEEnterprises.GEEnterpriseGeo")
                .Include("GEEnterprises.GEEnterpriseContacts"), includeProperties)
            .ToListAsync();

        public override async Task<VEOrders> GetByIdAsync(long id, string pEstablishmentKey, string pUniqueKey,
             params Expression<Func<VEOrders, object>>[] includeProperties) =>
            await DbEntity
            .Include(r => r.VEOrderItems)
            .Include("VEOrderItems.GEProducts")
            .Include("VEOrderItems.FSFiscalOper")
            .Include("VEOrderItems.GEProducts.FSNCM")
            .Include("VEOrderItems.VEOrderTaxes")
            .Include("VEOrderItems.GEFilesPath")
            .Include(e => e.GEUsers)
            .Include(e => e.GEUserDigitizer)
            .Include(e => e.GEPaymentType)
            .Include(e => e.GEPaymentCondition)
            .Include(e => e.GECarriers)
            .Include(e => e.GERedispatch)
            .Include(e => e.GEEnterprises)
            .Include(e => e.VETransactions)
            .Include(e => e.MainFiscalOper)
            .Include("GEEnterprises.GEEnterpriseGeo")
            .Include("GEEnterprises.GEEnterpriseContacts")
            .AsNoTracking()
            .Where(r =>
                r.Id == id &&
                r.EstablishmentKey == pEstablishmentKey)
            .FirstOrDefaultAsync();

        IQueryable<VEOrders> LocalAddQueryProperties(IQueryable<VEOrders> query,
            params Expression<Func<VEOrders, object>>[] includeProperties)
        {
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        public override VEOrders GetById(long id, string pEstablishmentKey, string pUniqueKey,
            params Expression<Func<VEOrders, object>>[] includeProperties) =>
            LocalAddQueryProperties(DbEntity, includeProperties)
                .Include(r => r.VEOrderItems)
                .Include("VEOrderItems.GEProducts")
                .Include("VEOrderItems.FSFiscalOper")
                .Include("VEOrderItems.GEProducts.FSNCM")
                .Include("VEOrderItems.VEOrderTaxes")
                .Include("VEOrderItems.GEFilesPath")
                .Include(e => e.GEUsers)
                .Include(e => e.GEUserDigitizer)
                .Include(e => e.GEPaymentType)
                .Include(e => e.GEPaymentCondition)
                .Include(e => e.GECarriers)
                .Include(e => e.GERedispatch)
                .Include(e => e.GEEnterprises)
                .Include(e => e.VETransactions)
                .Include(e => e.MainFiscalOper)
                .Include("GEEnterprises.GEEnterpriseGeo")
                .Include("GEEnterprises.GEEnterpriseContacts")
                .Where(r =>
                   r.Id == id &&
                   r.EstablishmentKey == pEstablishmentKey)
                .AsNoTracking()
                .FirstOrDefault();

    }
}
