﻿using Hino.API.Domain.Orders.Models;
using Hino.API.Domain.Orders.Interfaces.Repositories;
using Hino.API.Infra.Cross.Utils.Paging;
using Hino.API.Infra.Data.Repositories.Base.Repositories;
using Hino.API.Infra.Data.Repositories.Orders.Context;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hino.API.Infra.Data.Repositories.Orders
{
    public class VESalePriceRepository : BaseRepository<VESalePrice>, IVESalePriceRepository
    {
        public VESalePriceRepository(OrdersDbContext appDbContext) : base(appDbContext)
        {
            
        }
    }
}
