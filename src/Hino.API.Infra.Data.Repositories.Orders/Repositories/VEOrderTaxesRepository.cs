using Hino.API.Domain.Orders.Interfaces.Repositories;
using Hino.API.Domain.Orders.Models;
using Hino.API.Infra.Data.Repositories.Base.Repositories;
using Hino.API.Infra.Data.Repositories.Orders.Context;
using System.Threading.Tasks;

namespace Hino.API.Infra.Data.Repositories.Orders
{
    public class VEOrderTaxesRepository : BaseRepository<VEOrderTaxes>, IVEOrderTaxesRepository
    {
        public VEOrderTaxesRepository(OrdersDbContext appDbContext) : base(appDbContext)
        {
        }

        public async Task ClearOrderTaxesAsync(string pEstablishmentKey, long pOrderId)
        {
            var Taxes = await QueryAsync(r =>
                r.VEOrderItems.OrderID == pOrderId &&
                r.EstablishmentKey == pEstablishmentKey);

            foreach (var tax in Taxes)
                await RemoveById(tax.Id, tax.EstablishmentKey, tax.UniqueKey);
        }
    }
}
