using Hino.API.Domain.Orders.Interfaces.Repositories;
using Hino.API.Domain.Orders.Models;
using Hino.API.Infra.Data.Repositories.Base.Repositories;
using Hino.API.Infra.Data.Repositories.Orders.Context;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.API.Infra.Data.Repositories.Orders
{
    public class VEOrderItemsRepository : BaseRepository<VEOrderItems>, IVEOrderItemsRepository
    {
        public VEOrderItemsRepository(OrdersDbContext appDbContext) : base(appDbContext)
        {

        }

        public IEnumerable<VEOrderItems> GetItemsByOrderId(long id, string pEstablishmentKey) =>
            DbEntity
            .Where(r => 
                r.OrderID == id &&
                r.EstablishmentKey == pEstablishmentKey)
            .AsNoTracking()
            .ToList();

        public async Task<bool> RemoveAllItemsAsync(long pId, string pEstablishmentKey)
        {
            var Items = await QueryAsync(
                r => r.OrderID == pId &&
                     r.EstablishmentKey == pEstablishmentKey);


            foreach (VEOrderItems item in Items)
                await RemoveById(item.Id, item.EstablishmentKey, item.UniqueKey);

            return true;
        }
    }
}
