﻿using Hino.API.Application.Interfaces.Services.Products;
using Hino.API.Application.MapperModels;
using Hino.API.Application.ViewModel.Products;
using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Products.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using Hino.API.Infra.Cross.Utils.Paging;
using Hino.API.Server.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.API.Server.API.Products.Controllers
{
    [Route("api/Products/Family/{pEstablishmentKey}")]
    [ApiController]
    [Authorize]
    public class FamilyController : BaseApiController
    {
        private readonly IGEProductsFamilyAS _IGEProductsFamilyAS;

        public FamilyController(IGEProductsFamilyAS pIGEProductsFamilyAS)
        {
            _IGEProductsFamilyAS = pIGEProductsFamilyAS;

            Services = new IErrorBaseService[]
            {
                _IGEProductsFamilyAS
            };
        }

        [Route("all")]
        [HttpGet]
        public async Task<IActionResult> GetAll(string pEstablishmentKey)
        {
            try
            {
                var Results =
                    Mapper.Map<PagedResult<GEProductsFamily>, PagedResult<GEProductsFamilyVM>>
                    (
                        await _IGEProductsFamilyAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                            r => r.EstablishmentKey == pEstablishmentKey
                        )
                    );

                if (Results == null)
                    throw new HinoException(_IGEProductsFamilyAS.Errors.FirstOrDefault());

                return RequestOK(Results);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IActionResult> GetById(string pEstablishmentKey, long pId)
        {
            try
            {
                var Result =
                    Mapper.Map<GEProductsFamily, GEProductsFamilyVM>
                    (
                        await _IGEProductsFamilyAS.FirstOrDefaultAsync(
                            r => r.Id == pId &&
                                 r.EstablishmentKey == pEstablishmentKey)
                    );

                if (Result == null)
                    return NotFoundRequest();

                return RequestOK(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        public async Task<IActionResult> Put(string pUniqueKey, long id, [FromBody] GEProductsFamilyVM IGEProductsFamilyVM)
        {
            _IGEProductsFamilyAS.Errors.Clear();
            IGEProductsFamilyVM.Id = id;
            IGEProductsFamilyVM.UniqueKey = pUniqueKey;

            ValidateModelState(IGEProductsFamilyVM);

            if (!ModelState.IsValid)
                return InvalidRequest(IGEProductsFamilyVM, ModelState);

            try
            {
                var Result = _IGEProductsFamilyAS.Update(
                    Mapper.Map<GEProductsFamilyVM, GEProductsFamily>(IGEProductsFamilyVM));

                if (Result == null || _IGEProductsFamilyAS.Errors.Any())
                    throw new HinoException(_IGEProductsFamilyAS.Errors.FirstOrDefault());

                await _IGEProductsFamilyAS.SaveChanges();

                return RequestResult(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpPost]
        [Route("save")]
        public async Task<IActionResult> Post(string pEstablishmentKey, [FromBody] GEProductsFamilyVM IGEProductsFamilyVM)
        {
            _IGEProductsFamilyAS.Errors.Clear();
            IGEProductsFamilyVM.EstablishmentKey = pEstablishmentKey;
            ValidateModelState(IGEProductsFamilyVM);

            if (!ModelState.IsValid)
                return InvalidRequest(IGEProductsFamilyVM, ModelState);

            try
            {
                var Result = _IGEProductsFamilyAS.Add(
                    Mapper.Map<GEProductsFamilyVM, GEProductsFamily>(IGEProductsFamilyVM));

                if (Result == null || _IGEProductsFamilyAS.Errors.Any())
                    throw new HinoException(_IGEProductsFamilyAS.Errors.FirstOrDefault());

                await _IGEProductsFamilyAS.SaveChanges();

                return RequestResult(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        public async Task<IActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IGEProductsFamilyAS.Errors.Clear();

            try
            {
                var Result = await _IGEProductsFamilyAS.RemoveById(pId, pEstablishmentKey, pUniqueKey);

                if (Result == null || _IGEProductsFamilyAS.Errors.Any())
                    throw new HinoException(_IGEProductsFamilyAS.Errors.FirstOrDefault());

                await _IGEProductsFamilyAS.SaveChanges();
                return RequestResult(Mapper.Map<GEProductsFamily, GEProductsFamilyVM>(Result));
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IGEProductsFamilyAS.Dispose();
            }
        }
    }
}
