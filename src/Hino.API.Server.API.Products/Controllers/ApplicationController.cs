﻿using Hino.API.Application.Interfaces.Services.Products;
using Hino.API.Application.MapperModels;
using Hino.API.Application.ViewModel.Products;
using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Products.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using Hino.API.Infra.Cross.Utils.Paging;
using Hino.API.Server.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.API.Server.API.Products.Controllers
{
    [Route("api/Products/Application/{pEstablishmentKey}")]
    [ApiController]
    [Authorize]
    public class ApplicationController : BaseApiController
    {
        private readonly IGEProductAplicAS _IGEProductAplicAS;

        public ApplicationController(IGEProductAplicAS pIGEProductAplicAS)
        {
            _IGEProductAplicAS = pIGEProductAplicAS;

            Services = new IErrorBaseService[]
            {
                _IGEProductAplicAS
            };
        }

        [Route("all")]
        [HttpGet]
        public async Task<IActionResult> GetAll(string pEstablishmentKey)
        {
            try
            {
                var Results =
                    Mapper.Map<PagedResult<GEProductAplic>, PagedResult<GEProductAplicVM>>
                    (
                        await _IGEProductAplicAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                            r => r.EstablishmentKey == pEstablishmentKey
                        )
                    );

                if (Results == null)
                    throw new HinoException(_IGEProductAplicAS.Errors.FirstOrDefault());

                return RequestOK(Results);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IActionResult> GetById(string pEstablishmentKey, long pId)
        {
            try
            {
                var Result =
                    Mapper.Map<GEProductAplic, GEProductAplicVM>
                    (
                        await _IGEProductAplicAS.FirstOrDefaultAsync(
                            r => r.Id == pId &&
                                 r.EstablishmentKey == pEstablishmentKey)
                    );

                if (Result == null)
                    return NotFoundRequest();

                return RequestOK(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        public async Task<IActionResult> Put(string pUniqueKey, long id, [FromBody] GEProductAplicVM pGEProductAplicVM)
        {
            _IGEProductAplicAS.Errors.Clear();
            pGEProductAplicVM.Id = id;
            pGEProductAplicVM.UniqueKey = pUniqueKey;

            ValidateModelState(pGEProductAplicVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEProductAplicVM, ModelState);

            try
            {
                var Result = _IGEProductAplicAS.Update(
                    Mapper.Map<GEProductAplicVM, GEProductAplic>(pGEProductAplicVM));

                if (Result != null && !_IGEProductAplicAS.Errors.Any())
                    await _IGEProductAplicAS.SaveChanges();

                return RequestResult(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpPost]
        [Route("save")]
        public async Task<IActionResult> Post(string pEstablishmentKey, [FromBody] GEProductAplicVM pGEProductAplicVM)
        {
            _IGEProductAplicAS.Errors.Clear();
            pGEProductAplicVM.EstablishmentKey = pEstablishmentKey;
            ValidateModelState(pGEProductAplicVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEProductAplicVM, ModelState);

            try
            {
                var Result = _IGEProductAplicAS.Add(
                    Mapper.Map<GEProductAplicVM, GEProductAplic>(pGEProductAplicVM));

                if (Result != null && !_IGEProductAplicAS.Errors.Any())
                    await _IGEProductAplicAS.SaveChanges();

                return RequestResult(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        public async Task<IActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IGEProductAplicAS.Errors.Clear();

            try
            {
                var Result = await _IGEProductAplicAS.RemoveById(pId, pEstablishmentKey, pUniqueKey);

                if (Result != null && !_IGEProductAplicAS.Errors.Any())
                    await _IGEProductAplicAS.SaveChanges();

                return RequestResult(Mapper.Map<GEProductAplic, GEProductAplicVM>(Result));
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IGEProductAplicAS.Dispose();
            }
        }
    }
}
