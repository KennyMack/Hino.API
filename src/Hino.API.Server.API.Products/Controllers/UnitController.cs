﻿using Hino.API.Application.Interfaces.Services.Products;
using Hino.API.Application.MapperModels;
using Hino.API.Application.ViewModel.Products;
using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Products.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using Hino.API.Infra.Cross.Utils.Paging;
using Hino.API.Server.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.API.Server.API.Products.Controllers
{
    [Route("api/Products/Unit/{pEstablishmentKey}")]
    [ApiController]
    [Authorize]
    public class UnitController : BaseApiController
    {
        private readonly IGEProductsUnitAS _IGEProductsUnitAS;

        public UnitController(IGEProductsUnitAS pIGEProductsUnitAS)
        {
            _IGEProductsUnitAS = pIGEProductsUnitAS;

            Services = new IErrorBaseService[]
            {
                _IGEProductsUnitAS
            };
        }

        [Route("all")]
        [HttpGet]
        public async Task<IActionResult> GetAll(string pEstablishmentKey)
        {
            try
            {
                var Results =
                    Mapper.Map<PagedResult<GEProductsUnit>, PagedResult<GEProductsUnitVM>>
                    (
                        await _IGEProductsUnitAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                            r => r.EstablishmentKey == pEstablishmentKey
                        )
                    );

                if (Results == null)
                    throw new HinoException(_IGEProductsUnitAS.Errors.FirstOrDefault());

                return RequestOK(Results);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IActionResult> GetById(string pEstablishmentKey, long pId)
        {
            try
            {
                _IGEProductsUnitAS.PublishMessage(pEstablishmentKey, pId.ToString());
                var Result =
                    Mapper.Map<GEProductsUnit, GEProductsUnitVM>
                    (
                        await _IGEProductsUnitAS.FirstOrDefaultAsync(
                            r => r.Id == pId &&
                                 r.EstablishmentKey == pEstablishmentKey)
                    );

                if (Result == null)
                    return NotFoundRequest();

                return RequestOK(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        public async Task<IActionResult> Put(string pUniqueKey, long id, [FromBody] GEProductsUnitVM IGEProductsUnitVM)
        {
            _IGEProductsUnitAS.Errors.Clear();
            IGEProductsUnitVM.Id = id;
            IGEProductsUnitVM.UniqueKey = pUniqueKey;

            ValidateModelState(IGEProductsUnitVM);

            if (!ModelState.IsValid)
                return InvalidRequest(IGEProductsUnitVM, ModelState);

            try
            {
                var Result = _IGEProductsUnitAS.Update(
                    Mapper.Map<GEProductsUnitVM, GEProductsUnit>(IGEProductsUnitVM));

                if (Result == null || _IGEProductsUnitAS.Errors.Any())
                    throw new HinoException(_IGEProductsUnitAS.Errors.FirstOrDefault());

                await _IGEProductsUnitAS.SaveChanges();

                return RequestResult(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpPost]
        [Route("save")]
        public async Task<IActionResult> Post(string pEstablishmentKey, [FromBody] GEProductsUnitVM IGEProductsUnitVM)
        {
            _IGEProductsUnitAS.Errors.Clear();
            IGEProductsUnitVM.EstablishmentKey = pEstablishmentKey;
            ValidateModelState(IGEProductsUnitVM);

            if (!ModelState.IsValid)
                return InvalidRequest(IGEProductsUnitVM, ModelState);

            try
            {
                var Result = _IGEProductsUnitAS.Add(
                    Mapper.Map<GEProductsUnitVM, GEProductsUnit>(IGEProductsUnitVM));

                if (Result == null || _IGEProductsUnitAS.Errors.Any())
                    throw new HinoException(_IGEProductsUnitAS.Errors.FirstOrDefault());

                await _IGEProductsUnitAS.SaveChanges();

                return RequestResult(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        public async Task<IActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IGEProductsUnitAS.Errors.Clear();

            try
            {
                var Result = await _IGEProductsUnitAS.RemoveById(pId, pEstablishmentKey, pUniqueKey);

                if (Result == null || _IGEProductsUnitAS.Errors.Any())
                    throw new HinoException(_IGEProductsUnitAS.Errors.FirstOrDefault());

                await _IGEProductsUnitAS.SaveChanges();
                return RequestResult(Mapper.Map<GEProductsUnit, GEProductsUnitVM>(Result));
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IGEProductsUnitAS.Dispose();
            }
        }
    }
}
