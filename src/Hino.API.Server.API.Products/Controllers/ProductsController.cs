﻿using Hino.API.Application.Interfaces.Services.Products;
using Hino.API.Application.MapperModels;
using Hino.API.Application.ViewModel.Products;
using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Products.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using Hino.API.Infra.Cross.Utils.Paging;
using Hino.API.Server.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.API.Server.API.Products.Controllers
{
    [Route("api/Products/{pEstablishmentKey}")]
    [ApiController]
    [Authorize]
    public class ProductsController : BaseApiController
    {
        private readonly IGEProductsAS _IGEProductsAS;
        private readonly ILogger _ILogger;
        private readonly IConfiguration _IConfiguration;

        public ProductsController(IGEProductsAS pIGEProductsAS,
            ILogger<ProductsController> pILogger,
            IConfiguration pIConfiguration)
        {
            _IGEProductsAS = pIGEProductsAS;
            _IConfiguration = pIConfiguration;
            _ILogger = pILogger;

            Services = new IErrorBaseService[]
            {
                _IGEProductsAS
            };
        }

        [Route("all/test")]
        [HttpGet]
        [AllowAnonymous]
        public IActionResult GetAllTest(string pEstablishmentKey)
        {
            _ILogger.LogInformation("Produto");
            try
            {
                var num = Convert.ToInt32("434ads23");
            }
            catch (Exception ex)
            {
                _ILogger.LogError("Erro conversão", ex);
            }


            return RequestOK(_IConfiguration["ElasticConfiguration:Uri"]);
        }

        [Route("all")]
        [HttpGet]
        public async Task<IActionResult> GetAll(string pEstablishmentKey)
        {
            try
            {
                var Results =
                    Mapper.Map<PagedResult<GEProducts>, PagedResult<GEProductsVM>>
                    (
                        await _IGEProductsAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                            r => r.EstablishmentKey == pEstablishmentKey,
                            s => s.GEProductsFamily,
                            t => t.GEProductsType,
                            g => g.GEProductsSaleUnit,
                            l => l.GEProductsUnit,
                            f => f.FSNCM,
                            j => j.GEProductAplic
                        )
                    );

                if (Results == null)
                    throw new HinoException(_IGEProductsAS.Errors.FirstOrDefault());

                return RequestOK(Results);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IActionResult> GetById(string pEstablishmentKey, long pId)
        {
            try
            {
                var Result =
                    Mapper.Map<GEProducts, GEProductsVM>
                    (
                        await _IGEProductsAS.FirstOrDefaultAsync(
                            r => r.Id == pId &&
                                 r.EstablishmentKey == pEstablishmentKey,
                            s => s.GEProductsFamily,
                            t => t.GEProductsType,
                            g => g.GEProductsSaleUnit,
                            l => l.GEProductsUnit,
                            f => f.FSNCM,
                            j => j.GEProductAplic)
                    );

                if (Result == null)
                    return NotFoundRequest();

                return RequestOK(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        public async Task<IActionResult> Put(string pUniqueKey, long id, [FromBody] GEProductsVM pGEProductsVM)
        {
            _IGEProductsAS.Errors.Clear();
            pGEProductsVM.Id = id;
            pGEProductsVM.UniqueKey = pUniqueKey;

            ValidateModelState(pGEProductsVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEProductsVM, ModelState);

            try
            {
                var Result = await _IGEProductsAS.ChangeAsync(pGEProductsVM);

                if (Result == null)
                    throw new HinoException(_IGEProductsAS.Errors.FirstOrDefault());

                return RequestResult(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpPost]
        [Route("save")]
        public async Task<IActionResult> Post(string pEstablishmentKey, [FromBody] GEProductsVM pGEProductsVM)
        {
            _IGEProductsAS.Errors.Clear();
            pGEProductsVM.EstablishmentKey = pEstablishmentKey;
            ValidateModelState(pGEProductsVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEProductsVM, ModelState);

            try
            {
                var Result = await _IGEProductsAS.CreateAsync(pGEProductsVM);

                if (Result == null)
                    throw new HinoException(_IGEProductsAS.Errors.FirstOrDefault());

                return RequestResult(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        public async Task<IActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IGEProductsAS.Errors.Clear();

            try
            {
                var Result = await _IGEProductsAS.RemoveById(pId, pEstablishmentKey, pUniqueKey);

                if (Result == null || _IGEProductsAS.Errors.Any())
                    throw new HinoException(_IGEProductsAS.Errors.FirstOrDefault());

                await _IGEProductsAS.SaveChanges();
                return RequestResult(Mapper.Map<GEProducts, GEProductsVM>(Result));
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IGEProductsAS.Dispose();
            }
        }
    }
}