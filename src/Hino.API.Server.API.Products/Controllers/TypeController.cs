﻿using Hino.API.Application.Interfaces.Services.Products;
using Hino.API.Application.MapperModels;
using Hino.API.Application.ViewModel.Products;
using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Products.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using Hino.API.Infra.Cross.Utils.Paging;
using Hino.API.Server.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.API.Server.API.Products.Controllers
{
    [Route("api/Products/Type/{pEstablishmentKey}")]
    [ApiController]
    [Authorize]
    public class TypeController : BaseApiController
    {
        private readonly IGEProductsTypeAS _IGEProductsTypeAS;

        public TypeController(IGEProductsTypeAS pIGEProductsTypeAS)
        {
            _IGEProductsTypeAS = pIGEProductsTypeAS;

            Services = new IErrorBaseService[]
            {
                _IGEProductsTypeAS
            };
        }

        [Route("all")]
        [HttpGet]
        public async Task<IActionResult> GetAll(string pEstablishmentKey)
        {
            try
            {
                var Results =
                    Mapper.Map<PagedResult<GEProductsType>, PagedResult<GEProductsTypeVM>>
                    (
                        await _IGEProductsTypeAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                            r => r.EstablishmentKey == pEstablishmentKey
                        )
                    );

                if (Results == null)
                    throw new HinoException(_IGEProductsTypeAS.Errors.FirstOrDefault());

                return RequestOK(Results);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IActionResult> GetById(string pEstablishmentKey, long pId)
        {
            try
            {
                var Result =
                    Mapper.Map<GEProductsType, GEProductsTypeVM>
                    (
                        await _IGEProductsTypeAS.FirstOrDefaultAsync(
                            r => r.Id == pId &&
                                 r.EstablishmentKey == pEstablishmentKey)
                    );

                if (Result == null)
                    return NotFoundRequest();

                return RequestOK(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        public async Task<IActionResult> Put(string pUniqueKey, long id, [FromBody] GEProductsTypeVM IGEProductsTypeVM)
        {
            _IGEProductsTypeAS.Errors.Clear();
            IGEProductsTypeVM.Id = id;
            IGEProductsTypeVM.UniqueKey = pUniqueKey;

            ValidateModelState(IGEProductsTypeVM);

            if (!ModelState.IsValid)
                return InvalidRequest(IGEProductsTypeVM, ModelState);

            try
            {
                var Result = _IGEProductsTypeAS.Update(
                    Mapper.Map<GEProductsTypeVM, GEProductsType>(IGEProductsTypeVM));

                if (Result == null || _IGEProductsTypeAS.Errors.Any())
                    throw new HinoException(_IGEProductsTypeAS.Errors.FirstOrDefault());

                await _IGEProductsTypeAS.SaveChanges();

                return RequestResult(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpPost]
        [Route("save")]
        public async Task<IActionResult> Post(string pEstablishmentKey, [FromBody] GEProductsTypeVM IGEProductsTypeVM)
        {
            _IGEProductsTypeAS.Errors.Clear();
            IGEProductsTypeVM.EstablishmentKey = pEstablishmentKey;
            ValidateModelState(IGEProductsTypeVM);

            if (!ModelState.IsValid)
                return InvalidRequest(IGEProductsTypeVM, ModelState);

            try
            {
                var Result = _IGEProductsTypeAS.Add(
                    Mapper.Map<GEProductsTypeVM, GEProductsType>(IGEProductsTypeVM));

                if (Result == null || _IGEProductsTypeAS.Errors.Any())
                    throw new HinoException(_IGEProductsTypeAS.Errors.FirstOrDefault());

                await _IGEProductsTypeAS.SaveChanges();

                return RequestResult(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        public async Task<IActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IGEProductsTypeAS.Errors.Clear();

            try
            {
                var Result = await _IGEProductsTypeAS.RemoveById(pId, pEstablishmentKey, pUniqueKey);

                if (Result == null || _IGEProductsTypeAS.Errors.Any())
                    throw new HinoException(_IGEProductsTypeAS.Errors.FirstOrDefault());

                await _IGEProductsTypeAS.SaveChanges();
                return RequestResult(Mapper.Map<GEProductsType, GEProductsTypeVM>(Result));
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IGEProductsTypeAS.Dispose();
            }
        }
    }
}
