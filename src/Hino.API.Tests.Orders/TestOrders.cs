using Hino.API.Domain.Orders.Interfaces.Repositories;
using Hino.API.Domain.Orders.Interfaces.Services;
using Hino.API.Domain.Orders.Models;
using Hino.API.Domain.Orders.Services;
using Moq;
using NUnit.Framework;
using System;

namespace Hino.API.Tests.Orders
{
    public class TestOrders
    {
        private Mock<ICreateOrderService> CreateOrderServiceMock;
        private Mock<IVEOrdersService> VEOrdersServiceMock;
        private Mock<IVEOrdersRepository> VEOrdersRepositoryMock;
        private static Guid ESTABLISHMENTKEY = new("b421e29f-d411-4cf6-a74d-6db9de4fe6c8");
        private static Guid UNIQUEKEY = new("b421e29f-d411-4cf6-a74d-6db9de4fe6c8");
        private readonly VEOrders PEDIDOINVALIDO = new()
        {
            EstablishmentKey = ESTABLISHMENTKEY.ToString(),
            UniqueKey = UNIQUEKEY.ToString()
        };
        private readonly VEOrders PEDIDOVALIDO = new()
        {

        };

        [SetUp]
        public void Setup()
        {
            CreateOrderServiceMock = new Mock<ICreateOrderService>();
            VEOrdersServiceMock = new Mock<IVEOrdersService>();
            VEOrdersRepositoryMock = new Mock<IVEOrdersRepository>();

            VEOrdersRepositoryMock.Setup(s => s.GetById(It.IsAny<long>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(PEDIDOVALIDO);

            VEOrdersRepositoryMock.Setup(s => s.Add(PEDIDOINVALIDO)).Throws(new Exception("Pedido Inv�lido"));
        }

        [Test]
        public void TestCanConvertToOrder()
        {
            // VEOrdersRepositoryMock.Verify(r => r.Add(PEDIDOINVALIDO), Times.Once());
            var OrdersService = new VEOrdersService(VEOrdersRepositoryMock.Object);

            OrdersService.GetById(0, ESTABLISHMENTKEY.ToString(), UNIQUEKEY.ToString());

            VEOrdersRepositoryMock.Verify(s => s.GetById(0, ESTABLISHMENTKEY.ToString(), UNIQUEKEY.ToString()));
            


            // CreateOrderServiceMock.Setup(p => p.Charge(It.IsAny<double>(), cardMock.Object)).Returns(typeof(VEOrders));
            // VEOrdersServiceMock.Setup(p => p.CanConvertToOrderAsync(It.IsAny<string>(), It.IsAny<long>())).ReturnsAsync(new VEOrders());

            // act
            //var result = controller.CheckOut(cardMock.Object, addressInfoMock.Object);

            // assert
            // myInterfaceMock.Verify((m => m.DoesSomething()), Times.Once());
            //shipmentServiceMock.Verify(s => s.Ship(addressInfoMock.Object, items.AsEnumerable()), Times.Once());

            //Assert.AreEqual("charged", result);
        }
    }
}