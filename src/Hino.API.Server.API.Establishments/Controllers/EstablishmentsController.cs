﻿using Hino.API.Application.Interfaces.Services.Establishments;
using Hino.API.Application.MapperModels;
using Hino.API.Application.ViewModel.Establishments;
using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Establishments.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using Hino.API.Infra.Cross.Utils.Paging;
using Hino.API.Server.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.API.Server.API.Establishments.Controllers
{
    [Route("api/Establishments/{pEstablishmentKey}")]
    [ApiController]
    [Authorize]
    public class EstablishmentsController : BaseApiController
    {
        private readonly IGEEstablishmentsAS _IGEEstablishmentsAS;
        public EstablishmentsController(IGEEstablishmentsAS pIGEEstablishmentsAS)
        {
            _IGEEstablishmentsAS = pIGEEstablishmentsAS;

            Services = new IErrorBaseService[]
            {
                _IGEEstablishmentsAS
            };
        }

        [Route("all")]
        [HttpGet]
        public async Task<IActionResult> Get(string pEstablishmentKey)
        {
            try
            {
                var Results =
                    Mapper.Map<PagedResult<GEEstablishments>, PagedResult<GEEstablishmentsVM>>
                    (
                        await _IGEEstablishmentsAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                            r => r.EstablishmentKey == pEstablishmentKey,
                            m => m.GEEstabMenu,
                            s => s.GEEstabDevices,
                            p => p.FSFiscalOper)
                    );

                if (Results == null)
                    throw new HinoException(_IGEEstablishmentsAS.Errors.FirstOrDefault());

                return RequestOK(Results);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [Route("my-estab")]
        [HttpGet]
        public async Task<IActionResult> GetByEstablishmentKey(string pEstablishmentKey)
        {
            var Results = await _IGEEstablishmentsAS.GetByEstablishmentKeyAsync(pEstablishmentKey);

            if (Results == null)
                return InvalidRequest(null, _IGEEstablishmentsAS.Errors);

            return RequestOK(Results);
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IActionResult> GetById(string pEstablishmentKey, long pId)
        {
            try
            {
                var Result =
                    Mapper.Map<GEEstablishments, GEEstablishmentsVM>
                    (
                        await _IGEEstablishmentsAS.FirstOrDefaultAsync(
                            r => r.Id == pId &&
                                 r.EstablishmentKey == pEstablishmentKey,
                            s => s.GEEstabDevices,
                            m => m.GEEstabMenu,
                            p => p.FSFiscalOper,
                            pf => pf.PfFiscalGroup,
                            pj => pj.PjFiscalGroup)
                    );

                if (Result == null)
                    return NotFoundRequest();

                return RequestOK(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        public async Task<IActionResult> Put(string pUniqueKey, long id, GEEstablishmentsCreateVM pGEEstablishmentsCreateVM)
        {
            _IGEEstablishmentsAS.Errors.Clear();
            pGEEstablishmentsCreateVM.Id = id;
            pGEEstablishmentsCreateVM.UniqueKey = pUniqueKey;

            // ValidateModelState(pGEEstablishmentsVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEEstablishmentsCreateVM, ModelState);

            var Result = await _IGEEstablishmentsAS.UpdateEstablishmentAsync(pGEEstablishmentsCreateVM);

            if (_IGEEstablishmentsAS.Errors.Any())
                return InvalidRequest(Result, _IGEEstablishmentsAS.Errors);

            return RequestOK(Result);
        }

        [HttpPost]
        [Route("save")]
        public async Task<IActionResult> Post(string pEstablishmentKey, [FromBody] GEEstablishmentsCreateVM pGEEstablishmentsCreateVM)
        {
            _IGEEstablishmentsAS.Errors.Clear();
            ValidateModelState(pGEEstablishmentsCreateVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEEstablishmentsCreateVM, ModelState);

            var Estab = await _IGEEstablishmentsAS.CreateEstablishmentAsync(pGEEstablishmentsCreateVM);

            if (_IGEEstablishmentsAS.Errors.Any())
                return InvalidRequest(pGEEstablishmentsCreateVM, _IGEEstablishmentsAS.Errors);

            return RequestOK(Mapper.Map<GEEstablishments, GEEstablishmentsVM>(Estab));
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IGEEstablishmentsAS.Dispose();
            }
        }
    }
}
