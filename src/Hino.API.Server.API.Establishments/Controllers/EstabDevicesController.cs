﻿using Hino.API.Application.Interfaces.Services.Establishments;
using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Server.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Hino.API.Server.API.Establishments.Controllers
{
    [Route("api/Establishments/Devices/{pEstablishmentKey}")]
    [ApiController]
    [Authorize]
    public class EstabDevicesController : BaseApiController
    {
        private readonly IGEEstabDevicesAS _IGEEstabDevicesAS;
        public EstabDevicesController(IGEEstabDevicesAS pIGEEstabDevicesAS)
        {
            _IGEEstabDevicesAS = pIGEEstabDevicesAS;

            Services = new IErrorBaseService[]
            {
                _IGEEstabDevicesAS
            };
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IGEEstabDevicesAS.Dispose();
            }
        }
    }
}
