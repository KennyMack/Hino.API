﻿using Hino.API.Application.Interfaces.Services.Establishments;
using Hino.API.Application.MapperModels;
using Hino.API.Application.ViewModel.Establishments;
using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Establishments.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using Hino.API.Infra.Cross.Utils.Paging;
using Hino.API.Server.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.API.Server.API.Establishments.Controllers
{
    [Route("api/Establishments/Menu/{pEstablishmentKey}")]
    [ApiController]
    [Authorize]
    public class EstabMenuController : BaseApiController
    {
        private readonly IGEEstabMenuAS _IGEEstabMenuAS;
        public EstabMenuController(IGEEstabMenuAS pIGEEstabMenuAS)
        {
            _IGEEstabMenuAS = pIGEEstabMenuAS;

            Services = new IErrorBaseService[]
            {
                _IGEEstabMenuAS
            };
        }

        [Route("all")]
        [HttpGet]
        public async Task<IActionResult> Get(string pEstablishmentKey)
        {
            try
            {
                var Results =
                    Mapper.Map<PagedResult<GEEstabMenu>, PagedResult<GEEstabMenuVM>>
                    (
                        await _IGEEstabMenuAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                        r => r.EstablishmentKey == pEstablishmentKey)
                    );

                if (Results == null)
                    throw new HinoException(_IGEEstabMenuAS.Errors.FirstOrDefault());

                return RequestOK(Results);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IActionResult> GetById(string pEstablishmentKey, long pId)
        {
            try
            {
                var Result =
                    Mapper.Map<GEEstabMenu, GEEstabMenuVM>
                    (
                        await _IGEEstabMenuAS.FirstOrDefaultAsync(
                            r => r.Id == pId &&
                            r.EstablishmentKey == pEstablishmentKey)
                    );

                if (Result == null)
                    return NotFoundRequest();

                return RequestOK(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        public async Task<IActionResult> Put(string pUniqueKey, long id, GEEstabMenuVM pGEEstabMenuVM)
        {
            _IGEEstabMenuAS.Errors.Clear();
            pGEEstabMenuVM.Id = id;
            pGEEstabMenuVM.UniqueKey = pUniqueKey;

            ValidateModelState(pGEEstabMenuVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEEstabMenuVM, ModelState);

            try
            {
                var Result = _IGEEstabMenuAS.Update(Mapper.Map<GEEstabMenuVM, GEEstabMenu>(pGEEstabMenuVM));

                await _IGEEstabMenuAS.SaveChanges();

                return RequestResult(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpPost]
        [Route("save")]
        public async Task<IActionResult> Post(string pEstablishmentKey, [FromBody] GEEstabMenuVM pGEEstabMenuVM)
        {
            _IGEEstabMenuAS.Errors.Clear();
            ValidateModelState(pGEEstabMenuVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEEstabMenuVM, ModelState);

            try
            {
                var Result = _IGEEstabMenuAS.Add(Mapper.Map<GEEstabMenuVM, GEEstabMenu>(pGEEstabMenuVM));

                await _IGEEstabMenuAS.SaveChanges();

                return RequestResult(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        public async Task<IActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IGEEstabMenuAS.Errors.Clear();
            try
            {
                var Result = await _IGEEstabMenuAS.RemoveById(pId, pEstablishmentKey, pUniqueKey);
                await _IGEEstabMenuAS.SaveChanges();

                return RequestResult(Mapper.Map<GEEstabMenu, GEEstabMenuVM>(Result));
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IGEEstabMenuAS.Dispose();
            }
        }
    }
}
