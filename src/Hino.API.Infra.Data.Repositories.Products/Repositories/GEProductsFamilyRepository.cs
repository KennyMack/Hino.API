using Hino.API.Infra.Data.Repositories.Base.Repositories;
using Hino.API.Infra.Data.Repositories.Products.Context;
using Hino.API.Domain.Products.Models;
using Hino.API.Domain.Products.Interfaces.Repositories;

namespace Hino.API.Infra.Data.Repositories.Products.Repositories
{
    public class GEProductsFamilyRepository : BaseRepository<GEProductsFamily>, IGEProductsFamilyRepository
    {
        public GEProductsFamilyRepository(ProductsDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
