﻿using Hino.API.Infra.Data.Repositories.Base.Repositories;
using Hino.API.Infra.Data.Repositories.Products.Context;
using Hino.API.Domain.Products.Models;
using Hino.API.Domain.Products.Interfaces.Repositories;

namespace Hino.API.Infra.Data.Repositories.Products.Repositories
{
    public class GEProductAplicRepository : BaseRepository<GEProductAplic>, IGEProductAplicRepository
    {
        public GEProductAplicRepository(ProductsDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
