﻿using Hino.API.Domain.Products.Models;
using Hino.API.Infra.Data.Repositories.Base.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Hino.API.Infra.Data.Repositories.Products.Context
{
    public class ProductsDbContext : BaseDbContext
    {
        public virtual DbSet<GEProductAplic> GEProductAplic { get; set; }
        public virtual DbSet<GEProducts> GEProducts { get; set; }
        public virtual DbSet<GEProductsFamily> GEProductsFamily { get; set; }
        public virtual DbSet<GEProductsType> GEProductsType { get; set; }
        public virtual DbSet<GEProductsUnit> GEProductsUnit { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            // modelBuilder.ApplyConfiguration(new GEUsersConfiguration("GEUSERS"));
        }
    }
}
