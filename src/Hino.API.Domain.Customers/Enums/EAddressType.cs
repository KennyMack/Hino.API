﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Domain.Customers.Enums
{
    /// <summary>
    /// 0-Comercial 1-Entrega 2-Cobranca 3-Residencial
    /// </summary>
    public enum EAddressType
    {
        Commercial = 0,
        Delivery = 1,
        Levy = 2,
        Residential = 3
    }
}
