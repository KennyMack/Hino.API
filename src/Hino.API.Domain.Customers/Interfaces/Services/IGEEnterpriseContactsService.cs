using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Customers.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.API.Domain.Customers.Interfaces.Services
{
    public interface IGEEnterpriseContactsService : IBaseService<GEEnterpriseContacts>
    {
        Task ClearEnterpriseContactsAsync(string pEstablishmentKey, long pEnterpriseId);
        Task SaveEnterpriseContactsAsync(GEEnterprises pModel, IEnumerable<GEEnterpriseContacts> pEnterpriseContacts);
    }
}
