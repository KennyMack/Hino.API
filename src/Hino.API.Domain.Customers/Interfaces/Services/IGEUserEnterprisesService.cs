using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Customers.Models;
using Hino.API.Infra.Cross.Utils.Paging;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hino.API.Domain.Customers.Interfaces.Services
{
    public interface IGEUserEnterprisesService : IBaseService<GEUserEnterprises>
    {
        Task<PagedResult<GEUserEnterprises>> QuerySearchPagedAsync(int page, int pageSize,
            Expression<Func<GEUserEnterprises, bool>> predicate,
            params Expression<Func<GEUserEnterprises, object>>[] includeProperties);
        Task<GEUserEnterprises> CreateOrUpdateAsync(GEUserEnterprises enterprise);
        Task<GEUserEnterprises[]> CreateOrUpdateListAsync(string pEstablishmentKey, GEUserEnterprises[] enterprise);
        Task<GEUserEnterprises[]> RemoveListAsync(string pEstablishmentKey, GEUserEnterprises[] enterprise);
    }
}
