using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Customers.Models;

namespace Hino.API.Domain.Customers.Interfaces.Services
{
    public interface IGEEnterpriseCategoryService : IBaseService<GEEnterpriseCategory>
    {
    }
}
