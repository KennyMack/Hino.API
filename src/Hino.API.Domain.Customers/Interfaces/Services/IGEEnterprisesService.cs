﻿using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Customers.Enums;
using Hino.API.Domain.Customers.Models;
using System.Threading.Tasks;

namespace Hino.API.Domain.Customers.Interfaces.Services
{
    public interface IGEEnterprisesService : IBaseService<GEEnterprises>
    {
        Task UpdateEnterpriseRegionIdAsync(string pEstablishmentKey, string pUF, long pRegionId);
        Task<GEEnterprises> UpdateToClient(string pEstablishmentKey, string pUniqueKey, long pId);
        Task<GEEnterprises> CreateAsync(GEEnterprises model);
        Task<GEEnterprises> ChangeAsync(GEEnterprises model);
        Task<bool> EnterpriseExists(string pEstablishmentKey, string pCNPJ, EEnterpriseClassification classification, long idActual);
        Task UpdateEnterpriseRegionId(string pEstablishmentKey, string pUF, long pRegionId);
    }
}
