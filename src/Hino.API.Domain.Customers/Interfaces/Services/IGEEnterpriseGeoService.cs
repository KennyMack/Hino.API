using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Customers.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.API.Domain.Customers.Interfaces.Services
{
    public interface IGEEnterpriseGeoService : IBaseService<GEEnterpriseGeo>
    {
        Task ClearEnterpriseGeoAsync(string pEstablishementKey, long pEnterpriseId);
        Task SaveEnterpriseGeoAsync(GEEnterprises pModel, IEnumerable<GEEnterpriseGeo> pEnterpriseGeo);
    }
}
