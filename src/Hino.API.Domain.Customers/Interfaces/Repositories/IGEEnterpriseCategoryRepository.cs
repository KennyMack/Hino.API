using Hino.API.Domain.Base.Interfaces.Repositories;
using Hino.API.Domain.Customers.Models;

namespace Hino.API.Domain.Customers.Interfaces.Repositories
{
    public interface IGEEnterpriseCategoryRepository : IBaseRepository<GEEnterpriseCategory>
    {
    }
}
