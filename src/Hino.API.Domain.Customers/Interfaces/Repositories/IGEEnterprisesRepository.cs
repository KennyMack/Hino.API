﻿using Hino.API.Domain.Base.Interfaces.Repositories;
using Hino.API.Domain.Customers.Enums;
using Hino.API.Domain.Customers.Models;
using System.Threading.Tasks;

namespace Hino.API.Domain.Customers.Interfaces.Repositories
{
    public interface IGEEnterprisesRepository : IBaseRepository<GEEnterprises>
    {
        Task UpdateEnterpriseRegionIdAsync(string pEstablishmentKey, string pUF, long pRegionId);
        Task<bool> EnterpriseExists(string pEstablishmentKey, string pCNPJCPF, EEnterpriseClassification pClassification, long idActual);
    }
}
