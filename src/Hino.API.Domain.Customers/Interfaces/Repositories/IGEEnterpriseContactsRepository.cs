using Hino.API.Domain.Base.Interfaces.Repositories;
using Hino.API.Domain.Customers.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.API.Domain.Customers.Interfaces.Repositories
{
    public interface IGEEnterpriseContactsRepository : IBaseRepository<GEEnterpriseContacts>
    {
        Task ClearEnterpriseContactsAsync(string pEstablishmentKey, long pEnterpriseId);
        Task SaveEnterpriseContactsAsync(GEEnterprises pModel, IEnumerable<GEEnterpriseContacts> pEnterpriseContacts);
    }
}
