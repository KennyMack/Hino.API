using Hino.API.Domain.Base.Interfaces.Repositories;
using Hino.API.Domain.Customers.Models;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.API.Domain.Customers.Interfaces.Repositories
{
    public interface IGEEnterpriseGeoRepository : IBaseRepository<GEEnterpriseGeo>
    {
        Task ClearEnterpriseGeoAsync(string pEstablishmentKey, long pEnterpriseId);
        Task SaveEnterpriseGeoAsync(GEEnterprises pModel, IEnumerable<GEEnterpriseGeo> pEnterpriseGeo);
    }
}
