using Hino.API.Domain.Base.Interfaces.Repositories;
using Hino.API.Domain.Customers.Models;
using Hino.API.Infra.Cross.Utils.Paging;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hino.API.Domain.Customers.Interfaces.Repositories
{
    public interface IGEUserEnterprisesRepository : IBaseRepository<GEUserEnterprises>
    {
        Task<PagedResult<GEUserEnterprises>> QuerySearchPagedAsync(int page, int pageSize,
            Expression<Func<GEUserEnterprises, bool>> predicate,
            params Expression<Func<GEUserEnterprises, object>>[] includeProperties);
    }
}
