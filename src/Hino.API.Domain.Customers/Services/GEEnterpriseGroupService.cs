using Hino.API.Domain.Base.Services;
using Hino.API.Domain.Customers.Interfaces.Repositories;
using Hino.API.Domain.Customers.Interfaces.Services;
using Hino.API.Domain.Customers.Models;

namespace Hino.API.Domain.Customers.Services
{
    public class GEEnterpriseGroupService : BaseService<GEEnterpriseGroup>, IGEEnterpriseGroupService
    {
        private readonly IGEEnterpriseGroupRepository _IGEEnterpriseGroupRepository;

        public GEEnterpriseGroupService(IGEEnterpriseGroupRepository pIGEEnterpriseGroupRepository) :
             base(pIGEEnterpriseGroupRepository)
        {
            _IGEEnterpriseGroupRepository = pIGEEnterpriseGroupRepository;
        }
    }
}
