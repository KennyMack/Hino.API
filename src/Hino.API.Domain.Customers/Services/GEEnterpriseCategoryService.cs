using Hino.API.Domain.Base.Services;
using Hino.API.Domain.Customers.Interfaces.Repositories;
using Hino.API.Domain.Customers.Interfaces.Services;
using Hino.API.Domain.Customers.Models;

namespace Hino.API.Domain.Customers.Services
{
    public class GEEnterpriseCategoryService : BaseService<GEEnterpriseCategory>, IGEEnterpriseCategoryService
    {
        private readonly IGEEnterpriseCategoryRepository _IGEEnterpriseCategoryRepository;

        public GEEnterpriseCategoryService(IGEEnterpriseCategoryRepository pIGEEnterpriseCategoryRepository) :
             base(pIGEEnterpriseCategoryRepository)
        {
            _IGEEnterpriseCategoryRepository = pIGEEnterpriseCategoryRepository;
        }
    }
}
