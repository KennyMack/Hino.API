using Hino.API.Domain.Base.Services;
using Hino.API.Domain.Customers.Enums;
using Hino.API.Domain.Customers.Interfaces.Repositories;
using Hino.API.Domain.Customers.Interfaces.Services;
using Hino.API.Domain.Customers.Models;
using Hino.API.Infra.Cross.Utils.Enums;
using Hino.API.Infra.Cross.Utils.Exceptions;
using ValidationMessages = Hino.API.Infra.Cross.Resources.ValidationMessagesResource;
using DefaultMessages = Hino.API.Infra.Cross.Resources.MessagesResource;
using System;
using System.Threading.Tasks;
using System.Linq;

namespace Hino.API.Domain.Customers.Services
{
    public class GEEnterprisesService : BaseService<GEEnterprises>, IGEEnterprisesService
    {
        private readonly IGEEnterprisesRepository _IGEEnterprisesRepository;
        private readonly IGEEnterpriseFiscalGroupRepository _IGEEnterpriseFiscalGroupRepository;
        private readonly IGEEnterpriseGeoRepository _IGEEnterpriseGeoRepository;
        private readonly IGEEnterpriseContactsRepository _IGEEnterpriseContactsRepository;
        private readonly IGEUserEnterprisesRepository _IGEUserEnterprisesRepository;

        public GEEnterprisesService(IGEEnterprisesRepository pIGEEnterprisesRepository,
            IGEEnterpriseFiscalGroupRepository pIGEEnterpriseFiscalGroupRepository,
            IGEEnterpriseGeoRepository pIGEEnterpriseGeoRepository,
            IGEEnterpriseContactsRepository pIGEEnterpriseContactsRepository,
            IGEUserEnterprisesRepository pIGEUserEnterprisesRepository) :
             base(pIGEEnterprisesRepository)
        {
            _IGEUserEnterprisesRepository = pIGEUserEnterprisesRepository;
            _IGEEnterpriseContactsRepository = pIGEEnterpriseContactsRepository;
            _IGEEnterpriseGeoRepository = pIGEEnterpriseGeoRepository;
            _IGEEnterprisesRepository = pIGEEnterprisesRepository;
            _IGEEnterpriseFiscalGroupRepository = pIGEEnterpriseFiscalGroupRepository;
        }

        public override GEEnterprises Add(GEEnterprises model)
        {
            Errors.Add(ModelException.CreateValidationError(DefaultMessages.CreateNotAllowed, "Id", ""));
            return model;
        }

        public override GEEnterprises Update(GEEnterprises model)
        {
            Errors.Add(ModelException.CreateValidationError(DefaultMessages.CreateNotAllowed, "Id", ""));
            return model;
        }

        public async Task<bool> EnterpriseExists(string pEstablishmentKey, string pCNPJ, EEnterpriseClassification classification, long idActual) =>
            await _IGEEnterprisesRepository.EnterpriseExists(pEstablishmentKey, pCNPJ, classification, idActual);

        public async Task UpdateEnterpriseRegionId(string pEstablishmentKey, string pUF, long pRegionId) =>
            await _IGEEnterprisesRepository.UpdateEnterpriseRegionIdAsync(pEstablishmentKey, pUF, pRegionId);

        GEEnterprises Create(GEEnterprises model)
        {
            try
            {
                _IGEEnterprisesRepository.Add(model);
                AddedOrUpdatedItems.Add(model);
            }
            catch (Exception e)
            {
                Logging.Exception(e);
                Errors.Add(ModelException.CreateSqlError(e, EExceptionErrorCodes.InsertSQLError));
            }

            return model;
        }

        public async Task UpdateEnterpriseRegionIdAsync(string pEstablishmentKey, string pUF, long pRegionId) =>
            await _IGEEnterprisesRepository.UpdateEnterpriseRegionIdAsync(pEstablishmentKey, pUF, pRegionId);

        public async Task<GEEnterprises> UpdateToClient(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            var UpdateEnterprise = GetByIdToUpdate(pId, pEstablishmentKey, pUniqueKey);

            if (UpdateEnterprise == null)
            {
                Errors.Add(ModelException.CreateNotFoundError(DefaultMessages.EnterpriseNotFound, "EnterpriseId", pId.ToString()));
                return null;
            }

            var FiscalGroup = await _IGEEnterpriseFiscalGroupRepository.FirstOrDefaultAsync(r => 
                r.EstablishmentKey == pEstablishmentKey &&
                (r.Type == "C" || r.Type == "X"));

            if (FiscalGroup != null)
                UpdateEnterprise.FiscalGroupId = FiscalGroup.Id;

            UpdateEnterprise.Classification = EEnterpriseClassification.Client;
            UpdateEnterprise.ClassifEmpresa = "C1";

            _IGEEnterprisesRepository.Update(UpdateEnterprise);

            if (!Errors.Any())
                await SaveChanges();

            return UpdateEnterprise;
        }

        public async Task<GEEnterprises> CreateAsync(GEEnterprises model)
        {
            model.RazaoSocial = model.RazaoSocial.Trim();
            model.NomeFantasia = model.NomeFantasia.Trim();

            if (model.RegionId <= 0)
            {
                Errors.Add(ModelException.CreateValidationError(DefaultMessages.UFNotFoundInRegionSale, "GEEnterprises", model.RazaoSocial));
                return model;
            }

            if (await EnterpriseExists(model.EstablishmentKey, model.CNPJCPF, model.Classification, model.Id))
            {
                Errors.Add(ModelException.CreateValidationError(ValidationMessages.CnpjCpfDuplicated, "GEEnterprises", model.RazaoSocial));
                return model;
            }

            model.CreditUsed = 0;
            model.CreditLimit = 0;
            model.BirthDate = model.BirthDate?.Date;

            if (model.Type == EEnterpriseType.PessoaFisica)
                model.NomeFantasia = model.RazaoSocial;

            foreach (var item in model.GEEnterpriseGeo)
            {
                item.EstablishmentKey = model.EstablishmentKey;
                item.IE = model.FormatIE(item.UF);
                if (item.Type == EAddressType.Commercial)
                    model.IE = item.IE;
            }

            foreach (var item in model.GEEnterpriseContacts)
                item.EstablishmentKey = model.EstablishmentKey;

            foreach (var geo in model.GEEnterpriseGeo)
                geo.Address = geo.Address.Trim();

            Create(model);

            if (!Errors.Any())
                await _IGEEnterprisesRepository.SaveChanges();

            return model;
        }

        public async Task<GEEnterprises> ChangeAsync(GEEnterprises model)
        {
            var UpdateEnterprise = model;

            if (model.RegionId <= 0)
            {
                Errors.Add(ModelException.CreateValidationError(DefaultMessages.UFNotFoundInRegionSale, "GEEnterprises", model.RazaoSocial));
                return model;
            }



            try
            {
                await _IGEEnterpriseGeoRepository.ClearEnterpriseGeoAsync(UpdateEnterprise.EstablishmentKey, UpdateEnterprise.Id);
                await _IGEEnterpriseGeoRepository.SaveEnterpriseGeoAsync(UpdateEnterprise, UpdateEnterprise.GEEnterpriseGeo);
            }
            catch (Exception ex)
            {
                Errors.Add(ModelException.CreateSqlError(ex, "GEEnterpriseGeo", EExceptionErrorCodes.DeleteSQLError));
                return model;
            }

            try
            {
                await _IGEEnterpriseContactsRepository.ClearEnterpriseContactsAsync(UpdateEnterprise.EstablishmentKey, UpdateEnterprise.Id);
                await _IGEEnterpriseContactsRepository.SaveEnterpriseContactsAsync(UpdateEnterprise, UpdateEnterprise.GEEnterpriseContacts);
            }
            catch (Exception ex)
            {
                Errors.Add(ModelException.CreateSqlError(ex, "GEEnterpriseGeo", EExceptionErrorCodes.DeleteSQLError));
                return model;
            }

            UpdateEnterprise.GEEnterpriseContacts.Clear();
            UpdateEnterprise.GEEnterpriseGeo.Clear();

            var EntUpdate = GetByIdToUpdate(UpdateEnterprise.Id, UpdateEnterprise.EstablishmentKey, UpdateEnterprise.UniqueKey);
            EntUpdate.RazaoSocial = UpdateEnterprise.RazaoSocial;
            EntUpdate.NomeFantasia = UpdateEnterprise.NomeFantasia;
            EntUpdate.CNPJCPF = UpdateEnterprise.CNPJCPF;
            EntUpdate.IE = UpdateEnterprise.IE;
            EntUpdate.Type = UpdateEnterprise.Type;
            EntUpdate.Status = UpdateEnterprise.Status;
            EntUpdate.StatusSinc = UpdateEnterprise.StatusSinc;
            EntUpdate.IdERP = UpdateEnterprise.IdERP;
            EntUpdate.RegionId = UpdateEnterprise.RegionId;
            EntUpdate.BirthDate = UpdateEnterprise.BirthDate?.Date;
            EntUpdate.Classification = UpdateEnterprise.Classification;
            EntUpdate.ClassifEmpresa = UpdateEnterprise.ClassifEmpresa;
            EntUpdate.FiscalGroupId = UpdateEnterprise.FiscalGroupId;
            EntUpdate.PayConditionId = UpdateEnterprise.PayConditionId;
            EntUpdate.CategoryId = UpdateEnterprise.CategoryId;
            EntUpdate.GroupId = UpdateEnterprise.GroupId;
            EntUpdate.RegionId = UpdateEnterprise.RegionId;
            EntUpdate.RegionId = UpdateEnterprise.RegionId;

            if (UpdateEnterprise.Type == EEnterpriseType.PessoaFisica)
                EntUpdate.NomeFantasia = UpdateEnterprise.RazaoSocial;

            _IGEEnterprisesRepository.Update(EntUpdate);

            if (!Errors.Any())
                await _IGEEnterprisesRepository.SaveChanges();

            return EntUpdate;
        }

        public override async Task<GEEnterprises> RemoveById(long id, string pEstablishmentKey, string pUniqueKey)
        {
            var Enterprise = await GetByIdAsync(id, pEstablishmentKey, pUniqueKey,
                r => r.GEEnterpriseGeo,
                s => s.GEEnterpriseContacts);

            if (Enterprise == null)
            {
                Errors.Add(ModelException.CreateNotFoundError(DefaultMessages.EnterpriseNotFound, "GEEnterprises", id.ToString()));
                return null;
            }

            if (Enterprise.StatusSinc != EStatusSinc.Waiting)
            {
                Errors.Add(ModelException.CreateValidationError(DefaultMessages.StatusSincNotAllowRemove, "GEEnterprises", id.ToString()));
                return null;
            }

            if (Enterprise.VEOrders.Any())
            {
                Errors.Add(ModelException.CreateValidationError(ValidationMessages.RegisterChild, "VEOrders", id.ToString()));
                return null;
            }

            foreach (var item in Enterprise.GEEnterpriseGeo)
                await _IGEEnterpriseGeoRepository.RemoveById(item.Id, item.EstablishmentKey, item.UniqueKey);

            foreach (var item in Enterprise.GEEnterpriseContacts)
                await _IGEEnterpriseContactsRepository.RemoveById(item.Id, item.EstablishmentKey, item.UniqueKey);

            var UserEnterprise = await _IGEUserEnterprisesRepository.QueryAsync(r =>
                r.EstablishmentKey == Enterprise.EstablishmentKey &&
                r.EnterpriseId == Enterprise.Id);

            foreach (var item in UserEnterprise)
                await _IGEUserEnterprisesRepository.RemoveById(item.Id, item.EstablishmentKey, item.UniqueKey);

            Enterprise.GEEnterpriseGeo.Clear();
            Enterprise.GEEnterpriseContacts.Clear();

            await _IGEEnterprisesRepository.RemoveById(Enterprise.Id, Enterprise.EstablishmentKey, Enterprise.UniqueKey);
            await _IGEUserEnterprisesRepository.SaveChanges();
            await _IGEEnterpriseContactsRepository.SaveChanges();
            await _IGEEnterpriseGeoRepository.SaveChanges();
            await _IGEEnterprisesRepository.SaveChanges();
            return Enterprise;
        }
    }
}
