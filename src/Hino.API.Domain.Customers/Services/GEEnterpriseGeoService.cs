using Hino.API.Domain.Base.Services;
using Hino.API.Domain.Customers.Interfaces.Repositories;
using Hino.API.Domain.Customers.Interfaces.Services;
using Hino.API.Domain.Customers.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.API.Domain.Customers.Services
{
    public class GEEnterpriseGeoService : BaseService<GEEnterpriseGeo>, IGEEnterpriseGeoService
    {
        private readonly IGEEnterpriseGeoRepository _IGEEnterpriseGeoRepository;

        public GEEnterpriseGeoService(IGEEnterpriseGeoRepository pIGEEnterpriseGeoRepository) :
             base(pIGEEnterpriseGeoRepository)
        {
            _IGEEnterpriseGeoRepository = pIGEEnterpriseGeoRepository;
        }

        public async Task ClearEnterpriseGeoAsync(string pEstablishementKey, long pEnterpriseId) =>
            await _IGEEnterpriseGeoRepository.ClearEnterpriseGeoAsync(pEstablishementKey, pEnterpriseId);

        public async Task SaveEnterpriseGeoAsync(GEEnterprises pModel, IEnumerable<GEEnterpriseGeo> pEnterpriseGeo) =>
            await _IGEEnterpriseGeoRepository.SaveEnterpriseGeoAsync(pModel, pEnterpriseGeo);
    }
}
