using Hino.API.Domain.Base.Services;
using Hino.API.Domain.Customers.Interfaces.Repositories;
using Hino.API.Domain.Customers.Interfaces.Services;
using Hino.API.Domain.Customers.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using DefaultMessages = Hino.API.Infra.Cross.Resources.MessagesResource;
using System.Threading.Tasks;

namespace Hino.API.Domain.Customers.Services
{
    public class GEEnterpriseFiscalGroupService : BaseService<GEEnterpriseFiscalGroup>, IGEEnterpriseFiscalGroupService
    {
        private readonly IGEEnterpriseFiscalGroupRepository _IGEEnterpriseFiscalGroupRepository;

        public GEEnterpriseFiscalGroupService(IGEEnterpriseFiscalGroupRepository pIGEEnterpriseFiscalGroupRepository) :
             base(pIGEEnterpriseFiscalGroupRepository)
        {
            _IGEEnterpriseFiscalGroupRepository = pIGEEnterpriseFiscalGroupRepository;
        }

        public override void Remove(GEEnterpriseFiscalGroup model) =>
            Errors.Add(ModelException.CreateValidationError(DefaultMessages.RemoveNotAllowed, "Id", model.Id.ToString()));

        public override Task<GEEnterpriseFiscalGroup> RemoveById(long id, string pEstablishmentKey, string pUniqueKey)
        {
            Errors.Add(ModelException.CreateValidationError(DefaultMessages.RemoveNotAllowed, "Id", id.ToString()));

            return Task.FromResult<GEEnterpriseFiscalGroup>(null);
        }
    }
}
