using Hino.API.Domain.Base.Services;
using Hino.API.Domain.Customers.Interfaces.Repositories;
using Hino.API.Domain.Customers.Interfaces.Services;
using Hino.API.Domain.Customers.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.API.Domain.Customers.Services
{
    public class GEEnterpriseContactsService : BaseService<GEEnterpriseContacts>, IGEEnterpriseContactsService
    {
        private readonly IGEEnterpriseContactsRepository _IGEEnterpriseContactsRepository;

        public GEEnterpriseContactsService(IGEEnterpriseContactsRepository pIGEEnterpriseContactsRepository) :
             base(pIGEEnterpriseContactsRepository)
        {
            _IGEEnterpriseContactsRepository = pIGEEnterpriseContactsRepository;
        }

        public async Task ClearEnterpriseContactsAsync(string pEstablishmentKey, long pEnterpriseId) =>
            await _IGEEnterpriseContactsRepository.ClearEnterpriseContactsAsync(pEstablishmentKey, pEnterpriseId);

        public async Task SaveEnterpriseContactsAsync(GEEnterprises pModel, IEnumerable<GEEnterpriseContacts> pEnterpriseContacts) =>
            await _IGEEnterpriseContactsRepository.SaveEnterpriseContactsAsync(pModel, pEnterpriseContacts);
    }
}
