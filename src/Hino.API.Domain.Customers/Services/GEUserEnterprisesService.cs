using Hino.API.Domain.Base.Services;
using Hino.API.Domain.Customers.Interfaces.Repositories;
using Hino.API.Domain.Customers.Interfaces.Services;
using Hino.API.Domain.Customers.Models;
using Hino.API.Infra.Cross.Utils.Paging;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hino.API.Domain.Customers.Services
{
    public class GEUserEnterprisesService : BaseService<GEUserEnterprises>, IGEUserEnterprisesService
    {
        private readonly IGEUserEnterprisesRepository _IGEUserEnterprisesRepository;

        public GEUserEnterprisesService(IGEUserEnterprisesRepository pIGEUserEnterprisesRepository) :
             base(pIGEUserEnterprisesRepository)
        {
            _IGEUserEnterprisesRepository = pIGEUserEnterprisesRepository;
        }

        public async Task<PagedResult<GEUserEnterprises>> QuerySearchPagedAsync(int page, int pageSize, Expression<Func<GEUserEnterprises, bool>> predicate, params Expression<Func<GEUserEnterprises, object>>[] includeProperties) =>
            await _IGEUserEnterprisesRepository.QuerySearchPagedAsync(page, pageSize, predicate, includeProperties);

        public async Task<GEUserEnterprises> CreateOrUpdateAsync(GEUserEnterprises enterprise)
        {
            var ent = await QueryAsync(r =>
               r.EstablishmentKey == enterprise.EstablishmentKey &&
               r.UserId == enterprise.UserId &&
               r.EnterpriseId == enterprise.EnterpriseId);

            if (!ent.Any())
            {
                var entNew = new GEUserEnterprises
                {
                    EstablishmentKey = enterprise.EstablishmentKey,
                    UserId = enterprise.UserId,
                    EnterpriseId = enterprise.EnterpriseId
                };

                this.Add(entNew);

                await SaveChanges();

                return entNew;
            }

            return enterprise;
        }

        public async Task<GEUserEnterprises[]> CreateOrUpdateListAsync(string pEstablishmentKey, GEUserEnterprises[] enterprise)
        {

            for (int i = 0, length = enterprise.Length; i < length; i++)
            {
                if (enterprise[i].Id < 0)
                    enterprise[i].Id = 0;

                enterprise[i].EstablishmentKey = pEstablishmentKey;
                enterprise[i].GEEnterprises = null;
                enterprise[i].GEUsers = null;

                enterprise[i] = await CreateOrUpdateAsync(enterprise[i]);
            }

            return enterprise;
        }

        public async Task<GEUserEnterprises[]> RemoveListAsync(string pEstablishmentKey, GEUserEnterprises[] enterprise)
        {
            for (int i = 0, length = enterprise.Length; i < length; i++)
            {
                if (enterprise[i].Id < 0)
                    enterprise[i].Id = 0;

                enterprise[i] = await RemoveById(
                    enterprise[i].Id,
                    pEstablishmentKey,
                    enterprise[i].UniqueKey
                    );
            }

            return enterprise;
        }
    }
}
