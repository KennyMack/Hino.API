﻿using Hino.API.Domain.Base.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.API.Domain.Customers.Models
{
    public class GEEnterpriseFiscalGroup : BaseEntity
    {
        public string Description { get; set; }
        public string Type { get; set; }
        public long IdERP { get; set; }
    }
}
