﻿using Hino.API.Domain.Base.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Domain.Customers.Models
{
    public class GEPaymentCondition: BaseEntity
    {
        public string Description { get; set; }

        [ForeignKey("GEPaymentType")]
        public long TypePayID { get; set; }
        public virtual GEPaymentType GEPaymentType { get; set; }
    }
}
