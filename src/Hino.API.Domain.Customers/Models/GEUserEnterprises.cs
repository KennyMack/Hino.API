﻿using Hino.API.Domain.Base.Models;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.API.Domain.Customers.Models
{
    public class GEUserEnterprises : BaseEntity
    {
        [ForeignKey("GEUsers")]
        public long UserId { get; set; }
        public virtual GEUsers GEUsers { get; set; }

        [ForeignKey("GEEnterprises")]
        public long EnterpriseId { get; set; }
        public virtual GEEnterprises GEEnterprises { get; set; }
    }
}
