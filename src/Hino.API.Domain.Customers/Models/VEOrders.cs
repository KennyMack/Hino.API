﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hino.API.Domain.Base.Models;
using Hino.API.Domain.Customers.Enums;
using Hino.API.Infra.Cross.Utils.Attributes;

namespace Hino.API.Domain.Customers.Models
{
    public class VEOrders : BaseEntity
    {
        public VEOrders()
        {
        }

        [ForeignKey("GEEnterprises")]
        [Column("ENTERPRISEID")]
        public long EnterpriseID { get; set; }
        public virtual GEEnterprises GEEnterprises { get; set; }

        [ForeignKey("GECarriers")]
        [Column("CARRIERID")]
        public long? CarrierID { get; set; }
        public virtual GEEnterprises GECarriers { get; set; }

        [ForeignKey("GERedispatch")]
        [Column("REDISPATCHID")]
        public long? RedispatchID { get; set; }
        public virtual GEEnterprises GERedispatch { get; set; }

        [ForeignKey("GEUsers")]
        public long UserID { get; set; }
        public virtual GEUsers GEUsers { get; set; }

        [ForeignKey("GEPaymentType")]
        public long TypePaymentID { get; set; }
        public virtual GEPaymentType GEPaymentType { get; set; }

        [ForeignKey("GEPaymentCondition")]
        public long PayConditionID { get; set; }
        public virtual GEPaymentCondition GEPaymentCondition { get; set; }

        public long CodPedVenda { get; set; }
        public long NumPedMob { get; set; }
        public DateTime DeliveryDate { get; set; }
        public string Note { get; set; }
        public string DetailedNote { get; set; }
        public string InnerNote { get; set; }
        public string Status { get; set; }
        public string StatusCRM { get; set; }
        public EStatusSinc StatusSinc { get; set; }
        public bool IsProposal { get; set; }
        public string ClientOrder { get; set; }

        public long IdERP { get; set; }
    }
}
