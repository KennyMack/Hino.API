﻿using Hino.API.Domain.Base.Models;
using Hino.API.Domain.Customers.Enums;
using Hino.API.Infra.Cross.Utils;
using Hino.API.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Domain.Customers.Models
{
    [Queue("GEEnterprises")]
    public class GEEnterprises : BaseEntity
    {
        public GEEnterprises()
        {
            VEOrders = new HashSet<VEOrders>();
            VEOrdersCarrier = new HashSet<VEOrders>();
            VEOrdersRedispatch = new HashSet<VEOrders>();
            GEEnterpriseGeo = new HashSet<GEEnterpriseGeo>();
            GEEnterpriseContacts = new HashSet<GEEnterpriseContacts>();
            GEUserEnterprises = new HashSet<GEUserEnterprises>();
        }

        public string RazaoSocial { get; set; }
        public string NomeFantasia { get; set; }
        public string CNPJCPF { get; set; }
        public string IE { get; set; }
        public EEnterpriseType Type { get; set; }
        public EStatusEnterprise Status { get; set; }
        public EStatusSinc StatusSinc { get; set; }
        public long IdERP { get; set; }
        public long RegionId { get; set; }
        public string Search { get; set; }
        public decimal CreditLimit { get; set; }
        public decimal CreditUsed { get; set; }
        public DateTime? BirthDate { get; set; }
        public EEnterpriseClassification Classification { get; set; }
        public string ClassifEmpresa { get; set; }

        [ForeignKey("GEUsersCreatedBy")]
        public long? CreatedById { get; set; }
        public virtual GEUsers GEUsersCreatedBy { get; set; }

        [ForeignKey("GEEnterpriseFiscalGroup")]
        public long? FiscalGroupId { get; set; }
        public virtual GEEnterpriseFiscalGroup GEEnterpriseFiscalGroup { get; set; }

        [ForeignKey("GEPaymentCondition")]
        public long? PayConditionId { get; set; }
        public virtual GEPaymentCondition GEPaymentCondition { get; set; }

        [ForeignKey("GEEnterpriseCategory")]
        public long? CategoryId { get; set; }
        public virtual GEEnterpriseCategory GEEnterpriseCategory { get; set; }

        [ForeignKey("GEEnterpriseGroup")]
        public long? GroupId { get; set; }
        public virtual GEEnterpriseGroup GEEnterpriseGroup { get; set; }

        [InverseProperty("GEEnterprises")]
        public virtual ICollection<VEOrders> VEOrders { get; set; }

        [InverseProperty("GECarriers")]
        public virtual ICollection<VEOrders> VEOrdersCarrier { get; set; }

        [InverseProperty("GERedispatch")]
        public virtual ICollection<VEOrders> VEOrdersRedispatch { get; set; }

        public virtual ICollection<GEEnterpriseGeo> GEEnterpriseGeo { get; set; }

        public virtual ICollection<GEEnterpriseContacts> GEEnterpriseContacts { get; set; }

        public virtual ICollection<GEUserEnterprises> GEUserEnterprises { get; set; }

        #region Formata IE
        public string FormatIE(string pUF)
        {
            try
            {
                if (!IE.IsEmpty() && IE != "ISENTO" && IE != "N/INFO" &&
                    pUF.ValidarUF())
                    return IE.CleanSpecialChar().FormatarIE(pUF);
            }
            catch (Exception)
            {
            }
            return IE;
        }
        #endregion
    }
}
