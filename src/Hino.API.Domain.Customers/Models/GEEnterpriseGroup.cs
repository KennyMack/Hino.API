﻿using Hino.API.Domain.Base.Models;
using Hino.API.Infra.Cross.Utils.Attributes;

namespace Hino.API.Domain.Customers.Models
{
    [Queue("GEEnterpriseGroup")]
    public class GEEnterpriseGroup : BaseEntity
    {
        public string Description { get; set; }
        public string Identifier { get; set; }
        public long IdERP { get; set; }
    }
}
