﻿using Hino.API.Domain.Base.Models;
using Hino.API.Domain.Customers.Enums;
using Hino.API.Infra.Cross.Utils.Enums;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.API.Domain.Customers.Models
{
    public class GEEnterpriseGeo : BaseEntity
    {
        [ForeignKey("GEEnterprises")]
        public long Enterpriseid { get; set; }
        public virtual GEEnterprises GEEnterprises { get; set; }
        public EAddressType Type { get; set; }
        public string CNPJCPF { get; set; }
        public string IE { get; set; }
        public string RG { get; set; }
        public string CellPhone { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Complement { get; set; }
        public string District { get; set; }
        public string Num { get; set; }
        public string ZipCode { get; set; }
        public string Site { get; set; }
        public string CountryIni { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public string UF { get; set; }
        public string StateName { get; set; }
        public string IBGE { get; set; }
        public string CityName { get; set; }
        public decimal DisplayLat { get; set; }
        public decimal DisplayLng { get; set; }
        public decimal NavLat { get; set; }
        public decimal NavLng { get; set; }
        public string InscSuframa { get; set; }

        [NotMapped]
        public string CodBacen { get; set; }
    }
}
