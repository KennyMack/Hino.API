﻿using Hino.API.Domain.Base.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Domain.Customers.Models
{
    public class GEUsers : BaseEntity
    {
        public string Name { get; set; }
    }
}
