﻿using Hino.API.Domain.Base.Models;
using Hino.API.Domain.Customers.Enums;
using Hino.API.Infra.Cross.Utils.Enums;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.API.Domain.Customers.Models
{
    public class GEEnterpriseContacts : BaseEntity
    {
        [ForeignKey("GEEnterprises")]
        public long EnterpriseId { get; set; }
        public virtual GEEnterprises GEEnterprises { get; set; }

        public EContactReceptivity ReceptivityIndex { get; set; }
        public EContactSectors Sector { get; set; }
        public string Email { get; set; }
        public string Contact { get; set; }
        public string Ramal { get; set; }
        public string Phone { get; set; }
        public string Note { get; set; }
    }
}
