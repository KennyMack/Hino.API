﻿using Hino.API.Domain.Routing.Models;
using Hino.API.Infra.Data.Repositories.Base.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Hino.API.Infra.Data.Repositories.Products.Context
{
    public class RoutingDbContext : BaseDbContext
    {
        public virtual DbSet<LORoutes> LORoutes { get; set; }
        public virtual DbSet<LOTrips> LOTrips { get; set; }
        public virtual DbSet<LOWaypoints> LOWaypoints { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            // modelBuilder.ApplyConfiguration(new GEUsersConfiguration("GEUSERS"));
        }
    }
}
