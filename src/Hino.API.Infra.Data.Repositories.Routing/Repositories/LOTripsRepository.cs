using Hino.API.Domain.Routing.Interfaces.Repositories;
using Hino.API.Domain.Routing.Models;
using Hino.API.Infra.Data.Repositories.Base.Repositories;
using Hino.API.Infra.Data.Repositories.Products.Context;

namespace Hino.API.Infra.Data.Repositories.Routing
{
    public class LOTripsRepository : BaseRepository<LOTrips>, ILOTripsRepository
    {
        public LOTripsRepository(RoutingDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
