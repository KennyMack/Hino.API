﻿using Hino.API.Application.MapperModels.Profiles;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Hino.API.Server.API.Mordor.Configurations
{
    public static class AutoMapperConfig
    {
        public static void AddAutoMapperConfiguration(this IServiceCollection services)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            services.AddAutoMapper(typeof(DomainToViewMordorProfile), typeof(ViewToDomainMordorProfile));
        }
    }
}
