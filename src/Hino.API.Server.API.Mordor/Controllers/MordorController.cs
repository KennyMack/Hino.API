﻿using Hino.API.Application.Interfaces.Services.Mordor;
using Hino.API.Application.ViewModel.Mordor;
using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Mordor.Interfaces.Services;
using Hino.API.Server.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using System;
using System.Threading.Tasks;

namespace Hino.API.Server.API.Mordor.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MordorController : BaseApiController
    {
        private readonly IAuthenticationAS _IAuthenticationAS;
        private readonly IFirebaseMordorService _IFirebaseMordorProvider;

        public MordorController(IFirebaseMordorService pIFirebaseMordorProvider,
            IAuthenticationAS pIAuthenticationAS)
        {
            _IAuthenticationAS = pIAuthenticationAS;
            _IFirebaseMordorProvider = pIFirebaseMordorProvider;
            Services = new IErrorBaseService[]
            {
                _IAuthenticationAS
            };
        }

        [HttpPost]
        [Route("sign-in")]
        public async Task<IActionResult> PostSignInUser(SignInUserVM userModel)
        {
            try
            {
                var result = await _IAuthenticationAS.SignInWithEmailAsync(userModel);

                return RequestResult(result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpPost]
        [Route("sign-up")]
        public async Task<IActionResult> PostSignOn(CreateUserVM userModel)
        {
            try
            {
                var createdUser = await _IAuthenticationAS.CreateUserAsync(userModel);

                return RequestResult(createdUser);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpGet]
        [Route("me")]
        [Authorize]
        public async Task<IActionResult> GetMe()
        {
            var TokenRequest = Request.Headers[HeaderNames.Authorization].ToString().Replace("Bearer", "").Trim();

            var decoded = await _IFirebaseMordorProvider.VerifyTokenAsync(TokenRequest);

            return Ok(decoded);
        }
    }
}
