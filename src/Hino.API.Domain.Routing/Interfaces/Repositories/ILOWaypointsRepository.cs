using Hino.API.Domain.Base.Interfaces.Repositories;
using Hino.API.Domain.Routing.Models;

namespace Hino.API.Domain.Routing.Interfaces.Repositories
{
    public interface ILOWaypointsRepository : IBaseRepository<LOWaypoints>
    {
    }
}
