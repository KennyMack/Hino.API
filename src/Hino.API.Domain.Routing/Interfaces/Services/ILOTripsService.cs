using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Routing.Models;

namespace Hino.API.Domain.Routing.Interfaces.Services
{
    public interface ILOTripsService : IBaseService<LOTrips>
    {
    }
}
