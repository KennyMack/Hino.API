﻿using System.ComponentModel.DataAnnotations.Schema;
using Hino.API.Domain.Base.Models;

namespace Hino.API.Domain.Routing.Models
{
    public class LOWaypoints : BaseEntity
    {
        [ForeignKey("LORoutes")]
        public long RouteID { get; set; }
        public virtual LORoutes LORoutes { get; set; }

        public decimal Lat { get; set; }
        public decimal Lng { get; set; }
        public decimal Distance { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public int TripIndex { get; set; }
        public int WayPointIndex { get; set; }
    }
}
