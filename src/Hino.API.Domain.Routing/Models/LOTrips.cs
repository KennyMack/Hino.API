﻿using Hino.API.Domain.Base.Models;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.API.Domain.Routing.Models
{
    public class LOTrips : BaseEntity
    {
        [ForeignKey("LORoutes")]
        public long RouteID { get; set; }
        public virtual LORoutes LORoutes { get; set; }

        public decimal Lat { get; set; }
        public decimal Lng { get; set; }
    }
}
