﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Domain.Routing.Models
{
    public class Map
    {
        public string features { get; set; }
        public string coordinates { get; set; }
    }
}
