﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Domain.Routing.Models
{
    public class MapPoint
    {
        public string type { get; set; }

        public MapGeometry geometry { get; set; }
        public MapPointPropery properties { get; set; }
    }

    public class MapRouteGeometry
    {
        public string type { get; set; }
        public List<decimal[]> coordinates { get; set; }

        public MapRouteGeometry()
        {
            coordinates = new List<decimal[]>();
        }
    }

    public class MapGeometry
    {
        public string type { get; set; }
        public List<decimal> coordinates { get; set; }

        public MapGeometry()
        {
            coordinates = new List<decimal>();
        }
    }

    public class MapPointPropery
    {
        public string title { get; set; }
        public string address { get; set; }
        public string icon { get; set; }
    }
}
