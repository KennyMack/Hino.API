﻿using System.Collections.Generic;
using Hino.API.Domain.Base.Models;

namespace Hino.API.Domain.Routing.Models
{
    public class LORoutes : BaseEntity
    {
        public LORoutes()
        {
            this.LOTrips = new HashSet<LOTrips>();
            this.LOWaypoints = new HashSet<LOWaypoints>();
        }
        public decimal Weigth { get; set; }
        public decimal Duration { get; set; }
        public decimal Distance { get; set; }
        public string Note { get; set; }


        public virtual ICollection<LOTrips> LOTrips { get; set; }
        public virtual ICollection<LOWaypoints> LOWaypoints { get; set; }
    }
}
