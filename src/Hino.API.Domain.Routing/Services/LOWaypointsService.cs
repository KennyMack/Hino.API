using Hino.API.Domain.Base.Services;
using Hino.API.Domain.Routing.Interfaces.Repositories;
using Hino.API.Domain.Routing.Interfaces.Services;
using Hino.API.Domain.Routing.Models;

namespace Hino.API.Domain.Routing.Services
{
    public class LOWaypointsService : BaseService<LOWaypoints>, ILOWaypointsService
    {
        private readonly ILOWaypointsRepository _ILOWaypointsRepository;

        public LOWaypointsService(ILOWaypointsRepository pILOWaypointsRepository) :
             base(pILOWaypointsRepository)
        {
            _ILOWaypointsRepository = pILOWaypointsRepository;
        }
    }
}
