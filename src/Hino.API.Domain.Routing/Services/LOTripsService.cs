using Hino.API.Domain.Base.Services;
using Hino.API.Domain.Routing.Interfaces.Repositories;
using Hino.API.Domain.Routing.Interfaces.Services;
using Hino.API.Domain.Routing.Models;

namespace Hino.API.Domain.Routing.Services
{
    public class LOTripsService : BaseService<LOTrips>, ILOTripsService
    {
        private readonly ILOTripsRepository _ILOTripsRepository;

        public LOTripsService(ILOTripsRepository pILOTripsRepository) :
             base(pILOTripsRepository)
        {
            _ILOTripsRepository = pILOTripsRepository;
        }
    }
}
