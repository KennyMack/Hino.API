using Hino.API.Domain.Base.Services;
using Hino.API.Domain.Routing.Interfaces.Repositories;
using Hino.API.Domain.Routing.Interfaces.Services;
using Hino.API.Domain.Routing.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Hino.API.Domain.Routing.Services
{
    public class LORoutesService : BaseService<LORoutes>, ILORoutesService
    {
        private readonly ILORoutesRepository _ILORoutesRepository;

        public LORoutesService(ILORoutesRepository pILORoutesRepository) :
             base(pILORoutesRepository)
        {
            _ILORoutesRepository = pILORoutesRepository;
        }

        public async Task<Map> GetMapJsonAsync(string pEstablishmentKey, long pId)
        {
            var maps = await _ILORoutesRepository.FirstOrDefaultAsync(r => r.Id == pId && r.EstablishmentKey == pEstablishmentKey,
                s => s.LOTrips,
                s => s.LOWaypoints);

            if (maps == null)
                return null;

            var lstWayPoints = new List<MapPoint>();
            var lstGeomertyTrips = new List<decimal[]>();

            foreach (var item in maps.LOWaypoints.OrderBy(r => r.WayPointIndex))
            {
                var geo = new MapGeometry
                {
                    type = "Point"
                };
                geo.coordinates.Add(item.Lng);
                geo.coordinates.Add(item.Lat);

                lstWayPoints.Add(new MapPoint
                {
                    type = "Feature",
                    geometry = geo,
                    properties = new MapPointPropery
                    {
                        title = item.Name,
                        address = item.Address,
                        icon = item.WayPointIndex == 0 ? "town-hall" : "information"
                    }
                });
            }

            foreach (var item in maps.LOTrips)
            {
                lstGeomertyTrips.Add(new decimal[] { item.Lng, item.Lat });
            }

            return new Map
            {
                coordinates = JsonSerializer.Serialize(lstGeomertyTrips),
                features = JsonSerializer.Serialize(lstWayPoints)
            };
        }
    }
}
