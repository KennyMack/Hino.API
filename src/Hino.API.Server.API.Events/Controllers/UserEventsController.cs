﻿using Hino.API.Application.Interfaces.Services.Events;
using Hino.API.Application.MapperModels;
using Hino.API.Application.ViewModel.Events;
using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Events.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using Hino.API.Infra.Cross.Utils.Paging;
using Hino.API.Server.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.API.Server.API.Events.Controllers
{
    [Route("api/Events/User/{pEstablishmentKey}")]
    [ApiController]
    [Authorize]
    public class UserEventsController : BaseApiController
    {
        private readonly IGEEventsAS _IGEEventsAS;

        public UserEventsController(
            IGEEventsAS pIGEEventsAS)
        {
            _IGEEventsAS = pIGEEventsAS;

            Services = new IErrorBaseService[]
            {
                _IGEEventsAS
            };
        }

        [Route("Id/{pId}/all")]
        [HttpGet]
        public async Task<IActionResult> GetAllByUser(string pEstablishmentKey, long pId)
        {
            DateTime dateStart = new(2000, 1, 1);
            DateTime dateEnd = new(2999, 1, 1);
            try
            {
                dateStart = ConvertDateUrl(QueryString["start"]) ?? new DateTime(2000, 1, 1);
            }
            catch (Exception)
            {
            }
            try
            {
                dateEnd = ConvertDateUrl(QueryString["end"]) ?? new DateTime(2000, 1, 1);
            }
            catch (Exception)
            {
            }

            try
            {
                var Results =
                    Mapper.Map<PagedResult<GEEvents>, PagedResult<GEEventsVM>>
                    (
                        await _IGEEventsAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                            r => r.EstablishmentKey == pEstablishmentKey &&
                            r.GEUserCalendar.Any(s => s.EstablishmentKey == pEstablishmentKey && 
                                s.UserID == pId &&
                                ((r.Start >= dateStart && r.Start <= dateEnd) ||
                                (r.End >= dateStart && r.End <= dateEnd)))
                            )
                    );

                if (Results == null)
                    throw new HinoException(_IGEEventsAS.Errors.FirstOrDefault());

                return RequestOK(Results);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IGEEventsAS.Dispose();
            }
        }
    }
}
