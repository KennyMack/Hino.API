﻿using Hino.API.Application.Interfaces.Services.Events;
using Hino.API.Application.MapperModels;
using Hino.API.Application.ViewModel.Events;
using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Events.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using Hino.API.Infra.Cross.Utils.Paging;
using Hino.API.Server.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.API.Server.API.Events.Controllers
{
    [Route("api/Events/{pEstablishmentKey}")]
    [ApiController]
    [Authorize]
    public class EventsController : BaseApiController
    {
        private readonly IGEUserCalendarAS _IGEUserCalendarAS;
        private readonly IGEEstabCalendarAS _IGEEstabCalendarAS;
        private readonly IGEEnterpriseEventAS _IGEEnterpriseEventAS;
        private readonly IGEEventsAS _IGEEventsAS;

        public EventsController(IGEUserCalendarAS pIGEUserCalendarAS,
            IGEEstabCalendarAS pIGEEstabCalendarAS,
            IGEEnterpriseEventAS pIGEEnterpriseEventAS,
            IGEEventsAS pIGEEventsAS)
        {
            _IGEUserCalendarAS = pIGEUserCalendarAS;
            _IGEEstabCalendarAS = pIGEEstabCalendarAS;
            _IGEEnterpriseEventAS = pIGEEnterpriseEventAS;
            _IGEEventsAS = pIGEEventsAS;

            Services = new IErrorBaseService[]
            {
                _IGEUserCalendarAS,
                _IGEEstabCalendarAS,
                _IGEEnterpriseEventAS,
                _IGEEventsAS
            };
        }

        [Route("all")]
        [HttpGet]
        public async Task<IActionResult> GetAll(string pEstablishmentKey)
        {
            DateTime dateStart = new(2000, 1, 1);
            DateTime dateEnd = new(2999, 1, 1);
            try
            {
                dateStart = ConvertDateUrl(QueryString["start"]) ?? new DateTime(2000, 1, 1);
            }
            catch (Exception)
            {
            }
            try
            {
                dateEnd = ConvertDateUrl(QueryString["end"]) ?? new DateTime(2000, 1, 1);
            }
            catch (Exception)
            {
            }

            try
            {
                var Results =
                    Mapper.Map<PagedResult<GEEvents>, PagedResult<GEEventsVM>>
                    (
                        await _IGEEventsAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                            r => r.EstablishmentKey == pEstablishmentKey &&
                                 r.DtCalendar >= dateStart && r.DtCalendar <= dateEnd
                        )
                    );

                if (Results == null)
                    throw new HinoException(_IGEEventsAS.Errors.FirstOrDefault());

                return RequestOK(Results);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IActionResult> GetById(string pEstablishmentKey, long pId)
        {
            try
            {
                var Result =
                    Mapper.Map<GEEvents, GEEventsVM>
                    (
                        await _IGEEventsAS.FirstOrDefaultAsync(
                            r => r.Id == pId &&
                                 r.EstablishmentKey == pEstablishmentKey,
                            s => s.GEEventsClassification)
                    );

                if (Result == null)
                    return NotFoundRequest();

                return RequestOK(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        public async Task<IActionResult> Put(string pUniqueKey, long id, [FromBody] GECreateEventsVM pGEEventsVM)
        {
            _IGEEventsAS.Errors.Clear();
            pGEEventsVM.Id = id;
            pGEEventsVM.UniqueKey = pUniqueKey;

            ValidateModelState(pGEEventsVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEEventsVM, ModelState);

            try
            {
                var Result = await _IGEEventsAS.UpdateEventAsync(pGEEventsVM.EstablishmentKey, pGEEventsVM);

                if (Result == null)
                    throw new HinoException(_IGEEventsAS.Errors.FirstOrDefault());

                await _IGEEventsAS.SaveChanges();

                return RequestResult(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpPost]
        [Route("save")]
        public async Task<IActionResult> Post(string pEstablishmentKey, [FromBody] GECreateEventsVM pGEEventsVM)
        {
            _IGEEventsAS.Errors.Clear();
            pGEEventsVM.EstablishmentKey = pEstablishmentKey;
            ValidateModelState(pGEEventsVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEEventsVM, ModelState);

            try
            {
                var Result = await _IGEEventsAS.CreateEventAsync(pEstablishmentKey, pGEEventsVM);

                if (Result == null)
                    throw new HinoException(_IGEEventsAS.Errors.FirstOrDefault());

                return RequestResult(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        public async Task<IActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IGEEventsAS.Errors.Clear();

            try
            {
                var Result = await _IGEEventsAS.RemoveById(pId, pEstablishmentKey, pUniqueKey);

                await _IGEEventsAS.SaveChanges();
                return RequestResult(Mapper.Map<GEEvents, GEEventsVM>(Result));
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpPost]
        [Route("key/{pUniqueKey}/id/{pId}/complete")]
        public async Task<IActionResult> PostCompleteAsync(string pEstablishmentKey, string pUniqueKey, long pId, [FromBody] GEEventsStatusVM pEventStatus)
        {
            _IGEEventsAS.Errors.Clear();
            pEventStatus.EstablishmentKey = pEstablishmentKey;
            ValidateModelState(pEventStatus);

            if (!ModelState.IsValid)
                return InvalidRequest(pEventStatus, ModelState);

            try
            {
                var Result = await _IGEEventsAS.ChangeEventStatusAsync(pId, pEstablishmentKey, pUniqueKey, pEventStatus);

                if (Result == null)
                    throw new HinoException(_IGEEventsAS.Errors.FirstOrDefault());

                return RequestResult(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpPost]
        [Route("key/{pUniqueKey}/id/{pId}/generate")]
        public async Task<IActionResult> PostGenerateAsync(string pEstablishmentKey, string pUniqueKey, long pId, [FromBody] GEEventsChildVM pGEEventsChild)
        {
            _IGEEventsAS.Errors.Clear();
            pGEEventsChild.EstablishmentKey = pEstablishmentKey;
            ValidateModelState(pGEEventsChild);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEEventsChild, ModelState);

            try
            {
                var Result = await _IGEEventsAS.GenerateEventChildAsync(pId, pEstablishmentKey, pUniqueKey, pGEEventsChild);

                if (Result == null)
                    throw new HinoException(_IGEEventsAS.Errors.FirstOrDefault());

                return RequestResult(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IGEUserCalendarAS.Dispose();
                _IGEEstabCalendarAS.Dispose();
                _IGEEnterpriseEventAS.Dispose();
                _IGEEventsAS.Dispose();
            }
        }
    }
}
