using Hino.API.Domain.Payments.Interfaces.Repositories;
using Hino.API.Domain.Payments.Models;
using Hino.API.Infra.Data.Repositories.Base.Repositories;
using Hino.API.Infra.Data.Repositories.Payments.Context;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.API.Infra.Data.Repositories.Payments.Repositories
{
    public class GEPaymentCondInstallmentsRepository : BaseRepository<GEPaymentCondInstallments>, IGEPaymentCondInstallmentsRepository
    {
        public GEPaymentCondInstallmentsRepository(PaymentsDbContext appDbContext) : base(appDbContext)
        {
        }

        public async Task ClearInstallmentsAsync(string pEstablishmentKey, long pCondPayId)
        {
            var installments = await QueryAsync(r => r.EstablishmentKey == pEstablishmentKey && r.CondPayId == pCondPayId);

            foreach (var item in installments)
                await RemoveById(item.Id, pEstablishmentKey, item.UniqueKey);

            await SaveChanges();
        }

        public async Task SaveInstallmentsAsync(GEPaymentCondition pModel, IEnumerable<GEPaymentCondInstallments> pInstallments)
        {
            foreach (var item in pInstallments)
            {
                item.EstablishmentKey = pModel.EstablishmentKey;
                item.CondPayId = pModel.Id;

                Add(new GEPaymentCondInstallments
                {
                    Id = 0,
                    Days = item.Days,
                    CondPayId = pModel.Id,
                    EstablishmentKey = pModel.EstablishmentKey,
                    Percent = item.Percent
                });
            }

            await SaveChanges();
        }
    }
}
