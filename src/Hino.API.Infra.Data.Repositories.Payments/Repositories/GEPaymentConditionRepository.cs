using Hino.API.Domain.Payments.Interfaces.Repositories;
using Hino.API.Domain.Payments.Models;
using Hino.API.Infra.Data.Repositories.Base.Repositories;
using Hino.API.Infra.Data.Repositories.Payments.Context;

namespace Hino.API.Infra.Data.Repositories.Payments.Repositories
{
    public class GEPaymentConditionRepository : BaseRepository<GEPaymentCondition>, IGEPaymentConditionRepository
    {
        public GEPaymentConditionRepository(PaymentsDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
