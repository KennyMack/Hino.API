﻿using Hino.API.Domain.Payments.Models;
using Hino.API.Infra.Data.Repositories.Base.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Hino.API.Infra.Data.Repositories.Payments.Context
{
    public class PaymentsDbContext : BaseDbContext
    {
        public virtual DbSet<GEPaymentCondInstallments> GEPaymentCondInstallments { get; set; }
        public virtual DbSet<GEPaymentCondition> GEPaymentCondition { get; set; }
        public virtual DbSet<GEPaymentType> GEPaymentType { get; set; }
        public virtual DbSet<GEUserPayType> GEUserPayType { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            // modelBuilder.ApplyConfiguration(new GEUsersConfiguration("GEUSERS"));
        }
    }
}
