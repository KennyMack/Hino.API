﻿using Hino.API.Application.Interfaces.Services.Taxes;
using Hino.API.Application.MapperModels;
using Hino.API.Application.ViewModel.Taxes;
using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Taxes.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using Hino.API.Infra.Cross.Utils.Paging;
using Hino.API.Server.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.API.Server.API.Taxes.Controllers
{
    [Route("api/Taxes/NCM/{pEstablishmentKey}")]
    [ApiController]
    [Authorize]
    public class NCMController : BaseApiController
    {
        private readonly IFSNCMAS _IFSNCMAS;

        public NCMController(IFSNCMAS pIFSNCMAS)
        {
            _IFSNCMAS = pIFSNCMAS;
            Services = new IErrorBaseService[]
            {
                _IFSNCMAS
            };
        }

        [Route("all")]
        [HttpGet]
        public async Task<IActionResult> Get(string pEstablishmentKey)
        {
            try
            {
                var Results =
                Mapper.Map<PagedResult<FSNCM>, PagedResult<FSNCMVM>>
                (
                    await _IFSNCMAS.GetAllPagedAsync(GetPageNumber(), GetLimitNumber())
                );

                if (Results == null)
                    throw new HinoException(_IFSNCMAS.Errors.FirstOrDefault());
                
                return RequestOK(Results);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IActionResult> GetById(string pEstablishmentKey, long pId)
        {
            var Results =
                Mapper.Map<PagedResult<FSNCM>, PagedResult<FSNCMVM>>
                (
                await _IFSNCMAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.Id == pId));

            if (Results == null)
                return InvalidRequest(null, _IFSNCMAS.Errors);

            if (Results.Results.Count <= 0)
                return NotFoundRequest();

            return RequestOK(Results);
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IFSNCMAS.Dispose();
            }
        }
    }
}
