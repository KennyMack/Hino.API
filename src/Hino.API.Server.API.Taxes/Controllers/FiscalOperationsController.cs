﻿using Hino.API.Application.Interfaces.Services.Taxes;
using Hino.API.Application.MapperModels;
using Hino.API.Application.ViewModel.Taxes;
using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Taxes.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using Hino.API.Infra.Cross.Utils.Paging;
using Hino.API.Server.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.API.Server.API.Taxes.Controllers
{
    [Route("api/Fiscal/Operation/{pEstablishmentKey}")]
    [ApiController]
    [Authorize]
    public class FiscalOperationsController : BaseApiController
    {
        private readonly IFSFiscalOperAS _IFSFiscalOperAS;
        public FiscalOperationsController(IFSFiscalOperAS pIFSFiscalOperAS)
        {
            _IFSFiscalOperAS = pIFSFiscalOperAS;
            Services = new IErrorBaseService[]
            {
                _IFSFiscalOperAS
            };
        }

        [Route("all")]
        [HttpGet]
        public async Task<IActionResult> Get(string pEstablishmentKey)
        {
            try
            {
                var filter = GetQueryFilter();

                var Results =
                Mapper.Map<PagedResult<FSFiscalOper>, PagedResult<FSFiscalOperVM>>
                (
                    await _IFSFiscalOperAS.GetAllPagedAsync(GetPageNumber(), GetLimitNumber(),
                        r => r.EstablishmentKey == pEstablishmentKey &&
                        r.Description.ToUpper().Contains(filter.Trim())
                    )
                );

                if (Results == null)
                    throw new HinoException(_IFSFiscalOperAS.Errors.FirstOrDefault());

                return RequestOK(Results);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IActionResult> GetById(string pEstablishmentKey, long pId)
        {
            try
            {
                var Result =
                    Mapper.Map<FSFiscalOper, FSFiscalOperVM>
                    (
                        await _IFSFiscalOperAS.FirstOrDefaultAsync(
                                r => r.EstablishmentKey == pEstablishmentKey &&
                                r.Id == pId)
                    );

                if (Result == null)
                    return NotFoundRequest();

                return RequestOK(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IFSFiscalOperAS.Dispose();
            }
        }
    }
}
