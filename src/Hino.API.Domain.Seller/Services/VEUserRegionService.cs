using Hino.API.Domain.Base.Services;
using Hino.API.Domain.Seller.Interfaces.Repositories;
using Hino.API.Domain.Seller.Interfaces.Services;
using Hino.API.Domain.Seller.Models;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.API.Domain.Seller.Services
{
    public class VEUserRegionService : BaseService<VEUserRegion>, IVEUserRegionService
    {
        private readonly IVEUserRegionRepository _IVEUserRegionRepository;

        public VEUserRegionService(IVEUserRegionRepository pIVEUserRegionRepository) :
             base(pIVEUserRegionRepository)
        {
            _IVEUserRegionRepository = pIVEUserRegionRepository;
        }

        public async Task<VEUserRegion> CreateOrUpdateAsync(VEUserRegion userRegion)
        {
            var ent = await QueryAsync(r =>
                r.EstablishmentKey == userRegion.EstablishmentKey &&
                r.UserId == userRegion.UserId &&
                r.SaleWorkId == userRegion.SaleWorkId);

            if (!ent.Any())
            {
                var entNew = new VEUserRegion
                {
                    EstablishmentKey = userRegion.EstablishmentKey,
                    UserId = userRegion.UserId,
                    SaleWorkId = userRegion.SaleWorkId
                };

                Add(entNew);

                await SaveChanges();

                return entNew;
            }

            return userRegion;
        }

        public async Task<VEUserRegion[]> CreateOrUpdateListAsync(string pEstablishmentKey, VEUserRegion[] userRegion)
        {
            for (int i = 0, length = userRegion.Length; i < length; i++)
            {
                if (userRegion[i].Id < 0)
                    userRegion[i].Id = 0;

                userRegion[i].EstablishmentKey = pEstablishmentKey;
                userRegion[i].VESaleWorkRegion = null;
                userRegion[i].GEUsers = null;

                userRegion[i] = await CreateOrUpdateAsync(userRegion[i]);
            }

            return userRegion;
        }

        public async Task<VEUserRegion[]> RemoveListAsync(string pEstablishmentKey, VEUserRegion[] userRegion)
        {
            for (int i = 0, length = userRegion.Length; i < length; i++)
            {
                if (userRegion[i].Id < 0)
                    userRegion[i].Id = 0;

                userRegion[i] = await RemoveById(
                    userRegion[i].Id,
                    pEstablishmentKey,
                    userRegion[i].UniqueKey);
            }

            await SaveChanges();

            return userRegion;
        }

        public bool ZipCodeExists(string pEstablishmentKey, long pUserId, string pZipCode) =>
            _IVEUserRegionRepository.ZipCodeExists(pEstablishmentKey, pUserId, pZipCode);
    }
}
