using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Seller.Models;
using System.Threading.Tasks;

namespace Hino.API.Domain.Seller.Interfaces.Services
{
    public interface IVEUserRegionService : IBaseService<VEUserRegion>
    {
        bool ZipCodeExists(string pEstablishmentKey, long pUserId, string pZipCode);
        Task<VEUserRegion> CreateOrUpdateAsync(VEUserRegion userRegion);
        Task<VEUserRegion[]> CreateOrUpdateListAsync(string pEstablishmentKey, VEUserRegion[] userRegion);
        Task<VEUserRegion[]> RemoveListAsync(string pEstablishmentKey, VEUserRegion[] userRegion);
    }
}
