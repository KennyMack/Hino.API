using Hino.API.Domain.Base.Interfaces.Repositories;
using Hino.API.Domain.Seller.Models;

namespace Hino.API.Domain.Seller.Interfaces.Repositories
{
    public interface IVEUserRegionRepository : IBaseRepository<VEUserRegion>
    {
        bool ZipCodeExists(string pEstablishmentKey, long pUserId, string pZipCode);
    }
}
