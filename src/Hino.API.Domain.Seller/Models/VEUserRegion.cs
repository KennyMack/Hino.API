﻿
using Hino.API.Domain.Base.Models;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.API.Domain.Seller.Models
{
    public class VEUserRegion : BaseEntity
    {
        [ForeignKey("GEUsers")]
        public long UserId { get; set; }
        public virtual VESeller GEUsers { get; set; }

        [ForeignKey("VESaleWorkRegion")]
        public long SaleWorkId { get; set; }
        public virtual VESaleWorkRegion VESaleWorkRegion { get; set; }
    }
}
