﻿using Hino.API.Domain.Base.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Domain.Seller.Models
{
    [Table("GEUSERS")]
    public class VESeller: BaseEntity
    {
        public string Name { get; set; }
        public decimal PercDiscount { get; set; }
        public decimal PercCommission { get; set; }
        public decimal PercDiscountPrice { get; set; }
        public decimal PercIncreasePrice { get; set; }
        public string StoreId { get; set; }
        public string TerminalId { get; set; }
        public string UserKey { get; set; }
        public bool IsBlockedByPay { get; set; }
    }
}
