﻿using Hino.API.Domain.Base.Interfaces.Models;
using Hino.API.Domain.Base.Services;
using Hino.API.Domain.Files.Interfaces.Repositories;
using Hino.API.Domain.Files.Interfaces.Services;
using Hino.API.Domain.Files.Models;
using Hino.API.Infra.Cross.Utils;
using Hino.API.Infra.Cross.Utils.Exceptions;
using Hino.API.Infra.Cross.Utils.Files;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Domain.Files.Services
{
    public class GEFilesPathService : BaseService<GEFilesPath>, IGEFilesPathService
    {
        private readonly IGEFilesPathRepository _IGEFilesPathRepository;
        private readonly IHostingEnvironment _IHostingEnvironment;

        public GEFilesPathService(
            IHostingEnvironment pIHostingEnvironment,
            IGEFilesPathRepository pIGEFilesPathRepository) :
             base(pIGEFilesPathRepository)
        {
            _IHostingEnvironment = pIHostingEnvironment;
            _IGEFilesPathRepository = pIGEFilesPathRepository;
        }

        private string GenerateUploadFolder(UploadDestinationFolder pFolder, string pUploadFolder = "")
        {
            var destination = _IHostingEnvironment.GenerateDestinationFolder(pFolder);

            if (destination.IsEmpty())
            {
                Errors.Add(ModelException.CreateValidationError(Infra.Cross.Resources.MessagesResource.UploadFolderIsNotCreated, "GEFilesPath", ""));

                return null;
            }

            if (!pUploadFolder.IsEmpty())
            {
                var UploadDir = Path.Combine(destination, pUploadFolder);

                if (!Directory.Exists(UploadDir))
                    Directory.CreateDirectory(UploadDir);

                destination = UploadDir;
            }
            return destination;
        }

        public async Task<GEResultUploadFile> UploadFilesAsync(UploadDestinationFolder pFolder, IFormFile pFiles, IBaseEntity pModel)
        {
            if (pFolder == UploadDestinationFolder.NotIdentified)
            {
                Errors.Add(ModelException.CreateValidationError(Infra.Cross.Resources.ValidationMessagesResource.FileNotFound, "GEFilesPath", ""));

                return null;
            }

            var destination = GenerateUploadFolder(pFolder, Guid.NewGuid().ToString("N"));

            if (Errors.Any())
                return null;

            // var result = new GEResultUploadFile();
            await Task.Delay(1);

            throw new NotImplementedException("Não feito");
            /*
            foreach (string file in pFiles)
            {
                var item = new GEResultUploadItem();
                HttpPostedFile postedFile = null;
                string currpath = "";
                string AwsKey = "";
                try
                {
                    postedFile = pFiles.FileName;
                    currpath = Path.Combine(destination, postedFile.FileName);
                    item.FileName = postedFile.FileName;
                    item.Identifier = Guid.NewGuid().ToString();

                    postedFile.SaveAs(currpath);
                    FileInfo fi = new FileInfo(currpath);
                    item.Extension = fi.Extension;
                    item.FileSize = fi.Length;
                    item.Success = true;
                    item.MimeType =  MimeMapping.GetMimeMapping(currpath);
                    var date = DateTime.Now.ToString("ddMMyyyyHHmm");
                    AwsKey = $@"{pModel.EstablishmentKey}{pFolder}{item.Identifier}{date}";
                }
                catch (System.Exception ex)
                {
                    Logging.Exception(ex);
                    item.Success = false;
                    item.ErrorMessage = ex.Message;
                }

                if (item.Success)
                {
                    _IAWSFileAS.Errors.Clear();
                    if (!await _IAWSFileAS.UploadFileAsync(currpath, AwsKey))
                    {
                        item.Success = false;
                        Errors.Add(_IAWSFileAS.Errors.FirstOrDefault());
                    }
                    
                }

                if (item.Success)
                {
                    var FilesPath = new GEFilesPath
                    {
                        EstablishmentKey = pModel.EstablishmentKey,
                        Description = "",
                        Name = postedFile.FileName,
                        Identifier = item.Identifier,
                        FileSize = item.FileSize,
                        MimeType = item.MimeType,
                        Path = AwsKey,
                        Extension = item.Extension
                    };

                    switch (pFolder)
                    {
                        case UploadDestinationFolder.Users:
                            FilesPath.UserID = pModel.Id;
                            break;
                        case UploadDestinationFolder.Products:
                            FilesPath.ProductID = pModel.Id;
                            break;
                        case UploadDestinationFolder.Enterprises:
                            FilesPath.EnterpriseID = pModel.Id;
                            break;
                        case UploadDestinationFolder.Establishments:
                            FilesPath.GEEstabID = pModel.Id;
                            break;
                        case UploadDestinationFolder.Order:
                            FilesPath.OrderID = pModel.Id;
                            break;
                        case UploadDestinationFolder.OrderItem:
                            FilesPath.OrderItemID = pModel.Id;
                            break;
                    }

                    Add(FilesPath);

                    await SaveChanges();
                }

                result.Files.Add(item);
            }
            

            try
            {
                Directory.Delete(destination, true);
            }
            catch (Exception)
            {

            }

            return result;*/
        }

        public async Task<GEResultUploadItem> DeleteFileAsync(UploadDestinationFolder pFolder, string pEstablishmentKey, string pUniqueKey, long id, GEResultUploadItem pFileToRemove)
        {
            if (pFolder == UploadDestinationFolder.NotIdentified)
            {
                Errors.Add(ModelException.CreateNotFoundError(Infra.Cross.Resources.ValidationMessagesResource.FileNotFound, "GEFilesPath", id.ToString()));
                return null;
            }

            var destination = GenerateUploadFolder(pFolder);

            if (Errors.Any())
                return null;

            IEnumerable<GEFilesPath> files = null;

            switch (pFolder)
            {
                case UploadDestinationFolder.Users:
                    files = await QueryAsync(r =>
                        r.UserID == id &&
                        r.EstablishmentKey == pEstablishmentKey &&
                        r.Path == pFileToRemove.Path &&
                        r.Identifier == pFileToRemove.Identifier);
                    break;
                case UploadDestinationFolder.Products:
                    files = await QueryAsync(r =>
                        r.ProductID == id &&
                        r.EstablishmentKey == pEstablishmentKey &&
                        r.Path == pFileToRemove.Path &&
                        r.Identifier == pFileToRemove.Identifier);
                    break;
                case UploadDestinationFolder.Enterprises:
                    files = await QueryAsync(r =>
                        r.EnterpriseID == id &&
                        r.EstablishmentKey == pEstablishmentKey &&
                        r.Path == pFileToRemove.Path &&
                        r.Identifier == pFileToRemove.Identifier);
                    break;
                case UploadDestinationFolder.Establishments:
                    files = await QueryAsync(r =>
                        r.GEEstabID == id &&
                        r.EstablishmentKey == pEstablishmentKey &&
                        r.Path == pFileToRemove.Path &&
                        r.Identifier == pFileToRemove.Identifier);
                    break;
                case UploadDestinationFolder.Order:
                    files = await QueryAsync(r =>
                        r.OrderID == id &&
                        r.EstablishmentKey == pEstablishmentKey &&
                        r.Path == pFileToRemove.Path &&
                        r.Identifier == pFileToRemove.Identifier);
                    break;
                case UploadDestinationFolder.OrderItem:
                    files = await QueryAsync(r =>
                        r.OrderItemID == id &&
                        r.EstablishmentKey == pEstablishmentKey &&
                        r.Path == pFileToRemove.Path &&
                        r.Identifier == pFileToRemove.Identifier);
                    break;
            }

            if (files == null || !files.Any())
            {
                Errors.Add(ModelException.CreateNotFoundError(Infra.Cross.Resources.ValidationMessagesResource.FileNotFound, "GEFilesPath", ""));

                return null;
            }

            try
            {
                foreach (var item in files)
                {
                    var currpath = Path.Combine(destination, item.Name);
                    UploadFolder.RemoveFileByPath(currpath);

                    /*
                    if (!await _IAWSFileAS.DeleteFileAsync(item.Path))
                    {
                        Errors.Add(_IAWSFileAS.Errors.FirstOrDefault());
                    }
                    */
                    if (!Errors.Any())
                        await RemoveById(item.Id, item.EstablishmentKey, item.UniqueKey);
                }
            }
            catch (Exception ex)
            {
                Logging.Exception(ex);
                Errors.Add(ModelException.CreateValidationError(ex.Message, "GEFilesPath", pFileToRemove.Path));

                return null;
            }

            if (!Errors.Any())
                await SaveChanges();

            pFileToRemove.Success = !Errors.Any();

            return pFileToRemove;
        }

        public async Task<string> DownloadFileAsync(string pEstablishmentKey, string pFilePath)
        {
            var file = (await QueryAsync(r => r.EstablishmentKey == pEstablishmentKey &&
                r.Path == pFilePath)).FirstOrDefault();

            if (file == null)
            {
                Errors.Add(ModelException.CreateNotFoundError(Infra.Cross.Resources.ValidationMessagesResource.FileNotFound, "Path", pFilePath));

                return null;
            }

            throw new NotImplementedException("Não feito");
            /*
            var url = _IAWSFileAS.DownloadFileAsync(pFilePath);

            if (url.IsEmpty())
            {
                Errors.Add(ModelException.CreateValidationError(Infra.Cross.Resources.ValidationMessagesResource.FileNotFound, "Path", pFilePath));

                return null;
            }

            return url;
            */
        }

    }
}
