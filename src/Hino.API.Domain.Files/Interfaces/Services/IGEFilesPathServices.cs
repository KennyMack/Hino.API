﻿using Hino.API.Domain.Base.Interfaces.Models;
using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Files.Models;
using Hino.API.Infra.Cross.Utils.Files;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace Hino.API.Domain.Files.Interfaces.Services
{
    public interface IGEFilesPathService : IBaseService<GEFilesPath>
    {
        Task<GEResultUploadFile> UploadFilesAsync(UploadDestinationFolder pFolder, IFormFile pFiles, IBaseEntity pModel);
        Task<GEResultUploadItem> DeleteFileAsync(UploadDestinationFolder pFolder, string pEstablishmentKey, string pUniqueKey, long id, GEResultUploadItem pFileToRemove);
        Task<string> DownloadFileAsync(string pEstablishmentKey, string pFilePath);
    }
}
