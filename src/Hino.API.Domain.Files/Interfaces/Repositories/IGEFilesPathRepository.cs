﻿using Hino.API.Domain.Base.Interfaces.Repositories;
using Hino.API.Domain.Files.Models;

namespace Hino.API.Domain.Files.Interfaces.Repositories
{
    public interface IGEFilesPathRepository : IBaseRepository<GEFilesPath>
    {
    }
}
