﻿using Hino.API.Domain.Base.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Domain.Files.Models
{
    public class GEFilesPath : BaseEntity
    {
        public long? GEEstabID { get; set; }
        public long? EnterpriseID { get; set; }
        public long? UserID { get; set; }
        public long? ProductID { get; set; }
        public long? OrderID { get; set; }
        public long? OrderItemID { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public string Description { get; set; }
        public string MimeType { get; set; }
        public string Extension { get; set; }
        public long FileSize { get; set; }
        public string Identifier { get; set; }
    }
}
