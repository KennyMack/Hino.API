﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Domain.Files.Models
{
    public class GEResultUploadItem
    {
        public string FileName { get; set; }
        public string Identifier { get; set; }
        public string Path { get; set; }
        public bool Success { get; set; }
        public string MimeType { get; set; }
        public string Extension { get; set; }
        public long FileSize { get; set; }
        public string ErrorMessage { get; set; }
    }
}
