﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Domain.Files.Models
{
    public class GEResultUploadFile
    {
        public GEResultUploadFile()
        {
            Files = new List<GEResultUploadItem>();
        }

        public List<GEResultUploadItem> Files { get; set; }
    }
}
