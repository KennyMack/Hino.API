﻿using Hino.API.Application.Interfaces.Services.SalePrices;
using Hino.API.Application.MapperModels;
using Hino.API.Application.ViewModel.SalePrices;
using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.SalePrices.Models;
using Hino.API.Infra.Cross.Utils.Paging;
using Hino.API.Server.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.API.Server.API.Products.Controllers
{
    [Route("api/Sale/Price/{pEstablishmentKey}")]
    [ApiController]
    [Authorize]
    public class SalePriceController : BaseApiController
    {
        private readonly IVESalePriceAS _IVESalePriceAS;

        public SalePriceController(IVESalePriceAS pIVESalePriceAS)
        {
            _IVESalePriceAS = pIVESalePriceAS;

            Services = new IErrorBaseService[]
            {
                _IVESalePriceAS
            };
        }

        [Route("all")]
        [HttpGet]
        public async Task<IActionResult> GetAll(string pEstablishmentKey)
        {
            try
            {
                var Results =
                    Mapper.Map<PagedResult<VESalePrice>, PagedResult<VESalePriceVM>>
                    (
                        await _IVESalePriceAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                            r => r.EstablishmentKey == pEstablishmentKey
                        )
                    );

                return RequestOK(Results);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IActionResult> GetById(string pEstablishmentKey, long pId)
        {
            try
            {
                var Result =
                    Mapper.Map<VESalePrice, VESalePriceVM>
                    (
                        await _IVESalePriceAS.FirstOrDefaultAsync(
                            r => r.Id == pId &&
                                 r.EstablishmentKey == pEstablishmentKey)
                    );

                if (Result == null)
                    return NotFoundRequest();

                return RequestOK(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [Route("region/{pRegionId}/payCondition/{pPayConditionId}/user/{pUserId}/product/{pProductId}")]
        [HttpGet]
        public async Task<IActionResult> GetByRegionAndProductId(string pEstablishmentKey, long pRegionId, long pPayConditionId, long pUserId, long pProductId)
        {
            try
            {
                var Results = await _IVESalePriceAS.GetProductSalePrice(
                    pEstablishmentKey, pProductId, pRegionId, pPayConditionId, pUserId);

                if (Results == null && _IVESalePriceAS.Errors.Any())
                    return InvalidRequest(null, _IVESalePriceAS.Errors);

                return RequestResult(Results);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [Route("region/{pRegionId}/payCondition/{pPayConditionId}/user/{pUserId}/all")]
        [HttpGet]
        public async Task<IActionResult> GetAllRegionPrice(string pEstablishmentKey, long pRegionId, long pPayConditionId, long pUserId)
        {
            try
            {
                var filter = GetQueryFilter();
                var Results = await _IVESalePriceAS.GetAllSalePrice(
                    GetPageNumber(), GetLimitNumber(),
                    pEstablishmentKey, pRegionId, 
                    pPayConditionId, pUserId,
                    filter);

                if (Results == null && _IVESalePriceAS.Errors.Any())
                    return InvalidRequest(null, _IVESalePriceAS.Errors);

                return RequestResult(Results);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        public async Task<IActionResult> Put(string pUniqueKey, long id, [FromBody] VESalePriceVM pVESalePriceVM)
        {
            _IVESalePriceAS.Errors.Clear();
            pVESalePriceVM.Id = id;
            pVESalePriceVM.UniqueKey = pUniqueKey;

            ValidateModelState(pVESalePriceVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pVESalePriceVM, ModelState);

            try
            {
                var Result = _IVESalePriceAS.Update(
                    Mapper.Map<VESalePriceVM, VESalePrice>(pVESalePriceVM));

                if (Result != null && !_IVESalePriceAS.Errors.Any())
                    await _IVESalePriceAS.SaveChanges();

                return RequestResult(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpPost]
        [Route("save")]
        public async Task<IActionResult> Post(string pEstablishmentKey, [FromBody] VESalePriceVM pVESalePriceVM)
        {
            _IVESalePriceAS.Errors.Clear();
            pVESalePriceVM.EstablishmentKey = pEstablishmentKey;
            ValidateModelState(pVESalePriceVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pVESalePriceVM, ModelState);

            try
            {
                var Result = _IVESalePriceAS.CreateSalePrice(pVESalePriceVM);

                if (Result != null && !_IVESalePriceAS.Errors.Any())
                    await _IVESalePriceAS.SaveChanges();

                return RequestResult(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        public async Task<IActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IVESalePriceAS.Errors.Clear();

            try
            {
                var Result = await _IVESalePriceAS.RemoveById(pId, pEstablishmentKey, pUniqueKey);

                if (Result != null && !_IVESalePriceAS.Errors.Any())
                    await _IVESalePriceAS.SaveChanges();

                return RequestResult(Mapper.Map<VESalePrice, VESalePriceVM>(Result));
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IVESalePriceAS.Dispose();
            }
        }
    }
}
