﻿using Hino.API.Application.Interfaces.Services.SalePrices;
using Hino.API.Application.MapperModels;
using Hino.API.Application.ViewModel.SalePrices;
using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.SalePrices.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using Hino.API.Infra.Cross.Utils.Paging;
using Hino.API.Server.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.API.Server.API.SalePrices.Controllers
{
    [Route("api/Region/Sale/UF/{pEstablishmentKey}")]
    [ApiController]
    [Authorize]
    public class RegionSaleUFController : BaseApiController
    {
        private readonly IVERegionSaleUFAS _IVERegionSaleUFAS;

        public RegionSaleUFController(IVERegionSaleUFAS pIVERegionSaleUFAS)
        {
            _IVERegionSaleUFAS = pIVERegionSaleUFAS;

            Services = new IErrorBaseService[]
            {
                _IVERegionSaleUFAS
            };
        }

        [Route("all")]
        [HttpGet]
        public async Task<IActionResult> GetAll(string pEstablishmentKey)
        {
            try
            {
                var Results =
                    Mapper.Map<PagedResult<VERegionSaleUF>, PagedResult<VERegionSaleUFVM>>
                    (
                        await _IVERegionSaleUFAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                            r => r.EstablishmentKey == pEstablishmentKey
                        )
                    );

                if (Results == null)
                    throw new HinoException(_IVERegionSaleUFAS.Errors.FirstOrDefault());

                return RequestOK(Results);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IActionResult> GetById(string pEstablishmentKey, long pId)
        {
            try
            {
                var Result =
                    Mapper.Map<VERegionSaleUF, VERegionSaleUFVM>
                    (
                        await _IVERegionSaleUFAS.FirstOrDefaultAsync(
                            r => r.Id == pId &&
                                 r.EstablishmentKey == pEstablishmentKey)
                    );

                if (Result == null)
                    return NotFoundRequest();

                return RequestOK(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        public async Task<IActionResult> Put(string pUniqueKey, long id, [FromBody] VERegionSaleUFVM pVERegionSaleVM)
        {
            _IVERegionSaleUFAS.Errors.Clear();
            pVERegionSaleVM.Id = id;
            pVERegionSaleVM.UniqueKey = pUniqueKey;

            ValidateModelState(pVERegionSaleVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pVERegionSaleVM, ModelState);

            try
            {
                var Result = _IVERegionSaleUFAS.Update(
                    Mapper.Map<VERegionSaleUFVM, VERegionSaleUF>(pVERegionSaleVM));

                if (Result != null && !_IVERegionSaleUFAS.Errors.Any())
                    await _IVERegionSaleUFAS.SaveChanges();

                return RequestResult(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpPost]
        [Route("save")]
        public async Task<IActionResult> Post(string pEstablishmentKey, [FromBody] VERegionSaleUFVM pVERegionSaleVM)
        {
            _IVERegionSaleUFAS.Errors.Clear();
            pVERegionSaleVM.EstablishmentKey = pEstablishmentKey;
            ValidateModelState(pVERegionSaleVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pVERegionSaleVM, ModelState);

            try
            {
                var Result = _IVERegionSaleUFAS.Add(
                    Mapper.Map<VERegionSaleUFVM, VERegionSaleUF>(pVERegionSaleVM));

                if (Result != null && !_IVERegionSaleUFAS.Errors.Any())
                    await _IVERegionSaleUFAS.SaveChanges();

                return RequestResult(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        public async Task<IActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IVERegionSaleUFAS.Errors.Clear();

            try
            {
                var Result = await _IVERegionSaleUFAS.RemoveById(pId, pEstablishmentKey, pUniqueKey);

                if (Result != null && !_IVERegionSaleUFAS.Errors.Any())
                    await _IVERegionSaleUFAS.SaveChanges();

                return RequestResult(Mapper.Map<VERegionSaleUF, VERegionSaleUFVM>(Result));
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IVERegionSaleUFAS.Dispose();
            }
        }
    }
}
