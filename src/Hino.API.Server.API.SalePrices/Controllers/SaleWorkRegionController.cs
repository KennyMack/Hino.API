﻿using Hino.API.Application.Interfaces.Services.SalePrices;
using Hino.API.Application.MapperModels;
using Hino.API.Application.ViewModel.SalePrices;
using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.SalePrices.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using Hino.API.Infra.Cross.Utils.Paging;
using Hino.API.Server.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.API.Server.API.SalePrices.Controllers
{
    [Route("api/Sale/Work/Region/{pEstablishmentKey}")]
    [ApiController]
    [Authorize]
    public class SaleWorkRegionController : BaseApiController
    {
        private readonly IVESaleWorkRegionAS _IVESaleWorkRegionAS;

        public SaleWorkRegionController(IVESaleWorkRegionAS pIVESaleWorkRegionAS)
        {
            _IVESaleWorkRegionAS = pIVESaleWorkRegionAS;

            Services = new IErrorBaseService[]
            {
                _IVESaleWorkRegionAS
            };
        }

        [Route("all")]
        [HttpGet]
        public async Task<IActionResult> GetAll(string pEstablishmentKey)
        {
            try
            {
                var Results =
                    Mapper.Map<PagedResult<VESaleWorkRegion>, PagedResult<VESaleWorkRegionVM>>
                    (
                        await _IVESaleWorkRegionAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                            r => r.EstablishmentKey == pEstablishmentKey
                        )
                    );

                if (Results == null)
                    throw new HinoException(_IVESaleWorkRegionAS.Errors.FirstOrDefault());

                return RequestOK(Results);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IActionResult> GetById(string pEstablishmentKey, long pId)
        {
            try
            {
                var Result =
                    Mapper.Map<VESaleWorkRegion, VESaleWorkRegionVM>
                    (
                        await _IVESaleWorkRegionAS.FirstOrDefaultAsync(
                            r => r.Id == pId &&
                                 r.EstablishmentKey == pEstablishmentKey)
                    );

                if (Result == null)
                    return NotFoundRequest();

                return RequestOK(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        public async Task<IActionResult> Put(string pUniqueKey, long id, [FromBody] VESaleWorkRegionVM pVESaleWorkRegionVM)
        {
            _IVESaleWorkRegionAS.Errors.Clear();
            pVESaleWorkRegionVM.Id = id;
            pVESaleWorkRegionVM.UniqueKey = pUniqueKey;

            ValidateModelState(pVESaleWorkRegionVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pVESaleWorkRegionVM, ModelState);

            try
            {
                var Result = _IVESaleWorkRegionAS.Update(
                    Mapper.Map<VESaleWorkRegionVM, VESaleWorkRegion>(pVESaleWorkRegionVM));

                if (Result != null && !_IVESaleWorkRegionAS.Errors.Any())
                    await _IVESaleWorkRegionAS.SaveChanges();

                return RequestResult(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpPost]
        [Route("save")]
        public async Task<IActionResult> Post(string pEstablishmentKey, [FromBody] VESaleWorkRegionVM pVESaleWorkRegionVM)
        {
            _IVESaleWorkRegionAS.Errors.Clear();
            pVESaleWorkRegionVM.EstablishmentKey = pEstablishmentKey;
            ValidateModelState(pVESaleWorkRegionVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pVESaleWorkRegionVM, ModelState);

            try
            {
                var Result = _IVESaleWorkRegionAS.Add(
                    Mapper.Map<VESaleWorkRegionVM, VESaleWorkRegion>(pVESaleWorkRegionVM));

                if (Result != null && !_IVESaleWorkRegionAS.Errors.Any())
                    await _IVESaleWorkRegionAS.SaveChanges();

                return RequestResult(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        public async Task<IActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IVESaleWorkRegionAS.Errors.Clear();

            try
            {
                var Result = await _IVESaleWorkRegionAS.RemoveById(pId, pEstablishmentKey, pUniqueKey);

                if (Result != null && !_IVESaleWorkRegionAS.Errors.Any())
                    await _IVESaleWorkRegionAS.SaveChanges();

                return RequestResult(Mapper.Map<VESaleWorkRegion, VESaleWorkRegionVM>(Result));
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IVESaleWorkRegionAS.Dispose();
            }
        }
    }
}
