﻿using Hino.API.Application.Interfaces.Services.SalePrices;
using Hino.API.Application.MapperModels;
using Hino.API.Application.ViewModel.SalePrices;
using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.SalePrices.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using Hino.API.Infra.Cross.Utils.Paging;
using Hino.API.Server.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.API.Server.API.SalePrices.Controllers
{
    [Route("api/Region/Sale/{pEstablishmentKey}")]
    [ApiController]
    [Authorize]
    public class RegionSaleController : BaseApiController
    {
        private readonly IVERegionSaleAS _IVERegionSaleAS;

        public RegionSaleController(IVERegionSaleAS pIVERegionSaleAS)
        {
            _IVERegionSaleAS = pIVERegionSaleAS;

            Services = new IErrorBaseService[]
            {
                _IVERegionSaleAS
            };
        }

        [Route("all")]
        [HttpGet]
        public async Task<IActionResult> GetAll(string pEstablishmentKey)
        {
            try
            {
                var Results =
                    Mapper.Map<PagedResult<VERegionSale>, PagedResult<VERegionSaleVM>>
                    (
                        await _IVERegionSaleAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                            r => r.EstablishmentKey == pEstablishmentKey
                        )
                    );

                if (Results == null)
                    throw new HinoException(_IVERegionSaleAS.Errors.FirstOrDefault());

                return RequestOK(Results);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IActionResult> GetById(string pEstablishmentKey, long pId)
        {
            try
            {
                var Result =
                    Mapper.Map<VERegionSale, VERegionSaleVM>
                    (
                        await _IVERegionSaleAS.FirstOrDefaultAsync(
                            r => r.Id == pId &&
                                 r.EstablishmentKey == pEstablishmentKey)
                    );

                if (Result == null)
                    return NotFoundRequest();

                return RequestOK(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        public async Task<IActionResult> Put(string pUniqueKey, long id, [FromBody] VERegionSaleVM pVERegionSaleVM)
        {
            _IVERegionSaleAS.Errors.Clear();
            pVERegionSaleVM.Id = id;
            pVERegionSaleVM.UniqueKey = pUniqueKey;

            ValidateModelState(pVERegionSaleVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pVERegionSaleVM, ModelState);

            try
            {
                var Result = _IVERegionSaleAS.Update(
                    Mapper.Map<VERegionSaleVM, VERegionSale>(pVERegionSaleVM));

                if (Result != null && !_IVERegionSaleAS.Errors.Any())
                    await _IVERegionSaleAS.SaveChanges();

                return RequestResult(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpPost]
        [Route("save")]
        public async Task<IActionResult> Post(string pEstablishmentKey, [FromBody] VERegionSaleVM pVERegionSaleVM)
        {
            _IVERegionSaleAS.Errors.Clear();
            pVERegionSaleVM.EstablishmentKey = pEstablishmentKey;
            ValidateModelState(pVERegionSaleVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pVERegionSaleVM, ModelState);

            try
            {
                var Result = _IVERegionSaleAS.Add(
                    Mapper.Map<VERegionSaleVM, VERegionSale>(pVERegionSaleVM));

                if (Result != null && !_IVERegionSaleAS.Errors.Any())
                    await _IVERegionSaleAS.SaveChanges();

                return RequestResult(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        public async Task<IActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IVERegionSaleAS.Errors.Clear();

            try
            {
                var Result = await _IVERegionSaleAS.RemoveById(pId, pEstablishmentKey, pUniqueKey);

                if (Result != null && !_IVERegionSaleAS.Errors.Any())
                    await _IVERegionSaleAS.SaveChanges();

                return RequestResult(Mapper.Map<VERegionSale, VERegionSaleVM>(Result));
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IVERegionSaleAS.Dispose();
            }
        }
    }
}
