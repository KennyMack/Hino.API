using Hino.API.Domain.SalePrices.Models;
using Hino.API.Domain.SalePrices.Repositories;
using Hino.API.Infra.Data.Repositories.Base.Repositories;
using Hino.API.Infra.Data.Repositories.SalePrices.Context;

namespace Hino.API.Infra.Data.Repositories.SalePrices.Repositories
{
    public class VESaleWorkRegionRepository : BaseRepository<VESaleWorkRegion>, IVESaleWorkRegionRepository
    {
        public VESaleWorkRegionRepository(SalePricesDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
