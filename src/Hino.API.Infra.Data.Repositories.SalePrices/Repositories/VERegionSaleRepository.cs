﻿using Hino.API.Domain.SalePrices.Models;
using Hino.API.Domain.SalePrices.Repositories;
using Hino.API.Infra.Data.Repositories.Base.Repositories;
using Hino.API.Infra.Data.Repositories.SalePrices.Context;

namespace Hino.API.Infra.Data.Repositories.SalePrices.Repositories
{
    public class VERegionSaleRepository : BaseRepository<VERegionSale>, IVERegionSaleRepository
    {
        public VERegionSaleRepository(SalePricesDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
