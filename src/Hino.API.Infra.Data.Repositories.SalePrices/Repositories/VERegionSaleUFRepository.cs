using Hino.API.Domain.SalePrices.Models;
using Hino.API.Domain.SalePrices.Repositories;
using Hino.API.Infra.Data.Repositories.Base.Repositories;
using Hino.API.Infra.Data.Repositories.SalePrices.Context;
using System.Linq;
using System.Threading.Tasks;
using Hino.API.Infra.Data.Repositories.Base.Context;
using Hino.API.Infra.Data.Repositories.Base;
using System.Data;
using Oracle.ManagedDataAccess.Client;

namespace Hino.API.Infra.Data.Repositories.SalePrices.Repositories
{
    public class VERegionSaleUFRepository : BaseRepository<VERegionSaleUF>, IVERegionSaleUFRepository
    {
        public VERegionSaleUFRepository(SalePricesDbContext appDbContext) : base(appDbContext)
        {
        }

        public async Task<VERegionSaleUF> GetEnterpriseSaleRegionAsync(string pEstablishmentKey, string pUF) =>
            await FirstOrDefaultAsync(r => r.EstablishmentKey == pEstablishmentKey && r.UF.ToUpper() == pUF);

        public async Task<bool> ExistsUFRegionAsync(string pEstablishmentKey, string pUF)
        {
            var Result = await QueryAsync(r =>
                r.EstablishmentKey == pEstablishmentKey &&
                r.UF == pUF);
            return Result.Any();
        }

        public async Task UpdateEnterpriseRegionIdAsync(string pEstablishmentKey, string pUF, long pRegionId)
        {
            var sql = @"UPDATE GEENTERPRISES
                           SET GEENTERPRISES.REGIONID         = :pREGIONID
                         WHERE GEENTERPRISES.ESTABLISHMENTKEY = :pESTABLISHMENTKEY
                           AND EXISTS (SELECT 1
                                         FROM GEENTERPRISEGEO
                                        WHERE GEENTERPRISEGEO.ESTABLISHMENTKEY = GEENTERPRISES.ESTABLISHMENTKEY
                                          AND GEENTERPRISEGEO.ENTERPRISEID     = GEENTERPRISES.ID
                                          AND GEENTERPRISEGEO.TYPE             = 0
                                          AND GEENTERPRISEGEO.UF               = :pUF)";

            await DbConn.ExecuteSqlCommandAsync(sql,
                new OracleParameter("pREGIONID", OracleDbType.Int64, pRegionId, ParameterDirection.Input),
                new OracleParameter("pESTABLISHMENTKEY", OracleDbType.Varchar2, pEstablishmentKey, ParameterDirection.Input),
                new OracleParameter("pUF", OracleDbType.Varchar2, pUF, ParameterDirection.Input)
            );
        }
    }
}
