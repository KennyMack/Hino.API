﻿using Hino.API.Domain.SalePrices.Models;
using Hino.API.Domain.SalePrices.Repositories;
using Hino.API.Infra.Cross.Utils.Paging;
using Hino.API.Infra.Data.Repositories.Base.Repositories;
using Hino.API.Infra.Data.Repositories.SalePrices.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hino.API.Infra.Data.Repositories.SalePrices.Repositories
{
    public class VESalePriceRepository : BaseRepository<VESalePrice>, IVESalePriceRepository
    {
        public VESalePriceRepository(SalePricesDbContext appDbContext) : base(appDbContext)
        {
            
        }

        public async Task<bool> ExistsProductRegionAsync(string pEstablishmentKey, long pRegionId, long pProductId)
        {
            var Result = await QueryAsync(r =>
                r.EstablishmentKey == pEstablishmentKey &&
                r.ProductId == pProductId &&
                r.RegionId == pRegionId);
            return Result.Any();
        }

        public async Task<IEnumerable<VESalePrice>> OthersSalePriceAsync(string pEstablishmentKey, long pRegionId)
        {
            var Result = await QueryAsync(r =>
                r.EstablishmentKey == pEstablishmentKey &&
                r.RegionId == pRegionId &&
                r.CodPrVenda > 0);
            return Result;
        }

        public Task<PagedResult<VESalePrice>> GetAllPagedSearchProductWithStock(int pageNumber, int pageSize, string pEstablishmentKey, long pRegionId, string filter)
        {
            string filtro = $"%{filter}%";
            return this.QueryPagedAsync(pageNumber,
                                        pageSize,
                                        r => r.EstablishmentKey == pEstablishmentKey &&
                                             r.RegionId == pRegionId &&
                                             r.GEProducts.StockBalance > 0 &&
                                             (// DbFunctions.Like(r.GEProducts.Name.ToUpper(), "%" + filter + "%") ||
                                             r.GEProducts.ProductKey.ToUpper().Contains(filter)),
                                        p => p.GEProducts,
                                        s => s.VERegionSale);
        }

        public Task<PagedResult<VESalePrice>> GetAllPagedSearchProduct(int pageNumber, int pageSize, string pEstablishmentKey, long pRegionId, string filter)
        {
            string filtro = $"%{filter}%";
            return this.QueryPagedAsync(pageNumber,
                                        pageSize,
                                        r => r.EstablishmentKey == pEstablishmentKey &&
                                             r.RegionId == pRegionId &&
                                             (// DbFunctions.Like(r.GEProducts.Name.ToUpper(), "%" + filter + "%") ||
                                             r.GEProducts.ProductKey.ToUpper().Contains(filter)),
                                        p => p.GEProducts,
                                        s => s.VERegionSale);
        }
    }
}
