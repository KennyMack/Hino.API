﻿using Hino.API.Domain.SalePrices.Models;
using Hino.API.Infra.Data.Repositories.Base.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.API.Infra.Data.Repositories.SalePrices.Context
{
    public class SalePricesDbContext : BaseDbContext
    {
        public virtual DbSet<VERegionSale> VERegionSale { get; set; }
        public virtual DbSet<VERegionSaleUF> VERegionSaleUF { get; set; }
        public virtual DbSet<VESalePrice> VESalePrice { get; set; }
        public virtual DbSet<VESaleWorkRegion> VESaleWorkRegion { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            // modelBuilder.ApplyConfiguration(new GEUsersConfiguration("GEUSERS"));
        }
    }
}
