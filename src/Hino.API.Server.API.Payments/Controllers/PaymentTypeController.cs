﻿using Hino.API.Application.Interfaces.Services.Payments;
using Hino.API.Application.MapperModels;
using Hino.API.Application.ViewModel.Payments;
using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Payments.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using Hino.API.Infra.Cross.Utils.Paging;
using Hino.API.Server.Base;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.API.Server.API.Payments.Controllers
{
    [Route("api/Payments/Type/Payment/{pEstablishmentKey}")]
    [ApiController]
    public class PaymentTypeController : BaseApiController
    {
        readonly IGEPaymentTypeAS _IGEPaymentTypeAS;

        public PaymentTypeController(IGEPaymentTypeAS pIGEPaymentTypeAS)
        {
            _IGEPaymentTypeAS = pIGEPaymentTypeAS;

            Services = new IErrorBaseService[]
            {
                _IGEPaymentTypeAS
            };
        }

        [Route("all")]
        [HttpGet]
        public async Task<IActionResult> Get(string pEstablishmentKey)
        {
            try
            {
                var filter = GetQueryFilter();
                var Results =
                    Mapper.Map<PagedResult<GEPaymentType>, PagedResult<GEPaymentTypeVM>>
                    (
                        await _IGEPaymentTypeAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                            r => (r.EstablishmentKey == pEstablishmentKey) &&
                                    (
                                        r.Id.ToString().Contains(filter.Trim()) ||
                                        r.Description.ToUpper().Contains(filter.Trim())
                                    ),
                            s => s.GEPaymentCondition)
                    );

                if (Results == null)
                    throw new HinoException(_IGEPaymentTypeAS.Errors.FirstOrDefault());

                return RequestOK(Results);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IActionResult> GetById(string pEstablishmentKey, long pId)
        {
            try
            {
                var Result =
                    Mapper.Map<GEPaymentType, GEPaymentTypeVM>
                    (
                        await _IGEPaymentTypeAS.FirstOrDefaultAsync(
                            r => r.Id == pId &&
                                 r.EstablishmentKey == pEstablishmentKey,
                            s => s.GEPaymentCondition)
                    );

                if (Result == null)
                    return NotFoundRequest();

                return RequestOK(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        public async Task<IActionResult> Put(string pUniqueKey, long id, [FromBody]GEPaymentTypeVM pGEPaymentTypeVM)
        {
            _IGEPaymentTypeAS.Errors.Clear();
            pGEPaymentTypeVM.Id = id;
            pGEPaymentTypeVM.UniqueKey = pUniqueKey;

            ValidateModelState(pGEPaymentTypeVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEPaymentTypeVM, ModelState);

            try
            {
                var Result = _IGEPaymentTypeAS.Update(Mapper.Map<GEPaymentTypeVM, GEPaymentType>(pGEPaymentTypeVM));

                if (Result == null)
                    throw new HinoException(_IGEPaymentTypeAS.Errors.FirstOrDefault());

                await _IGEPaymentTypeAS.SaveChanges();

                return RequestResult(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpPost]
        [Route("save")]
        public async Task<IActionResult> Post(string pEstablishmentKey, [FromBody] GEPaymentTypeVM pGEPaymentTypeVM)
        {
            _IGEPaymentTypeAS.Errors.Clear();
            pGEPaymentTypeVM.EstablishmentKey = pEstablishmentKey;
            ValidateModelState(pGEPaymentTypeVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEPaymentTypeVM, ModelState);

            try
            {
                var Result = _IGEPaymentTypeAS.Add(Mapper.Map<GEPaymentTypeVM, GEPaymentType>(pGEPaymentTypeVM));

                if (Result == null)
                    throw new HinoException(_IGEPaymentTypeAS.Errors.FirstOrDefault());

                await _IGEPaymentTypeAS.SaveChanges();

                return RequestResult(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        public async Task<IActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IGEPaymentTypeAS.Errors.Clear();

            try
            {
                var Result = await _IGEPaymentTypeAS.RemoveById(pId, pEstablishmentKey, pUniqueKey);

                await _IGEPaymentTypeAS.SaveChanges();
                return RequestResult(Mapper.Map<GEPaymentType, GEPaymentTypeVM>(Result));
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IGEPaymentTypeAS.Dispose();
            }
        }
    }
}
