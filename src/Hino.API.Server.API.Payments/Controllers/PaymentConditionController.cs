﻿using Hino.API.Application.Interfaces.Services.Payments;
using Hino.API.Application.MapperModels;
using Hino.API.Application.ViewModel.Payments;
using Hino.API.Domain.Base.Interfaces.Services;
using Hino.API.Domain.Payments.Models;
using Hino.API.Infra.Cross.Utils.Exceptions;
using Hino.API.Infra.Cross.Utils.Paging;
using Hino.API.Server.Base;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.API.Server.API.Payments.Controllers
{
    [Route("api/Payments/Condition/Payment/{pEstablishmentKey}")]
    [ApiController]
    public class PaymentConditionController : BaseApiController
    {
        readonly IGEPaymentConditionAS _IGEPaymentConditionAS;

        public PaymentConditionController(IGEPaymentConditionAS pIGEPaymentConditionAS)
        {
            _IGEPaymentConditionAS = pIGEPaymentConditionAS;

            Services = new IErrorBaseService[]
            {
                _IGEPaymentConditionAS
            };
        }

        [Route("all")]
        [HttpGet]
        public async Task<IActionResult> Get(string pEstablishmentKey)
        {
            try
            {
                var filter = GetQueryFilter();
                var Results =
                    Mapper.Map<PagedResult<GEPaymentCondition>, PagedResult<GEPaymentConditionVM>>
                    (
                        await _IGEPaymentConditionAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                            r => (r.EstablishmentKey == pEstablishmentKey) &&
                                    (
                                        (
                                            r.Id.ToString().Contains(filter.Trim()) ||
                                            r.Description.ToUpper().Contains(filter.Trim()) ||
                                            r.GEPaymentType.Description.ToUpper().Contains(filter.Trim())
                                        )
                                    ),
                            s => s.GEPaymentType,
                            g => g.GEPaymentCondInstallments)
                    );

                if (Results == null)
                    throw new HinoException(_IGEPaymentConditionAS.Errors.FirstOrDefault());

                return RequestOK(Results);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [Route("user/{pUserId}/all")]
        [HttpGet]
        public async Task<IActionResult> GetByUser(string pEstablishmentKey, long pUserId)
        {
            try
            {
                var Results =
                    Mapper.Map<PagedResult<GEPaymentCondition>, PagedResult<GEPaymentConditionVM>>
                    (
                        await _IGEPaymentConditionAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                            r => r.EstablishmentKey == pEstablishmentKey &&
                                 r.GEPaymentType.GEUserPayType.Any(u => u.UserId == pUserId),
                            s => s.GEPaymentType,
                            g => g.GEPaymentCondInstallments)
                    );

                if (Results == null)
                    throw new HinoException(_IGEPaymentConditionAS.Errors.FirstOrDefault());

                return RequestOK(Results);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IActionResult> GetById(string pEstablishmentKey, long pId)
        {
            try
            {
                var Result =
                    Mapper.Map<GEPaymentCondition, GEPaymentConditionVM>
                    (
                        await _IGEPaymentConditionAS.FirstOrDefaultAsync(
                            r => r.Id == pId &&
                                 r.EstablishmentKey == pEstablishmentKey,
                            s => s.GEPaymentType,
                            g => g.GEPaymentCondInstallments)
                    );

                if (Result == null)
                    return NotFoundRequest();

                return RequestOK(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        public async Task<IActionResult> Put(string pUniqueKey, long id, GEPaymentConditionCreateVM pGEPaymentConditionVM)
        {
            _IGEPaymentConditionAS.Errors.Clear();
            pGEPaymentConditionVM.Id = id;
            pGEPaymentConditionVM.UniqueKey = pUniqueKey;

            ValidateModelState(pGEPaymentConditionVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEPaymentConditionVM, ModelState);

            try
            {
                var Result = await _IGEPaymentConditionAS.ChangeAsync(Mapper.Map<GEPaymentConditionCreateVM, GEPaymentCondition>(pGEPaymentConditionVM));

                if (Result == null)
                    throw new HinoException(_IGEPaymentConditionAS.Errors.FirstOrDefault());

                await _IGEPaymentConditionAS.SaveChanges();

                return RequestResult(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpPost]
        [Route("save")]
        public async Task<IActionResult> Post(string pEstablishmentKey, [FromBody] GEPaymentConditionCreateVM pGEPaymentConditionVM)
        {
            _IGEPaymentConditionAS.Errors.Clear();
            ValidateModelState(pGEPaymentConditionVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEPaymentConditionVM, ModelState);

            try
            {
                var Result = _IGEPaymentConditionAS.Add(Mapper.Map<GEPaymentConditionCreateVM, GEPaymentCondition>(pGEPaymentConditionVM));

                if (Result == null)
                    throw new HinoException(_IGEPaymentConditionAS.Errors.FirstOrDefault());

                await _IGEPaymentConditionAS.SaveChanges();

                return RequestResult(Result);
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        public async Task<IActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IGEPaymentConditionAS.Errors.Clear();

            try
            {
                var Result = await _IGEPaymentConditionAS.RemoveById(pId, pEstablishmentKey, pUniqueKey);

                await _IGEPaymentConditionAS.SaveChanges();
                return RequestResult(Mapper.Map<GEPaymentCondition, GEPaymentConditionVM>(Result));
            }
            catch (Exception ex)
            {
                return InvalidRequest(null, ex.Message);
            }
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IGEPaymentConditionAS.Dispose();
            }
        }
    }
}
