﻿using Hino.API.Server.Base;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.API.Server.API.Payments.Controllers
{
    [Route("api/Payments/User/Payment/Type/{pEstablishmentKey}")]
    [ApiController]
    public class UserPayTypeController : BaseApiController
    {
    }
}
