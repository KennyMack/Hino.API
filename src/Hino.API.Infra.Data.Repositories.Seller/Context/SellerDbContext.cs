﻿using Hino.API.Domain.Seller.Models;
using Hino.API.Infra.Data.Repositories.Base.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Hino.API.Infra.Data.Repositories.Seller.Context
{
    public class SellerDbContext : BaseDbContext
    {
        public virtual DbSet<VESaleWorkRegion> VESaleWorkRegion { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            // modelBuilder.ApplyConfiguration(new GEUsersConfiguration("GEUSERS"));
        }
    }
}
