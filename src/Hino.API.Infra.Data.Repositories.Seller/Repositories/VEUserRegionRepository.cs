using Hino.API.Domain.Seller.Interfaces.Repositories;
using Hino.API.Domain.Seller.Models;
using Hino.API.Infra.Cross.Utils;
using Hino.API.Infra.Data.Repositories.Base;
using Hino.API.Infra.Data.Repositories.Base.Repositories;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using System;
using System.Linq;

namespace Hino.API.Infra.Data.Repositories.Seller.Context.Repositories
{
    public class VEUserRegionRepository : BaseRepository<VEUserRegion>, IVEUserRegionRepository
    {
        readonly SellerDbContext _AppDbContext;
        public VEUserRegionRepository(SellerDbContext appDbContext) : base(appDbContext)
        {
            _AppDbContext = appDbContext;
        }

        public bool ZipCodeExists(string pEstablishmentKey, long pUserId, string pZipCode)
        {
            var exists = Convert.ToInt32(_AppDbContext.RawSqlQuery(@"SELECT COUNT(1)
                                                                       FROM VESALEWORKREGION,
                                                                            VEUSERREGION 
                                                                      WHERE VEUSERREGION.ESTABLISHMENTKEY = VESALEWORKREGION.ESTABLISHMENTKEY
                                                                        AND VEUSERREGION.SALEWORKID       = VESALEWORKREGION.ID
                                                                        AND VEUSERREGION.ESTABLISHMENTKEY = :pESTABLISHMENTKEY
                                                                        AND VEUSERREGION.USERID           = :pUSERID
                                                                        AND :pZIPCODE BETWEEN REPLACE(VESALEWORKREGION.ZIPCODESTART, '-', '')
                                                                                        AND REPLACE(VESALEWORKREGION.ZIPCODEEND, '-', '')",
                   new OracleParameter("pESTABLISHMENTKEY", OracleDbType.Varchar2, pEstablishmentKey, ParameterDirection.Input),
                   new OracleParameter("pUSERID", OracleDbType.Int64, pUserId, ParameterDirection.Input),
                   new OracleParameter("pZIPCODE", OracleDbType.Varchar2, pZipCode.OnlyNumbers(), ParameterDirection.Input)
            ));

            return exists > 0;
        }
    }
}
